Contact: mailto:security@kde.org
Contact: https://kde.org/info/security/
Expires: 2025-10-31T12:12:00.000Z
Preferred-Languages: en
Canonical: https://kde.org/.well-known/security.txt
Policy: https://community.kde.org/Policies/Security_Policy

# KDE is an open source community, so it is not a vulnerability to see the code
# for the sourcecode. Not having DMARC is not a vulnerability either. If you've
# just run some automated tooling, found something trivial then reached out with
# the expectation of cashing in, you're going to be disappointed.
