---
title: KDE for You
description: KDE is developed by a community of passionate people. We develop KDE for everyone, from kids to grandparents and from professionals to hobbyists.
images:
- /for/developers/thumbnail.png
sassFiles:
- /scss/for/you.scss
menu:
  main:
    weight: 3
---
