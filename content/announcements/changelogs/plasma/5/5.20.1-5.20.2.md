---
aliases:
- /announcements/plasma-5.20.1-5.20.2-changelog
hidden: true
plasma: true
title: Plasma 5.20.2 complete changelog
type: fulllog
version: 5.20.2
---

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
Remove the shadow in widgetDestroyed(). [Commit.](http://commits.kde.org/breeze/e370403c44aa6f1c3ad558d879673a6bead49c75)
{{< /details >}}



{{< details title="KDE Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Fix translations for tooltips. [Commit.](http://commits.kde.org/kdeplasma-addons/a89ebe42449ee0d98b3af6493470daeebb3181ad) Fixes bug [#428154](https://bugs.kde.org/428154)
{{< /details >}}



{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Daemon: improve consistency of the lid behaviour. [Commit.](http://commits.kde.org/kscreen/420c60adfcb95b09ab352b614fdd2dc2a66dcfa8) 
{{< /details >}}



{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Provide cursor geometry in device-independent pixels. [Commit.](http://commits.kde.org/kwin/ea82e10a590268f7b8c179b3cc97a6542c8b9e2c) 
+ Platform/drm: Fix clipped HiDPI hardware cursors. [Commit.](http://commits.kde.org/kwin/44dcb6d0a586cb9c5e3ba4d030a7b06bf00ca4f4) See bug [#424589](https://bugs.kde.org/424589)
+ Clip software cursors. [Commit.](http://commits.kde.org/kwin/ff4a0d97138afb3055d42c49e0058ed20dca9a33) 
+ Mark the cursor as rendered after performing compositing. [Commit.](http://commits.kde.org/kwin/838d7e7a14e32f29f22e32b26aae02ed347ffbf5) 
+ [kwinrules] Allow negative numbers in position. [Commit.](http://commits.kde.org/kwin/9ae4021ef4d4f83f9e615c3419ad2b9ad3b6036c) Fixes bug [#428083](https://bugs.kde.org/428083)
+ Screencast: Handle the case where pipewire is not installed. [Commit.](http://commits.kde.org/kwin/6577a35ec9a5a8f892085aa54c78ffb334001cbd) Fixes bug [#427949](https://bugs.kde.org/427949)
+ Scenes/opengl: Properly render cursors with hidpi. [Commit.](http://commits.kde.org/kwin/9d2b27aebb862ed6ad3fa88760efd49369a4e4ae) See bug [#424589](https://bugs.kde.org/424589)
+ Wayland: Fix drag-and-drop cursors with hidpi. [Commit.](http://commits.kde.org/kwin/f123990930bf43f2e31d44d5c318f933ee20591c) See bug [#424589](https://bugs.kde.org/424589)
+ Platforms/drm: Use a heuristic to determine if EGLDevice backend can be used. [Commit.](http://commits.kde.org/kwin/f6c4cc6b8da7b2edd86843c5bde93a45b3eabcf3) 
+ ScreenshotEffect: don't expect authorization for interactive screenshots. [Commit.](http://commits.kde.org/kwin/853ce5bcb8d5c2ed5fae34b6cd8bdeb359cc7051) 
{{< /details >}}


{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Handle smap read result in the correct thread. [Commit.](http://commits.kde.org/libksysguard/2d522614357d6dc5a5a7b738a35574f5d8f69da5) Fixes bug [#428160](https://bugs.kde.org/428160). See bug [#428048](https://bugs.kde.org/428048)
{{< /details >}}



{{< details title="plasma-desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ [desktoppackage] Don't set a widget on an item in a layout. [Commit.](http://commits.kde.org/plasma-desktop/a6414ab28cd77d7a51b766bbb9da5ec968ab9c6e) Fixes bug [#428214](https://bugs.kde.org/428214)
+ [Widget Explorer] Don't set the list index to something random on mouse exit. [Commit.](http://commits.kde.org/plasma-desktop/5298171862ded64cfd637d313bd1fa85ba66a36c) Fixes bug [#428174](https://bugs.kde.org/428174)
+ Fix bug: Some user profile fields won't apply unless they all have unique new values. [Commit.](http://commits.kde.org/plasma-desktop/3308dee942d1982a0aba68fc41368a284afc8e21) Fixes bug [#427348](https://bugs.kde.org/427348)
+ [kcms/activities] Port to QQuickWidget from nested QQuickWindows. [Commit.](http://commits.kde.org/plasma-desktop/7079d0952b313b636126c39f0437ff6ba8687ca7) Fixes bug [#394899](https://bugs.kde.org/394899). Fixes bug [#423682](https://bugs.kde.org/423682). Fixes bug [#416253](https://bugs.kde.org/416253)
+ Fix bug: plasmashell high cpu usage. [Commit.](http://commits.kde.org/plasma-desktop/8945f4185dcc76c2e6de73c64cb55e9b06b3a732) 
{{< /details >}}


{{< details title="plasma-pa" href="https://commits.kde.org/plasma-pa" >}}
+ [applet] Remove unnecessary separator over menu section header. [Commit.](http://commits.kde.org/plasma-pa/6fbcaf6a80c2fe0d4581a2849b633c8f9223dff0) Fixes bug [#427804](https://bugs.kde.org/427804)
{{< /details >}}


{{< details title="plasma-phone-components" href="https://commits.kde.org/plasma-phone-components" >}}
+ Fix thumbnails. [Commit.](http://commits.kde.org/plasma-phone-components/51898ac99e1d75597b6fec001632b487a2ce3b45) 
+ Icon in front of the thumbnail. [Commit.](http://commits.kde.org/plasma-phone-components/848629c92ad380d17c30a404e4107db5f2880842) 
{{< /details >}}



{{< details title="plasma-workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ [plasma-session] Fix application lifespan. [Commit.](http://commits.kde.org/plasma-workspace/e67dc48ae2706dcf0a2785a37823bf54c838cac9) Fixes bug [#422948](https://bugs.kde.org/422948)
+ Sync OSD properties. [Commit.](http://commits.kde.org/plasma-workspace/595ec0308069280b877ae73a79fbaaa149a85732) Fixes bug [#425908](https://bugs.kde.org/425908)
+ [applets/systemtray] Fixes for vertical and huge panel. [Commit.](http://commits.kde.org/plasma-workspace/1f3b69087c9b4b1c9c3248b643907c480bcf8c6a) Fixes bug [#428158](https://bugs.kde.org/428158). Fixes bug [#428127](https://bugs.kde.org/428127). Fixes bug [#428198](https://bugs.kde.org/428198)
+ [applets/systemtray] Make highlight wider. [Commit.](http://commits.kde.org/plasma-workspace/6b25f37496c1a1338b27491afc6dfd548001e4e1) Fixes bug [#427638](https://bugs.kde.org/427638)
+ Fix tile expansion in ShellRunner. [Commit.](http://commits.kde.org/plasma-workspace/23b25e7e52337c46c828f933a0e7d13107f1a65f) Fixes bug [#427824](https://bugs.kde.org/427824)

{{< /details >}}


{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ FileChooser: escape slash character in user visible filter name. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/e8289ad3ac9ea2c189a17dc6cb656e312f6e71b3) Fixes bug [#427306](https://bugs.kde.org/427306)
{{< /details >}}