---
aliases:
- /announcements/plasma-5.17.4-5.17.5-changelog
hidden: true
plasma: true
title: Plasma 5.17.5 Complete Changelog
type: fulllog
version: 5.17.5
---

### <a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a>

- Add item spacing to size hint. <a href='https://commits.kde.org/breeze/74285aea999cb05e41a5755d183d8b2f05b00f5a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25740'>D25740</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- [applets/weather] Fix regression in temperature size with short panels. <a href='https://commits.kde.org/kdeplasma-addons/865ae05e8fe280a177fcd6dcf5f847327de36a00'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415187'>#415187</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [nightcolor] Fix division by zero. <a href='https://commits.kde.org/kwin/5646c781c88ab0f0427f23102bea889a835bd378'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415359'>#415359</a>. Phabricator Code review <a href='https://phabricator.kde.org/D26493'>D26493</a>
- Possible fix for KDecoration crash in systemsettings. <a href='https://commits.kde.org/kwin/1a13015d2d1de3ffb9450143480e729057992c45'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/411166'>#411166</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25913'>D25913</a>

### <a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a>

- Speed up sending request to ksysguardd. <a href='https://commits.kde.org/libksysguard/2791a0c71f245713ef78f71d8fccf843a2d492c6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D23595'>D23595</a>

### <a name='milou' href='https://commits.kde.org/milou'>Milou</a>

- [Applet] Replace missing icon. <a href='https://commits.kde.org/milou/4550095c47711139b5760bc9b4589fb6d1935764'>Commit.</a>

### <a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a>

- [Purpose Plugin] Reset pending reply serial when aborting. <a href='https://commits.kde.org/plasma-browser-integration/3646039502586ca18660961100216d08d8b2203c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25803'>D25803</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [Notifications KCM] Fixup current item syncing logic. <a href='https://commits.kde.org/plasma-desktop/b639338291d42314ce3eabde712fee41babbbb69'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/410842'>#410842</a>. Phabricator Code review <a href='https://phabricator.kde.org/D26425'>D26425</a>
- [KRunner KCM] Mark KCM as dirty when plugin configuration changes. <a href='https://commits.kde.org/plasma-desktop/d2d30a9667d2c3d57b0ca2a35926e5de6fc3f8ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D26445'>D26445</a>
- [Pager] Fix switching pages on drag. <a href='https://commits.kde.org/plasma-desktop/f5d1675a0dc1a1a0098eb5b1c727b5fe197e9930'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415423'>#415423</a>. Phabricator Code review <a href='https://phabricator.kde.org/ferential Revision:'>ferential Revision:</a>
- [Colors KCM] Increase delegate height. <a href='https://commits.kde.org/plasma-desktop/fb98c2cb5d2733f29332e5db66bff2ab4bbc78c5'>Commit.</a>
- Fix regression in "Port the pager applet away from QtWidgets". <a href='https://commits.kde.org/plasma-desktop/2b5e86323f180f0c51ef9af898a69a522bc379ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414849'>#414849</a>
- [KCMs/Activities] Fix jagginess for activities list icons on high DPI systems. <a href='https://commits.kde.org/plasma-desktop/567f71714faac305947573e307f486a9960cf1d1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414945'>#414945</a>
- [Icons KCM] Use help-browser icon. <a href='https://commits.kde.org/plasma-desktop/0381ed06a07a68e05df88d330c4ae7fef2b2a09f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25840'>D25840</a>

### <a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a>

- Resize file dialog window before size restore. <a href='https://commits.kde.org/plasma-integration/2e9f96847432c707a9dfbfe0748f0524a128abb7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25986'>D25986</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Do not crash when changing advanced IPv4 configuration. <a href='https://commits.kde.org/plasma-nm/fa1257cfc9f59cdaba9e7fba57960e2ad23f39aa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/415856'>#415856</a>
- Port KCM menu away from PlasmaComponents. <a href='https://commits.kde.org/plasma-nm/5e055c1e1754971178ce8136417b07f1a2b71e5b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414762'>#414762</a>. Phabricator Code review <a href='https://phabricator.kde.org/D26382'>D26382</a>
- Fortisslvpn: add option to ignore the password. <a href='https://commits.kde.org/plasma-nm/fd8303b6a5e781c4cede89de2580f52b51132097'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414975'>#414975</a>

### <a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a>

- [Cuttlefish] Make AppStream file valid. <a href='https://commits.kde.org/plasma-sdk/bcff5c557c5ed670153ee44fe02d894280161633'>Commit.</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Fix build with gpsd 3.20. <a href='https://commits.kde.org/plasma-workspace/0c4974d68804cdaff2efb6317f7853a89d3a3d2b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/425939'>#425939</a>. Phabricator Code review <a href='https://phabricator.kde.org/D26474'>D26474</a>
- [Notifications] Don't show DrKonqi notifications in history. <a href='https://commits.kde.org/plasma-workspace/e7e747b9bea1fea7afe042e11861c4f64e1bfcf4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D26297'>D26297</a>
- Revert "Add Meta+Space as a default shortcut to activate KRunner". <a href='https://commits.kde.org/plasma-workspace/469a7c1e33f83d6b73610997811fb131b5af80ca'>Commit.</a>
- [Notifications] Release all cookies when service unregisters. <a href='https://commits.kde.org/plasma-workspace/1967dc08e3db65f45303f1c6a81bb9755288b188'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D26125'>D26125</a>
- Revert "[sddm-theme] Fix initial focus after SDDM QQC2 Port". <a href='https://commits.kde.org/plasma-workspace/c2bc5243d460c306f995130880494eec6f54b18a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414875'>#414875</a>
- Keep at least one system monitor alive. <a href='https://commits.kde.org/plasma-workspace/eb6f6c2566622b883ca43138f8adee3c29f1f3fa'>Commit.</a>
- Use the right value of EnableMagicMimeActions. <a href='https://commits.kde.org/plasma-workspace/895e31d6946b63ddba923e58eb08d1ece7de85df'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25310'>D25310</a>
- [sddm-theme] Fix initial focus after SDDM QQC2 Port. <a href='https://commits.kde.org/plasma-workspace/e7803ecc58272236a6dd491a9d05cf230202d88c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25261'>D25261</a>

### <a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a>

- Don't use qmlRegisterType with QT-5.14. <a href='https://commits.kde.org/plymouth-kcm/92ffa670f0e51ed949c34fd14c7773a4ad2d0946'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414679'>#414679</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25624'>D25624</a>

### <a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a>

- Check for DPMS extension being present. <a href='https://commits.kde.org/powerdevil/18e53dbaaea261e1e07a75e01955baa7e373071b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414921'>#414921</a>. Phabricator Code review <a href='https://phabricator.kde.org/D26219'>D26219</a>

### <a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a>

- Use text color for "No preview image" text. <a href='https://commits.kde.org/sddm-kcm/7e13183d044b549a62d3c4e8c9e919113eb9030d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25971'>D25971</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- [Icon View] Fix titles of previously opened KCMs bleeding into QML KCMs. <a href='https://commits.kde.org/systemsettings/64fb5eb98eb8a096f4f190eea47b0384f4b96cad'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25726'>D25726</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- Print portal: xdg-desktop-portal assumes the pages to be used as indexes. <a href='https://commits.kde.org/xdg-desktop-portal-kde/6f4c4f350315d5d8f2bb60c2b5e0857db740b527'>Commit.</a>