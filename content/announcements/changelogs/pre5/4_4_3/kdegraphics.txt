------------------------------------------------------------------------
r1107752 | pino | 2010-03-27 06:41:19 +1300 (Sat, 27 Mar 2010) | 6 lines

bump version to 0.10.2 (kde 4.4.2)
bump comicboook version to 0.1.4

Dirk, can you please merge it to the 4.4.2 tag? (sorry for the late commit)
CCMAIL: mueller@kde.org

------------------------------------------------------------------------
r1108170 | scripty | 2010-03-28 15:36:51 +1300 (Sun, 28 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1108275 | aacid | 2010-03-29 02:02:36 +1300 (Mon, 29 Mar 2010) | 4 lines

Backport r1108269 | glenkaukola | 2010-03-28 13:25:35 +0100 (Sun, 28 Mar 2010) | 2 lines

Fix so that the okular presentation window grabs the focus right away when it's created so that mouse auto-hide works if enabled.

------------------------------------------------------------------------
r1109213 | aacid | 2010-03-31 08:47:42 +1300 (Wed, 31 Mar 2010) | 15 lines

backports

SVN commit 1108908 by glenkaukola:
Disable mouse cursor autohide when the cursor is over the top toolbar.


SVN commit 1108916 by glenkaukola:
Show mouse cursor if it's over the top toolbar, even if the hidden mouse cursor option is enabled.
Also makes sure the pencil mouse cursor stays on if drawing mode is on and hidden mouse is enabled.


SVN commit 1108918 by glenkaukola:
Okular presentation mode pencil cursor fix: when in drawing mode a left mouse button release over a link shouldn't change the mouse cursor.


------------------------------------------------------------------------
r1109219 | aacid | 2010-03-31 09:00:53 +1300 (Wed, 31 Mar 2010) | 2 lines

backport aacid * r1109218 okular/trunk/KDE/kdegraphics/okular/ui/ (presentationwidget.cpp presentationwidget.h): bring back the leaveEvent as as pino noted its is needed for multimonitor setups

------------------------------------------------------------------------
r1109640 | tokoe | 2010-04-01 04:34:03 +1300 (Thu, 01 Apr 2010) | 2 lines

Backport bugfix #217135

------------------------------------------------------------------------
r1109989 | gateau | 2010-04-02 02:27:22 +1300 (Fri, 02 Apr 2010) | 1 line

Make wheel scroll more natural
------------------------------------------------------------------------
r1109990 | gateau | 2010-04-02 02:27:26 +1300 (Fri, 02 Apr 2010) | 1 line

Updated
------------------------------------------------------------------------
r1109991 | gateau | 2010-04-02 02:29:21 +1300 (Fri, 02 Apr 2010) | 1 line

Bumped version numbers
------------------------------------------------------------------------
r1110074 | rdieter | 2010-04-02 09:00:32 +1300 (Fri, 02 Apr 2010) | 2 lines

backport r1102476 , fixes build against Qt 4.7 

------------------------------------------------------------------------
r1110147 | scripty | 2010-04-02 15:01:52 +1300 (Fri, 02 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1110426 | scripty | 2010-04-03 14:51:09 +1300 (Sat, 03 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1111287 | gateau | 2010-04-06 00:18:04 +1200 (Tue, 06 Apr 2010) | 5 lines

Patch by Christoph Feck to fix loading of damaged JPEG files.

BUG:230164

@Christoph: I tried Qt jpeg loader, but it's still a bit slower than Gwenview loader, so I am going to keep it there for now.
------------------------------------------------------------------------
r1111288 | gateau | 2010-04-06 00:18:08 +1200 (Tue, 06 Apr 2010) | 1 line

Bumped version numbers
------------------------------------------------------------------------
r1111289 | gateau | 2010-04-06 00:18:11 +1200 (Tue, 06 Apr 2010) | 1 line

Updated
------------------------------------------------------------------------
r1113768 | tokoe | 2010-04-12 05:57:28 +1200 (Mon, 12 Apr 2010) | 2 lines

Backport of bugfix #233944

------------------------------------------------------------------------
r1114559 | cfeck | 2010-04-14 10:28:53 +1200 (Wed, 14 Apr 2010) | 4 lines

Fix crash when zoom operation is aborted (backport r1114558)

CCBUG: 234300

------------------------------------------------------------------------
r1114579 | scripty | 2010-04-14 13:55:49 +1200 (Wed, 14 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1118220 | scripty | 2010-04-24 14:03:31 +1200 (Sat, 24 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1118538 | scripty | 2010-04-25 13:58:05 +1200 (Sun, 25 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1119952 | scripty | 2010-04-28 13:55:29 +1200 (Wed, 28 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1120584 | pino | 2010-04-29 22:45:05 +1200 (Thu, 29 Apr 2010) | 2 lines

got a bugfix, so bump its version to 0.1.2

------------------------------------------------------------------------
r1120585 | pino | 2010-04-29 22:45:41 +1200 (Thu, 29 Apr 2010) | 2 lines

bump version to 0.10.3 (kde 4.4.3)

------------------------------------------------------------------------
