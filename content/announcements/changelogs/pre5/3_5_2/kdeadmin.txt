2006-02-03 19:20 +0000 [r505375]  jriddell

	* branches/KDE/3.5/kdeadmin/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdeadmin
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdeadmin

2006-03-13 14:57 +0000 [r518255]  toivo

	* branches/KDE/3.5/kdeadmin/kpackage/debInterface.cpp: Fix
	  searching subdirectories for package files

2006-03-16 14:34 +0000 [r519203]  toivo

	* branches/KDE/3.5/kdeadmin/kpackage/kplview.cpp,
	  branches/KDE/3.5/kdeadmin/kpackage/kpackage.cpp,
	  branches/KDE/3.5/kdeadmin/kpackage/kplview.h: Fix marking when
	  mark column in package tree has been moved Fix saving column
	  moves

2006-03-16 17:19 +0000 [r519253]  bram

	* branches/KDE/3.5/kdeadmin/kuser/propdlg.cpp: Untranslated string,
	  patch by Stephan Johach. Thanks! CCMAIL:kde-i18n-doc@kde.org

2006-03-17 13:39 +0000 [r519552]  toivo

	* branches/KDE/3.5/kdeadmin/kpackage/fbsdInterface.cpp: Patch from
	  Tanguy Bouzeloc <zauron@wanadoo.fr> for FreeBSD

2006-03-17 21:34 +0000 [r519777]  coolo

	* branches/KDE/3.5/kdeadmin/kdeadmin.lsm: tagging 3.5.2

