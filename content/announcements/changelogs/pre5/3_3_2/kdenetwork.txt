Dir: kdenetwork
----------------------------
new version numbers



Dir: kdenetwork/kget
----------------------------
backport fix
CCBUG: 84450



Dir: kdenetwork/knewsticker
----------------------------
- Backport of revision 1.170



Dir: kdenetwork/kopete/kopete
----------------------------
new version number
----------------------------
Backport the fixes for bugs 90871 and 90281

Both of these should be in KDE 3.3.2

CCBUG: 90871
CCBUG: 90281



Dir: kdenetwork/kopete/kopete/chatwindow
----------------------------
partial backport of 87416
----------------------------
Backport the fixes for 59080 and 89911. Should be in KDE 3.3.2
BUG: 89911
CCBUG: 59080
----------------------------
finish the backport for 87416. Should be in KDE 3.3.2

CCBUG: 87416

2 down, 3 to go



Dir: kdenetwork/kopete/kopete/config/appearance
----------------------------
Backport the fix for 84730



Dir: kdenetwork/kopete/kopete/config/plugins
----------------------------
Backport fix mem leak



Dir: kdenetwork/kopete/kopete/contactlist
----------------------------
Backport the fix for bug 89133 for KDE 3.3.2

CCBUG: 89133

1 down, 4 to go. :)



Dir: kdenetwork/kopete/libkopete
----------------------------
Backport for:

CVS commit by lilachaze:

Fixed insane behaviour where the 'processing complete' callback was being
run from some random thread. Caused reproducible crashes.
----------------------------
missing "{"
thanks adrian :)
----------------------------
Backport the fixes for bugs 90871 and 90281

Both of these should be in KDE 3.3.2

CCBUG: 90871
CCBUG: 90281
----------------------------
Backport the fix for the crash when we remove the temporary group from the
contact list.



Dir: kdenetwork/kopete/libkopete/compat
----------------------------
Fixing compilation and backporting Thiago's fix in kresolvermanager.cpp (r1.31)



Dir: kdenetwork/kopete/libkopete/ui
----------------------------
Backport the fix for 82336. Should be in KDE 3.3.2

CCBUG: 82336
----------------------------
Backport fix mem leak



Dir: kdenetwork/kopete/protocols/gadu/libgadu
----------------------------
backport,
I don't need to bother coolo, we're not in freeze ;)



Dir: kdenetwork/kopete/protocols/irc/ui
----------------------------
Remove mSearchButton->setAccel( QKeySequence( i18n( "Alt+E" ) ) );
the mSearchButton->setText( i18n( "S&earch" ) ); is enough to have an accelerator
and setting the accelerator with setAccel can result in translation having a letter underlined but
having the accelerator in another letter because the translator did not put the & in the same letter
he translated Alt+E



Dir: kdenetwork/kopete/protocols/oscar/oscarsocket
----------------------------
Backport the change that fixes 82926. Should be in KDE 3.3.2

BUG: 82926
----------------------------
fix out-of-bounds access



Dir: kdenetwork/kppp
----------------------------
Backport enable/disable ok button
----------------------------
Last Backport
----------------------------
increased max length of callback number



Dir: kdenetwork/kppp/Rules/Italy
----------------------------
updated email



Dir: kdenetwork/kppp/logview
----------------------------
fix compile (gcc 4.0)



Dir: kdenetwork/ksirc/KSOpenkSirc
----------------------------
Backport QString cache



Dir: kdenetwork/wifi
----------------------------
backporting the fix for compilation with switch --without-arts
