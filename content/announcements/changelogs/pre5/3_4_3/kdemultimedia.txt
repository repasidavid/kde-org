2005-07-21 16:09 +0000 [r437357]  binner

	* branches/KDE/3.4/kdemultimedia/juk/playlistsearch.h,
	  branches/KDE/3.4/kdemultimedia/juk/playlistsearch.cpp: Backport
	  SVN commit 434275 by hermier: Workaround GCC-3.3 bug of
	  -fexceptions with templates and visibility.

2005-07-23 09:40 +0000 [r437847]  esken

	* branches/KDE/3.4/kdemultimedia/kmix/kmixdockwidget.cpp,
	  branches/KDE/3.4/kdemultimedia/kmix/version.h: Hide 'select
	  master channel' if there is no Mixer avaliable

2005-07-26 13:39 +0000 [r438886]  bmeyer

	* branches/KDE/3.4/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.cpp:
	  In KDE 3.5 audiocd will no longer use (manually open) the lame
	  library, but will call the lame executable. If there is a 3.4.2
	  (or any packagers want to incorperate this change) lame manually
	  searches and so I added lib64. BUG: 108345

2005-07-26 14:57 +0000 [r438911]  bmeyer

	* branches/KDE/3.4/kdemultimedia/kioslave/audiocd/audiocd.cpp: 3.4
	  branch backport BUG: 95676

2005-07-27 15:01 +0000 [r439274]  bmeyer

	* branches/KDE/3.4/kdemultimedia/kaudiocreator/encodefileimp.cpp,
	  branches/KDE/3.4/kdemultimedia/kaudiocreator/encodefile.ui:
	  Backport\nSVNIGNORE

2005-08-03 09:18 +0000 [r442590]  lunakl

	* branches/KDE/3.4/kdemultimedia/kscd/kscd.cpp: QCloseEvent is not
	  accepted by default.

2005-08-03 09:33 +0000 [r442594]  lunakl

	* branches/KDE/3.4/kdemultimedia/kscd/kscd.cpp: Backport a fix from
	  r401488 that prevents multiple 'select cddb entry' dialogs.

2005-08-05 06:41 +0000 [r443157]  bmeyer

	* branches/KDE/3.4/kdemultimedia/libkcddb/cdinfodialogbase.ui.h:
	  backport 109950

2005-08-09 13:24 +0000 [r444245]  coolo

	* branches/KDE/3.4/kdemultimedia/akode/plugins/jack_sink/jack_sink.cpp:
	  Patch by Takashi Iwai <tiwai@suse.de> to fix possible memory
	  leaks

2005-08-12 09:27 +0000 [r445975]  mlaurent

	* branches/KDE/3.4/kdemultimedia/kscd/kscd.cpp: Fix crash when we
	  don't have cd into cdrom track was equal to "nb-1" => -1 !

2005-08-12 13:48 +0000 [r446090]  mueller

	* branches/KDE/3.4/kdemultimedia/akode/arts_plugin/Makefile.am: fix
	  nonsense

2005-08-16 03:12 +0000 [r449598]  mpyne

	* branches/KDE/3.4/kdemultimedia/juk/playlistsearch.cpp: Backport
	  fix for bug 102952 (Missing tracks in Tree View for Artists) to
	  KDE 3.4. BUG:102952

2005-08-17 21:26 +0000 [r450310]  mueller

	* branches/KDE/3.4/kdemultimedia/kioslave/audiocd/plugins/flac/encoderflac.cpp:
	  fix export

2005-08-18 02:25 +0000 [r450395]  mpyne

	* branches/KDE/3.4/kdemultimedia/juk/tracksequenceiterator.cpp:
	  Backport fix for bug 102238 (Random play is not random between
	  starts) to KDE 3.4. BUG:102238

2005-08-18 02:31 +0000 [r450399]  mpyne

	* branches/KDE/3.4/kdemultimedia/juk/main.cpp: While I'm at it,
	  update version info for the 3.4 branch version of JuK.

2005-08-18 18:39 +0000 [r450657]  mueller

	* branches/KDE/3.4/kdemultimedia/kaudiocreator/job.cpp,
	  branches/KDE/3.4/kdemultimedia/kaudiocreator/encoder.cpp,
	  branches/KDE/3.4/kdemultimedia/kaudiocreator/job.h: backport
	  directory traversal fix from trunk

2005-08-19 13:03 +0000 [r450902]  wstephens

	* branches/KDE/3.4/kdemultimedia/kaudiocreator/tracksimp.cpp,
	  branches/KDE/3.4/kdemultimedia/libkcddb/client.cpp: Backport fix
	  for double deletion segfault on cd change while showing entry
	  selection dialog.

2005-08-24 17:58 +0000 [r452937]  esken

	* branches/KDE/3.4/kdemultimedia/kmix/mixer_sun.cpp: Backport:
	  Resolve two compilation issues for Solaris backend. 1) Wrong
	  destructor 2) Change open() to ::open()

2005-09-06 22:12 +0000 [r457901]  mpyne

	* branches/KDE/3.4/kdemultimedia/juk/googlefetcher.cpp: Backport
	  fix for bug 112118 (JuK Google cover search doesn't work anymore)
	  to KDE 3.4. Sorry about forgetting to do so earlier. BUG:112118

2005-09-23 07:41 +0000 [r463226]  mueller

	* branches/KDE/3.4/kdemultimedia/noatun/modules/artseffects/Makefile.am:
	  fix build

2005-09-24 09:29 +0000 [r463486]  mueller

	* branches/KDE/3.4/kdemultimedia/kscd/libwm/include/wm_struct.h:
	  fix declaration

2005-09-24 09:51 +0000 [r463500]  mueller

	* branches/KDE/3.4/kdemultimedia/akode/plugins/jack_sink/jack_sink.cpp:
	  compile

2005-09-26 01:36 +0000 [r464008]  mpyne

	* branches/KDE/3.4/kdemultimedia/juk/playlistcollection.cpp:
	  Backport potential crasher fix to KDE 3.4, involving KDirLister
	  and calling openURL on non-existant directories during JuK
	  startup. If this doesn't fix the bug please re-open. BUG:112917

2005-09-29 08:39 +0000 [r465236]  bmeyer

	* branches/KDE/3.4/kdemultimedia/kaudiocreator/ripper.cpp: don't
	  create files group writable, just user writable. BUG: 113316

2005-10-02 04:10 +0000 [r466262]  mpyne

	* branches/KDE/3.4/kdemultimedia/juk/playermanager.h,
	  branches/KDE/3.4/kdemultimedia/juk/exampleoptionsbase.ui,
	  branches/KDE/3.4/kdemultimedia/juk/googlefetcherdialog.h,
	  branches/KDE/3.4/kdemultimedia/juk/exampleoptions.cpp,
	  branches/KDE/3.4/kdemultimedia/juk/nowplaying.cpp,
	  branches/KDE/3.4/kdemultimedia/juk/playermanager.cpp: Backport
	  bugfixes to KDE 3.4 before the 3.4.3 release. * Don't call
	  setup() twice. * Fix tabstops in some dialogs. * Set a caption
	  for the Example Tags Dialog, reusing a translated string. * Fix a
	  compile warning by using the correct return type for a signal. *
	  Correctly update the Now Playing bar when you change the tag of
	  the playing file.

2005-10-04 01:54 +0000 [r467028]  mpyne

	* branches/KDE/3.4/kdemultimedia/juk/playlist.cpp,
	  branches/KDE/3.4/kdemultimedia/juk/tageditor.cpp,
	  branches/KDE/3.4/kdemultimedia/juk/collectionlist.cpp: Backport
	  some of Stephan's style guide fixes to KDE 3.4 when they do not
	  break string freeze. (i.e. can be implemented by using a
	  different KMessageBox static method or a different KStdGuiItem).

2005-10-05 13:48 +0000 [r467520]  coolo

	* branches/KDE/3.4/kdemultimedia/kdemultimedia.lsm: 3.4.3

