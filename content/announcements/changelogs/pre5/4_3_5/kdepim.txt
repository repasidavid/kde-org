------------------------------------------------------------------------
r1055157 | cgiboudeaux | 2009-11-27 13:14:24 +0000 (Fri, 27 Nov 2009) | 1 line

akregator 1.5.4
------------------------------------------------------------------------
r1055243 | winterz | 2009-11-27 17:11:44 +0000 (Fri, 27 Nov 2009) | 8 lines

Backport r1054642 by tokoe from trunk to the 4.3 branch:

Save the 'last_modified' field from the OX server
temporary and pass it when updating the incidence.

CCBUG: 174789


------------------------------------------------------------------------
r1055244 | winterz | 2009-11-27 17:13:25 +0000 (Fri, 27 Nov 2009) | 5 lines

Backport r1055134 by tokoe from trunk to the 4.3 branch:

Fix deletion and editing of events and todos with OX/6


------------------------------------------------------------------------
r1055856 | dfaure | 2009-11-28 21:39:00 +0000 (Sat, 28 Nov 2009) | 7 lines

Fix wrong initial focus in CategorySelectDialog -- made me lose a few seconds for each event I created :-)

Apparently the re-creation of the layout in KDialog loses the focuswidget; so fixing the bug there,
and removing the enterEvent workaround in CategorySelectDialog itself (IMHO, better have correct focus
upfront, without moving the mouse into the widget).
CCMAIL: winter@kde.org

------------------------------------------------------------------------
r1056365 | smartins | 2009-11-29 22:44:55 +0000 (Sun, 29 Nov 2009) | 4 lines

Backport r1056212 by smartins from trunk to the 4.3 branch:

Fix category list focus.

------------------------------------------------------------------------
r1056470 | djarvie | 2009-11-30 10:17:34 +0000 (Mon, 30 Nov 2009) | 2 lines

Fix reference to parent class

------------------------------------------------------------------------
r1057675 | scripty | 2009-12-03 04:13:02 +0000 (Thu, 03 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1058127 | smartins | 2009-12-03 23:51:26 +0000 (Thu, 03 Dec 2009) | 6 lines

Backport r1058032 by smartins from trunk to the 4.3 branch:

Fix elided text in todoview's quickaddline.

http://reviewboard.kde.org/r/2310/

------------------------------------------------------------------------
r1059605 | winterz | 2009-12-07 01:45:22 +0000 (Mon, 07 Dec 2009) | 8 lines

Backport r1059603 by winterz from trunk to the 4.3 branch:

give the scene an initial view rectangle; else all hell breaks loose.
should fix the initial view being month view.

MERGE: e4,4.3,akonadi-ports


------------------------------------------------------------------------
r1059980 | winterz | 2009-12-07 20:37:55 +0000 (Mon, 07 Dec 2009) | 22 lines

Backport r1041796 by winterz from trunk to the 4.3 branch:

Merged revisions 1041793 via svnmerge from 
https://svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1041793 | winterz | 2009-10-28 10:21:27 -0400 (Wed, 28 Oct 2009) | 12 lines
  
  Merged revisions 1041791 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1041791 | winterz | 2009-10-28 10:13:23 -0400 (Wed, 28 Oct 2009) | 6 lines
    
    refactoring to possibly make the Marcus Bains QTimer work better.
    kolab/issue3930
    
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1061661 | annma | 2009-12-12 16:15:43 +0000 (Sat, 12 Dec 2009) | 2 lines

correct path

------------------------------------------------------------------------
r1062074 | winterz | 2009-12-13 18:14:51 +0000 (Sun, 13 Dec 2009) | 7 lines

Backport r1062073 by winterz from trunk to the 4.3 branch:

provide a "do not ask again" option on the dialog for missing/empty To: field.
CCBUG: 194823
MERGE: 4.3


------------------------------------------------------------------------
r1062126 | winterz | 2009-12-13 21:01:16 +0000 (Sun, 13 Dec 2009) | 7 lines

Backport r1062073 by winterz from trunk to the 4.3 branch:

provide a "do not ask again" option on the dialog for missing/empty To: field.
CCBUG: 194823
MERGE: 4.3


------------------------------------------------------------------------
r1062544 | scripty | 2009-12-15 04:04:00 +0000 (Tue, 15 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1063235 | winterz | 2009-12-17 14:25:55 +0000 (Thu, 17 Dec 2009) | 9 lines

Backport r1063234 by winterz from trunk to the 4.3 branch:

in emitMsgAddSignals(), don't emit msgAdded if the serial number
for the specified index is 0.

CCBUG: 206738
MERGE: 4.3


------------------------------------------------------------------------
r1063994 | winterz | 2009-12-19 19:36:59 +0000 (Sat, 19 Dec 2009) | 6 lines

Backport r1063930 by winterz from trunk to the 4.3 branch:

make the wizards subdirectory build optional.
MERGE: 4.3


------------------------------------------------------------------------
r1064857 | winterz | 2009-12-22 00:00:48 +0000 (Tue, 22 Dec 2009) | 5 lines

backport SVN commit 1064851 by winterz:

in headerField() and headerFields() also make sure that the message hasHeaders()
before trying to access headers. This might help fix some crashes.

------------------------------------------------------------------------
r1064858 | winterz | 2009-12-22 00:01:25 +0000 (Tue, 22 Dec 2009) | 6 lines

Backport r1064359 by otrichet from trunk to the 4.3 branch:

Fix the behaviour of the "cancel" button of the scoring rules editor when a rule is added through the manager

CCBUG: 101092

------------------------------------------------------------------------
r1065232 | tmcguire | 2009-12-22 19:51:37 +0000 (Tue, 22 Dec 2009) | 9 lines

Backport r1065228 by tmcguire from trunk to the 4.3 branch:

Specify the correct algorithm which was used for creating the hash of the signature.

Thanks to Michael Gorven for the patch!

BUG: 128784


------------------------------------------------------------------------
r1065907 | otrichet | 2009-12-24 18:51:41 +0000 (Thu, 24 Dec 2009) | 5 lines

Backport commit by otrichet from trunk to the 4.3 branch:

1065265: Correctly set the path of the url of ArticleFetchJob. Especially don't use QDir::separator() which is '\' on Windows.
990006: By default, fetch articles using their server-side Id instead of their msg-id.

------------------------------------------------------------------------
r1066152 | scripty | 2009-12-26 03:56:59 +0000 (Sat, 26 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1067975 | scripty | 2009-12-31 04:15:15 +0000 (Thu, 31 Dec 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1072384 | scripty | 2010-01-10 04:12:13 +0000 (Sun, 10 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1072831 | scripty | 2010-01-11 04:19:18 +0000 (Mon, 11 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1073885 | scripty | 2010-01-13 04:04:26 +0000 (Wed, 13 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1074984 | scripty | 2010-01-15 04:02:52 +0000 (Fri, 15 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1075866 | winterz | 2010-01-16 22:29:08 +0000 (Sat, 16 Jan 2010) | 2 lines

increase version number for the upcoming 4.3.5 release

------------------------------------------------------------------------
r1075869 | winterz | 2010-01-16 22:33:34 +0000 (Sat, 16 Jan 2010) | 6 lines

backport SVN commit 1072266 by winterz:

timchen119's patch for supporting Unicode in notes.
thanks for the patch!
CCBUG:220766

------------------------------------------------------------------------
r1076194 | tstaerk | 2010-01-17 18:52:00 +0000 (Sun, 17 Jan 2010) | 2 lines

Backport 1075317. Track time of tasks by title of focus window.

------------------------------------------------------------------------
r1076230 | tstaerk | 2010-01-17 20:11:36 +0000 (Sun, 17 Jan 2010) | 5 lines

Backport of #1073419, #1073402 and #1073389: Recalculate Total Times on
Drag&Drop.
CCBUGS:220059


------------------------------------------------------------------------
r1076233 | tstaerk | 2010-01-17 20:20:12 +0000 (Sun, 17 Jan 2010) | 3 lines

Backport #1073713: Adapt totalSessionTime and totalTime on drag&drop.
CCBUGS:220059

------------------------------------------------------------------------
r1077395 | scripty | 2010-01-20 04:19:36 +0000 (Wed, 20 Jan 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1077865 | djarvie | 2010-01-21 01:34:03 +0000 (Thu, 21 Jan 2010) | 2 lines

Bug 222222: Fix non-Latin characters being illegible in emails (when send by KMail - sendmail not yet fixed)

------------------------------------------------------------------------
r1078198 | djarvie | 2010-01-21 18:41:52 +0000 (Thu, 21 Jan 2010) | 1 line

Remove warning
------------------------------------------------------------------------
r1078244 | djarvie | 2010-01-21 21:15:06 +0000 (Thu, 21 Jan 2010) | 2 lines

Bug 222222: Fix non-Latin characters being illegible in emails (when sent by sendmail)

------------------------------------------------------------------------
r1078258 | djarvie | 2010-01-21 22:10:43 +0000 (Thu, 21 Jan 2010) | 2 lines

Bug 222222: Fix encoding when copying sent mail to KMail's sent-mail folder.

------------------------------------------------------------------------
r1078260 | djarvie | 2010-01-21 22:12:28 +0000 (Thu, 21 Jan 2010) | 1 line

Fix end of line settings
------------------------------------------------------------------------
