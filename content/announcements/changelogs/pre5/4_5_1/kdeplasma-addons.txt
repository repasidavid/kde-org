------------------------------------------------------------------------
r1168568 | scripty | 2010-08-27 03:33:51 +0100 (dv, 27 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1168079 | scripty | 2010-08-26 03:40:57 +0100 (dj, 26 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1167272 | mfuchs | 2010-08-24 09:25:03 +0100 (dt, 24 ago 2010) | 3 lines

Backport r1167271
Do not show empty area at top as it stopped working correctly with 4.5
CCBUG:248810
------------------------------------------------------------------------
r1167089 | nwallace | 2010-08-23 15:47:58 +0100 (dl, 23 ago 2010) | 5 lines

(Backport from trunk at 1166819)

Fixed off-by-one error which led to the last cell in the array never coming alive and added parentheses to remove ambiguity and enforce order of operations for arithmetic expressions in the neighbors() function.

Also, removed trailing spaces.
------------------------------------------------------------------------
r1166315 | ruberg | 2010-08-21 16:22:49 +0100 (ds, 21 ago 2010) | 4 lines

UG: 245184
Made plasmaboard display the keyboard layout that is selected by the layout switcher


------------------------------------------------------------------------
r1166034 | nlecureuil | 2010-08-20 17:12:11 +0100 (dv, 20 ago 2010) | 3 lines

Initialize m_link
BUG: 248399

------------------------------------------------------------------------
r1162744 | mmrozowski | 2010-08-12 15:46:55 +0100 (dj, 12 ago 2010) | 2 lines

Provide means to install kimpanel's scim backend files to non-root owned location by setting:
-DSCIM_PANEL_INSTALL_DIR=<path>
------------------------------------------------------------------------
r1162149 | mart | 2010-08-11 15:06:35 +0100 (dc, 11 ago 2010) | 2 lines

backport: fix the layout

------------------------------------------------------------------------
r1161709 | mfuchs | 2010-08-10 17:47:46 +0100 (dt, 10 ago 2010) | 2 lines

Fullview of comic applet should be displayed on the correct screen now if the user have multiple displays.
BUG:245503
------------------------------------------------------------------------
r1161708 | mfuchs | 2010-08-10 17:47:45 +0100 (dt, 10 ago 2010) | 2 lines

Revert r1153571 "Sets a parent to the FullViewWidget, might solve the issue of displaying it on the wrong screen."
As aseigo pointed out it did not fix the problem and could cause problems itself.
------------------------------------------------------------------------
r1161683 | mart | 2010-08-10 16:42:39 +0100 (dt, 10 ago 2010) | 2 lines

backport: don't access m_link if it wasn't yet created, crash--

------------------------------------------------------------------------
r1161325 | scripty | 2010-08-10 03:47:09 +0100 (dt, 10 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1160084 | ruberg | 2010-08-07 01:27:13 +0100 (ds, 07 ago 2010) | 3 lines

BUG: 246467


------------------------------------------------------------------------
