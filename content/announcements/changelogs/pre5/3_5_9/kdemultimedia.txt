2007-10-23 10:49 +0000 [r728454]  binner

	* branches/KDE/3.5/kdemultimedia/arts/modules/synth/synth_sequence_freq_impl.cc,
	  branches/KDE/3.5/kdemultimedia/arts/modules/synth/synth_sequence_impl.cc,
	  branches/KDE/3.5/kdemultimedia/arts/runtime/artsbuilderloader_impl.cc,
	  branches/KDE/3.5/kdemultimedia/arts/modules/synth/synth_play_pat_impl.cc,
	  branches/KDE/3.5/kdemultimedia/arts/modules/effects/synth_stereo_fir_equalizer_impl.cc,
	  branches/KDE/3.5/kdemultimedia/arts/modules/synth/synth_noise_impl.cc,
	  branches/KDE/3.5/kdemultimedia/arts/builder/structure.cpp,
	  branches/KDE/3.5/kdemultimedia/arts/modules/synth/synth_capture_wav_impl.cc,
	  branches/KDE/3.5/kdemultimedia/arts/modules/synth/synth_midi_test_impl.cc,
	  branches/KDE/3.5/kdemultimedia/arts/modules/synth/synth_osc_impl.cc:
	  fix compilation with gcc 4.3

2007-11-02 04:27 +0000 [r731862]  wheeler

	* branches/KDE/3.5/kdemultimedia/juk/mediafiles.cpp: Use the newer
	  mime-type (already fixed in trunk)

2007-11-04 02:56 +0000 [r732506]  djarvie

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/kcmaudiocd/audiocdconfig.ui:
	  Make it compile

2007-11-21 22:26 +0000 [r739797]  mueller

	* branches/KDE/3.5/kdemultimedia/mpeglib/example/yaf/yafvorbis/vorbis_control.cpp:
	  fix build

2007-11-26 09:14 +0000 [r741731]  coolo

	* branches/KDE/3.5/kdemultimedia/noatun/modules/artseffects/extrastereo_impl.cc:
	  fix compilation

2007-12-29 19:45 +0000 [r754430]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/mixerIface.h,
	  branches/KDE/3.5/kdemultimedia/kmix/mixer.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/mixer.h: Adding a patch for a
	  KMilo issue - KMilo always uses index #0, which is not alway the
	  master. This regression came into Kmilo with KDE3.5.7 or
	  KDE3.5.8. The patch is applied as-is - to use it, KMilo must be
	  changed as well (a corresponding patch is found in
	  http://bugs.kde.org/show_bug.cgi?id=134820#c14 CCBUGS: 134820

2008-02-12 08:45 +0000 [r773958]  cartman

	* branches/KDE/3.5/kdemultimedia/xine_artsplugin/configure.in.in:
	  Update xine-config detection so that it detects 1.1.10.1
	  correctly, patch from upstream

2008-02-13 09:53 +0000 [r774472]  coolo

	* branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm: 3.5.9

