2007-10-08 11:49 +0000 [r722996]  rm

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/oscartypes.h:
	  sync with coolo

2007-10-28 21:12 +0000 [r730464]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetepluginmanager.cpp:
	  Check if plugin should be loaded be default if we don't have
	  entry in Plugins group in kopeterc file. Backport fix for bug
	  145313: kopete history plugin by default is marked as enabled but
	  is not really active. BUG:145313

2007-11-02 23:47 +0000 [r732161]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp:
	  Fix bug 151292: Kopete crash (SIGSEGV) in Configuration > Device
	  (périphérique in French) Always initialize min max values. Big
	  thanks to Romain Vimont for helping debug this! BUG: 151292

2007-11-06 10:20 +0000 [r733442]  mueller

	* branches/KDE/3.5/kdenetwork/krfb/libvncserver/main.c: fix locking
	  error CCBUG: 124529

2007-11-11 14:50 +0000 [r735379]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/client.cpp:
	  Backport: Filter out Miranda's invisible check. CCBUG: 148035

2007-11-29 20:00 +0000 [r743073]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/logintask.cpp:
	  Don't fail to connect if the contact list is entirely empty.

2007-12-17 13:17 +0000 [r749620-749619]  jkerr

	* branches/KDE/3.5/kdenetwork/kopete/protocols/meanwhile/meanwhilesession.cpp:
	  Add RC-168 cipher support for meanwhile connections We need to
	  allow RC-128 for some servers.

	* branches/KDE/3.5/kdenetwork/kopete/protocols/meanwhile/meanwhilesession.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/meanwhile/meanwhileeditaccountwidget.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/meanwhile/meanwhileaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/meanwhile/meanwhileaccount.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/meanwhile/meanwhilesession.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/meanwhile/ui/meanwhileeditaccountbase.ui,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/meanwhile/meanwhileeditaccountwidget.cpp:
	  Allow arbitrary meanwhile client identifiers Some servers require
	  specific client IDs - this allows the user to specify their own
	  IDs.

2008-01-01 10:20 +0000 [r755367]  schafer

	* branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/kcmsambaconf.cpp,
	  branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/kcminterface.ui,
	  branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/kcminterface.ui.h:
	  The description of the selected user level, does not default to
	  "share" anymore BUG: 102401

2008-01-01 19:57 +0000 [r755533]  schafer

	* branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/sharedlgimpl.cpp,
	  branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/share.ui:
	  Fixed the widgets of the following samba options: case sensitive,
	  strict locking, msdfs proxy The wrong widgets resulted in wrong
	  default values, which in turn resulted in wrongly generated
	  settings in the smb.conf file. BUG: 137577

2008-01-02 18:22 +0000 [r756090]  pino

	* branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/kcminterface.ui,
	  branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/share.ui,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/meanwhile/ui/meanwhileeditaccountbase.ui:
	  fixuifiles

2008-01-08 08:30 +0000 [r758554]  taupter

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.cpp:
	  Fixed image saturation problem with uvc devices.

2008-01-10 09:58 +0000 [r759288]  bero

	* branches/KDE/3.5/kdenetwork/krfb/kcm_krfb/kcm_krfb.cpp: Fix build
	  with --enable-final and gcc 4.3 (--enable-final causes config.h,
	  which #defines VERSION, to be included before kcm_krfb.cpp, which
	  #defines VERSION as well - gcc 4.3 makes a double definition a
	  fatal error even if the value is the same)

2008-01-10 12:09 +0000 [r759326]  bero

	* branches/KDE/3.5/kdenetwork/krfb/krfb/main.cpp: Another fix for
	  the gcc 4.3 + --enable-final combo

2008-01-17 08:29 +0000 [r762475]  taupter

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/avdevice/avdeviceconfig.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.h,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/avdevice/avdeviceconfig.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/config/avdevice/avdeviceconfig_videoconfig.ui,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevicepool.h,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp:
	  Preliminary patch to support sn9c1xx devices (WIP)

2008-01-17 08:59 +0000 [r762481]  taupter

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/sonix_compress.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/sonix_compress.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/bayer.cpp
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/bayer.h
	  (added),
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/Makefile.am:
	  Hopefully fixes problems people with sn9c1xx drivers (bug 114684)
	  This commit is complementary to 762475. As aggred by Matt Rogers
	  (Kopete leader), four new files were added to 3.5.x branch.

2008-01-17 09:58 +0000 [r762584]  taupter

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp:
	  Fixed some kdDebug messages.

2008-01-17 20:44 +0000 [r762773]  taupter

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.cpp:
	  Final fix for the sn9c1xx driver. I tested myself with actual
	  hardware. sn9c1xx drivers have non-conformant behavior, diverging
	  from V4L2 specification. Instead of return an error when setting
	  a given unsupported pixel format, it returns SUCCESS but the
	  selected format is not the one you asked for. The maintainer must
	  be notified, so he will (hopefully) fix the driver.

2008-01-21 10:42 +0000 [r764276]  hasso

	* branches/KDE/3.5/kdenetwork/kppp/pppstats.cpp,
	  branches/KDE/3.5/kdenetwork/kppp/opener.cpp,
	  branches/KDE/3.5/kdenetwork/kppp/configure.in.in: Backport rev
	  764273: Make kppp compile in DragonFly BSD.

2008-01-26 10:47 +0000 [r766580]  jritzerfeld

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/localcontactlisttask.cpp
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/icqaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/oscarsettings.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/localcontactlisttask.h
	  (removed),
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/oscarsettings.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/ui/icqeditaccountwidget.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/ui/icqeditaccountui.ui,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/icq/icqpresence.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/servicesetuptask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/userdetails.cpp:
	  The feature of ignoring other contacts' wish to require
	  authorization does no longer work: Last November, AOL dropped the
	  support for local contact lists that were used to circumvent the
	  authorization. Therefore, the checkbox "Respect other contacts'
	  wish to require authorization" of group "Privacy Options" in
	  icq's account preferences is removed. CCBUG: 101587

2008-02-13 09:53 +0000 [r774473]  coolo

	* branches/KDE/3.5/kdenetwork/kdenetwork.lsm: 3.5.9

