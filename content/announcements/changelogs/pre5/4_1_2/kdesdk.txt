------------------------------------------------------------------------
r854120 | ilic | 2008-08-28 23:09:53 +0200 (Thu, 28 Aug 2008) | 2 lines

Allow user to specify comment style in output. (bport: 850087)

------------------------------------------------------------------------
r854122 | ilic | 2008-08-28 23:11:17 +0200 (Thu, 28 Aug 2008) | 1 line

Extract more informative message context from XML structure. (bport: 851961 through 852072)
------------------------------------------------------------------------
r854898 | lueck | 2008-08-30 17:09:05 +0200 (Sat, 30 Aug 2008) | 1 line

documentation backport fron trunk
------------------------------------------------------------------------
r858184 | shaforo | 2008-09-07 17:01:51 +0200 (Sun, 07 Sep 2008) | 3 lines

apply fixes by Chusslove Illich


------------------------------------------------------------------------
r859353 | scripty | 2008-09-10 08:47:10 +0200 (Wed, 10 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r860659 | shaforo | 2008-09-13 20:43:56 +0200 (Sat, 13 Sep 2008) | 14 lines

Add hack to support inserting unbreakable spaces, and presering them.
they are still lost when you work with clipboard, though.

i already submitted appropriate request to Qt.
It may be fixed by removing
	txt.replace(QChar::Nbsp, QLatin1Char(' '));
from
	QTextDocument::toPlainText()


CC'ing the bug, although bugs.kde.org is still not fixed!
CCBUG: 162016


------------------------------------------------------------------------
r861973 | shaforo | 2008-09-17 19:52:51 +0200 (Wed, 17 Sep 2008) | 4 lines

BUG: 171145
please don't tell me closing bugs via svn commits still ain't supported


------------------------------------------------------------------------
r862860 | scripty | 2008-09-20 08:50:32 +0200 (Sat, 20 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r863101 | scripty | 2008-09-21 08:15:29 +0200 (Sun, 21 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r864077 | mlaurent | 2008-09-23 23:21:22 +0200 (Tue, 23 Sep 2008) | 1 line

Allow to compile with strigi 0.6.0
------------------------------------------------------------------------
r864079 | shaforo | 2008-09-23 23:24:43 +0200 (Tue, 23 Sep 2008) | 3 lines

better fix miltiline msgctxt hnadling in Original Diff view


------------------------------------------------------------------------
r864332 | woebbe | 2008-09-24 17:02:46 +0200 (Wed, 24 Sep 2008) | 1 line

bump version number
------------------------------------------------------------------------
r864480 | uwolfer | 2008-09-24 22:48:37 +0200 (Wed, 24 Sep 2008) | 4 lines

Backport:
SVN commit 864476 by uwolfer:

a little icon love
------------------------------------------------------------------------
