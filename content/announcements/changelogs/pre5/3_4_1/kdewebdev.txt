2005-03-04 18:00 +0000 [r394908]  amantia

	* quanta/dialogs/tagdialogs/tagxml.cpp,
	  quanta/components/cvsservice/cvsservice.cpp,
	  quanta/treeviews/doctreeview.cpp,
	  quanta/dialogs/tagdialogs/tagimgdlg.cpp, quanta/parsers/qtag.h,
	  quanta/src/quanta.cpp, quanta/parsers/dtd/dtdparser.cpp,
	  quanta/treeviews/doctreeview.h,
	  quanta/dialogs/tagdialogs/tagimgdlg.h, quanta/src/dtds.cpp,
	  quanta/src/quanta.h, quanta/ChangeLog,
	  quanta/treeviews/templatestreeview.cpp,
	  quanta/dialogs/dtepeditdlg.cpp: Backport fixes: Read/write the
	  correct entry from description.rc. Get the image sizes for remote
	  images as well. Insert img tags for remote images as well with
	  D&D. More context-sensitive documentation. Nicer processing of
	  "cvs" output.

2005-03-05 12:13 +0000 [r395021]  amantia

	* lib/compatibility/knewstuff/knewstuffsecure.cpp,
	  lib/compatibility/knewstuff/engine.cpp: KNewStuff fixes.

2005-03-06 10:32 +0000 [r395204]  mrudolf

	* kommander/widget/specials.cpp: Backport: update copyright.

2005-03-06 11:09 +0000 [r395207]  mrudolf

	* kommander/widget/parser.cpp,
	  kommander/widget/kommanderwidget.cpp, kommander/widget/parser.h:
	  Backport: Make Message.* functions work also with old parser.

2005-03-06 19:57 +0000 [r395333]  mrudolf

	* kommander/widget/kommanderwidget.cpp: Backport: Fix a probably
	  harmless bug - missing break in case statement. Port @Info family
	  functions to old parser.

2005-03-06 23:06 +0000 [r395364]  mrudolf

	* kommander/widget/kommanderfunctions.cpp,
	  kommander/widget/kommanderwidget.h,
	  kommander/widget/kommanderwidget.cpp: Backport: Merge old and new
	  parser support for File function family.

2005-03-07 10:43 +0000 [r395444]  amantia

	* quanta/ChangeLog, quanta/project/projectprivate.cpp: Backport the
	  exclude regexp fix plus some coding style fixes.

2005-03-07 12:15 +0000 [r395459]  mrudolf

	* kommander/editor/main.cpp, kommander/editor/pics/qtlogo.png,
	  kommander/editor/pics/Makefile.am,
	  kommander/editor/pics/kommandersplash.png,
	  kommander/editor/pics/splash.png (removed),
	  kommander/editor/pics/splash.h (removed),
	  kommander/editor/pics/background.png: Backport: Re-enable splash
	  after KDE-ification. Probably noticeable only on slower
	  computers.

2005-03-10 22:18 +0000 [r396502]  mrudolf

	* kommander/widget/kommanderfunctions.cpp,
	  kommander/widget/kommanderwidget.h,
	  kommander/widget/kommanderwidget.cpp: Backport: Further
	  optimization: remove duplicated string-handling functions code.

2005-03-10 22:41 +0000 [r396516]  mrudolf

	* kommander/editor/main.cpp: Backport: Fix warning.

2005-03-10 23:46 +0000 [r396530]  mrudolf

	* kommander/editor/Makefile.am: Backport: Re-enable splash after
	  KDE-ification - backport Makefile.am which was omitted before.

2005-03-11 19:00 +0000 [r396757]  mrudolf

	* kommander/executor/instance.cpp: Backport: Warn against temporary
	  files in /var/tmp too, not only in /tmp.

2005-03-17 09:18 +0000 [r398290]  mrudolf

	* kommander/executor/instance.cpp: Backport: Do better job
	  recognizing paths of temporary directories.

2005-03-21 14:23 +0000 [r399421]  waba

	* kxsldbg/kxsldbgpart/libxsldbg/xsldbg.cpp: Use proper format
	  string.

2005-03-29 19:13 +0000 [r401718]  mrudolf

	* kommander/widget/specials.cpp: Backport: Fix wrong number of
	  parameters in type() function.

2005-04-03 08:02 +0000 [r402799]  mrudolf

	* kommander/editor/mainwindow.cpp: Backport: Properly run dialogs
	  with path containing spaces.

2005-04-04 15:43 +0000 [r403142]  linusmc

	* quanta/components/debugger/pathmapper.cpp: Fixed problem with
	  eternal pathmapper dialogs on local projects

2005-04-08 07:09 +0000 [r403961]  mrudolf

	* kommander/editor/messagelog.cpp: Backport: Automatically scroll
	  the content if new debug text is added.

2005-04-09 22:02 +0000 [r404473-404472]  mrudolf

	* kommander/kommander.kdevelop: Not in HEAD directory anymore.

	* kommander/widget/parser.cpp: Fixes severe bug #103572. Functions
	  in expression are no longer executed in non-executed code.

2005-04-10 09:43 +0000 [r404522]  mrudolf

	* kommander/widget/parser.cpp: Backport: Fix logic bug in "And"
	  handling.

2005-04-11 13:00 +0000 [r404782]  mrudolf

	* kommander/editor/assoctexteditorimpl.cpp: Backport: Don't append
	  @ to widget name in new parser mode.

2005-04-13 15:43 +0000 [r405352]  amantia

	* quanta/components/csseditor/cssselector.cpp,
	  quanta/quanta.kdevelop, quanta/ChangeLog: Backport: don't crash
	  when Selected is pressed and nothing is selected (in the CSS
	  editor) [#101919]

2005-04-17 08:56 +0000 [r406027]  mrudolf

	* kommander/executor/instance.cpp: Backport. Security fix: don't
	  run scripts without *.kmdr extenstion. Adds two i18n strings
	  (approved by Dirk Mueller, announced on kde-i18n-doc).

2005-04-21 08:33 +0000 [r406882]  amantia

	* quanta/quanta.kdevelop, quanta/src/document.cpp,
	  quanta/ChangeLog: Backport: show the correct relative paths in
	  URL autocompletion

2005-04-22 11:17 +0000 [r407113]  amantia

	* quanta/ChangeLog, quanta/components/debugger/debuggermanager.cpp:
	  Backport crashfix when removing breakpoints.

2005-04-27 15:01 +0000 [r408190]  hasso

	* quanta/src/quanta_init.cpp: Backport.

2005-04-28 00:08 +0000 [r408288]  mojo

	* quanta/parts/kafka/kafkacommon.cpp,
	  quanta/parts/kafka/htmlenhancer.cpp,
	  quanta/parts/kafka/wkafkapart.cpp: Backport: Never propagate
	  exceptions between module boundaries (in C++). KHTML
	  DOM::DOMException can be raised when calling Node::insertBefore
	  and, in some situations/distributions, caughting the exception
	  doesn't work outside KHTML, i.e., Quanta. So, try to prevent the
	  exception to be raised when possible. This should fix all the
	  reported problems.

2005-05-02 09:59 +0000 [r409165]  mueller

	* kommander/executor/instance.cpp: fix check

2005-05-08 20:35 +0000 [r411302]  mlaurent

	* quanta/src/quanta.cpp: Backport fix mem leak

2005-05-09 19:52 +0000 [r411639]  amantia

	* lib/compatibility/cvsservice/Makefile.am (added),
	  lib/compatibility/cvsservice/cvsjob.h (added),
	  lib/compatibility/cvsservice/cvsservice.cpp (added),
	  quanta/src/Makefile.am, quanta/quanta.kdevelop,
	  lib/compatibility/cvsservice (added),
	  lib/compatibility/cvsservice/repository.cpp (added),
	  lib/compatibility/cvsservice/cvsservice.h (added),
	  lib/compatibility/cvsservice/repository.h (added),
	  lib/compatibility/cvsservice/cvsjob.cpp (added),
	  quanta/components/cvsservice/Makefile.am: Use an own version of
	  libcvsservice. Should fix #102996 and its duplicates. BUG: 102996

2005-05-10 10:32 +0000 [r411876]  binner

	* kdewebdev.lsm: update lsm for release

2005-05-10 10:42 +0000 [r411884]  binner

	* quanta/src/quanta.h: Increasing versions listed in
	  RELEASE-CHECKLIST

2005-05-10 22:05 +0000 [r412195]  mojo

	* quanta/parsers/qtag.cpp, quanta/parsers/qtag.h,
	  quanta/parts/kafka/wkafkapart.cpp: Final patch against 3.4 branch
	  that I forgot to commit. 3.4 branch has now all the fixes to
	  #99826. CCBUG: 99826 CCMAIL: Gour <gour@mail.inet.hr>

2005-05-10 22:27 +0000 [r412206]  mojo

	* klinkstatus/src/parser/url.cpp: Backport: Compare domains in a
	  case insensitive way. BUG: 105415

2005-05-12 09:10 +0000 [r412645]  mojo

	* klinkstatus/src/parser/url.cpp: Backport: Use
	  KResolver::normalizeDomain() instead of QString::lower().

2005-05-14 09:10 +0000 [r413645]  benb

	* debian/changelog, debian/klinkstatus.override (added),
	  debian/rules, debian/kimagemapeditor.manpages (added),
	  debian/kommander-dev.install (added), debian/kxsldbg.menu,
	  debian/kimagemapeditor.1 (added), debian/kommander.docs,
	  debian/quanta.doc-base (removed), debian/klinkstatus.manpages
	  (added), debian/libkommander0.shlibs (removed),
	  debian/kimagemapeditor.install, debian/copyright,
	  debian/kmdr-plugins.1 (added), debian/klinkstatus.1 (added),
	  debian/kmdr2po.1 (added), debian/quanta.docs, debian/control,
	  debian/kmdr-editor.1, debian/klinkstatus.install,
	  debian/kommander.install, debian/libkommander-dev.install
	  (removed), debian/quanta-data.docs, debian/extractkmdr.1 (added),
	  debian/quanta.1, debian/kmdr-executor.1,
	  debian/kimagemapeditor.docs (added),
	  debian/klinkstatus.README.Debian (added),
	  debian/libkommander0.install (removed),
	  debian/kommander.README.Debian (added),
	  debian/kommander.override,
	  debian/kdewebdev-doc-html.doc-base.klinkstatus (added),
	  debian/kxsldbg.1, debian/kdewebdev-doc-html.doc-base.kommander
	  (added), debian/source.lintian-overrides, debian/kommander.shlibs
	  (added), debian/klinkstatus.docs (added),
	  debian/kimagemapeditor.xpm (added), debian/kimagemapeditor.menu
	  (added), debian/quanta.README.Debian, debian/kommander.manpages,
	  debian/kdewebdev-doc-html.doc-base.quanta (added),
	  debian/kxsldbg.doc-base (removed), debian/kxsldbg.README.Debian
	  (added), debian/kdewebdev-doc-html.doc-base.kxsldbg (added),
	  debian/klinkstatus.xpm (added), debian/klinkstatus.menu (added),
	  debian/php.docrc, debian/kommander-dev.docs (added),
	  debian/kimagemapeditor.override (added): The packaging for this
	  module in the 3.4 branch is quite out of date. Bring it up to the
	  current state of packaging in debian unstable.

2005-05-20 19:25 +0000 [r416163]  amantia

	* VERSION, ChangeLog, quanta/quanta.lsm, quanta/ChangeLog: Prepare
	  for release.

2005-05-22 19:00 +0000 [r417068]  amantia

	* lib/compatibility/Makefile.am, quanta/src/Makefile.am,
	  quanta/quanta.kdevelop, configure.in.in, README: Build fix.

2005-05-22 19:12 +0000 [r417077]  amantia

	* lib/compatibility/knewstuff/knewstuffsecure.h,
	  lib/compatibility/knewstuff/uploaddialog.h,
	  lib/compatibility/knewstuff/engine.h,
	  lib/compatibility/knewstuff/downloaddialog.h,
	  lib/compatibility/knewstuff/security.h,
	  lib/compatibility/knewstuff/knewstuffgeneric.h,
	  lib/compatibility/knewstuff/provider.h,
	  lib/compatibility/knewstuff/providerdialog.h,
	  lib/compatibility/knewstuff/knewstuff.h,
	  lib/compatibility/knewstuff/entry.h: Trying to workaround the
	  visibility bug in KDE < 3.3.2.

