2005-03-09 14:07 +0000 [r396083]  waba

	* flow/audioioalsa9.cc, flow/audioioalsa.cc: Attempt to fix artsd
	  problem on suspend: handle EINTR

2005-04-19 23:18 +0000 [r406661]  mueller

	* mcop/dynamicskeleton.cc, flow/gslschedule.cc,
	  mcop/dynamicskeleton.h, soundserver/soundserver_impl.cc,
	  soundserver/tradercheck.h, soundserver/soundserver_impl.h,
	  flow/stereovolumecontrol_impl.cc, flow/audiomanager_impl.cc:
	  unbreak compilation (gcc 4)

2005-05-10 10:34 +0000 [r411877]  binner

	* configure.in.in, arts.lsm: update version

