------------------------------------------------------------------------
r946365 | gladhorn | 2009-03-29 11:02:09 +0000 (Sun, 29 Mar 2009) | 1 line

backport the relevant part of r946363
------------------------------------------------------------------------
r946587 | annma | 2009-03-29 18:23:37 +0000 (Sun, 29 Mar 2009) | 3 lines

backport of 946557 but without strings 
BUG=175310

------------------------------------------------------------------------
r946588 | annma | 2009-03-29 18:24:37 +0000 (Sun, 29 Mar 2009) | 2 lines

one warning less

------------------------------------------------------------------------
r946592 | rahn | 2009-03-29 18:41:31 +0000 (Sun, 29 Mar 2009) | 7 lines


- Backporting:

  * Harshit Jain's Patch to enable the lock float item functionality for the Qt version
  * screenshot fix for MarbleMaps


------------------------------------------------------------------------
r947476 | jakselsen | 2009-03-31 15:27:18 +0000 (Tue, 31 Mar 2009) | 1 line

the last 5 SVGs backport for 4.2
------------------------------------------------------------------------
r948325 | annma | 2009-04-02 19:32:10 +0000 (Thu, 02 Apr 2009) | 2 lines

backport of r948308 (fix for Indian languages)

------------------------------------------------------------------------
r949002 | jakselsen | 2009-04-04 09:16:08 +0000 (Sat, 04 Apr 2009) | 1 line

fix wrong text from Ge to Ga
------------------------------------------------------------------------
r949376 | annma | 2009-04-05 08:08:04 +0000 (Sun, 05 Apr 2009) | 2 lines

Swiss German keyboard thanks to Daniel Stoni!

------------------------------------------------------------------------
r950432 | scripty | 2009-04-07 07:28:33 +0000 (Tue, 07 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r950985 | jakselsen | 2009-04-08 11:04:51 +0000 (Wed, 08 Apr 2009) | 1 line

reworked for more realistics flames. Bp for 4.2.
------------------------------------------------------------------------
r951153 | rahn | 2009-04-08 15:03:52 +0000 (Wed, 08 Apr 2009) | 4 lines


- Make sure we set our default input handler before others do it ...


------------------------------------------------------------------------
r951770 | scripty | 2009-04-10 07:27:33 +0000 (Fri, 10 Apr 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r952980 | mboquien | 2009-04-13 02:32:22 +0000 (Mon, 13 Apr 2009) | 4 lines

Backport r952978.

CCMAIL: kstars-devel@kde.org

------------------------------------------------------------------------
r955840 | mlaurent | 2009-04-18 16:25:34 +0000 (Sat, 18 Apr 2009) | 3 lines

Backport:
Fix mem leak

------------------------------------------------------------------------
r956083 | annma | 2009-04-19 12:29:23 +0000 (Sun, 19 Apr 2009) | 4 lines

backport of r956072 
bug reported to Debian tracker
CCMAIL: 518854@bug.debian.org

------------------------------------------------------------------------
r956200 | annma | 2009-04-19 13:49:51 +0000 (Sun, 19 Apr 2009) | 2 lines

fix typos

------------------------------------------------------------------------
r956222 | annma | 2009-04-19 15:05:33 +0000 (Sun, 19 Apr 2009) | 4 lines

fix wrong city name
tackat please update the binary file, the process is too complicated for me
CCMAIL=tackat@kde.org

------------------------------------------------------------------------
r956424 | annma | 2009-04-20 06:54:20 +0000 (Mon, 20 Apr 2009) | 2 lines

add Québec layout

------------------------------------------------------------------------
r956453 | annma | 2009-04-20 07:18:27 +0000 (Mon, 20 Apr 2009) | 3 lines

j is not yet trained so suppress it before it is learnt
CCBUG=163680

------------------------------------------------------------------------
r956509 | annma | 2009-04-20 07:30:32 +0000 (Mon, 20 Apr 2009) | 4 lines

Wrong title name
Thanks for your report!
BUG=190075

------------------------------------------------------------------------
r956572 | annma | 2009-04-20 09:44:38 +0000 (Mon, 20 Apr 2009) | 2 lines

fix encoding

------------------------------------------------------------------------
r956573 | annma | 2009-04-20 09:45:59 +0000 (Mon, 20 Apr 2009) | 3 lines

fix Italian typos thanks to  Marco Costantini   
BUG=162583

------------------------------------------------------------------------
r956575 | annma | 2009-04-20 09:54:34 +0000 (Mon, 20 Apr 2009) | 2 lines

my bad

------------------------------------------------------------------------
r956621 | lueck | 2009-04-20 12:50:34 +0000 (Mon, 20 Apr 2009) | 1 line

backport from trunk r956620 to extract a missing string from src/lib/global.h
------------------------------------------------------------------------
r961280 | apol | 2009-04-29 20:45:04 +0000 (Wed, 29 Apr 2009) | 2 lines

Backport to 4.2 branch so that plasma doesn't crash anymore because of kalgebra's plasmoid.

------------------------------------------------------------------------
