------------------------------------------------------------------------
r1216041 | nienhueser | 2011-01-21 11:17:52 +1300 (Fri, 21 Jan 2011) | 3 lines

Guard against faulty routes.
CCBUG: 263753
Backport of commit 1216036.
------------------------------------------------------------------------
r1216175 | rysin | 2011-01-22 07:24:57 +1300 (Sat, 22 Jan 2011) | 4 lines

Use new kxkb dbus APIs
BUG: 263006
FIXED-IN: 4.6

------------------------------------------------------------------------
r1217232 | scripty | 2011-01-27 01:26:48 +1300 (Thu, 27 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1217548 | nienhueser | 2011-01-28 09:23:32 +1300 (Fri, 28 Jan 2011) | 5 lines

Work around gpsmm parsing gpsd output (C locale) with current locale.
BUG: 262330
REVIEW: 6327
FIXED-IN: 4.6.1
Backport of commit 1217540.
------------------------------------------------------------------------
r1218186 | aacid | 2011-02-01 12:58:39 +1300 (Tue, 01 Feb 2011) | 2 lines

Backport fix for 263481 as agreed with kde-i18n-doc

------------------------------------------------------------------------
r1218560 | aacid | 2011-02-03 10:09:26 +1300 (Thu, 03 Feb 2011) | 2 lines

backport fix for junin capital

------------------------------------------------------------------------
r1219303 | reck | 2011-02-08 14:57:52 +1300 (Tue, 08 Feb 2011) | 1 line

Fix the data structure change. Also fix tool plugin running in data structure. And also fixes a bug that prevents add nodes. Was needed to change a bit the API.
------------------------------------------------------------------------
r1219423 | scripty | 2011-02-09 02:30:02 +1300 (Wed, 09 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1219614 | scripty | 2011-02-10 01:41:02 +1300 (Thu, 10 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1220716 | scripty | 2011-02-15 10:26:27 +1300 (Tue, 15 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1221295 | scripty | 2011-02-18 04:27:31 +1300 (Fri, 18 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1221500 | scripty | 2011-02-19 02:48:57 +1300 (Sat, 19 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1221604 | scripty | 2011-02-20 02:34:26 +1300 (Sun, 20 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1221647 | nienhueser | 2011-02-20 10:51:34 +1300 (Sun, 20 Feb 2011) | 4 lines

Extend drawAnnotation() to calculate and use the optimal text height when an invalid height is passed. Use it for painting driving instructions in the map. Fixes long instructions being cut off. Reduce the rounded corner radius for a better text/bubble ratio.
BUG: 265926
REVIEW: 6525
Backport of commit 1221644.
------------------------------------------------------------------------
r1221940 | apol | 2011-02-21 22:12:19 +1300 (Mon, 21 Feb 2011) | 2 lines

backport changes in plugin lookup.

------------------------------------------------------------------------
r1221966 | apol | 2011-02-22 01:18:57 +1300 (Tue, 22 Feb 2011) | 2 lines

Backport a bunch of changes for the mobile version, mostly usability.

------------------------------------------------------------------------
r1222055 | scripty | 2011-02-22 02:32:06 +1300 (Tue, 22 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1222064 | apol | 2011-02-22 04:18:10 +1300 (Tue, 22 Feb 2011) | 2 lines

Backport changes from trunk.

------------------------------------------------------------------------
r1222084 | apol | 2011-02-22 06:22:47 +1300 (Tue, 22 Feb 2011) | 2 lines

Backport enable final compilation

------------------------------------------------------------------------
r1222087 | apol | 2011-02-22 06:36:52 +1300 (Tue, 22 Feb 2011) | 2 lines

Backport this files from trunk. Helps reorganize code to compile using enablefinal.

------------------------------------------------------------------------
r1222090 | apol | 2011-02-22 06:58:14 +1300 (Tue, 22 Feb 2011) | 2 lines

Backport changes to compile using enablefinal.

------------------------------------------------------------------------
r1222104 | erebetez | 2011-02-22 08:49:44 +1300 (Tue, 22 Feb 2011) | 1 line

backporting i18n fixes. CCBUG:266761
------------------------------------------------------------------------
r1222159 | apol | 2011-02-22 13:01:59 +1300 (Tue, 22 Feb 2011) | 2 lines

Backport input adaptation.

------------------------------------------------------------------------
r1222325 | yurchor | 2011-02-23 09:23:43 +1300 (Wed, 23 Feb 2011) | 2 lines

Backport "Clipoard->Clipboard" fix from trunk.
CCMAIL:kde-i18n-doc@kde.org
------------------------------------------------------------------------
r1222344 | erebetez | 2011-02-23 10:55:10 +1300 (Wed, 23 Feb 2011) | 1 line

backport commit 1222340. Not translated visible strings. CCBUG: 266761
------------------------------------------------------------------------
r1222395 | apol | 2011-02-24 02:19:50 +1300 (Thu, 24 Feb 2011) | 2 lines

Backported some actions to the mobile functions dialog so that we have all the features we can.

------------------------------------------------------------------------
r1222488 | annma | 2011-02-24 22:19:41 +1300 (Thu, 24 Feb 2011) | 3 lines

backport of 1222486 
BUG=173074

------------------------------------------------------------------------
r1222582 | reck | 2011-02-25 16:33:46 +1300 (Fri, 25 Feb 2011) | 1 line

backport from Pointer's removal (r1222573)
------------------------------------------------------------------------
r1222727 | schwarzer | 2011-02-26 06:13:07 +1300 (Sat, 26 Feb 2011) | 1 line

do not extract placeholder strings for translation
------------------------------------------------------------------------
