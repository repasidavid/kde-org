------------------------------------------------------------------------
r1216108 | scripty | 2011-01-22 01:04:10 +1300 (Sat, 22 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1216380 | rkcosta | 2011-01-23 11:17:32 +1300 (Sun, 23 Jan 2011) | 4 lines

[part] Put the focus on the "Cancel" button by default.

Backport of r1216363.

------------------------------------------------------------------------
r1216498 | rkcosta | 2011-01-24 07:43:50 +1300 (Mon, 24 Jan 2011) | 4 lines

clirarplugin: Use the correct pattern for wrong passwords.

Backport of r1216497.

------------------------------------------------------------------------
r1216503 | rkcosta | 2011-01-24 08:01:34 +1300 (Mon, 24 Jan 2011) | 8 lines

clirarplugin: Use both the old and the new pattern for wrong passwords.

The pattern introduced in r1216497 is used by the RAR 4.00 betas (and
will possibly be the default in the stable 4.x releases). The previous
one is still valid for RAR 3.93 or earlier.

Backport of r1216502.

------------------------------------------------------------------------
r1216531 | kossebau | 2011-01-24 11:01:01 +1300 (Mon, 24 Jan 2011) | 1 line

added: Mainpage.dox also for 4.6 branch
------------------------------------------------------------------------
r1217233 | scripty | 2011-01-27 01:27:59 +1300 (Thu, 27 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1218774 | scripty | 2011-02-05 00:33:04 +1300 (Sat, 05 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1219109 | scripty | 2011-02-07 00:25:48 +1300 (Mon, 07 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1219186 | dakon | 2011-02-07 10:11:36 +1300 (Mon, 07 Feb 2011) | 1 line

fix the version number
------------------------------------------------------------------------
r1219187 | dakon | 2011-02-07 10:12:30 +1300 (Mon, 07 Feb 2011) | 5 lines

fix the default value for decrypted file to have a path delimiter at the right position

CCBUG:261904
backport of r1219185

------------------------------------------------------------------------
r1220437 | rkcosta | 2011-02-14 13:12:49 +1300 (Mon, 14 Feb 2011) | 2 lines

libarchive: Use the constness hammer in copyFiles().

------------------------------------------------------------------------
r1220439 | rkcosta | 2011-02-14 13:22:13 +1300 (Mon, 14 Feb 2011) | 15 lines

libarchive: Consider renamed files valid files when extracting.

When one or more files are selected for extraction and they already
exist on the filesystem, if the user chooses to rename it, extraction
will fail for that file. That happens because when we are not
extracting all files, the new file name fails the
files.contains(entryName) check.

We now keep track of the current file being renamed to avoid this
problem.

BUG: 266159
FIXED-IN: 4.6.1


------------------------------------------------------------------------
r1222157 | shaforo | 2011-02-22 12:59:01 +1300 (Tue, 22 Feb 2011) | 2 lines

backport win32 fixes

------------------------------------------------------------------------
r1222283 | dakon | 2011-02-23 03:21:48 +1300 (Wed, 23 Feb 2011) | 4 lines

editor: remove unused local variable

backport of r1220539

------------------------------------------------------------------------
r1222284 | dakon | 2011-02-23 03:23:32 +1300 (Wed, 23 Feb 2011) | 6 lines

editor: make sure the decrypted text ends with a linebreak

This will "damage" the text if the original text before encryption did not end with a linebreak. I assume here that this is usually by mistake. And if not it's a plain text file shown in an editor so this shouldn't hurt either.

Backport of r1220541

------------------------------------------------------------------------
r1222285 | dakon | 2011-02-23 03:24:57 +1300 (Wed, 23 Feb 2011) | 6 lines

get the width of the key count status field right

This also removes a string.

backport of r1220543

------------------------------------------------------------------------
r1222286 | dakon | 2011-02-23 03:25:40 +1300 (Wed, 23 Feb 2011) | 6 lines

do not crash if searching for signatures gives no results

BUG:265339

backport of r1220544

------------------------------------------------------------------------
r1222431 | dakon | 2011-02-24 08:35:04 +1300 (Thu, 24 Feb 2011) | 6 lines

backport key generation fixes

BUG:263805

backport of r1222429 and r1222430

------------------------------------------------------------------------
