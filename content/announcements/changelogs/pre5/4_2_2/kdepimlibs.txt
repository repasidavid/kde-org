------------------------------------------------------------------------
r932900 | smartins | 2009-02-27 15:17:33 +0000 (Fri, 27 Feb 2009) | 6 lines

Backport r932890 by smartins from trunk to the 4.2 branch:

endDateTime is invalid if the recurrence doesn't have an end.

Thanks to Pete for his patch.

------------------------------------------------------------------------
r932969 | smartins | 2009-02-27 17:08:30 +0000 (Fri, 27 Feb 2009) | 4 lines

Backport r932967 by smartins from trunk to the 4.2 branch:

Add unit test for Todo::setCompleted()

------------------------------------------------------------------------
r932982 | cgiboudeaux | 2009-02-27 18:01:52 +0000 (Fri, 27 Feb 2009) | 1 line

add testSetCompleted
------------------------------------------------------------------------
r933037 | winterz | 2009-02-27 21:07:10 +0000 (Fri, 27 Feb 2009) | 6 lines

backport SVN commit 932997 by winterz:

Per RFC, if an organizer or attendee email address is empty, do not write that property.

Based on a patch by Marco Nelles, and reviewed at http://reviewboard.kde.org/r/148/

------------------------------------------------------------------------
r933039 | winterz | 2009-02-27 21:08:43 +0000 (Fri, 27 Feb 2009) | 4 lines

backport SVN commit 933000 by winterz:

Don't add an ORGANIZER property if there is no email for the organizer.

------------------------------------------------------------------------
r933042 | winterz | 2009-02-27 21:12:07 +0000 (Fri, 27 Feb 2009) | 7 lines

backport SVN commit 931509 by winterz:

Patch from Csaba.
Revert back a small typo change from 11 years ago which is a difference between the libical's
and kcal's vobject in versit. Obviously, it is not an option to send this to upstream,
but revert back this change. No direct effect on the sources.

------------------------------------------------------------------------
r933082 | winterz | 2009-02-27 23:01:21 +0000 (Fri, 27 Feb 2009) | 5 lines

backport SVN commit 933079 by winterz:

another attempt to fix the timezone compares with th reference data.


------------------------------------------------------------------------
r933351 | chehrlic | 2009-02-28 18:22:00 +0000 (Sat, 28 Feb 2009) | 2 lines

backport from trunk to find ical headers on windows
libical headers are in $whatever/include/libical/ and the include directives are #include <libical/ical.h> - therefore we need $whatever/include as include dir, not $whatever/include/libical
------------------------------------------------------------------------
r933816 | cgiboudeaux | 2009-03-01 17:29:05 +0000 (Sun, 01 Mar 2009) | 1 line

Backport rev. 933805: Libical is not optional
------------------------------------------------------------------------
r933951 | smartins | 2009-03-02 00:17:46 +0000 (Mon, 02 Mar 2009) | 4 lines

Backport r933949 by smartins from trunk to the 4.2 branch:

Support for alarms relative to to-do's dtStart().

------------------------------------------------------------------------
r935076 | tmcguire | 2009-03-04 14:12:03 +0000 (Wed, 04 Mar 2009) | 5 lines

Backport r932578 by tmcguire from trunk to the 4.2 branch:

Fix memory leak by using boost::shared_ptr.


------------------------------------------------------------------------
r935447 | scripty | 2009-03-05 07:51:32 +0000 (Thu, 05 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r937028 | lueck | 2009-03-08 21:47:16 +0000 (Sun, 08 Mar 2009) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r938165 | vkrause | 2009-03-11 13:33:28 +0000 (Wed, 11 Mar 2009) | 4 lines

Backport SVN commit 938149 by vkrause from trunk:

Handle cancel correctly.

------------------------------------------------------------------------
r938405 | mueller | 2009-03-11 19:50:54 +0000 (Wed, 11 Mar 2009) | 2 lines

bump requires

------------------------------------------------------------------------
r939240 | krake | 2009-03-14 13:31:04 +0000 (Sat, 14 Mar 2009) | 8 lines

Backport of Revision 939239

Fixes a bug reported here:
https://bugzilla.novell.com/show_bug.cgi?id=480957

Basically the vcard data is written into the temporay file but not to the file system yet, thus resulting in a zero sized fi


------------------------------------------------------------------------
r939625 | winterz | 2009-03-15 12:50:44 +0000 (Sun, 15 Mar 2009) | 4 lines

backport SVN commit 938673 by smartins:

After deleting, doSave was recreating the file again, so incidences never were deleted.

------------------------------------------------------------------------
r939626 | winterz | 2009-03-15 12:52:26 +0000 (Sun, 15 Mar 2009) | 5 lines

backport SVN commit 938806 by smartins:

Ignore changes in tmp files. This saves 7 or 8 view reloads after editing an incidence.


------------------------------------------------------------------------
r939627 | winterz | 2009-03-15 12:53:44 +0000 (Sun, 15 Mar 2009) | 5 lines

backport SVN commit 939322 by djarvie:

Ignore backup and temporary files in doLoad() as well.
Tidy up.

------------------------------------------------------------------------
r939629 | winterz | 2009-03-15 12:55:09 +0000 (Sun, 15 Mar 2009) | 4 lines

backport SVN commit 939334 by smartins:

Mode mDeletedIncidences to the Private class.

------------------------------------------------------------------------
r939630 | winterz | 2009-03-15 12:57:41 +0000 (Sun, 15 Mar 2009) | 4 lines

backport SVN commit 939344 by djarvie:

Rationalise

------------------------------------------------------------------------
r940569 | otrichet | 2009-03-17 17:47:28 +0000 (Tue, 17 Mar 2009) | 3 lines

Backport SVN commit 940126 by otrichet:
Fix selection of address in drop down menu

------------------------------------------------------------------------
r941239 | scripty | 2009-03-19 07:48:59 +0000 (Thu, 19 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r941695 | smartins | 2009-03-20 12:05:00 +0000 (Fri, 20 Mar 2009) | 4 lines

Backport r941693 by smartins from trunk to the 4.2 branch:

Fix binary compatibility problem unfortunately introduced in trunk's r926668 and branch4.2's r926671.

------------------------------------------------------------------------
r942597 | scripty | 2009-03-22 07:43:29 +0000 (Sun, 22 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r943678 | scripty | 2009-03-24 08:20:33 +0000 (Tue, 24 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r944120 | reed | 2009-03-25 04:45:11 +0000 (Wed, 25 Mar 2009) | 1 line

unsetenv is a void on OSX 10.4, work around it
------------------------------------------------------------------------
r944175 | scripty | 2009-03-25 08:46:55 +0000 (Wed, 25 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r944320 | krake | 2009-03-25 12:42:26 +0000 (Wed, 25 Mar 2009) | 4 lines

Backport of revision 944020

Fixing incremental sync with "remove" items which only have remote id but can be mapped to items from the internal item fetch job.

------------------------------------------------------------------------
r944523 | cgiboudeaux | 2009-03-25 16:14:42 +0000 (Wed, 25 Mar 2009) | 1 line

bump version
------------------------------------------------------------------------
r944611 | winterz | 2009-03-25 20:29:34 +0000 (Wed, 25 Mar 2009) | 6 lines

backport SVN commit 944565 by winterz:

Do not shift Periods if the old or new timeSpec is invalid OR if they are the same.

This fixes an infinite loop reported in kolab/issue3502.

------------------------------------------------------------------------
r944613 | winterz | 2009-03-25 20:32:24 +0000 (Wed, 25 Mar 2009) | 6 lines

backport SVN commit 940551 by winterz:

use preferences-desktop-notification-bell for the "has an alarm" icon.

still no oxygen icon for recurring incidences yet :(

------------------------------------------------------------------------
r944614 | winterz | 2009-03-25 20:33:49 +0000 (Wed, 25 Mar 2009) | 5 lines

backport SVN commit 940609 by winterz:

use the "edit-redo" icon for recurrning.  not so bad, really.


------------------------------------------------------------------------
r944619 | winterz | 2009-03-25 20:36:30 +0000 (Wed, 25 Mar 2009) | 8 lines

backport SVN commit 939810 by woebbe:

burn less CPU cycles:
- memset() the buffer isn't necessary
- use QByteArray::fromRawData() to avoid a deep copy

CCMAIL: woebbeking@kde.org

------------------------------------------------------------------------
r944620 | winterz | 2009-03-25 20:39:20 +0000 (Wed, 25 Mar 2009) | 7 lines

merge 
r938648 | vkrause | 2009-03-12 11:27:08 -0400 (Thu, 12 Mar 2009) | 4 lines
  
Fix double quoting of attendee CN parameters containing a comma.
  
Kolab issue 3477

------------------------------------------------------------------------
r944621 | winterz | 2009-03-25 20:42:55 +0000 (Wed, 25 Mar 2009) | 5 lines

backport SVN commit 933828 by winterz:

use a text value for X-FOO non-standard properties, except if those are vendor specific
properties. note that text values are quoted and folded.

------------------------------------------------------------------------
r944882 | winterz | 2009-03-26 12:15:54 +0000 (Thu, 26 Mar 2009) | 5 lines

backport SVN commit 944880 by winterz:

Fix an infinite loop in FreeBusy::shiftTimes().


------------------------------------------------------------------------
