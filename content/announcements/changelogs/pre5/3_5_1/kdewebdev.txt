2005-11-30 08:51 +0000 [r484310]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Better handling of
	  quotation marks when editing tags inside a script area.

2005-11-30 09:54 +0000 [r484325]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/document.h,
	  branches/KDE/3.5/kdewebdev/quanta/src/quantaview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp: Don't show
	  the file changed dialog after using save as and save again.

2005-12-19 20:59 +0000 [r489822]  adridg

	* branches/KDE/3.5/kdewebdev/quanta/utility/tagaction.cpp: A
	  subtlety: gcc 2.95 won't convert false to 0 to a void * to a Node
	  * in these two function calls. It _looks_ like some APIs were
	  changed and not all the calls fully converted. CCMAIL:
	  kde@freebsd.org

2005-12-19 21:24 +0000 [r489830]  mueller

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/projecttreeview.cpp:
	  remove misplaced semicolon

2005-12-20 21:06 +0000 [r490123-490122]  adridg

	* branches/KDE/3.5/kdewebdev/quanta/components/debugger/variableslistview.cpp:
	  Don't mix QString and char * constants in ternary ?:, even if
	  some GCC versions deal with it nicely. CCMAIL: kde@freebsd.org

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/structtreeview.cpp:
	  gcc 2.95 has trouble with pointer disambiguation. CCMAIL:
	  kde@freebsd.org

2005-12-21 11:28 +0000 [r490274]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/parsercommon.h,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/parser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/saparser.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/parser.h,
	  branches/KDE/3.5/kdewebdev/quanta/src/quantaview.h,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/parsercommon.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Do not corrupt the
	  node tree, even for a little period as it causes strange crashes,
	  this time in VPL. Also guard the kafkaDocument pointer to avoid
	  random crashes on exit. BUG: 118686

2005-12-21 15:47 +0000 [r490343]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Better handling of
	  quotation marks when editing tags inside a script area. BUG:
	  118693

2005-12-21 16:13 +0000 [r490355]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parts/kafka/wkafkapart.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Don't loose the
	  comment closing character when formatting the XML code. BUG:
	  118453

2005-12-22 21:44 +0000 [r490714]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Insert valid img tag
	  for XHTML documents. BUG: 118805

2005-12-23 09:29 +0000 [r490803]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/dialogs/dtepeditdlg.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Don't show the Pages
	  tab in DTEP editing dialog more than once. BUG: 118840

2005-12-23 09:38 +0000 [r490806]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/scripts/htmlquickstart.kmdr:
	  Add XHTML 1.1 and XHTML 1.0 Basic to the quickstart dialog. AFAIK
	  the strings are already present and they anyway should not be
	  translated. BUG: 118813

2005-12-23 14:16 +0000 [r490875]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/scripts/htmlquickstart.kmdr,
	  branches/KDE/3.5/kdewebdev/quanta/src/dcopwindowmanagerif.h,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/xhtml-basic/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Set the DTEP of the
	  document to the one selected in the Quick Start dialog. Requires
	  a newly introduced WindowManagerIf::setDtep DCOP method. Fix the
	  nickname of XHTML 1.0 Basic. BUG: 118814

2005-12-23 14:23 +0000 [r490879]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/scripts/externalpreview.sh:
	  Firefox 1.5 doesn't like spaces in remote commands. Thankfully it
	  works OK in older versions and Opera without spaces as well.
	  CCMAIL: quanta@kde.org

2005-12-23 14:35 +0000 [r490885]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta_init.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Don't have two Close
	  actions. BUG: 118448

2005-12-24 11:50 +0000 [r491046]  mueller

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/basetreeview.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/htmldocumentproperties.cpp:
	  fix various bad if() statements

2005-12-27 23:37 +0000 [r491909]  mhunter

	* branches/KDE/3.5/kdewebdev/quanta/dialogs/dtepeditdlgs.ui,
	  branches/KDE/3.5/kdewebdev/quanta/components/debugger/dbgp/dbgpsettingss.ui,
	  branches/KDE/3.5/kdewebdev/quanta/project/uploadprofiledlgs.ui,
	  branches/KDE/3.5/kdewebdev/quanta/components/debugger/gubed/gubedsettingss.ui:
	  What's This? consistency fixes

2005-12-29 14:03 +0000 [r492282]  mhunter

	* branches/KDE/3.5/kdewebdev/quanta/project/teammembersdlg.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/components/debugger/dbgp/quantadebuggerdbgp.cpp:
	  Typographical corrections and changes CCMAIL:kde-i18n-doc@kde.org

2006-01-01 09:45 +0000 [r493019]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/dialogs/tagdialogs/tagimgdlgdata.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/dialogs/tagdialogs/tagdialog.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/dialogs/tagdialogs/tagimgdlg.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/dialogs/tagdialogs/tagimgdlg.h:
	  More standard compliance regarding the img tag insertion. Now the
	  dialog uses the information from the DTEP files.

2006-01-02 14:24 +0000 [r493454]  scripty

	* branches/KDE/3.5/kdewebdev/klinkstatus/data/icons/hi64-app-klinkstatus.png,
	  branches/KDE/3.5/kdewebdev/klinkstatus/data/icons/hi128-app-klinkstatus.png,
	  branches/KDE/3.5/kdewebdev/klinkstatus/data/icons/hi48-app-klinkstatus.png,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/pics/php.png,
	  branches/KDE/3.5/kdewebdev/klinkstatus/data/icons/hi22-app-klinkstatus.png,
	  branches/KDE/3.5/kdewebdev/doc/quanta/script-action.png: Remove
	  svn:executable from some typical non-executable files (goutte)

2006-01-11 10:08 +0000 [r496777]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/src/dcopquanta.cpp: Don't show
	  CSS pseudo-classes/elements in autocompletion for the class
	  attribute. BUG: 119373

2006-01-12 21:02 +0000 [r497459]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp,
	  branches/KDE/3.5/kdewebdev/lib/qextfileinfo.h,
	  branches/KDE/3.5/kdewebdev/quanta/src/dtds.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Use QDir for local
	  file listing. Using KIO and the NetAccess style hack with an
	  internal event loop is just too error-prone and can cause
	  deadlock in the event loop. :-( Small optimization and fix
	  loading of tag files from subdirectories.

2006-01-17 13:11 +0000 [r499280]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/data/dtep/php/description.rc,
	  branches/KDE/3.5/kdewebdev/quanta/src/document.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Recognize PHP
	  functions which returns references. BUG: 118914

2006-01-19 15:56 +0000 [r500183]  coolo

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.h: updating version

