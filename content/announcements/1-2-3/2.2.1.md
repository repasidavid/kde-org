---
aliases:
- ../announce-2.2.1
custom_about: true
custom_contact: true
date: '2001-09-19'
description: In response to customer demand, we have made KDE the default desktop
  environment in the latest release of our Turbolinux Workstation product," said Dino
  Brusco, VP of Marketing at Turbolinux Inc. "Our customers really appreciate the
  features and stability that KDE provides and we will be offering this latest version
  of KDE in an upcoming release of our Turbolinux Server product.
title: KDE 2.2.1 Release Announcement
---

FOR IMMEDIATE RELEASE

<h3 align="center">Free KDE Desktop Ready for Enterprise Deployment</h3>

KDE Ships Leading Desktop with Advanced Web Browser and Development
Environment for Linux and Other UNIXes

The <a href="/">KDE
Project</a> today announced the immediate release of KDE 2.2.1,
a powerful and easy-to-use Internet-enabled desktop for Linux.
KDE 2.2.1 is available in
<a href="http://i18n.kde.org/teams/distributed.html">42
languages</a>
and ships with the core KDE libraries, the core
desktop environment, and over 100 applications from the other
base KDE packages (administration, multimedia, network, PIM,
development, etc.).
The KDE Project strongly encourages
all users of the <a href="/community/awards">award-winning</a>
KDE to upgrade to KDE 2.2.1.

The primary goals of the 2.2.1 release, which follows one month after
the release of KDE 2.2, are to improve documentation
and provide additional and improved language translations for the
user interface, although the release includes some performance enhancements
(particularly application startup time), numerous minor bugfixes, and
significant improvements to the HTTP protocol.
A <a href="/announcements/changelogs/changelog2_2to2_2_1">list of
these changes</a> and a <a href="/info/1-2-3/2.2.1">FAQ about
the release</a> are available at the KDE
<a href="/">website</a>.
Code development is currently focused
on the branch that will lead to KDE 3.0, scheduled for its first beta release
this December and for final release in late February 2002.

"In response to customer demand, we have made KDE the default desktop
environment in the latest release of our Turbolinux Workstation product,"
said Dino Brusco, VP of Marketing at
<a href="http://www.turbolinux.com/">Turbolinux Inc</a>.
"Our customers really appreciate the features and stability that KDE
provides and we will be offering this latest version of KDE in an
upcoming release of our Turbolinux Server product."

"KDE 2.2.1 delivers the stability that people have come to expect from
KDE," said Waldo Bastian, release manager for KDE 2.2.1 and a
<a href="http://www.suse.com/">SuSE</a> Labs developer.
"We are very pleased to provide our users with this stable release while
we get to work on the next generation of KDE."

KDE 2.2.1 complements the release of KOffice 1.1 late last month.
KOffice is a comprehensive, modular, component-based
suite of office productivity applications. This
combination is the first to provide a complete
Open Source desktop and productivity environment for Linux/Unix.

KDE and all its components (including KOffice) are available
<em><strong>for free</strong></em> under Open Source licenses from the KDE
<a href="http://download.kde.org/stable/2.2.1/src/">server</a>
and its <a href="http://www.kde.org/ftpmirrors.html">mirrors</a> and can
also be obtained on <a href="http://www.kde.org/cdrom.html">CD-ROM</a>.

For more information about KDE 2.2, please see the
<a href="/announcements/announce-2.2">KDE 2.2
press release</a> and the <a href="/info/1-2-3/2.2.1">KDE
2.2.1 Info Page</a>, which is an evolving FAQ about the release.

#### Installing KDE 2.2.1 Binary Packages

<em>Binary Packages</em>.
All major Linux distributors and some Unix distributors have provided
binary packages of KDE 2.2.1 for recent versions of their distribution. Some
of these binary packages are available for free download under
<a href="http://download.kde.org/stable/2.2.1/">http://download.kde.org/stable/2.2.1/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution (if you cannot find a binary package for your distribution,
please read the <a href="http://dot.kde.org/986933826/">KDE Binary Package
Policy</a>).

<em>Library Requirements</em>.
The library requirements for a particular binary package vary with the
system on which the package was compiled. Please bear in mind that
some binary packages may require a newer version of Qt and other libraries
than was included with the applicable distribution (e.g., LinuxDistro 8.0
may have shipped with qt-2.2.3 but the packages below may require
qt-2.3.x). For general library requirements for KDE, please see the text at
<a href="#source_code-library_requirements">Source Code - Library
Requirements</a> below.

<a id="package_locations"><em>Package Locations</em></a>.
At the time of this release, pre-compiled packages are available for:

<ul>
  <li><a href="http://www.caldera.com/">Caldera Systems</a> (<a href="http://download.kde.org/stable/2.2.1/Caldera/kde2.2.1/README">README</a>)</li>
  <ul>
    <li>OpenLinux-3.1:  <a href="http://download.kde.org/stable/2.2.1/Caldera/kde2.2.1/RPMS/">Intel i386</a></li>
  </ul>
  <li><a href="http://www.conectiva.com/">Conectiva Linux</a></li>
  <ul>
    <li>7.0:  <a href="http://download.kde.org/stable/2.2.1/Conectiva/7.0/i386/">Intel i386</a></li>
  </ul>
  <li><a href="http://www.debian.org/">Debian GNU/Linux</a> (package "kde"):  <a href="ftp://ftp.debian.org/">ftp.debian.org</a>:  sid (devel) (see also <a href="http://http.us.debian.org/debian/pool/main/k/">here</a>)</li>

  <li><a href="http://www.redhat.com/">RedHat Linux</a>:
  <ul>
    <li>7.2-beta (Roswell):  <a href="http://download.kde.org/stable/2.2.1/RedHat/roswell/i386/">Intel i386</a>, <a href="http://download.kde.org/stable/2.2.1/RedHat/roswell/ia64/">HP/Intel IA-64</a> and <a href="http://download.kde.org/stable/2.2.1/RedHat/roswell/alpha/">Alpha</a></li>
  </ul>
  <li><a href="http://www.suse.com/">SuSE Linux</a> (<a href="http://download.kde.org/stable/2.2.1/SuSE/README">README</a>):
  <ul>
    <li>7.2:  <a href="http://download.kde.org/stable/2.2.1/SuSE/i386/7.2/">Intel i386</a> and <a href="http://download.kde.org/stable/2.2.1/SuSE/ia64/7.2/">HP/Intel IA-64</a> (see also the <a href="http://download.kde.org/stable/2.2.1/SuSE/noarch/">noarch</a> directory)</li>
    <li>7.1:  <a href="http://download.kde.org/stable/2.2.1/SuSE/i386/7.1/">Intel i386</a>, <a href="http://download.kde.org/stable/2.2.1/SuSE/ppc/7.1/">PowerPC</a>, <a href="http://download.kde.org/stable/2.2.1/SuSE/sparc/7.1/">Sun Sparc</a> and <a href="http://download.kde.org/stable/2.2.1/SuSE/axp/7.1/">Alpha</a> (see also the <a href="http://download.kde.org/stable/2.2.1/SuSE/noarch/">noarch</a> directory)</li>
    <li>7.0:  <a href="http://download.kde.org/stable/2.2.1/SuSE/i386/7.0/">Intel i386</a>, <a href="http://download.kde.org/stable/2.2.1/SuSE/ppc/7.0/">PowerPC</a> and <a href="http://download.kde.org/stable/2.2.1/SuSE/s390/7.0/">IBM S390</a> (see also the <a href="http://download.kde.org/stable/2.2.1/SuSE/noarch/">noarch</a> directory)</li>
    <li>6.4:  <a href="http://download.kde.org/stable/2.2.1/SuSE/i386/6.4/">Intel i386</a> (see also the <a href="http://download.kde.org/stable/2.2.1/SuSE/noarch/">noarch</a> directory)</li>
  </ul>
  <li><a href="http://www.tru64unix.compaq.com/">Tru64 Systems</a> (<a href="http://download.kde.org/stable/2.2.1/Tru64/README.Tru64">README</a>)</li>
  <ul>
    <li>Tru64 4.0d, e, f and g and 5.x:  <a href="http://download.kde.org/stable/2.2.1/Tru64/">Alpha</a></li>
  </ul>
  <li><a href="http://www.yellowdoglinux.com/">Yellow Dog Linux</a></li>
  <ul>
    <li>2.x:  <a href="http://download.kde.org/stable/2.2.1/YellowDog/2.x/ppc/">PowerPC</a> (see also the <a href="http://download.kde.org/stable/2.2.1/YellowDog/2.x/non-kde/ppc/">add-ons</a> directory)</li>
  </ul>
</ul>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days and weeks.

#### Downloading and Compiling KDE 2.2.1

<a id="source_code-library_requirements"></a><em>Library
Requirements</em>.
KDE 2.2.1 requires qt-2.2.4, which is available in source code from Trolltech as
<a href="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.4.tar.gz">qt-x11-2.2.4.tar.gz</a>. In addition, for SSL support, KDE 2.2.1 requires <a href="http://www.openssl.org/">OpenSSL</a> &gt;= 0.9.6x; versions 0.9.5x are no longer
supported. For Java support, KDE 2.2.1 requires a JVM &gt;= 1.3. For
Netscape Communicator plugin support, KDE requires a recent version of
<a href="http://www.lesstif.org/">Lesstif</a> or Motif. Searching
local documentation requires <a href="http://www.htdig.org/">htdig</a>.
Other special features, such as drag'n'drop audio CD ripping, require
other packages.

<em>Compiler Requirements</em>.
Please note that some components of
KDE 2.2.1 will not compile with older versions of
<a href="http://gcc.gnu.org/">gcc/egcs</a>, such as egcs-1.1.2 or
gcc-2.7.2. At a minimum gcc-2.95-\* is required. In addition, some
components of KDE 2.2.1 (such as the multimedia backbone of KDE,
<a href="http://www.arts-project.org/">aRts</a>) will not compile with
<a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc 3.0</a> (the
problems are being corrected by the KDE and GCC teams).

<a id="source_code"></a><em>Source Code/RPMs</em>.
The complete source code for KDE 2.2.1 is available for free download at
<a href="http://download.kde.org/stable/2.2.1/src/">http://download.kde.org/stable/2.2.1/src/</a>
or in the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>.
Additionally, source rpms are available for the following distributions:

<ul>
  <li><a href="http://download.kde.org/stable/2.2.1/Conectiva/7.0/SRPMS.kde/">Conectiva Linux</a></li>
  <li><a href="http://download.kde.org/stable/2.2.1/Caldera/kde2.2.1/SRPMS/">Caldera Systems</a></li>
  <li><a href="http://download.kde.org/stable/2.2.1/RedHat/roswell/SRPMS/">RedHat Linux</a></li>
  <li><a href="http://download.kde.org/stable/2.2.1/SuSE/SRPMS/">SuSE Linux</a></li>
  <li><a href="http://download.kde.org/stable/2.2.1/YellowDog/2.x/">Yellow Dog Linux</a> (see also the <a href="http://download.kde.org/stable/2.2.1/YellowDog/2.x/non-kde/SRPMS/">add-ons</a> directory)</li>
</ul>

<em>Further Information</em>. For further
instructions on compiling and installing KDE 2.2.1, please consult
the <a href="http://www.kde.org/install-source.html">installation
instructions</a> and, if you should encounter problems, the
<a href="http://www.kde.org/compilationfaq.html">compilation FAQ</a>. For
problems with source rpms, please contact the person listed in the .spec
file.

#### About KDE

KDE is an independent, collaborative project by hundreds of developers
worldwide working over the Internet to create a sophisticated,
customizable and stable desktop environment employing a component-based,
network-transparent architecture. KDE is working proof of the power of
the Open Source "Bazaar-style" software development model to create
first-rate technologies on par with and superior to even the most complex
commercial software.

Please visit the KDE family of web sites for the
<a href="http://www.kde.org/faq.html">KDE FAQ</a>,
<a href="/screenshots/kde2shots">screenshots</a>,
<a href="http://www.koffice.org/">KOffice information</a> and
<a href="http://developer.kde.org/documentation/kde2arch.html">developer
information</a>.
Much more <a href="http://www.kde.org/whatiskde/">information</a>
about KDE is available from KDE's
<a href="http://www.kde.org/family.html">family of web sites</a>.

#### Corporate KDE Sponsors

Besides the valuable and excellent efforts by the
<a href="http://www.kde.org/gallery/index.html">KDE developers</a>
themselves, significant support for KDE development has been provided by
<a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
<a href="http://www.suse.com/">SuSE</a>. Thanks!

<hr noshade="noshade" size="1" width="90%" align="center">
<font size=2><em>Trademarks Notices.</em>

KDE, K Desktop Environment and KOffice are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix and Motif are registered trademarks of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
Netscape Communicator is a trademark or registered trademark of Netscape Communications Corporation in the United States and other countries.
Java is a trademark of Sun Microsystems, Inc.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.</font>

<hr />
<table id ="press" border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
  Eunice Kim<br>
  The Terpin Group<br>

[ekim@terpin.com](mailto:ekim@terpin.com)<br>
(1) 650 344 4944 ext. 105<br>&nbsp;<br>
Kurt Granroth <br>

[granroth@kde.org](mailto:granroth@kde.org)
<br>
(1) 480 732 1752<br>&nbsp;<br>
Andreas Pour<br>

[pour@kde.org](mailto:pour@kde.org)<br>
(1) 718-456-1165

  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(44) 1225 837409

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Ralf Nolden<br>
    
  [nolden@kde.org](mailto:nolden@kde.org) <br>
  (49) 2421 502758
  </td>
</tr>
</table>
