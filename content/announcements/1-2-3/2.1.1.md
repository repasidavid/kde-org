---
aliases:
- ../announce-2.1.1
custom_about: true
custom_contact: true
date: '2001-03-27'
description: The primary goals of the 2.1.1 release are to improve documentation and
  provide additional language translations for the user interface, although the release
  includes a few bugfixes, and improvements to the HTML rendering engine
title: KDE 2.1.1 Release Announcement
---

FOR IMMEDIATE RELEASE

<h3 ALIGN="center">New KDE Release for Linux Desktop Ready for Enterprise</h3>

KDE Ships Leading Desktop with Advanced Web Browser, Anti-Aliased Font Capabilities for Linux and Other UNIXes

The <a href="/">KDE
Project</a> today announced the release of KDE 2.1.1,
a powerful and easy-to-use Internet-enabled desktop for Linux. KDE
features <a href="http://konqueror.kde.org/">Konqueror</a>, a
state-of-the-art web browser, as an integrated
component of its user-friendly desktop environment, as well as
<a href="http://www.kdevelop.org/">KDevelop</a>,
an advanced IDE, as a central component of KDE's powerful
development environment. KDE 2.1 is the first stable Linux desktop
completely to integrate the
<a href="http://xfree86.org/4.0.2/fonts5.html#30">new XFree anti-aliased
font extensions</a> and can provide
a fully anti-aliased font-enabled desktop.

This release follows four weeks after the release of the
<a href="/announcements/announce-2.1">industry-acclaimed</a> KDE 2.1, which
marked a leap forward in Linux desktop stability, usability
and maturity. KDE 2.1.1 is suitable for enterprise deployment and the
KDE Project strongly encourages all users of the
<a href="/community/awards">award-winning</a> KDE 1.x
series and of the KDE 2.0 series to upgrade to KDE 2.1.1.

The primary goals of the 2.1.1 release are to improve documentation
and provide additional language translations for the user interface,
although the release includes a few bugfixes, and improvements to the HTML
rendering engine. A
<a href="/announcements/changelogs/changelog2_1to2_1_1">list of
these changes</a> and a <a href="/info/1-2-3/2.1.1">FAQ about
the release</a> are available at the KDE
<a href="/">website</a>.
Code development is currently focused
on the branch that will lead to KDE 2.2, scheduled for its first beta release
in two weeks.

KDE and all its components are available for free under
Open Source licenses from the KDE
<a href="http://ftp.kde.org/stable/2.1.1/distribution/">server</a>
and its <a href="/mirrors">mirrors</a> and can
also be obtained on <a href="http://www.kde.org/cdrom.html">CD-ROM</a>.
As a result of the dedicated efforts of hundreds of translators,
KDE 2.1.1 is available in
<a href="http://i18n.kde.org/teams/distributed.html">34 languages and
dialects</a>, including the addition of Lithuanian in
this release. KDE 2.1.1 ships with the core KDE
libraries, the core desktop environment (including Konqueror), developer
packages (including KDevelop), as well
as the over 100 applications from the other
standard base KDE packages (administration, games,
graphics, multimedia, network, PIM and utilities).

For more information about the KDE 2.1 series, please see the
<a href="/announcements/announce-2.1">KDE 2.1
press release</a> and the <a href="/info/1-2-3/2.1.1">KDE
2.1.1 Info Page</a>, which is an evolving FAQ about the release.
Information on using anti-aliased fonts with KDE is available
<a href="http://dot.kde.org/984693709/">here</a>.

#### Downloading and Compiling KDE

The source packages for KDE 2.1.1 are available for free download at
<a href="http://ftp.kde.org/stable/2.1.1/distribution/tar/generic/src/">http://ftp.kde.org/stable/2.1.1/distribution/tar/generic/src/</a> or in the
equivalent directory at one of the many KDE ftp server
<a href="http://www.kde.org/mirrors.html">mirrors</a>
(<a href="http://ftp.kde.org/stable/2.1.1/distribution/tar/generic/src/diffs/">diffs</a>
are also available). KDE 2.1.1 requires
qt-2.2.3, which is available from
<a href="http://www.trolltech.com/">Trolltech</a> at
<a href="ftp://ftp.trolltech.com/qt/source/">ftp://ftp.trolltech.com/qt/source/</a>
under the name <a href="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.3.tar.gz">qt-x11-2.2.3.tar.gz</a>,
although
<a href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.2.4.tar.gz">qt-2.2.4</a>
or
<a href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</a>
is recommended (for anti-aliased fonts,
<a href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</a>
and <a href="ftp://ftp.xfree86.org/pub/XFree86/4.0.3/">XFree 4.0.3</a> or
newer is required).
KDE 2.1.1 will not work with versions of Qt older than 2.2.3.

For further instructions on compiling and installing KDE, please consult
the <a href="http://www.kde.org/install-source.html">installation
instructions</a> and, if you encounter problems, the
<a href="http://www.kde.org/compilationfaq.html">compilation FAQ</a>.

#### Installing Binary Packages

Some distributors choose to provide binary packages of KDE for certain
versions of their distribution. Some of these binary packages for KDE 2.1.1
will be available for free download under
<a href="http://ftp.kde.org/stable/2.1.1/distribution/">http://ftp.kde.org/stable/2.1.1/distribution/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="http://www.kde.org/mirrors.html">mirrors</a>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.

KDE 2.1.1 requires qt-2.2.3, the free version of which is available
from the above locations usually under the name qt-x11-2.2.3, although
qt-2.2.4 or qt-2.3.0 is recommended (for anti-aliased fonts,
qt-2.3.0 and XFree 4.0.3 or newer is required).
KDE 2.1.1 will not work with versions of Qt
older than 2.2.3.

At the time of this release, pre-compiled packages are available for:

<ul>
<li><a href="http://www.caldera.com/">Caldera</a> eDesktop 2.4: <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/Caldera/eDesktop24/RPMS/">i386</a></li>
<li><a href="http://www.debian.org/">Debian GNU/Linux</a> stable (2.2):  <a href="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-i386/">i386</a> and <a href="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-powerpc/">PPC</a>; please also check the <a href="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-all/">main</a> directory for common files</li>
<li><a href="http://www.linux-mandrake.com/en/">Linux-Mandrake</a> 7.2:  <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/Mandrake/7.2/RPMS/">i586</a></li>

<li><a href="http://www.suse.com/">SuSE Linux</a> (<a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/README">README</a>):
<ul>
<li>7.1:  <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/i386/7.1/">i386</a>, <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/sparc/7.1/">Sparc</a> and <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/ppc/7.1/">PPC</a>; please also check the <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/localisation/">i18n</a> and <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/noarch/">noarch</a> directories for common files</li>
<li>7.0:  <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/i386/7.0/">i386</a> and <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/ppc/7.0/">PPC</a>; please also check the <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/localisation/">i18n</a> and <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/noarch/">noarch</a> directories for common files</li>
<li>6.4:  <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/i386/6.4/">i386</a>; please also check the <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/localisation/">i18n</a> and <a href="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/noarch/">noarch</a> directories for common files</li>
</ul>
<li><a href="http://www.tru64unix.compaq.com/">Tru64</a> Systems:  <a href="http://ftp.kde.org/stable/2.1.1/distribution/tar/Tru64/">4.0e,f,g, or 5.x</a> (<a href="http://ftp.kde.org/stable/2.1.1/distribution/tar/Tru64/README.Tru64">README</a>)</li>
</ul>

Please check the servers periodically for pre-compiled packages for other
distributions. More binary packages will become available over the
coming days and weeks. In particular
<a href="http://www.redhat.com/">RedHat</a> packages are expected soon.

#### About KDE

KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.

For more information about KDE, please visit KDE's
<a href="http://www.kde.org/whatiskde/">web site</a>.
More information about KDE 2 is available in two
(<a href="http://devel-home.kde.org/~granroth/LWE2000/index.html">1</a>,
<a href="http://mandrakesoft.com/~david/OSDEM/">2</a>) slideshow
presentations and on
<a href="/">KDE's web site</a>, including an evolving
<a href="/info/1-2-3/2.1">FAQ</a> to answer questions about
migrating to KDE 2.1 from KDE 1.x,
<a href="http://dot.kde.org/984693709/">anti-aliased font tutorials</a>, a
number of
<a href="/screenshots/kde2shots">screenshots</a>, <a href="http://developer.kde.org/documentation/kde2arch.html">developer information</a> and
a developer's
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</a>.

<hr />
<font SIZE=2>

_Trademarks Notices._
KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
</font>

<hr /><table border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
    Kurt Granroth <br>
    
  [granroth@kde.org](mailto:granroth@kde.org)
    <br>
    (1) 480 732 1752<br>&nbsp;<br>
    Andreas Pour<br>
    [pour@kde.org](mailto:pour@kde.org)<br>
    (1) 718-456-1165
  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(44) 1225 837409

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Martin Konold<br>
    
  [konold@kde.org](mailto:konold@kde.org)
    <br>
    (49) 179 2252249
  </td>
</tr>
<table>
