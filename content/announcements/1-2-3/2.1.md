---
aliases:
- ../announce-2.1
custom_about: true
custom_contact: true
date: '2001-02-26'
description: KDE Ships Leading Desktop with Advanced Web Browser for GNU/Linux and
  Other UNIXes. [...] This release marks a leap forward in GNU/Linux desktop stability,
  usability and maturity and is suitable for enterprise deployment.
title: KDE 2.1 Release Announcement
---

FOR IMMEDIATE RELEASE

<h3 align="center">New KDE Desktop Ready for the Enterprise</h3>

KDE Ships Leading Desktop with Advanced Web Browser for Linux
and Other UNIXes

The <a href="/">KDE Project</a> today announced the release of KDE 2.1,
a powerful and easy-to-use Internet-enabled desktop for Linux. KDE
features <a href="http://konqueror.kde.org/">Konqueror</a>, a
state-of-the-art web browser, as an integrated
component of its user-friendly desktop environment, as well as
<a href="http://www.kdevelop.org/">KDevelop</a>,
an advanced IDE, as a central component of KDE's powerful
development environment.
This release marks a leap forward in Linux desktop stability, usability
and maturity and is suitable for enterprise deployment.
The KDE Project strongly encourages all users to upgrade to KDE 2.1.

KDE and all its components are available for free under
Open Source licenses from the KDE
<a href="http://ftp.kde.org/stable/2.1/distribution/">server</a>
and its <a href="http://www.kde.org/mirrors.html">mirrors</a> and can
also be obtained on <a href="http://www.kde.org/cdrom.html">CD-ROM</a>.
KDE 2.1 is available in
<a href="http://i18n.kde.org/teams/distributed.html">33 languages</a> and
ships with the core KDE
libraries, the core desktop environment (including Konqueror), developer
packages (including KDevelop), as well
as the over 100 applications from the other
standard base KDE packages (administration, games,
graphics, multimedia, network, PIM and utilities).

"This second major release of the KDE 2 series is a real improvement in
terms of stability, performance
and features," said David Faure, release manager for KDE 2.1 and
KDE Representative at
<a href="http://www.mandrakesoft.com/">Mandrakesoft</a>.
"KDE 2 has
now matured into a solid, intuitive and complete desktop for daily use.
Konqueror is a full-featured and robust web browser
and important applications like the mail client (KMail) have greatly
improved.
The multimedia architecture has made great strides and
this release inaugurates the new media player noatun,
which has a modular, plugin design for playing the latest audio and
video formats.
For development, KDE 2.1 for the first time is bundled
with KDevelop, an outstanding IDE/RAD which will be comfortably familiar to
developers with Windows development backgrounds.
In short, KDE 2.1
is a state-of-the-art desktop and development environment,
and positions Linux/Unix to make significant inroads in the home and
enterprise."

"KDE 2.1 opens the door to widespread adoption of the Linux desktop
and will help provide the success on the desktop that Linux already
enjoys in the server space," added Dirk Hohndel, CTO of
<a href="http://www.suse.com/">Suse AG</a>.
"With its intuitive
interface, code maturity and excellent development tools and environment, I am
confident that enterprises and third party developers will realize
the enormous potential KDE offers and will migrate their workstations
and applications to Linux/KDE."

"KDE boasts an outstanding graphical design and robust functionality," said
Sheila Harnett, Senior Technical Staff Member for IBM's Linux Technology
Center.
"KDE 2.1 significantly raises the bar for Linux desktop
functionality, usability and quality in virtually every aspect of the
desktop."

<STRONG><EM>KDE 2: The K Desktop Environment</EM></STRONG>.
<a id="Konqueror"></a><a href="http://konqueror.kde.org/">Konqueror</a>
is KDE 2's next-generation web browser,
file manager and document viewer.
The standards-compliant
Konqueror has a component-based architecture which combines the features and
functionality of Internet Explorer/Netscape
Communicator and Windows Explorer.
Konqueror supports the full gamut of current Internet technologies,
including JavaScript, Java, XML, HTML 4.0, CSS-1 and -2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator plug-ins (for Flash, RealAudio, RealVideo
and similar technologies).

In addition, KDE offers seamless network transparency for accessing
or browsing files on Linux, NFS shares, MS Windows
SMB shares, HTTP pages, FTP directories, LDAP directories and audio CDs.
The modular,
plug-in nature of KDE's file architecture makes it simple to add additional
protocols (such as IPX, WebDAV or digital cameras) to KDE, which would
then automatically be available to all KDE applications.

KDE's <a id="customizability">configurability and customizability</a>
touches every aspect of the desktop.
KDE offers a unique cascading
customization feature where customization settings are inherited through
different layers, from global to per-user, permitting enterprise-wide
and group-based configurations.
KDE's sophisticated theme support
starts with Qt's style engine, which permits developers and artists to
create their own widget designs.
KDE 2.1 ships with over 14 of these styles,
some of which emulate the look of various operating systems.
Additionally
KDE includes a new theme manager and does an excellent job of
importing themes from GTK and GNOME.
Moreover, KDE 2 fully
supports Unicode and KHTML is the only free HTML rendering engine on
Linux/X11 that features nascent support for BiDi scripts
such as Arabic and Hebrew.

KDE 2 features an advanced, network-transparent multimedia architecture
based on <a id="arts">aRts</a>, the Analog Realtime Synthesizer.
ARts is a full-featured sound system which
includes a number of effects and filters, a modular analog synthesizer
and a mixer.
The aRts sound server provides network transparent sound support for
both input and output using MCOP, a CORBA-like network design, enabling
applications running on remote computers to output sound and receive
input from the local workstation.
This architecture provides a much-needed complement
to the network transparency provided by X and for the first time permits
users to run their applications remotely with sound enabled.
Moreover, aRts enables multiple applications (local or remote) to
output sound and/or video concurrently.
Video support is <a
href="http://mpeglib.sourceforge.net/">available</a> for MPEG versions
1, 2 and 4 (experimental), as well as the AVI and DivX formats.
Using the aRts component technology, it is very easy to develop
multimedia applications.

Besides the exceptional compliance with Internet and file-sharing standards
<a href="#Konqueror">mentioned above</a>, KDE 2 is a leader in
compliance with the available Linux desktop standards.
KWin, KDE's new
re-engineered window manager, complies to the new
<a href="http://www.freedesktop.org/standards/wm-spec.html">Window Manager
Specification</a>.
Konqueror and KDE comply with the <a
HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/kio/DESKTOP_ENTRY_STANDARD">Desktop
Entry Standard</a>.
Konqueror uses the
<a href="http://pyxml.sourceforge.net/topics/xbel/docs/html">XBEL</a>
standard for its bookmarks.
KDE 2 largely complies with the
<a href="http://www.newplanetsoftware.com/xdnd/">X Drag-and-Drop (XDND)
protocol</a> as well as with the
<a href="http://www.rzg.mpg.de/rzg/batch/NEC/sx4a_doc/g1ae04e/chap12.html">X11R6 session management protocol (XSMP)</a>.

<STRONG><EM>KDE 2: The K Development Environment</EM></STRONG>.
KDE 2.1 offers developers a sophisticated IDE as well as a rich set
of major technological improvements over the critically acclaimed
KDE 1 series.
Chief among the technologies are
the <a href="#DCOP">Desktop COmmunication Protocol (DCOP)</a>, the
<a href="#KIO">I/O libraries (KIO)</a>, <a href="#KParts">the component
object model (KParts)</a>, an <a href="#XMLGUI">XML-based GUI class</a>, and
a <a href="#KHTML">standards-compliant HTML rendering engine (KHTML)</a>.

KDevelop is a leading Linux IDE
with numerous features for rapid application
development, including a GUI dialog builder, integrated debugging, project
management, documentation and translation facilities, built-in concurrent
development support, and much more.

<a id="KParts">KParts</a>, KDE 2's proven component object model, handles
all aspects of application embedding, such as positioning toolbars and inserting
the proper menus when the embedded component is activated or deactivated.
KParts can also interface with the KIO trader to locate available handlers for
specific mimetypes or services/protocols.
This technology is used extensively by the
<a href="http://www.koffice.org/">KOffice</a> suite and Konqueror.

<a id="KIO">KIO</a> implements application I/O in a separate
process to enable a
non-blocking GUI without the use of threads.
The class is network and protocol transparent
and hence can be used seamlessly to access HTTP, FTP, POP, IMAP,
NFS, SMB, LDAP and local files.
Moreover, its modular
and extensible design permits developers to "drop in" additional protocols,
such as WebDAV, which will then automatically be available to all KDE
applications.
KIO also implements a trader which can locate handlers
for specified mimetypes; these handlers can then be embedded within
the requesting application using the KParts technology.

The <a id="XMLGUI">XML GUI</a> employs XML to create and position
menus, toolbars and possibly
other aspects of the GUI.
This technology offers developers and users
the advantage of simplified configurability of these user interface elements
across applications and automatic compliance with the
<a href="http://developer.kde.org/documentation/standards/">KDE Standards
and Style Guide</a> irrespective of modifications to the standards.

<a id="DCOP">DCOP</a> is a client-to-client communications
protocol intermediated by a
server over the standard X11 ICE library.
The protocol supports both
message passing and remote procedure calls using an XML-RPC to DCOP "gateway".
Bindings for C, C++ and Python, as well as experimental Java bindings, are
available.

<a id="KHTML">KHTML</a> is an HTML 4.0 compliant rendering
and drawing engine.
The class
will support the full gamut of current Internet technologies, including
JavaScript, Java, HTML 4.0, CSS-2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator plugins (for
viewing Flash,
RealAudio, RealVideo and similar technologies).
The KHTML class can easily
be used by an application as either a widget (using normal window
parenting) or as a component (using the KParts technology).
KHTML, in turn, has the capacity to embed components within itself
using the KParts technology.

#### Downloading and Compiling KDE 2.1

The source packages for KDE 2.1 are available for free download at
<a href="http://ftp.kde.org/stable/2.1/distribution/tar/generic/src/">http://ftp.kde.org/stable/2.1/distribution/tar/generic/src/</a> or in the
equivalent directory at one of the many KDE ftp server
<a href="http://www.kde.org/mirrors.html">mirrors</a>.
KDE 2.1 requires
qt-2.2.4, which is available in source code from Trolltech as
<a href="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.4.tar.gz">qt-x11-2.2.4.tar.gz</a>.
KDE 2.1 should work with Qt-2.2.3 but Qt-2.2.4 is recommended.

For further instructions on compiling and installing KDE 2.1, please consult
the <a href="http://www.kde.org/install-source.html">installation
instructions</a> and, if you encounter problems, the
<a href="http://www.kde.org/compilationfaq.html">compilation FAQ</a>.

#### Installing Binary Packages

Some distributors choose to provide binary packages of KDE for certain
versions of their distribution.
Some of these binary packages for KDE 2.1
will be available for free download under
<a href="http://ftp.kde.org/stable/2.1/distribution/">http://ftp.kde.org/stable/2.1/distribution/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="http://www.kde.org/mirrors.html">mirrors</a>.
Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.

KDE 2.1 requires qt-2.2.4, the free version of which is available
from the above locations usually under the name qt-x11-2.2.4.
KDE 2.1
should work with Qt-2.2.3 but Qt-2.2.4 is recommended.

At the time of this release, pre-compiled packages are available for:

<ul>
<li>Caldera eDesktop 2.4: <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/Caldera/eDesktop24/RPMS/">i386</a></li>
<li>Debian GNU/Linux:
<ul>
<li>Potato (2.2):  <a href="http://ftp.kde.org/stable/2.1/distribution/deb/dists/potato/main/binary-i386/">i386</a>, <a href="http://ftp.kde.org/stable/2.1/distribution/deb/dists/potato/main/binary-sparc/">Sparc</a> and <a href="http://ftp.kde.org/stable/2.1/distribution/deb/dists/potato/main/binary-powerpc/">PPC</a></li>
</ul>
<li>Linux-Mandrake 7.2:  <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/Mandrake/7.2/i586/">i586</a></li>
<li>RedHat Linux:
<ul>
<li>Wolverine:  <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/i386/">i386</a>; please also check the <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/common/">common</a> directory for common files</li>
<li>7.0:  <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/i386/">i386</a> and <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/alpha/">Alpha</a>; please also check the <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/common/">common</a> directory for common files</li>
<li>6.x:  <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/i386/">i386</a>, <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/alpha/">Alpha</a> and <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/sparc/">Sparc</a>; please also check the <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/common/">common</a> directory for common files</li>
</ul>
<li>SuSE Linux:
<ul>
<li>7.1:  <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.1-i386/">i386</a> and <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.1-sparc/">Sparc</a>; please also check the <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</a> or <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</a> directory for common files</li>
<li>7.0:  <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.0-i386/">i386</a> and <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.0-ppc/">PPC</a>; please also check the <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</a> or <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</a> directory for common files</li>
<li>6.4:  <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/6.4-i386/">i386</a>; please also check the <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</a> or <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</a> directory for common files</li>
<li>6.3:  <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/6.3-i386/">i386</a>; please also check the <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</a> or <a href="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</a> directory for common files</li>
</li>
</ul>

<li><a href="http://ftp.kde.org/stable/2.1/distribution/tar/Tru64/">Tru64 Systems</a></li>
<li><a href="http://ftp.kde.org/stable/2.1/distribution/tar/FreeBSD/">FreeBSD</a></li>
</ul>

Please check the servers periodically for pre-compiled packages for other
distributions.
More binary packages will become available over the
coming days and weeks.

#### What Others Are Saying

KDE 2.1 has already earned accolades from industry leaders worldwide.
A sampling of comments follows.

"We welcome the release of KDE 2.1," stated Dr. Markus Draeger, Senior
Manager for Partner Relations at Fujitsu Siemens Computers. "The release
introduces several important new components, like KDevelop and the media
player noatun, and overall is a major step forward for this leading GUI
on Linux."

"We are very excited about the enhancements in KDE 2.1 and we are pleased
to be able to contribute to the project," said Rene Schmidt, Corel's
Executive Vice-President, Linux Products. "KDE continues to improve with
each release, and these enhancements will make our easy-to-use Linux
distribution for the desktop even better."

"A greater number and availability of Linux applications is an important
factor that will determine if Linux permeates the enterprise desktop,"
said Drew Spencer, Chief Technology Officer for Caldera Systems, Inc.
"KDE 2.1 addresses this issue with the integration of the Konqueror
browser and KDevelop, a tool that allows developers to create
applications in C++ for all kinds of environments. Together with the
existing tools available for KDE, KDevelop is a one-stop solution for
developers."

"With the 2.1 release, KDE again demonstrates its capacity to offer rich
software and provide a complete and stable environment for everyday use",
added Ga&euml;l Duval, co-founder of Mandrakesoft. "This latest release
has paved the way for KDE on user's desktops in the enterprise as well
as at home. From the full-featured web browser to the friendly
configuration center, it provides all the common facilities many
computers users need to abandon Windows&reg; entirely."

#### About KDE

KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.

For more information about KDE, please visit KDE's
<a href="http://www.kde.org/whatiskde/">web site</a>.
More information about KDE 2 is available in two
(<a href="http://devel-home.kde.org/~granroth/LWE2000/index.html">1</a>,
<a href="http://mandrakesoft.com/~david/OSDEM/">2</a>) slideshow
presentations and on
<a href="/">KDE's web site</a>, including an evolving
<a href="/info/1-2-3/2.1">FAQ</a> to answer questions about
migrating to KDE 2.1 from KDE 1.x, a number of
<a href="/screenshots/kde2shots">screenshots</a>, <a href="http://developer.kde.org/documentation/kde2arch.html">developer information</a> and
a developer's
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</a>.

<hr /><FONT SIZE=2>

_Trademarks Notices._
KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
MS Windows, Internet Explorer and Windows Explorer are trademarks or registered
trademarks of Microsoft Corporation.
Netscape and Netscape Communicator are trademarks or registered trademarks of Netscape Communications Corporation in the United States and other countries and JavaScript is a trademark of Netscape Communications Corporation.
Java is a trademark of Sun Microsystems, Inc.
Flash is a trademark or registered trademark of Macromedia, Inc. in the United States and/or other countries.
RealAudio and RealVideo are trademarks or registered trademarks of RealNetworks, Inc.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners. </font>
<br>

<hr /><table border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
    Kurt Granroth <br>
    
  [granroth@kde.org](mailto:granroth@kde.org)
    <br>
    (1) 480 732 1752<br>&nbsp;<br>
    Andreas Pour<br>
    [pour@kde.org](mailto:pour@kde.org)<br>
    (1) 718-456-1165
  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(44) 1225 837409

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Martin Konold<br>
    
  [konold@kde.org](mailto:konold@kde.org)
    <br>
    (49) 179 2252249
  </td>
</tr>
</table>
