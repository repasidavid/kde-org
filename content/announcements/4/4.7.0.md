---
aliases:
- ../4.7
custom_about: true
custom_contact: true
date: '2011-07-27'
description: KDE Ships 4.7.0 Workspaces, Applications and Platform.
title: New KDE Applications, Workspaces and Development Platform Releases Bring New
  Features, Improve Stability
---

<p>
KDE is delighted to announce its latest set of releases, providing major updates to the KDE Plasma Workspaces, KDE Applications, and the KDE Platform that provides the foundation for KDE software. Version 4.7 of these releases provide many new features and improved stability and performance.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/general-desktop.png">
	<img src="/announcements/4/4.7.0/thumbs/general-desktop.png" class="img-fluid" alt="Plasma and Applications 4.7">
	</a> <br/>
	<em>Plasma and Applications 4.7</em>
</div>
<br/>

<h3>
<a href="./plasma">
Plasma Workspaces Become More Portable Thanks to KWin
</a>
</h3>

<p>
<a href="./plasma">
<img src="/announcements/4/4.7.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.7" />
</a>

The Plasma Workspaces gain from extensive work to KDE’s compositing window manager, KWin, and from the leveraging of new Qt technologies such as Qt Quick. For full details, read the <a href="./plasma">Plasma Desktop and Plasma Netbook 4.7 release announcement</a>.
<br />
<br/>

</p>

<h3>
<a href="./applications">
Updated KDE Applications Bring Many Exciting Features
</a>
</h3>

<p>
<a href="./applications">
<img src="/announcements/4/4.6.0/images/applications.png" class="app-icon float-left m-3" alt="The KDE Applications 4.7"/>
</a>
Many KDE applications are updated. In particular, KDE's groupware solution, Kontact, rejoins the main KDE release cycle with all major components ported to Akonadi. The Digikam Software Collection, KDE's feature-rich photo management tools, come with a major new version. For full details, read the <a href="./applications">KDE Applications 4.7 release announcement</a>.
<br />
</p>

<h3>
<a href="./platform">
Improved Multimedia, Instant Messaging and Semantic Capabilities in the KDE Platform
</a>
</h3>

<p>
<a href="./platform">
<img src="/announcements/4/4.7.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.7"/>
</a>

A wide range of KDE and third party software will be able to take advantage of extensive work in Phonon and major improvements to the semantic desktop components, with enriched APIs and improved stability. The new KDE Telepathy framework offers integration of instant messaging directly into workspaces and applications. Performance and stability improvements in nearly all components lead to a smoother user experience and a reduced footprint of applications using the KDE Platform 4.7. For full details, read the <a href="./platform">KDE Platform 4.7 release announcement</a>.
<br />

</p>

<h3>
New Instant Messaging integrated directly into desktop
</h3>
<p>
The KDE-Telepathy team is proud to announce the technical preview and historic first release of the new IM solution for KDE. Even though it's still in its early stages, you can already set up all sorts of accounts, including GTalk and Facebook Chat and use them in your everyday life. The chat interface lets you choose among many appearances with its support of Adium themes. You can also put the Presence Plasma widget right into your panel to manage your online status. As this project is not yet mature enough to be part of the big KDE family, it is packaged and released separately, alongside the other major parts of KDE. The source code for KDE-Telepathy 0.1.0 is available on <a href="http://download.kde.org/download?url=unstable/telepathy-kde/0.1.0/src/">download.kde.org</a>. Installation instructions are available on <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration/Installing_stable_release">community.kde.org</a>.
</p>

<h3>
Stability As Well As Features
</h3>
<p>
In addition to the many new features described in the release announcements, KDE contributors have closed over 12,000 bug reports (including over 2,000 unique bugs in the software released today) since the last major releases of KDE software. As a result, our software is more stable than ever before.
</p>

<h3>
Spread the Word and See What Happens: Tag as "KDE"
</h3>
<p>
KDE encourages everybody to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, Vimeo and others. Please tag uploaded material with "KDE", so it is easier to find, and so that the KDE team can compile reports of coverage for the 4.7 releases of KDE software.

<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.7/&amp;title=KDE%20releases%20version%204.7%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="/announcements/4.7/" data-text="#KDE releases version 4.7 of Plasma Workspaces, Applications and Platform → https://www.kde.org/announcements/4.7/ #kde47" data-count="horizontal" data-via="kdecommunity">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.7/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.7%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="/announcements/4.7/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde47"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde47"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde47"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde47"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>

<h3>
About these release announcements
</h3><p>
These release announcements were prepared by Algot Runeman, Dennis Nienhüser, Dominik Haumann, Jos Poortvliet, Markus Slopianka, Martin Klapetek, Nick Pantazis, Sebastian K&uuml;gler, Stuart Jarvis, Vishesh Handa, Vivek Prakash, Carl Symons and other members of the KDE Promotion Team and wider community. They cover only a few highlights of the many changes made to KDE software over the past six months.
</p>

<h4>Support KDE</h4>

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.7.0/images/join-the-game.png" class="img-fluid float-left mr-3"
alt="Join the Game"/> </a>

<p align="justify"> KDE e.V.'s new <a
href="http://jointhegame.kde.org/">Supporting Member programme</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.</p>
<br />
<p>&nbsp;</p>
