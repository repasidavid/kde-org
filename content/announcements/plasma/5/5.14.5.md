---
aliases:
- ../../plasma-5.14.5
changelog: 5.14.4-5.14.5
date: 2019-01-08
layout: plasma
figure:
  src: /announcements/plasma/5/5.14.0/plasma-5.14.png
asBugfix: true
---

- [weather dataengine] Updates to bbc, envcan and noaa weather sources.
- KDE Plasma Addons Comic Plasmoid: several fixes to make updates more reliable.
- Make accessibility warning dialog usable again and fix event handling. <a href="https://commits.kde.org/plasma-desktop/36e724f1f458d9ee17bb7a66f620ab7378e5e05e">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D17536">D17536</a>
