---
aliases:
- ../../plasma-5.2.0
changelog: 5.1.2-5.2.0
date: '2015-01-27'
layout: plasma
---

Tuesday, 27 January 2015. Today KDE releases Plasma 5.2. This release adds a number of new components, many new features and many more bugfixes.

{{<figure src="/announcements/plasma/5/5.2.0/full-screen.png" alt="Dual monitor setup" class="text-center" width="600px">}}

## New Components

{{<figure src="/announcements/plasma/5/5.2.0/full-screen.png" alt="Dual monitor setup" class="text-center" width="600px">}}

{{<figure src="/announcements/plasma/5/5.2.0/kscreen.png" alt="Dual monitor setup" class="text-center" width="600px" caption="KScreen dual monitor setup">}}

This release of Plasma comes with some new components to make your desktop even more complete:

<strong>BlueDevil</strong>: a range of desktop components to manage Bluetooth devices. It'll set up your mouse, keyboard, send &amp; receive files and you can browse for devices.

<strong>KSSHAskPass</strong>: if you access computers with ssh keys but those keys have passwords this module will give you a graphical UI to enter those passwords.

<strong>Muon</strong>: install and manage software and other addons for your computer.

<strong>Login theme configuration (SDDM)</strong>: SDDM is now the login manager of choice for Plasma and this new System Settings module allows you to configure the theme.

<strong>KScreen</strong>: getting its first release for Plasma 5 is the System Settings module to set up multiple monitor support.

<strong>GTK Application Style</strong>: this new module lets you configure themeing of applications from Gnome.

<strong>KDecoration</strong>: this new library makes it easier and
more reliable to make themes for KWin, Plasma's window manager. It has
impressive memory, performance and stability improvements. If you are
missing a feature don't worry it'll be back in Plasma 5.3.

## Other highlights

<strong>Undo</strong> changes to Plasma desktop layout

{{<figure src="/announcements/plasma/5/5.2.0/output.gif" alt="Undo desktop changes" class="text-center" width="600px" caption="Undo changes to desktop layout">}}

Smarter sorting of results in <strong>KRunner</strong>, press Alt-space to easily search through your computer

{{<figure src="/announcements/plasma/5/5.2.0/window_decoration.png" alt="Smart sorting in KRunner" class="text-center" width="600px" caption="Smart sorting in KRunner">}}

<strong>Breeze window decoration</strong> theme adds a new look to your desktop and is now used by default

{{<figure src="/announcements/plasma/5/5.2.0/krunner.png" alt="New Breeze Window Decoration" class="text-center" width="600px" caption="New Breeze Window Decoration">}}

The artists in the visual design group have been hard at work on many new <strong>Breeze icons</strong>

{{<figure src="/announcements/plasma/5/5.2.0/icons.png" alt="More Breeze Icons" class="text-center" width="600px" caption="More Breeze Icons">}}

They are have added a new white mouse <strong>cursor theme</strong> for Breeze.

<strong>New plasma widgets</strong>: 15 puzzle, web browser, show desktop

{{<figure src="/announcements/plasma/5/5.2.0/new_plasmoid.png" alt="Web browser plasmoid" class="text-center" width="600px" caption="Web browser plasmoid">}}

<strong>Audio Player controls</strong> in KRunner, press Alt-Space and type next to change music track

The Kicker alternative application menu can install applications from the menu and adds menu editing features.

Our desktop search feature Baloo sees optimisations on
startup. It now consumes 2-3x less CPU on startup.  The query parser
supports "type" / "kind" properties, so you can type "kind:Audio" in
krunner to filter out Audio results.

In the screen locker we improved the integration with
logind to ensure the screen is properly locked before suspend. The
background of the lock screen can be configured. Internally this uses
part of the Wayland protocol which is the future of the Linux
desktop.

There are improvements in the handling of multiple monitors. The
detection code for multiple monitors got ported to use the XRandR
extension directly and multiple bugs related to it were fixed.

<strong>Default applications in Kickoff</strong> panel menu have been updated to list Instant Messaging, Kontact and Kate.

There is a welcome return to the touchpad enable/disable feature for laptop keypads with these keys.

Breeze will <strong>set up GTK themes</strong> on first login to match.

Over 300 bugs fixed throughout Plasma modules.