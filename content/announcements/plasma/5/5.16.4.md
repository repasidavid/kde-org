---
aliases:
- ../../plasma-5.16.4
changelog: 5.16.3-5.16.4
date: 2019-07-30
layout: plasma
youtube: T-29hJUxoFQ
figure:
  src: /announcements/plasma/5/5.16.0/plasma-5.16.png
  class: text-center mt-4
asBugfix: true
---

- Fix compilation with Qt 5.13 (missing include QTime). <a href="https://commits.kde.org/plasma-desktop/7c151b8d850f7270ccc3ffb9a6b3bcd9860609a3">Commit.</a>
- [LNF KCM] make it possible to close the preview. <a href="https://commits.kde.org/plasma-desktop/d88f6c0c89e2e373e78867a5ee09b092239c72de">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D22543">D22543</a>
- Airplane mode improvements. <a href="https://commits.kde.org/plasma-nm/7dd740aa963057c255fbbe83366504bbe48a240e">Commit.</a> Fixes bug <a href="https://bugs.kde.org/399993">#399993</a>. Fixes bug <a href="https://bugs.kde.org/400535">#400535</a>. Fixes bug <a href="https://bugs.kde.org/405447">#405447</a>. Phabricator Code review <a href="https://phabricator.kde.org/D22680">D22680</a>
