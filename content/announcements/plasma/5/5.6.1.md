---
aliases:
- ../../plasma-5.6.1
changelog: 5.6.0-5.6.1
date: 2016-03-29
layout: plasma
youtube: v0TzoXhAbxg
figure:
  src: /announcements/plasma/5/5.6.0/plasma-5.6.png
  class: mt-4 text-center
asBugfix: true
---

- Fix drawing QtQuickControls ComboBox popups
- Fix untranslatable string in Activities KCM.
- Show ratings in Discover Packagekit backend
