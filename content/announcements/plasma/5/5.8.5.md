---
aliases:
- ../../plasma-5.8.5
changelog: 5.8.4-5.8.5
date: 2016-12-27
layout: plasma
youtube: LgH1Clgr-uE
figure:
  src: /announcements/plasma/5/5.8.0/plasma-5.8.png
  class: text-center mt-4
asBugfix: true
---

- Notice when the only screen changes. <a href="https://commits.kde.org/plasma-workspace/f7b170de9fd9c4075fee324d33212b0d69909ba4">Commit.</a> Fixes bug <a href="https://bugs.kde.org/373880">#373880</a>
- Revert 'Do not ask for root permissions when it's unnecessary', it caused problems with adding a new user. <a href="https://commits.kde.org/user-manager/f2c69db182fb20453e671359e90a3bc6de40c7b0">Commit.</a> Fixes bug <a href="https://bugs.kde.org/373276">#373276</a>
- Fix compilation with Qt 5.8. <a href="https://commits.kde.org/plasma-integration/6b405fead515df417514c9aa9bb72cfa5372d2e7">Commit.</a>
