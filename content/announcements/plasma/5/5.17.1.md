---
aliases:
- ../../plasma-5.17.1
changelog: 5.17.0-5.17.1
date: 2019-10-22
layout: plasma
peertube: 5a315252-2790-42b4-8177-94680a1c78fc
figure:
  src: /announcements/plasma/5/5.17.0/plasma-5.17.png
asBugfix: true
---

- [Mouse KCM] Fix acceleration profile on X11. <a href="https://commits.kde.org/plasma-desktop/829501dd777966091ddcf94e5c5247aaa78ac832">Commit.</a> Fixes bug <a href="https://bugs.kde.org/398713">#398713</a>. Phabricator Code review <a href="https://phabricator.kde.org/D24711">D24711</a>
- Fix slideshow crashing in invalidate(). <a href="https://commits.kde.org/plasma-workspace/a1cf305ffb21b8ae8bbaf4d6ce03bbaa94cff405">Commit.</a> Fixes bug <a href="https://bugs.kde.org/413018">#413018</a>. Phabricator Code review <a href="https://phabricator.kde.org/D24723">D24723</a>
- [Media Controller] Multiple artists support. <a href="https://commits.kde.org/plasma-workspace/1be4bb880fdeea93381eb45846a7d487e58beb93">Commit.</a> Fixes bug <a href="https://bugs.kde.org/405762">#405762</a>. Phabricator Code review <a href="https://phabricator.kde.org/D24740">D24740</a>
