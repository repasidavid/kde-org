---
aliases:
- ../../plasma-5.15.90
changelog: 5.15.5-5.15.90
date: 2019-05-16
layout: plasma
title: 'KDE Plasma 5.16 Beta: Your Three Week Notification of a More Tidy and Composed
  Desktop'
figure:
  src: /announcements/plasma/5/5.16.0/plasma-5.16-full-2.png
---

{{% i18n_date %}}

Today KDE launches the beta release of Plasma 5.16.

In this release, many aspects of Plasma have been polished and
rewritten to provide high consistency and bring new features. There is a completely rewritten notification system supporting Do Not Disturb mode, more intelligent history with grouping, critical notifications in fullscreen apps, improved notifications for file transfer jobs, a much more usable System Settings page to configure everything, and many other things. The System and Widget Settings have been refined and worked on by porting code to newer Kirigami and Qt technologies and polishing the user interface. And of course the VDG and Plasma team effort towards <a href="https://community.kde.org/Goals/Usability_%26_Productivity">Usability & Productivity goal</a> continues, getting feedback on all the papercuts in our software that make your life less smooth and fixing them to ensure an intuitive and consistent workflow for your daily use.

For the first time, the default wallpaper of Plasma 5.16 will be decided by a contest where everyone can participate and submit art. The winner will receive a Slimbook One v2 computer, an eco-friendly, compact machine, measuring only 12.4 x 12.8 x 3.7 cm. It comes with an i5 processor, 8 GB of RAM, and is capable of outputting video in glorious 4K. Naturally, your One will come decked out with the upcoming KDE Plasma 5.16 desktop, your spectacular wallpaper, and a bunch of other great software made by KDE. You can find more information and submitted work on <a href="https://community.kde.org/KDE_Visual_Design_Group/Plasma_5.16_Wallpaper_Competition">the competition wiki page</a>, and you can <a href="https://forum.kde.org/viewtopic.php?f=312&t=160487">submit your own wallpaper in the subforum</a>.

### Desktop Management

{{<figure src="/announcements/plasma/5/5.16.0/notifications.png" data-toggle="lightbox" alt="New Notifications" caption="New Notifications" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.16.0/plasma-theme-fixes.png" data-toggle="lightbox" alt="Theme Engine Fixes for Clock Hands!" caption="Theme Engine Fixes for Clock Hands!" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.16.0/panel-edit-mode-alternatives.png" data-toggle="lightbox" alt="Panel Editing Offers Alternatives" caption="Panel Editing Offers Alternatives" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.16.0/sddm-theme.png" data-toggle="lightbox" alt="Login Screen Theme Improved" caption="Login Screen Theme Improved" width="600px" >}}

- Completely rewritten notification system supporting Do Not Disturb mode, more intelligent history with grouping, critical notifications in fullscreen apps, improved notifications for file transfer jobs, a much more usable System Settings page to configure everything, and more!
- Plasma themes are now correctly applied to panels when selecting a new theme.
- More options for Plasma themes: offset of analog clock hands and toggling blur behind.
- All widget configuration settings have been modernized and now feature an improved UI. The Color Picker widget also improved, now allowing dragging colors from the plasmoid to text editors, palette of photo editors, etc.
- The look and feel of lock, login and logout screen have been improved with new icons, labels, hover behavior, login button layout and more.
- When an app is recording audio, a microphone icon will now appear in the System Tray which allows for changing and muting the volume using mouse middle click and wheel. The Show Desktop icon is now also present in the panel by default.
- The Wallpaper Slideshow settings window now displays the images in the selected folders, and allows selecting and deselecting them.
- The Task Manager features better organized context menus and can now be configured to move a window from a different virtual desktop to the current one on middle click.
- The default Breeze window and menu shadow color are back to being pure black, which improves visibility of many things especially when using a dark color scheme.
- The "Show Alternatives..." button is now visible in panel edit mode, use it to quickly change widgets to similar alternatives.
- Plasma Vaults can now be locked and unlocked directly from Dolphin.

### Settings

{{<figure src="/announcements/plasma/5/5.16.0/color-scheme.png" data-toggle="lightbox" alt="Color Scheme" caption="Color Scheme" width="600px" >}}

{{<figure src="/announcements/plasma/5/5.16.0/application-style.png" data-toggle="lightbox" alt="Application Style and Appearance Settings" caption="Application Style and Appearance Settings" width="600px" >}}

- There has been a general polish in all pages; the entire Appearance section has been refined, the Look and Feel page has moved to the top level, and improved icons have been added in many pages.

- The Color Scheme and Window Decorations pages have been redesigned with a more consistent grid view. The Color Scheme page now supports filtering by light and dark themes, drag and drop to install themes, undo deletion and double click to apply.

- The theme preview of the Login Screen page has been overhauled.

- The Desktop Session page now features a "Reboot to UEFI Setup" option.

- There is now full support for configuring touchpads using the Libinput driver on X11.

### Window Management

{{<figure src="/announcements/plasma/5/5.16.0/window-management.png" data-toggle="lightbox" alt="Window Management" caption="Window Management" width="600px" >}}

- Initial support for using Wayland with proprietary Nvidia drivers has been added. When using Qt 5.13 with this driver, graphics are also no longer distorted after waking the computer from sleep.

- Wayland now features drag and drop between XWayland and Wayland native windows.

- Also on Wayland, the System Settings Libinput touchpad page now allows you to configure the click method, switching between "areas" or "clickfinger".

- KWin's blur effect now looks more natural and correct to the human eye by not unnecessary darkening the area between blurred colors.

- Two new default shortcuts have been added: Meta+L can now be used by default to lock the screen and Meta+D can be used to show and hide the desktop.

- GTK windows now apply correct active and inactive colour scheme.

### Plasma Network Manager

{{<figure src="/announcements/plasma/5/5.16.0/plasma-nm-wireguard.png" data-toggle="lightbox" alt="Plasma Network Manager with Wireguard" caption="Plasma Network Manager with Wireguard" width="600px" >}}

- The Networks widget is now faster to refresh Wi-Fi networks and more reliable at doing so. It also has a button to display a search field to help you find a particular network from among the available choices. Right-clicking on any network will expose a "Configure…" action.
- WireGuard is now compatible with NetworkManager 1.16.
- One Time Password (OTP) support in Openconnect VPN plugin has been added.

### Discover

{{<figure src="/announcements/plasma/5/5.16.0/discover-update.png" data-toggle="lightbox" alt="Updates in Discover" caption="Updates in Discover" width="600px" >}}

- In Discover's Update page, apps and packages now have distinct "downloading" and "installing" sections. When an item has finished installing, it disappears from the view.
- Tasks completion indicator now looks better by using a real progress bar. Discover now also displays a busy indicator when checking for updates.
- Improved support and reliability for AppImages and other apps that come from store.kde.org.
- Discover now allows you to force quit when installation or update operations are proceeding.
- The sources menu now shows the version number for each different source for that app.
