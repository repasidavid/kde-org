---
date: 2023-08-01
changelog: 5.27.6-5.27.7
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Discover: Fix SteamOS dbus path. [Commit.](http://commits.kde.org/discover/31e05554222ac60127cbef760153e11b2f7b6684)
+ Emojier: Update Unicode data for emojier to 15.0. [Commit.](http://commits.kde.org/plasma-desktop/5b2be7c874caab6f178656df68af7c28682890c6)
+ System Settings: Properly show the custom header widget when started with non-QML KCMs. [Commit.](http://commits.kde.org/systemsettings/3d306d1ef5446db5925c887964ab75b2c82a2701)
