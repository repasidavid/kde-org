---
aliases:
- ../../plasma-5.20.1
changelog: 5.20.0-5.20.1
date: 2020-10-20
layout: plasma
video: true
asBugfix: true
---

+ KSysGuard: Divide network speeds by 2 to match reality. <a href='https://commits.kde.org/ksysguard/33694eafa0178ecb1e33d7d26bb15dc97defbf3c'>Commit.</a>
+ Powerdevil: Ignore players from KDE Connect when suspending. <a href='https://commits.kde.org/powerdevil/4ac78e7118238414d3f2d603c21975413eea8bb0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/427209'>#427209</a>
+ Bluedevil kcm: Set sane default size. <a href='https://commits.kde.org/bluedevil/fdc622af5f49b6ccfc83641c9ce8e8d77a99e82a'>Commit.</a>
