---
date: 2022-06-28
changelog: 5.25.1-5.25.2
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Everywhere: update Qt 5 version requirement to 5.15.2 and KF5 requirement to 5.94. [Commit.](http://commits.kde.org/plasma-tests/f95c79a15528b4152ca7cd95d1f5194d0388e936)
+ Kcms/colors: Fix window titlebar tinting in colorsapplicator. [Commit.](http://commits.kde.org/plasma-workspace/b6b34f14901d8e493be4caa7f54c2dd36a2f0b46) Fixes bug [#455395](https://bugs.kde.org/455395)
+ System Settings: Make sidebar tooltips respect the "Display informational tooltips" global setting. [Commit.](http://commits.kde.org/systemsettings/aa827bd682b33b54828b8520b458a5b4a13c27c8) Fixes bug [#455073](https://bugs.kde.org/455073)

