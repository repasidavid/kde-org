---
date: 2024-07-02
changelog: 6.1.1-6.1.2
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Discover: Fix share dialog. [Commit.](http://commits.kde.org/discover/1ba781bd504c7b04d6b827a4ea98ec5c400df0ed) Fixes bug [#488976](https://bugs.kde.org/488976)
+ Libtaskmanager: improve efficiency when window icon frequently changes. [Commit.](http://commits.kde.org/plasma-workspace/cd563dff3a5f5b61de0d3142768c47707f8d8e55) Fixes bug [#487390](https://bugs.kde.org/487390)
+ Do not hide panel settings when a panel-parented dialog takes focus. [Commit.](http://commits.kde.org/plasma-workspace/3acaa98ff89120cc93cdcb87210c061a7de2a004) Fixes bug [#487161](https://bugs.kde.org/487161)
