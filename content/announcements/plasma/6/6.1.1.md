---
date: 2024-06-25
changelog: 6.1.0-6.1.1
layout: plasma
video: false
asBugfix: true
draft: false
---

+ KScreenLocker Greeter: Fix Shader Wallpaper plugin and possibly others. [Commit.](http://commits.kde.org/kscreenlocker/82db40779730d284bcf474e3f23dfc0573387bb6) 
+ Increase minimum plasma wayland protocols version to 1.13. [Commit.](http://commits.kde.org/libkscreen/b00d21942a4fa02fefe36500dbaf50b29bcff362)
+ Use snap:// URLs rather than appstream:// ones on snap only distros. [Commit.](http://commits.kde.org/plasma-welcome/c23401051d5f553989ff92d6ed01c1d187451fdc) 
