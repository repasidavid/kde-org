---
date: 2023-12-07
appCount: 120
image: true
layout: gear
---
+ dolphin-plugins: Mountisoaction: add preferred raw disk mimetypes ([Commit](http://commits.kde.org/dolphin-plugins/507052906beca5d66991d156036bec0d75708556), fixes bug [#475659](https://bugs.kde.org/475659))
+ falkon: Fix StartPage search engine default configuration ([Commit](http://commits.kde.org/falkon/00a1b8c1009b638aca1a0d41876a317186420741), fixes bug [#419530](https://bugs.kde.org/419530))
+ kdepim-runtime: Correctly reload configuration ([Commit](http://commits.kde.org/kdepim-runtime/fdaec5c3f9ac54fa1978a8f31230c5c2325cb931), fixes bug [#473897](https://bugs.kde.org/473897))
