---
title: "KDE Mailing Lists"
---


The KDE mailing lists are one of the main communication channels in the KDE Community.
This is a list of general mailing lists to give a quick overview. For application specific lists, please see the <a href="../../applications/">application's page</a>. For a complete overview, see the <a href="https://mail.kde.org/mailman/listinfo">full list of all mailing lists</a>.

## General user lists

- <b>kde</b> - for KDE users
  with questions strictly about KDE applications
  <br />(<a href="http://mail.kde.org/mailman/listinfo/kde">subscribe kde</a>, <a href="http://lists.kde.org/?l=kde&amp;r=1&amp;w=2">archive of kde</a>)

* <b>kde-announce</b> -
  announcements of new KDE releases, security announcements, and notification of new KDE applications (very low traffic)
  <br />(<a href="http://mail.kde.org/mailman/listinfo/kde-announce">subscribe kde-announce</a>, <a href="http://lists.kde.org/?l=kde-announce&amp;r=1&amp;w=2">archive of kde-announce</a>)

## General development lists

- <b>kde-devel</b> -
  for application developers (both applications in central KDE packages and contributed applications
  <br />(<a href="http://mail.kde.org/mailman/listinfo/kde-devel">subscribe kde-devel</a>, <a href="http://lists.kde.org/?l=kde-devel&amp;r=1&amp;w=2">archive of kde-devel</a>)

* <b>kde-core-devel</b> -
  for KDE library developers. The discussions on this list are limited at KDE
  libraries development, git and other central development issues.
  <br />(<a href="http://mail.kde.org/mailman/listinfo/kde-core-devel">subscribe kde-core-devel</a>, <a href="http://lists.kde.org/?l=kde-core-devel&amp;r=1&amp;w=2">archive of kde-core-devel</a>)

To <b>Subscribe</b>: Either use the above links and use the mailman interface,
or send a mail to <i>list-name</i>&#x2d;r&#101;&#00113;&#117;e&#x73;&#116;&#064;k&#0100;&#101;&#0046;org with
<i>subscribe your-email-address</i> in the subject of the
message. Please write your real email address instead of the plain
"<i>your-email-address</i>" words, and the name of the list instead
of the "<i>list-name</i>" words. Leave the body of the message
empty. Do not include a signature or other stuff which might confuse
the mail server which processes your request.

To <b>Unsubscribe</b>: Either use the above links and use the mailman interface,
or send a mail to <i>list-name</i>&#x2d;r&#x65;q&#117;e&#115;&#116;&#x40;kde&#x2e;&#111;&#114;g with
<i>unsubscribe your-email-address</i> in the subject of the
message. Please replace "<i>your-email-address</i>" with the email
address you did use for the subscription, and "<i>list-name</i>" with the
name of the list you subscribed to. Leave the body of the
message empty. Do not include a signature or other stuff which might
confuse the mail server which processes your request.

<b>Notes</b>:<br />
Please do not use HTML to send your request because it makes it difficult
for our mail server to process your request. If the subscription address
is identical to the address you are posting from you may omit the address
in the subject line.

Contact addresses for developers can be found in the documentation of
their individual applications.

The KDE Project consists of a loose affiliation of developers,
writers, translators, artists and other contributors from around the
world. Views expressed on the mailing lists are, unless explicitly
stated otherwise, the views of the poster only, do not reflect the
views of the KDE Project and may or may not reflect the views of other
KDE contributors.
