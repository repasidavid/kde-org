---
aliases:
- /info/releases-20.08.1
announcement: /announcements/releases/2020-09-apps-update
date: 2020-09-03
title: "20.08.1 Releases Source Info Page"
layout: releases
signer: Christoph Feck
signing_fingerprint: F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87
---
