KDE Project Security Advisory
=============================

Title:          konqueror: HTML Thumbnailer automatic remote file access
Risk Rating:    Low
Date:           23 April 2024

Overview
========
Various KDE applications share a plugin system to create thumbnails
of various file types for displaying in file managers, file dialogs, etc.

konqueror contains a thumbnailer plugin for HTML files.

The konqueror HTML thumbnailer was incorrectly accessing some content of
remote URLs listed in HTML files. This meant that the owners of the servers
referred in HTML files in your system could have seen in their access logs
your IP address every time the thumbnailer tried to create the thumbnail.

The HTML thumbnailer using Qt6 is fixed and does not access remote URLs anymore.

Workaround
==========
Remove the HTML Thumbnailer plugin from your system.
The file name is webarchivethumbnail.so and should be in your Qt plugin path.
The Qt plugin path can be queried with
    qmake -query QT_INSTALL_PLUGINS

Solution
========
Update to a konqueror version using Qt6

Credits
=======
Thanks to Markus Niemi for the report.
