KDE Security Advisory: Konqueror SSL vulnerability
Original Release Date: 2002-08-18
URL: http://www.kde.org/info/security/advisory-20020818-1.txt

0. References
        http://online.securityfocus.com/archive/1/286290/2002-07-31/2002-08-06/0
        http://online.securityfocus.com/archive/1/287050/2002-08-07/2002-08-13/2

1. Systems affected:

        All versions of KDE up to and including KDE 3.0.2

2. Overview:

        KDE's SSL implementation fails to check the basic constraints on
certificates and as a result may accept certificates as valid that were signed
by an issuer who was not authorized to do so.
      
3. Impact:

        Users of Konqueror and other SSL enabled KDE software may fall victim
to a malicious man-in-the-middle attack without noticing. In such case the
user will be under the impression that there is a secure connection with a
trusted site while in fact a different site has been connected to.

4. Solution:

        Upgrade kdelibs to KDE 3.0.3. A patch for KDE 2.2.2 is available as
well for users that are unable to upgrade to KDE 3.

5. Patch:
        A patch for KDE 2.2.2 is available from 
ftp://ftp.kde.org/pub/kde/security_patches :

        0e0da738b276567e9ee36aa824e86124  post-2.2.2-kdelibs-kssl.diff
