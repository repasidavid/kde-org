-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: kppp Privileged fd Leak Vulnerability
Original Release Date: 2005-02-28
URL: http://www.kde.org/info/security/advisory-20050228-1.txt

0. References

        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2005-0205
        http://www.idefense.com/application/poi/display?type=vulnerabilities


1. Systems affected:

        kppp as included in KDE up to including KDE 3.1.5. KDE 3.2.x
        and newer are not affected.


2. Overview:

        kppp, if installed suid root, allows local attackers to hijack
        a system's domain name resolution function. 

        A fix introduced for a similiar vulnerability, added to the code
        base in 1998, was incomplete and can be bypassed.

        In 2002 a proper fix was made by Dirk Mueller for KDE 3.2 as part
        of a code audit. No advisory was issued because the problem was
        considered to be unexploitable at that time. iDEFENSE now
        rediscovered the issue and supplied an example exploit for this
        vulnerability.


3. Impact:

        Modifications to /etc/hosts and /etc/resolv.conf can be done by
        local users which allows manipulation of host and domain name
        lookups, enabling other phishing and social engineering attacks.


4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        A patch for KDE 3.1 is available from 

        ftp://ftp.kde.org/pub/kde/security_patches :

        0e999df54963edd5f565b6d541f408d9  post-3.1.5-kdenetwork.diff


6. Time line and credits:

        09/02/2005 Notification of kppp maintainer by iDEFENSE, Inc.
        09/02/2005 Notification of KDE Security by Harri Porten
        28/02/2005 Coordinated Public Disclosure


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.5 (GNU/Linux)

iD8DBQFCI3/GvsXr+iuy1UoRAm/zAJoC2eicq1bM9h5yO10Z5ieUZmC3BgCeLdkV
SXalkg0PMGqXtsecgVkGV0E=
=oNoJ
-----END PGP SIGNATURE-----
