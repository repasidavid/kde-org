KDE Security Advisory: Calligra and KOffice Input Validation Failure
Original Release Date: 2012-08-10
URL: http://www.kde.org/info/security/advisory-20120810-1.txt

0. References:
    CVE-2012-3456 Calligra
    CVE-2012-3455 KOffice

1. Systems affected:

    All versions of Calligra before 2.5.
    All released versions of KOffice, and KOffice 
    
2. Overview:

    A flaw has been found which can allow malicious code to take advantage
    of an input validation failure in the Microsoft import filter in Calligra
    and KOffice.

    This flaw was discovered by Charlie Miller. Details can be found in his
    Black Hat 2012 paper:
    http://media.blackhat.com/bh-us-12/Briefings/C_Miller/BH_US_12_Miller_NFC_attack_surface_WP.pdf

3. Impact:

    Exploitation can allow the attacker to gain control of the running
    process and execute code on its behalf.

4. Solution:

    Source code patches have been made available which fix these
    vulnerabilities. At the time of this writing most OS vendor / binary
    package providers should have updated binary packages for Calligra,
    but may not have updated packages for KOffice. Contact your OS
    vendor / binary package provider for information about how to obtain
    updated binary packages.

5. Patch:

    Patches have been committed to the Calligra Git repository in the
    following commit IDs:

    2.4 branch: 7d72f7dd
    2.5 branch: f04d585c
    master:     8652ab67

    Patches can be retrieved by cloning the Calligra repository at
    git://anongit.kde.org/calligra.git and running "git show <commit-id>"

    A patch has been committed to master branch of the KOffice Git repository
    in the following commit ID:

    4e851f9

    Patches can be retrieved by cloning the KOffice repository at
    git://anongit.kde.org/koffice.git and running "git show <commit-id>"

