---
title: "KDE Applications 15.04.3 Info Page"
announcement: /announcements/announce-applications-15.04-3
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
