---
version: "5.245.0"
title: "KDE Frameworks 6 Alpha Source Info and Download"
type: info/frameworks6
date: 2023-11-08
signer: Jonathan Esk-Riddell
signing_fingerprint: E0A3EB202F8E57528E13E72FD7574483BB57B18D
custom_annc: kdes-6th-megarelease-alpha
draft: false
---
