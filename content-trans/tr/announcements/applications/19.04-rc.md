---
aliases:
- ../announce-applications-19.04-rc
date: 2019-04-05
description: KDE Uygulamalar 19.04  Sürüm Adayını Gönderdi.
layout: application
release: applications-19.03.90
title: KDE, KDE Uygulamalar 19.04 Sürüm Adayını Gönderdi
version_number: 19.03.90
version_text: 19.04 Release Candidate
---
5 Nisan 2019. Bugün KDE, KDE Uygulamalarının yeni sürümlerinin Sürüm Adayını yayınladı. Bağımlılık ve özellik donmalarıyla birlikte, KDE ekibinin odak noktası artık hataları düzeltmek ve daha fazla parlatmaktır.

Check the <a href='https://community.kde.org/Applications/19.04_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release.

The KDE Applications 19.04 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the Release Candidate <a href='https://bugs.kde.org/'>and reporting any bugs</a>.
