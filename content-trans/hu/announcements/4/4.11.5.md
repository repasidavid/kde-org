---
aliases:
- ../announce-4.11.5
date: 2014-01-07
description: A KDE elérhetővé teszi a Plasma Workspaces, Applications és Platform
  4.11.5-ös verzióját
title: KDE Announces 4.11.5
---
January 7, 2014. Today KDE released updates for its Workspaces, Applications and Development Platform. These updates are the fifth in a series of monthly stabilization updates to the 4.11 series. As was announced on the release, the workspaces will continue to receive updates until August 2015. This release only contains bugfixes and translation updates and will be a safe and pleasant update for everyone.

Several recorded bugfixes include improvements to the personal information management suite Kontact, the UML tool Umbrello, the document viewer Okular, the web browser Konqueror, the file manager Dolphin, and others. The Plasma calculator can handle greek letters now and Okular can print pages with long titles. And Konqueror got better web fonts support through fixing a bug.

A változások bővebb <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.5&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>listáját</a> megtalálhatja a KDE hibakövető rendszerében. A 4.11.5 változásainak részletes listáját a Git naplók böngészésével is megkaphatja.

To download source code or packages to install go to the <a href='/info/4/4.11.5'>4.11.5 Info Page</a>. If you want to find out more about the 4.11 versions of KDE Workspaces, Applications and Development Platform, please refer to the <a href='/announcements/4.11/'>4.11 release notes</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Az új, küldés később munkafolyamat a Kontactban` width="600px">}}

A KDE szoftverek, beleértve az összes függvénykönyvtárat és alkalmazást, szabadon elérhetők nyílt forráskódú licencek alatt. A KDE szoftvereinek forráskódját vagy az azokból készült bináris csomagokat letöltheti a <a href='http://download.kde.org/stable/4.11.5/'>download.kde.org</a> oldalról vagy a <a href='/distributions'>főbb GNU/Linux és UNIX rendszerek</a> szállítóitól.
