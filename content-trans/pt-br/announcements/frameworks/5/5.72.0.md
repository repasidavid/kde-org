---
aliases:
- ../../kde-frameworks-5.72.0
date: 2020-07-04
layout: framework
libCount: 70
---
### KDAV: Novo Módulo

Implementação do protocolo DAV com o KJobs.

Os calendários e itens por-fazer são suportados, usando tanto o GroupDAV como o CalDAV, e os contactos são suportados com o GroupDAV ou o CardDAV.

### Baloo

+ [Indexadores] Ignorar o tipo MIME baseado no nome para as decisões de indexação iniciais (erro 422085)

### BluezQt

+ Exposição dos dados de publicação do serviço de um dispositivo

### Ícones Breeze

+ Correcção dos três ícones a 16px para o application-x-... para serem mais perfeitos ao nível do pixel (erro 423727)
+ Adição do suporte para escalas &gt;200% nos ícones que deveriam continuar monocromáticos
+ Recorte do centro do 'window-close-symbolic'
+ Actualização do ícone da aplicação e acção do Cervisia
+ Adição de secção README sobre o tipo de letra da Web
+ Uso do 'grunt-webfonts' em vez do 'grunt-webfont' e desactivação da geração de ícones com ligações simbólicas
+ Adição do ícone do Kup a partir do seu repositório respectivo

### Módulos extra do CMake

+ Remoção do suporte para o 'png2ico'
+ Lidar com o código do CMake para o Qt que modifica o CMAKE_SHARED_LIBRARY_SUFFIX
+ Adição do módulo de pesquisa do FindTaglib

### Ferramentas Doxygen do KDE

+ Suporte para o "repo_id" no metainfo.yaml para substituir o nome do repositório detectado

### KCalendarCore

+ Verificação de erros de escrita no save() no caso de o disco ficar sem espaço (erro 370708)
+ Correcção dos nomes dos ícones dos itens por-fazer recorrentes
+ Correcção da serialização da data inicial do item por-fazer recorrente (erro 345565)

### KCMUtils

+ Correcção do sinal 'changed' no selector de 'plugins'

### KCodecs

+ Adição de algumas mudanças de linha, caso contrário o texto poderá ficar grande na mensagem

### KConfig

+ Passagem também do 'locationType' no KConfigSkeleton na construção do grupo
+ Mudança do texto "Mudar a Língua da Aplicação..." por questões de consistência

### KConfigWidgets

+ Mudança do texto "Mudar a Língua da Aplicação..." por questões de consistência

### KCoreAddons

+ KRandom::random -&gt; QRandomGenerator::global()
+ Descontinuação do KRandom::random

### KCrash

+ Adição da declaração em falta do 'environ', caso contrário só fica disponível no GNU

### KDocTools

+ Uso de estilo consistente para o aviso da FDL (erro 423211)

### KFileMetaData

+ Adaptação do 'kfilemetadata' para o "audio/x-speex+ogg", dada a alteração recente no 'shared-mime-info'

### KI18n

+ Adição também de aspas em torno do texto formatado do &lt;ficheiro&gt;

### KIconThemes

+ Eliminação da atribuição no 'resetPalette'
+ Devolução de QPalette() se não existir uma paleta personalizada
+ Respeito do QIcon::fallbackSearchpaths() (erro 405522)

### KIO

+ [kcookiejar] Correcção da leitura da opção de 'cookies' "Aceitar para a Sessão" (erro 386325)
+ KDirModel: correcção de regressão no hasChildren() para as árvores com ficheiros, ligações simbólicas, 'pipes' e dispositivos de caracteres escondidos (erro 419434)
+ OpenUrlJob: correcção do suporte para programas da consola com um espaço no nome do ficheiro (erro 423645)
+ Adição de atalho da Web para o DeepL e o ArchWiki
+ [KFilePlacesModel] Mostrar os dispositivos AFC (Apple File Conduit)
+ Codificar também os espaços nos URL's de atalhos da Web (erro 423255)
+ [KDirOperator] Activação de facto do 'dirHighlighting' por omissão
+ [KDirOperator] Realce da pasta anterior ao recuar/subir (erro 422748)
+ [Lixo] Tratamento do erro ENOENT ao mudar os nomes dos ficheiros (erro 419069)
+ IOSlave 'file': definição de data/hora ao nanosegundo na cópia (erro 411537)
+ Ajuste dos URL's para os motores de busca do Qwant (erro 423156)
+ IOSlave 'file': Adição do suporte para a cópia de ligações de referência (erro 326880)
+ Correcção da configuração do atalho predefinido no KCM de atalhos da Web (erro 423154)
+ Garantia de legibilidade do KCM de atalhos da Web ao definir uma largura mínima (erro 423153)
+ FileSystemFreeSpaceJob: emissão de erro se o 'kioslave' não tiver fornecido os meta-dados
+ [Lixo] Remoção dos ficheiros 'trashinfo' que referenciam ficheiros/pastas que não existem (erro 422189)
+ kio_http: Adivinhar nós mesmos o tipo MIME se o servidor devolver 'application/octet-stream'
+ Descontinuação dos sinais 'totalFiles' e 'totalDirs', por não serem emitidos
+ Correcção da actualização dos novos atalhos da Web (erro 391243)
+ kio_http: Correcção do estado da mudança de nome com a sobreposição activa
+ Não interpretar o nome como um ficheiro ou localização escondidos (erro 407944)
+ Não mostrar os atalhos da Web apagados/escondidos (erro 412774)
+ Correcção de estoiro na remoção do item (erro 412774)
+ Não permitir a atribuição de atalhos existentes
+ [kdiroperator] Uso de melhores dicas para as acções 'recuar' e 'avançar' (erro 422753)
+ [BatchRenameJob] Uso do KJob::Items ao devolver a informação de progresso (erro 422098)

### Kirigami

+ [aboutpage] Uso do OverlaySheet para o texto da licença
+ correcção dos ícones do modo recolhido
+ Uso de separadores claros para o DefaultListItemBackground
+ Adição da propriedade 'weight' (peso) no Separator
+ [overlaysheet] Evitar uma altura fraccionária para o 'contentLayout' (erro 422965)
+ Correcção da documentação do 'pageStack.layers'
+ Reintrodução do suporte de camadas no Page.isCurrentPage
+ suporte da cor do ícone
+ uso do componente interno MnemonicLabel
+ Redução global do preenchimento à esquerda da barra de ferramentas quando o título não é usado
+ Só definir o 'implicit{Width,Height}' para o Separator
+ Actualização do KF5Kirigami2Macros.cmake para usar o HTTPS com o repositório Git para evitar erros ao tentar aceder ao mesmo
+ Correcção: carregamento do avatar
+ Tornar a propriedade PageRouterAttached#router modificável
+ Correcção do fecho do  OverlaySheet ao carregar dentro do formato (erro 421848)
+ Adição de 'id' em falta ao GlobalDrawerActionItem no GlobalDrawer
+ Correcção do OverlaySheet por estar demasiado alto
+ Correcção do exemplo de código do PlaceholderMessage
+ desenho do contorno no fundo dos grupos
+ uso de um componente de LSH
+ Adição de fundo aos cabeçalhos
+ Melhor tratamento da recolha
+ Melhor apresentação dos itens do cabeçalho da lista
+ Adição da propriedade 'bold' ao BasicListItem
+ Correcção do Kirigami.Units.devicePixelRatio=1.3 quando deveria ser 1.0 com 96PPP
+ Esconder a pega do OverlayDrawer quando não for interactivo
+ Ajuste dos cálculos do ActionToolbarLayoutDetails para tirar melhor partido do espaço real do ecrã
+ ContextDrawerActionItem: Preferir a propriedade 'text' face à 'tooltip'

### KJobWidgets

+ Integração do KJob::Unit::Items (erro 422098)

### KJS

+ Correcção de estoiro ao usar o KJSContext::interpreter

### KNewStuff

+ Passagem do texto explicativo do cabeçalho da folha para o cabeçalho da lista
+ Correcção das localizações do programa de instalação e da descompressão
+ Esconder o ShadowRectangle para as antevisões não carregadas (erro 422048)
+ Não permitir que o conteúdo extravase nas delegações da grelha (erro 422476)

### KNotification

+ Não usar o 'notifybysnore.h' no MSYS2

### KParts

+ Descontinuação do PartSelectEvent e companhia

### KQuickCharts

+ Adição de reticências ao valor do LegendDelegate quando não existir largura suficiente
+ Correcção do erro "C1059: expressão não constante ..."
+ Ter em conta a espessura da linha na verificação dos limites
+ Não usar o 'fwidth' ao desenhar as linhas no gráfico de linhas
+ Remodelação do 'removeValueSource' para não usar QObject's destruídos
+ Uso do 'insertValueSource' no Chart::appendSource
+ Inicialização adequada do ModelHistorySource::{m_row,m_maximumHistory}

### KRunner

+ Correcção do RunnerContextTest para não assumir a presença do '.bashrc'
+ Uso dos meta-dados de JSON incorporados nos 'plugins' binários &amp; personalizados nos 'plugins' de D-Bus
+ Emissão do 'queryFinished' quando todas as tarefas da pesquisa actual tiverem terminado (erro 422579)

### KTextEditor

+ Mudança do "ir para a linha" para funcionar para trás (erro 411165)
+ correcção de estoiro na remoção da janela se os intervalos ainda estiverem activos (erro 422546)

### KWallet Framework

+ Introdução de três novos métodos que devolvem todos os "itens" numa pasta

### KWidgetsAddons

+ Correcção do KTimeComboBox para regiões com caracteres fora do normal nos formatos (erro 420468)
+ KPageView: remoção de imagem invisível do lado direito do cabeçalho
+ KTitleWidget: passagem da propriedade de QPixmap para QIcon
+ Descontinuação de alguma API do KMultiTabBarButton/KMultiTabBarTab que usa o QPixmap
+ KMultiTabBarTab: mudança da lógica do do estado 'styleoption' para seguir ainda mais do QToolButton
+ KMultiTabBar: não mostrar os botões assinalados no QStyle::State_Sunken
+ [KCharSelect] Atribuição inicial do foco ao campo de texto da pesquisa

### KWindowSystem

+ [xcb] Envio da geometria do ícone com uma escala correcta (erro 407458)

### KXMLGUI

+ Uso do 'kcm_users' em vez do 'user_manager'
+ Uso da nova API do KTitleWidget::icon/iconSize
+ Mudança do "Mudar a Língua da Aplicação" para o menu Configuração (erro 177856)

### Plasma Framework

+ Mostrar um aviso mais claro se o KCM solicitado não foi encontrado
+ [spinbox] Não usar os itens do QQC2 quando deveria ser usado o PlasmaComponents (erro 423445)
+ Componentes do Plasma: repor o controlo perdido da cor na legenda do TabButton
+ Introdução do PlaceholderMessage
+ [calendário] Redução do tamanho da legenda do mês
+ [ExpandableListItem] Uso da mesma lógica para a visibilidade da acção e do botão da seta
+ [Sombras das Janelas] Migração para a API de sombras do KWindowSystem (erro 416640)
+ Criação de ligação simbólica do 'widgets/plasmoidheading.svgz' no Brisa escuro/claro
+ Correcção do Kirigami.Units.devicePixelRatio=1.3 quando deveria ser 1.0 com 96PPP
+ Adição de propriedade para aceder ao item do carregador do ExpandableListItem

### Prison

+ Descontinuação do AbstractBarcode::minimumSize() também para o compilador

### Purpose

+ Uso explícito das unidades do Kirigami em vez de usar implicitamente as unidades do Plasma
+ Migração das janelas de tarefas para o Kirigami.PlaceholderMessage

### QQC2StyleBridge

+ Mudança do 'selectByMouse' para 'true' no SpinBox
+ Correcção dos borrões nos ícones dos meus com alguns tamanhos de letra (erro 423236)
+ Evitar a sobreposição de delegados e da barra de deslocamento nas listas suspensas
+ Correcção do 'implicitWidth' vertical do Slider e um erro de associação
+ Tornar as ToolTip's mais consistentes com as dicas no estilo gráfico do Brisa
+ Mudança do 'editable' para 'true' por omissão no SpinBox
+ Correcção dos avisos de ligações no Menu.qml

### Solid

+ Activação incondicional da infra-estrutura do UDisks2 no FreeBSD

### Sonnet

+ Correcção do "Uso de QCharRef com um índice que aponta para fora do intervalo válido de uma QString"
+ Reposição do comportamento predefinido de detecção automática
+ Correcção da língua predefinida (erro 398245)

### Sindicância

+ Correcção de alguns cabeçalhos da licença BSD-2-Clause

### Realce de sintaxe

+ CMake: Actualizações para o CMake 3.18
+ Adição do realce de sintaxe para a linguagem Snort/Suricata
+ Adição da linguagem YARA
+ remoção do JSON binário obsoleto em detrimento do CBOR
+ JavaScript/TypeScript: realce das marcas nos modelos

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
