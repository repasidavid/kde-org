---
aliases:
- ../../kde-frameworks-5.13.0
date: 2015-08-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Novos frameworks

- KFileMetadata: Biblioteca de extração e metadados de arquivos
- Baloo: Framework para pesquisa e indexação de arquivos

### Alterações que afetam todos os frameworks

- A versão mínima necessária do Qt passou de 5.2 para 5.3
- O resultado de depuração foi migrado para um resultado por categoria, para gerar menos ruído por padrão
- A documentação em Docbook foi revista e atualizada

### Integração do Framework

- Correção da falha na caixa de diálogo de arquivos com apenas pastas
- Não confiar em options()-&gt;initialDirectory() para o Qt &lt; 5.4

### Ferramentas Doxygen do KDE

- Adição de páginas de manual para scripts do <i>kapidox</i> e atualização das informações do mantenedor no setup.py

### KBookmarks

- KBookmarkManager: Uso do o KDirWatch em vez do QFileSystemWatcher para detectar a criação do arquivo <i>user-places.xbel</i>.

### KCompletion

- Correções de HiDPI para o KLineEdit/KComboBox
- KLineEdit: Não permitir ao usuário excluir o texto quando a linha de edição for somente para leitura

### KConfig

- Não recomendar o uso de APIs obsoletas
- Não gerar código obsoleto

### KCoreAddons

- Adição do Kdelibs4Migration::kdeHome() para casos não cobertos pelos recursos
- Correção de aviso no tr()
- Correção da compilação do KCoreAddons no Clang+ARM

### KDBusAddons

- KDBusService: Documentação de como elevar a janela ativa no Activate()

### KDeclarative

- Correção da chamada obsoleta KRun::run
- Mesmo comportamento do MouseArea para associar as coordenadas dos eventos filhos filtrados
- Detectar a criação do ícone de rosto
- Não atualizar a janela inteira ao renderizar o plotador (erro <a href='https://bugs.kde.org/show_bug.cgi?id=348385'>348385</a>)
- Adição da propriedade de contexto <i>userPaths</i>
- Não bloquear com QIconItem vazio

### KDELibs 4 Support

- O <i>kconfig_compiler_kf5</i> passou para a <i>libexec</i> - usar o <i>kreadconfig5</i> em vez do teste <i>findExe</i>
- Documentação dos substitutos (sub-ótimos) do KApplication::disableSessionManagement

### KDocTools

- Alteração da frase dos relatórios de erros, confirmada por 'dfaure'
- Adaptação do <i>user.entities</i> alemão de acordo com o <i>en/user.entities</i>
- Atualização do <i>general.entities</i>: Mudança da formatação dos frameworks + Plasma do aplicativo para o nome do produto
- Atualização do <i>en/user.entities</i>
- Atualização dos modelos Docbook e das páginas de manual
- Uso do CMAKE_MODULE_PATH no cmake_install.cmake
- ERRO: <a href='https://bugs.kde.org/show_bug.cgi?id=350799'>350799</a>
- Atualização do arquivo general.entities
- Pesquisa dos módulos de Perl obrigatórios.
- <i>Namespace</i>, uma macro auxiliar no arquivo de macros instalado.
- Adaptação das traduções de nomes de chaves para traduções padrão oferecidas pelo Termcat

### KEmoticons

- Instalação do tema Breeze
- Kemoticons: Uso dos emoticons do Breeze em vez do Glass
- Pacote de emoticons Breeze criado por Uri Herrera

### KHTML

- Permitir o uso do KHTML sem pesquisar as dependências privadas

### KIconThemes

- Remoção de alocações de strings temporárias.
- Remoção do item de depuração da árvore Theme

### KIdleTime

- Instalação dos arquivos de cabeçalho privados dos plugins da plataforma.

### KIO

- Eliminação de interfaces desnecessárias do QUrl

### KItemModels

- Novo proxy: KExtraColumnsProxyModel, que permite adicionar colunas a um modelo existente.

### KNotification

- Correção da posição Y inicial para mensagens popups alternativas
- Redução das dependências e passagem para o Tier 2
- Captura de itens de notificação desconhecidos (eliminação de ponteiros nulos) (erro <a href='https://bugs.kde.org/show_bug.cgi?id=348414'>348414</a>)
- Remoção de uma mensagem de aviso inútil

### Package Framework

- Colocar os subtítulos como subtítulos ;)
- kpackagetool: Correção da entrada de texto não-latino para a saída padrão (stdout)

### KPeople

- Adição do AllPhoneNumbersProperty
- O PersonsSortFilterProxyModel pode agora ser usado no QML

### Kross

- krosscore: Instalação do cabeçalho CamelCase "KrossConfig"
- Correção dos testes de Python2 para executarem com o PyQt5

### KService

- Correção do kbuildsycoca --global
- KToolInvocation::invokeMailer: Correção dos anexos quando existirem múltiplos

### KTextEditor

- Guarda do nível de registro padrão para o Qt &lt; 5.4.0, correção dos nomes de categorias no registro
- Adição de realce de sintaxe para o Xonotic (erro <a href='https://bugs.kde.org/show_bug.cgi?id=342265'>342265</a>)
- Adição de realce de sintaxe para o Groovy (erro <a href='https://bugs.kde.org/show_bug.cgi?id=329320'>329320</a>)
- Atualização do realce de sintaxe para J (erro <a href='https://bugs.kde.org/show_bug.cgi?id=346386'>346386</a>)
- Permite compilar com o MSVC2015
- Menor uso do carregador de ícones, correção de mais ícones "pixelizados"
- Ativação/desativação do botão 'Procurar tudo' com as mudanças de padrão
- Melhorias na barra de pesquisa e substituição
- Remoção de regra inútil no modo de energia
- Barra de pesquisa mais fina
- vi: Correção de processamento incorreto da opção <i>markType01</i>
- Uso da qualificação correta para chamar o método de base
- Remoção de verificações, o QMetaObject::invokeMethod já se protege contra isso
- Correção de problemas com o HiDPI nos seletores de cor
- Limpeza de código: O QMetaObject::invokeMethod está seguro contra ponteiros nulos.
- Mais comentários
- Alteração da forma como as interface se protegem contra ponteiros nulos
- Por padrão, mostrar apenas avisos ou problemas mais graves
- Remoção de tarefas antigas
- Uso do QVarLengthArray para salvar a iteração temporária do QVector
- Mover o truque para as legendas dos grupos de recuo durante a construção
- Correção de diversos problemas com o KateCompletionModel no modo em árvore
- Correção do desenho problemático de modelos, que se baseava no comportamento do Qt 4
- Seguir as regras do <i>umask</i> ao salvar um novo arquivo (erro <a href='https://bugs.kde.org/show_bug.cgi?id=343158'>343158</a>)
- Adição do realce de sintaxe para o Meson
- Dado que o Varnish 4.x introduz diversas mudanças na sintaxe face ao Varnish 3.x, foram criados ficheiros de realce de sintaxe separados para o Varnish 4 (varnish4.xml,  varnishtest4.xml).
- Correção de problemas com o HiDPI
- Modo VI: Não falhar se o comando &lt;c-e&gt; for executado no fim de um documento (erro <a href='https://bugs.kde.org/show_bug.cgi?id=350299'>350299</a>)
- Suporte para strings multilinhas no QML
- Correção da sintaxe do arquivo 'oors.xml'
- Adição do realce de sintaxe do CartoCSS criado por Lukas Sommer (erro <a href='https://bugs.kde.org/show_bug.cgi?id=340756'>340756</a>)
- Correção do realce de sintaxe de ponto flutuante com uso do <i>Float</i> incorporado, como na linguagem C (erro <a href='https://bugs.kde.org/show_bug.cgi?id=348843'>348843</a>)
- As direções divididas foram revertidas (erro <a href='https://bugs.kde.org/show_bug.cgi?id=348845'>348845</a>)
- Erro 348317 - [MODIFICAÇÃO] o realce de sintaxe do Katepart deverá reconhecer as sequências de escape do JavaScript (erro 348317)
- Adição de *.cljs (erro <a href='https://bugs.kde.org/show_bug.cgi?id=349844'>349844</a>)
- Atualização do arquivo de realce de sintaxe do GLSL
- Correção das cores padrão para ser mais fácil distingui-las

### KTextWidgets

- Exclusão do realce de sintaxe antigo

### KWallet Framework

- Correção da compilação para Windows
- Apresentação de um aviso com o código de erro quando falhar a abertura da carteira pelo PAM
- Devolução do erro da infraestrutura, em vez de -1, quando falhar a abertura de uma carteira
- Alteração da infraestrutura "cifra desconhecida" para um código de erro negativo
- Monitoramento do PAM_KWALLET5_LOGIN no KWallet5
- Correção da falha quando a verificação MigrationAgent::isEmptyOldWallet() falhar
- O KWallet pode agora ser desbloqueado pelo PAM, usando o módulo <i>kwallet-pam</i>

### KWidgetsAddons

- Nova API que recebe parâmetros QIcon para definir os ícones na barra de abas
- KCharSelect: Correção da categoria Unicode e o uso do <i>boundingRect</i> para cálculo da largura
- KCharSelect: Correção da largura da célula para se ajustar ao conteúdo
- As margens do KMultiTabBar agora são corretamente mostrados em telas HiDPI
- KRuler: O método não-implementado KRuler::setFrameStyle() é agora obsoleto, limpeza de comentários
- KEditListWidget: Remoção da margem, para melhor alinhamento com outros widgets

### KWindowSystem

- Reforço da leitura de dados do NETWM (erro <a href='https://bugs.kde.org/show_bug.cgi?id=350173'>350173</a>)
- Proteção contra versões do Qt mais antigas, como no <i>kio-http</i>
- Os arquivos de cabeçalho privados dos plugins da plataforma são agora instalados
- Partes do código específicas da plataforma carregadas como plugins

### KXMLGUI

- Correção do comportamento do método KShortcutsEditorPrivate::importConfiguration

### Plasma Framework

- O uso do gesto de redução pode agora alternar entre os diferentes níveis de zoom do calendário
- Comentário sobre a duplicação de código na janela de ícones
- A cor do relevo da barra deslizante era fixo, mas foi modificada para usar o esquema de cores
- Utilização do QBENCHMARK em vez de um requisito fixo para desempenho da máquina
- A navegação do calendário foi significativamente melhorada, oferecendo uma visualização anual e por décadas
- O PlasmaCore.Dialog agora tem uma propriedade <i>opacity</i>
- Reserva de algum espaço para os botões de opção
- Não mostrar o plano de fundo circular se existir um menu
- Adição da definição X-Plasma-NotificationAreaCategory
- Definição das notificações e do OSD para mostrar em todas as áreas de trabalho
- Apresentação de um aviso útil quando não é possível obter um KPluginInfo válido
- Correção de uma potencial recorrência infinita no PlatformStatus::findLookAndFeelPackage()
- Renomeação de software-updates.svgz para software.svgz

### Sonnet

- Adição de código do CMake para ativar a compilação do plugin Voikko
- Implementação da <i>factory</i> Sonnet::Client para os verificadores ortográficos Voikko.
- Implementação de um verificador ortográfico baseado no Voikko (Sonnet::SpellerPlugin)

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
