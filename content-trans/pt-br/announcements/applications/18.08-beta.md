---
aliases:
- ../announce-applications-18.08-beta
date: 2018-07-20
description: O KDE Lança as Aplicações do KDE 18.08 Beta.
layout: application
release: applications-18.07.80
title: O KDE Lança a Primeira Versão 18.08 Beta das Aplicações
---
20 de julho de 2018. Hoje o KDE disponibilizou o beta da nova versão do KDE Applications. Com as dependências e funcionalidades estabilizadas, o foco das equipes do KDE agora é a correção de erros e pequenos ajustes.

Veja mais informações nas <a href='https://community.kde.org/Applications/18.08_Release_Notes'>notas de lançamento da comunidade</a> sobre novos pacotes e problemas conhecidos. Será disponibilizado um anúncio mais completo para a versão final

As versões Aplicações do KDE 18.08 precisam de testes aprofundados para manter e melhorar a qualidade e a experiência do utilizador. Os utilizadores actuais são críticos para manter a alta qualidade do KDE, dado que os programadores não podem simplesmente testar todas as configurações possíveis. Contamos consigo para nos ajudar a encontrar erros antecipadamente, para que possam ser rectificados antes da versão final. Por favor, pense em juntar-se à equipa, instalando a versão beta e <a href='https://bugs.kde.org/'>comunicando todos os erros encontrados</a>.
