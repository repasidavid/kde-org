---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE wydało Aplikacje KDE 16.04.1
layout: application
title: KDE wydało Aplikacje KDE 16.04.1
version: 16.04.1
---
10 maja 2016. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../16.04.0'>Aplikacji KDE 16.04</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 25 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdepim, ark, kate, dolphin, kdenlive, lokalize, spectacle, oraz inne.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.20.
