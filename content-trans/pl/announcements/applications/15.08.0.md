---
aliases:
- ../announce-applications-15.08.0
changelog: true
date: 2015-08-19
description: KDE wydało Aplikacje 15.08.0.
layout: application
release: applications-15.08.0
title: KDE wydało Aplikacje KDE 15.08.0
version: 15.08.0
---
{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/dolphin15.08_0.png" alt=`Dolphin zyskał nowy wygląd - od teraz jest oparty na Szkieletach KDE 5` class="text-center" width="600px" caption=`Dolphin zyskał nowy wygląd - od teraz jest oparty na Szkieletach KDE 5`>}}

19 sierpień 2015. Dzisiaj KDE wydało Aplikacje KDE 15.08.

Z tym wydaniem, ponad 107 aplikacje zostały przeniesione na <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Szkielety KDE 5</a>. Zespół stara się zapewniać najlepszą jakość na twoim pulpicie dla tych aplikacji, więc liczymy na twoją informację zwrotną.

W tym wydaniu dodano kilka nowych aplikacji opartych na Szkieletach KDE 5, takich jak <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, <a href='https://www.kde.org/applications/office/kontact/'>pakiet Kontact</a>, <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, <a href='https://games.kde.org/game.php?game=picmi'>Picmi</a>, itp.

### Przegląd technologiczny pakietu Kontact

Przez kilka minionych miesięcy, zespół ZIO KDE włożył dużo pracy w przeniesienie Kontactu na Qt 5 i Szkielety KDE 5. Dodatkowo poprawiono wydajność dostępu do danych poprzez zoptymalizowaną warstwę kontaktową.
Zespół ZIO KDE poleruje teraz pakiet Kontact i czeka na twoją informację zwrotną.Po więcej informacji na temat tego co zmieniło się w ZIO KDE zajrzyj na <a href='http://www.aegiap.eu/kdeblog/'>bloga Laurent Montels</a>.

### Kdenlive oraz Okular

Wydanie Kdenlive zawiera wiele poprawek w pomocniku tworzenia DVD, a także dużą liczbę ogólnych poprawek i zmian takich jak integracja przepisanego kodu w większych częściach. Więcej informacji o zmianach w Kdenlive można znaleźć w <a href='https://kdenlive.org/discover/15.08.0'>szczegółowym dzienniku zmian</a>. Okular od teraz obsługuje przejścia w postaci wyłaniania/zanikania w trybie prezentacji.

{{<figure src="https://dot.kde.org/sites/dot.kde.org/files/ksudoku_small_0.png" alt=`KSudoku z puzzlami znakowymi` class="text-center" width="600px" caption=`KSudoku z puzzlami znakowymi`>}}

### Dolphin, Edu i Gry

Dolphin także został przeniesiony na Szkielety KDE 5. W Marble ulepszono obsługę <a href='https://en.wikipedia.org/wiki/Universal_Transverse_Mercator_coordinate_system'>UTM</a>, przypisów, edytowania i nakładek KML.

W Arku wdrożono wiele nowych rzeczy, a także usunięto wiele małych błędów. W Kstars też wdrożono wiele nowych rzeczy, wliczając w to poprawę płaskiego algorytmu ADU, sprawdzanie wartości granicznych, zapisywanie, odwrócenie południka, odchylenie naprowadzania, granicę samo-ogniskowania HFR w pliku sekwencji oraz dodano obsługę szybkości obrotu teleskopu wraz z wyparkowywaniem. KSudoku dostało: nowy graficzny interfejs użytkownika i silnik do rozwiązywania Mathdoku oraz Zabójczego Sudoku, nowy algorytm rozwiązujący oparty na Tańczących powiązaniach Donalda Knuth'a.

### Inne wydania

Równolegle zostanie wydana Plazma 4 w swojej wersji długoterminowej po raz ostatni jako wersja 4.11.22.
