---
aliases:
- ../announce-applications-16.08.3
changelog: true
date: 2016-11-10
description: KDE wydało Aplikacje KDE 16.08.3
layout: application
title: KDE wydało Aplikacje KDE 16.08.3
version: 16.08.3
---
10 listopada 2016. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../16.08.0'>Aplikacji KDE 16.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 20 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdepim, ark, okteta, umbrello, kmines oraz innych.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.26.
