---
date: 2013-08-14
hidden: true
title: Przestrzenie Pracy Plazmy 4.11 kontynuują ulepszanie odczuć użytkownika
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`Przestrzenie Pracy Plazmy KDE 4.11` width="600px" >}}

W wydaniu 4.11 Przestrzeni Roboczych Plazmy, pasek zadań – jeden z najbardziej używanych elementów interfejsu Plazmy –  <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>został przeportowany na QtQuick</a>. Nowy pasek zadań, przy zachowaniu wyglądu i funkcjonalności jego starszego poprzednika, okazuje się bardziej spójny i płynny w zachowaniu. Port rozwiązał także pewną liczbę długo zalegających błędów. Element interfejsu baterii (który poprzednio mógł dostosować jasność ekranu) teraz wspiera również jasność klawiatury i może obsłużyć wiele baterii w urządzeniach peryferyjnych, takich jak bezprzewodowa mysz i klawiatura. Pokazuje ładunek baterii dla każdego urządzenia i ostrzega, gdy któryś będzie na niskim poziomie. Menu wysuwane pokazuje teraz ostatnio zainstalowane aplikacje z kilku dni. Ostatnie, ale nie mniej ważne, okna wysuwne powiadomień mają teraz przycisk konfiguracji, za pomocą którego można łatwo zmienić ustawienia dla jednego szczególnego rodzaju powiadomień.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Ulepszona obsługa powiadomień` width="600px" >}}

KMix, mikser dźwięku KDE, został poddany znacznej pracy nakierowanej na wydajność i stabilność jak również na <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>pełną obsługę sterowania odtwarzaczem mediów</a> opartą o standard MPRIS2. 

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`Przeprojektowany aplet baterii w akcji` width="600px" >}}

## Menadżer okien KWin i kompozytor

Nasz zarządca okien, KWin, po raz kolejny otrzymał znaczące uaktualnienie, zostawiając przestarzałą technologię w tyle i integrując protokół porozumiewania się 'XCB'. Skutkuje to płynniejszym, szybszym zarządzaniem oknami. Została wprowadzona obsługa dla OpenGL 3.1 oraz OpenGL ES 3.0. Wydanie to integruje pierwszą wersję obsługi, jeszcze eksperymentalnej, dla następcy X11, a mianowicie Wayland. Pozwala to na użycie KWin z X11 na wierzchu stosu Wayland. Po więcej informacji na temat tego jak używać eksperymentalnego trybu, przeczytaj <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>ten post</a>. Interfejs pisania skryptów KWin doświadczył znacznych ulepszeń, teraz wspierając konfigurację interfejsu użytkownika, nowe animacje i efekty graficzne oraz wiele małych poprawek. Wydanie to odznacza się lepszą świadomością o wielu ekranach (włączając w to opcję świecenia się krawędzi dla 'gorących narożników'), ulepszone szybkie kafelkowanie (z konfigurowalnymi obszarami kafelków) i jak zazwyczaj dużą liczbę poprawionych błędów i optymalizacji. Zajrzyj <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>tutaj</a> i <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>tutaj</a> po więcej szczegółów.

## Obsługa monitora i skróty sieciowe

Konfiguracja monitora w Ustawieniach Systemowych została <a href='http://www.afiestas.org/kscreen-1-0-released/'>zastąpiona nowym narzędziem KEkran</a>. KEkran zapewnia bardziej inteligentne wsparcie dla wielu monitorów w Przestrzeniach Roboczych Plazmy, samoczynnie konfigurując nowe ekrany i zapamiętując ustawienia dla monitorów ręcznie skonfigurowanych. Wspiera intuicyjny, wizualnie zorientowany interfejs i obsługuje zmianę kolejności monitorów poprzez szybkie przeciąganie i upuszczanie.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`Nowa obsługa monitora KEkran` width="600px" >}}

Skróty sieciowe, najłatwiejsza droga na szybkie znalezienie tego czego poszukujesz w sieci, została uporządkowana i ulepszona. Wiele zostało uaktualnionych do użycia bezpiecznie zaszyfrowanych połączeń (TLS/SSL), zostały dodane nowe skróty sieciowe, a kilka nieaktualnych zostało usuniętych. Proces dodawania swojego własnego skrótu sieciowego również został ulepszony. Więcej szczegółów można znaleźć <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>tutaj</a>.

Wydanie to oznacza koniec Przestrzeni Roboczych Plazmy 1, części szeregu funkcji KDE SC 4. Aby ułatwić przejście do następnej generacji, wydanie to będzie wspierane przez co najmniej dwa lata. Od teraz praca nad rozwojem funkcji zostanie przesunięta do Przestrzeni Roboczych Plazmy 2, poprawienie wydajności i błędów będzie się koncentrować w serii 4.11.

#### Instalowanie Plazmy

Oprogramowanie KDE, włączając w to wszystkie jego biblioteki i aplikacje, jest dostępne na warunkach Wolnego Oprogramowania. Oprogramowanie KDE działa na rozmaitych konfiguracjach sprzętowych i architekturach procesora, takich jak ARM i x86, systemach operacyjnych oraz działa z każdym rodzajem menadżera okien czy otoczeniem pulpitu. Poza Linuksem i innymi, opartymi o UNIKSA, systemami operacyjnymi można też znaleźć wersję na Microsoft Windows na stronie <a href='http://windows.kde.org'>Oprogramowanie KDE na Windows</a> i wersję dla Apple Mac OS X na stronie <a href='http://mac.kde.org/'>Oprogramowanie KDE na Mac</a>. Eksperymentalne wydania aplikacji KDE, dla różnych platform przenośnych, takich jak MeeGo, MS Windows Mobile i Symbian można znaleźć w sieci, aczkolwiek są one teraz niewspierane. <a href='http://plasma-active.org'>Plasma Active</a> jest środowiskiem użytkownika dla szerokiego zakresu urządzeń, takich jak komputery typu tablet i inne przenośne.

Oprogramowanie KDE można pobrać w postaci źródła i różnych formatów binarnych z <a href='http://download.kde.org/stable/Szkieletów KDE %1'> download.kde.org</a>, ale można je także uzyskać na  <a href='/download'>CD-ROM</a>lub w każdym <a href='/distributions'>znaczącym systemie operacyjnym GNU/Linuks i UNIX</a> obecnie dostępnym.

##### Pakiety

Niektórzy wydawcy Linux/UNIX OS uprzejmie dostarczyli pakiety binarne Szkieletów KDE %[1] dla niektórych wersji ich dystrybucji, a w niektórych przypadkach dokonali tego wolontariusze społeczności. <br />

##### Położenie pakietów

Po bieżącą listę dostępnych pakietów binarnych, o których został poinformowany Zespół wydawniczy KDE, proszę zajrzyj na stronę <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'> Wiki Społeczności</a>.

Pełny kod źródłowy dla Szkieletów KDE %[1] można <a href='/info/4/4.11.0'> pobrać za darmo</a>. Instrukcje dotyczące kompilowania i wgrywania oprogramowania KDE Szkieletów KDE %[1] są dostępne na <a href='/info/4/4.11.0#binary'>Stronie informacyjnej Szkieletów KDE %[1]</a>.

#### Wymagania systemowe

Aby uzyskać najwięcej z tych wydań, zalecamy użycie najnowszej wersji Qt, takiej jak 4.8.4. Jest to potrzebne, aby zapewnić odczucie stabilności i wydajności, jako iż pewne ulepszenia poczynione w oprogramowaniu KDE, w rzeczywistości zostały dokonane w strukturze programistycznej Qt.<br />Aby w pełni wykorzystać możliwości oprogramowania KDE, zalecamy także użycie najnowszych sterowników graficznych dla twojego systemu, jako iż może to znacznie polepszyć odczucia użytkownika, zarówno w opcjonalnej funkcjonalności, jak i w ogólnej wydajności i stabilności.

## Ogłoszone również dzisiaj:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="Aplikacje KDE w wersji 4.11"/> Aplikacje KDE 4.11 są olbrzymim krokiem naprzód w zarządzaniu informacją osobistą i dostarczają wiele ogólnych ulepszeń</a>

Wydanie to oznacza znaczne ulepszenia w stosie ZIO KDE, z dużo lepszą wydajnością i wiele nowych funkcji. Kate poprawia produktywność programistów Pythona oraz Javascript dzięki nowym wtyczkom, Dolphin stał się szybszy, a aplikacje edukacyjne niosą ze sobą wiele nowych funkcji.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="KDE Development Platform w wersji 4.11"/>  Platforma KDE 4.11 dostarcza wiele ogólnych ulepszeń </a>

Wydanie Platformy KDE 4.11 kontynuuje prace skupione na stabilności. Dla naszego przyszłego wydania Frameworks KDE 5.0, implementowane są nowe funkcje, lecz dla stabilnego wydania daliśmy radę upchnąć optymalizacje dla naszego modułu Nepomuka.
