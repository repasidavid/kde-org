---
aliases:
- ../../kde-frameworks-5.41.0
date: 2017-12-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Redução e remodelação do 'KIO slave' 'tags' do Baloo (erro 340099)

### BluezQt

- Não criar fugas de descritores do 'rfkill' (erro 386886)

### Ícones do Brisa

- Adição de tamanhos de ícones em falta (erro 384473)
- adição de ícones de instalação e desinstalação do Discover

### Módulos Extra do CMake

- Adição da marca de descrição aos ficheiros do 'pkgconfig' gerados
- ecm_add_test: Uso do separador de pastas adequado no Windows
- Adição do FindSasl2.cmake ao ECM
- Só passar o item ARGS ao criar Makefile's
- Adição do FindGLIB2.cmake e do FindPulseAudio.cmake
- ECMAddTests: configuração do QT_PLUGIN_PATH para que se possam encontrar os 'plugins' compilados localmente
- KDECMakeSettings: mais documentação sobre o formato da pasta de compilação

### Integração da Plataforma

- Suporte para a transferência da 2a ou 3a hiperligação de um produto do KNS (erro 385429)

### KActivitiesStats

- Início da correcção do libKActivitiesStats.pc: (erro 386933)

### KActivities

- Correcção de erro de sincronização que lançava o 'kactivitymanagerd' várias vezes

### KAuth

- Permitir compilar apenas o gerador do código do 'kauth-policy-gen'
- Adição de uma nota sobre a invocação do utilitário em aplicações multi-tarefa

### KBookmarks

- Não mostrar a acção de edição de favoritos se o 'keditbookmarks' não estiver instalado
- Migração do KAuthorized::authorizeKAction para o authorizeAction

### KCMUtils

- navegação com teclado de dentro e para fora dos KCM's em QML

### KCompletion

- Não estoirar ao definir o novo campo de edição numa lista editável
- KComboBox: Regressar rapidamente ao colocar o valor anterior de volta num campo editável
- KComboBox: Reutilizar o objecto de completação existente no campo de edição de novas linhas

### KConfig

- Não procurar sempre o /etc/kderc a toda a hora

### KConfigWidgets

- Actualizar as cores predefinidas para corresponder às novas no D7424

### KCoreAddons

- Validação de entrada dos SubJobs
- Avisar por erros ao processar os ficheiros JSON
- Instalar as definições de tipos MIME dos ficheiros kcfg/kcfgc/ui.rc/knotify &amp; qrc
- Adição de uma nova função para medir o comprimento do texto
- Correcção de erro no KAutoSave num ficheiro com espaços no nome

### KDeclarative

- Capacidade de compilar em Windows
- capacidade de compilar com o QT_NO_CAST_FROM_ASCII/QT_NO_CAST_FROM_BYTEARRAY
- [MouseEventListener] Permitir a aceitação do evento do rato
- usar um único motor de QML

### KDED

- kded: remoção das chamadas de DBus ao KSplash

### KDocTools

- Actualização das traduções para Português do Brasil
- Actualização das traduções para Russo
- Actualização das traduções para Russo
- actualização do 'customization/xsl/ru.xml' (falta o 'nav-home')

### KEmoticons

- KEmoticons: migração dos 'plugins' para JSON e adição do suporte para o carregamento com o KPluginMetaData
- Não criar fugas de símbolos das classes 'pimpl', protegendo com o Q_DECL_HIDDEN

### KFileMetaData

- O 'usermetadatawritertest' precisa do Taglib
- Se o valor da propriedade for nulo, remove o atributo 'user.xdg.tag' (erro 376117)
- Abrir os ficheiros no extractor da TagLib apenas para leitura

### KGlobalAccel

- Agrupar algumas chamadas de DBus bloqueantes
- kglobalacceld: Evitar a leitura de um carregador de ícones sem qualquer razão
- gerar textos de atalho correctos

### KIO

- KUriFilter: retirar os 'plugins' duplicados na filtragem
- KUriFilter: simplificação das estruturas de dados, correcção de fuga de memória
- [CopyJob] Não começar tudo de novo após a remoção de um ficheiro
- Correcção da criação de uma pasta com o KNewFileMenu+KIO::mkpath no Qt 5.9.3+ (erro 387073)
- Criação de uma função auxiliar 'KFilePlacesModel::movePlace'
- Exposição do papel 'iconName' do KFilePlacesModel
- KFilePlacesModel: Evitar um sinal 'dataChanged' desnecessário
- Devolução de um objecto de favorito válido para qualquer elemento no KFilePlacesModel
- Criação de uma função 'KFilePlacesModel::refresh'
- Criação da função estática 'KFilePlacesModel::convertedUrl'
- KFilePlaces: Criação de uma secção 'remote'
- KFilePlaces: Adição de uma secção para dispositivos removíveis
- Adição dos URL's do Baloo ao modelo dos locais
- Correcção do KIO::mkpath com o qtbase 5.10 beta 4
- [KDirModel] Emissão de uma alteração para o HasJobRole quando as tarefas mudam
- Correcção do texto "Opções avançadas" &gt; "Opções do terminal"

### Kirigami

- Desvio da barra de deslocamento pelo tamanho do cabeçalho (erro 387098)
- margem inferior com base na presença do botão de acção
- não assumir que o applicationWidnow() estivesse disponível
- Não notificar as alterações dos valores, caso esteja ainda no construtor
- Substituição do nome da biblioteca no código
- suporte para cores em mais locais
- coloração dos ícones nas barras de ferramentas, se necessário
- considerar cores nos ícones nos botões de acções principais
- início de uma propriedade agrupada "icon"

### KNewStuff

- Reversão da funcionalidade "Detach before setting the d pointer" (separar antes da definição do ponteiro 'd'" (erro 386156)
- não instalar a ferramenta de desenvolvimento para agregar ficheiros desktop
- [knewstuff] Não criar fugas do ImageLoader em caso de erro

### Plataforma KPackage

- Criar os textos de forma adequada na plataforma do KPackage
- Não tentar gerar o ficheiro 'metadata.json' se não existir o 'metadata.desktop'
- correcção da 'cache' do kpluginindex
- Melhoria na apresentação dos erros

### KTextEditor

- Correcção dos comandos de 'buffer' no Modo VI
- evitar uma ampliação acidental

### KUnitConversion

- migração do QDom para o QXmlStreamReader
- Uso do HTTPS para transferir as taxas de câmbio monetárias

### KWayland

- Exposição do 'wl_display_set_global_filter' como um método virtual
- Correcção do 'kwayland-testXdgShellV6'
- Adição do suporte para o 'zwp_idle_inhibit_manager_v1' (erro 385956)
- [servidor] Suporte de inibição da IdleInterface

### KWidgetsAddons

- Evitar uma 'passworddialog' (janela de senhas) inconsistente
- Definição da sugestão 'enable_blur_behind' a pedido
- KPageListView: Actualização da largura em caso de mudança do tipo de letra

### KWindowSystem

- [KWindowEffectsPrivateX11] Adição da chamada reserve()

### KXMLGUI

- Correcção da tradução do nome da barra de ferramentas, caso tenha contexto de 'i18n'

### Plataforma do Plasma

- A directiva #warning não é universal e, em particular, NÃO é suportada pelo MSVC
- [IconItem] Uso do ItemSceneHasChanged em vez de se ligar ao 'windowChanged'
- [Item de Ícones] Emitir explicitamente o 'overlaysChanged' no método de alteração em vez de se ligar a ele
- [Dialog] Uso do KWindowSystem::isPlatformX11()
- Redução da quantidade de mudanças inúteis de propriedades no ColorScope
- [Item de Ícone] Emitir o 'validChanged' apenas se tiver alterado de facto
- Eliminação de indicadores de deslocamento se o 'flickable' for um ListView com orientação conhecida
- [AppletInterface] Emissão de sinais de mudança para o 'configurationRequired' e o '-Reason'
- Uso do setSize() em vez do 'setProperty' para a largura e altura
- Correcção de um problema onde o menu dos PlasmaComponents iria aparecer com cantos quebrados (erro 381799)
- Correcção de um problema onde os menus de contexto iriam aparecer com cantos quebrados (erro 381799)
- Documentação da API: adição de nota de descontinuação encontrada no histórico do Git
- Sincronização do componente com o do Kirigami
- Pesquisa de todos os componentes do KF5 como tal, em vez de propriedades separadas
- Redução de emissões de sinais inúteis (erro 382233)
- Adição de sinais a indicar se foi adicionado ou removido um ecrã
- instalação de elementos do Switch
- Não se basear em inclusões de inclusões
- Optimização dos nomes dos papéis do SortFilterModel
- Remoção do DataModel::roleNameToId

### Prisão

- Adição do gerador de código Aztec

### QQC2StyleBridge

- detecção da versão do QQC2 na altura da compilação (erro 386289)
- por omissão, manter o fundo invisível
- adição de um fundo ao ScrollView

### Solid

- UDevManager::devicesFromQuery mais rápido

### Sonnet

- Possibilidade de compilação multi-plataforma do Sonnet

### Realce de Sintaxe

- Adição do PKGUILD à sintaxe do Bash
- JavaScript: inclusão de tipos MIME padrão
- debchangelog: adição do Bionic Beaver
- Actualização do ficheiro de sintaxe SQL (Oracle) (erro 386221)
- SQL: passagem da detecção de comentários para antes dos operadores
- crk.xml: adição da linha de cabeçalho &lt;?xml&gt;

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
