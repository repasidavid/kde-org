---
aliases:
- ../../kde-frameworks-5.31.0
date: 2017-02-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Geral

Muitos módulos têm agora interfaces em Python.

### Attica

- adição do suporte para o 'display_name' nas categorias

### Ícones do Brisa

- demasiadas alterações de ícones para as enumerar aqui

### Módulos Extra do CMake

- Activação do -Wsuggest-override para o g++ &gt;= 5.0.0
- Passagem do -fno-operator-names se suportado
- ecm_add_app_icon : ignorar os ficheiros SVG de forma silenciosa se não suportados
- Interfaces: Muitas correcções e melhorias

### Integração da Plataforma

- Suporte a algumas das questões do KNSCore no uso das notificações

### KArchive

- Correcção do KCompressionDevice (pesquisa) para funcionar com o Qt &gt;= 5.7

### KAuth

- Actualização da maioria dos exemplo, eliminando os desactualizados

### KConfig

- Correcção da compilação em Windows: não ligar o 'kentrymaptest' ao KConfigCore

### KConfigWidgets

- Não fazer nada no ShowMenubarActionFilter::updateAction se não existirem menus

### KCoreAddons

- Correcção do erro 363427 - caracteres inseguros processados incorrectamente como parte do URL (erro 363427)
- kformat: Possibilitar a tradução adequada das datas relativas (erro 335106)
- KAboutData: Documentar que o endereço de e-mail do erro pode também ser um URL

### KDeclarative

- [IconDialog] Definição do grupo de ícones adequado
- [QuickViewSharedEngine] Uso do 'setSize' em vez do 'setWidth/setHeight'

### Suporte para a KDELibs 4

- Sincronização com o KDE4Defaults.cmake do 'kdelibs'
- Correcção de verificação do HAVE_TRUNC no Cmake

### KEmoticons

- KEmoticons: uso do DBus para notificar os processos em execução das alterações feitas no KCM
- KEmoticons: grandes melhorias na performance

### KIconThemes

- KIconEngine: Centrar o ícone no rectângulo pedido

### KIO

- Adição do KUrlRequester::setMimeTypeFilters
- Correcção do processamento da listagem de pastas num dado servidor de FTP (erro 375610)
- preservação do grupo/dono na cópia de ficheiros (erro 103331)
- KRun: descontinuação do runUrl() em detrimento do runUrl() com RunFlags
- kssl: Garantir que a pasta de certificados do utilizador foi criada antes de ser usada (erro 342958)

### KItemViews

- Aplicação imediata do filtro ao 'proxy'

### KNewStuff

- Possibilitar a adopção de recursos, principalmente para as configurações do sistema
- Não falhar ao mover para a pasta temporária durante a instalação
- Descontinuação da classe de segurança
- Não bloquear ao executar o comando de pós-instalação (erro 375287)
- [KNS] Ter em conta o tipo de distribuição
- Não perguntar se estamos a obter o ficheiro na pasta '/tmp'

### KNotification

- Adição de notificações de registo aos ficheiros (erro 363138)
- Marcação das notificações não-persistentes como transitórias
- Suporte das "acções predefinidas"

### Plataforma KPackage

- Não gerar o 'appdata' se estiver marcado como NoDisplay
- Correcção da listagem quando o local pedido é absoluto
- correcção do tratamento dos pacotes com uma pasta dentro deles (erro 374782)

### KTextEditor

- correcção do desenho do mini-mapa para ambientes HiDPI

### KWidgetsAddons

- Adição de métodos para esconder a acção de revelação da senha
- KToolTipWidget: não tirar a pertença ao elemento do conteúdo
- KToolTipWidget: esconder imediatamente se o conteúdo for destruído
- Correcção da sobreposição de foco no KCollapsibleGroupBox
- Correcção de aviso ao destruir um KPixmapSequenceWidget
- Instalar também os ficheiros de inclusão 'CamelCase' para as classes com ficheiros de inclusão multi-classes
- KFontRequester: Pesquisa da ocorrência mais próxima de um tipo de letra em falta (erro 286260)

### KWindowSystem

- Permitir que o Tab seja modificado pelo Shift (erro 368581)

### KXMLGUI

- Relatório de erros: Permitir um URL (não só um endereço de e-mail) para os relatórios personalizados
- Ignorar os atalho vazios na verificação de ambiguidade

### Plataforma do Plasma

- [Interface de Contentores] Não é necessário o values(), dado que o contains() procura pelas chaves
- Dialog: Esconder quando o foco muda para uma ConfigView com o hideOnWindowDeactivate
- [Menu do PlasmaComponents] Adição da propriedade 'maximumWidth'
- Ícone em falta quando se liga ao openvpn' com uma rede Bluetooth (erro 366165)
- Validação de que é apresentado o ListItem activo à passagem
- fazer com que todas as alturas no cabeçalho do calendário sejam iguais (erro 375318)
- correcção do estilo de cores no ícone de rede do Plasma (erro 373172)
- Mudança do 'wrapMode' para 'Text.WrapAnywhere' (erro 375141)
- actualização do ícone do KAlarm (erro 362631)
- encaminhar correctamente o estado das 'applets' para o contentor (erro 372062)
- Uso do KPlugin para carregar os 'plugins' do Calendário
- uso da cor de realce para o texto seleccionado (erro 374140)
- [Item de Ícone] Arredondamento do tamanho desejado para uma imagem
- a propriedade 'portrait' não é relevante quando não existe texto (erro 374815)
- Correcção das propriedades 'renderType' para os vários componentes

### Solid

- Correcção alternativa do erro da leitura de propriedades de DBus (erro 345871)
- Tratar da ausência de senha como Solid::UserCanceled

### Sonnet

- Adição do ficheiro de dados de trigramas Gregos
- Correcção de erro de segmentação na geração de trigramas e na exposição da constante MAXGRAMS no ficheiro de inclusão
- Pesquisa por um 'libhunspell.so' sem versões, o que deverá ser mais estável de futuro

### Realce de Sintaxe

- realce do C++: actualização para o Qt 5.8

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
