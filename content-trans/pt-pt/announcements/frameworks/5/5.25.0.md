---
aliases:
- ../../kde-frameworks-5.25.0
date: 2016-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Geral

- O Qt &gt;= 5.5 é agora obrigatório

### Attica

- Seguir os direccionamentos do HTTP

### Ícones do Brisa

- actualização do correio - ícones de 16px para os reconhecer melhor
- actualização dos ícones de estado do microfone e do áudio para terem o mesmo formato e tamanho
- Novo ícone da aplicação Configuração do Sistema
- adição de ícones simbólicos de estado do Gnome
- adição do suporte de ícones simbólicos do Gnome 3
- Adição de ícones para o Diaspora e o Vector, ver em phabricator.kde.org/M59
- Novos ícones para o Dolphin e o Gwenview
- os ícones meteorológicos são ícones de estado, não aplicacionais
- adição de algumas ligações ao 'xliff', graças ao 'gnastyle'
- adição de ícone para o Kig
- adição dos ícones de tipos MIME, do ícone do 'krdc' e de outras aplicações com o 'gnastyle'
- adição do ícone de tipo MIME para a adição de certificados (erro 365094)
- actualização dos ícones do Gimp, graças ao 'gnastyle' (erro 354370)
- o ícone de acção do globo agora não é um ficheiro ligado; usá-lo por favor no Digikam
- actualização dos ícones do Labplot de acordo com o e-mail 13.07. de Alexander Semke
- adição de ícones aplicacionais do 'gnastyle'
- adição de ícone do KRuler de Yuri Fabirovsky
- correcção dos ficheiros SVG inválidos, graças ao 'fuchs' (erro 365035)

### Módulos Extra do CMake

- Correcção da inclusão quando não existe o Qt5
- Adição de um método de contingência para o query_qmake() quando não está instalado nenhum Qt5
- Certificação de que o ECMGeneratePriFile.cmake se comporta como o resto do ECM
- Os dados do Appstream mudaram a sua localização preferida

### KActivities

- [KActivities-CLI] comandos para iniciar e parar uma actividade
- [KActivities-CLI] modificação e leitura do nome, ícone e descrição da actividade
- Adição de uma aplicação CLI (linha de comandos) para controlar as actividades
- Adição de programas para mudar para as actividades anteriores e seguintes
- O método para inserir no QFlatSet agora devolve o índice em conjunto com o iterador (erro 365610)
- Adição de funções do ZSH para parar e apagar as actividades não-actuais
- Adição da propriedade 'isCurrent' ao KActivities::Info
- Uso de iteradores constantes ao procurar por uma actividade

### Ferramentas de Doxygen do KDE

- Muitas melhorias na formatação do resultado
- O Mainpage.dox agora tem maior prioridade que o README.md

### KArchive

- Tratamento de diversas sequências do 'gzip' (erro 232843)
- Detecção de uma directoria como pasta, mesmo que o 'bit' das permissões esteja mal configurado (erro 364071)

### KBookmarks

- KBookmarkGroup::moveBookmark: correcção do valor devolvido quando o item já está na posição certa

### KConfig

- Adição do atalho-padrão para o DeleteFile e o RenameFile

### KConfigWidgets

- Adição da acção-padrão para o DeleteFile e o RenameFile
- A página de configuração agora tem barras de deslocamento quando for necessário (erro 362234)

### KCoreAddons

- Instalação e detecção no arranque das licenças conhecidas (correcção de regressão) (erro 353939)

### KDeclarative

- Emissão de facto do 'valueChanged'

### KFileMetaData

- Verificação do 'xattr' durante o passo de configuração, caso contrário a compilação poderá falhar (se o xattr.h estiver em falta)

### KGlobalAccel

- Uso do 'klauncher dbus' em vez do KRun (erro 366415)
- Lançamento de acções rápidas com o KGlobalAccel
- KGlobalAccel: Correcção de bloqueio à saída com o Windows

### KHTML

- Suporte da unidade de percentagem no raio dos contornos
- Remoção da versão com prefixo das propriedades 'background' (fundo) e 'border radius' (raio do contorno)
- Correcções na função curta de 4 valores
- Criação de objectos de texto, só se forem usados

### KIconThemes

- Grande melhoria na performance do 'makeCacheKey', dado que é um caminho crítico no código da pesquisa de ícones
- KIconLoader: redução do número de pesquisas no teste das alternativas
- KIconLoader: grande melhoria na velocidade para carregar os ícones indisponíveis
- Não limpar o campo de pesquisa ao mudar de categorias
- KIconEngine: Correcção do QIcon::hasThemeIcon que devolvia sempre 'true' (erro 365130)

### KInit

- Adaptação do KInit para o Mac OS X

### KIO

- Correcção do KIO::linkAs() para funcionar como documentado, i.e. falhar se o destino já existir
- Correcção do KIO::put("file:///local") para respeitar o 'umask' (erro 359581)
- Correcção do KIO::pasteActionText para um item de destino nulo e com um URL vazio
- Adição do suporte para a anulação da criação de ligações simbólicas
- Opção da GUI para configurar o MarkPartial global para os 'KIO slaves'
- Correcção do MaxCacheSize limitado a 99 KiB
- Adição de botões da área de transferência para a página de códigos de validação
- KNewFileMenu: correcção da cópia do ficheiro-modelo de um recurso incorporado (erro 359581)
- KNewFileMenu: Correcção da criação de atalhos para aplicações (erro 363673)
- KNewFileMenu: Correcção da sugestão de novo nome do ficheiro quando o mesmo já existe no ecrã
- KNewFileMenu: verificação se o fileCreated() é emitido também para os ficheiros 'desktop' das aplicações
- KNewFileMenu: correcção da criação de ligações simbólicas com um destino relativo
- KPropertiesDialog: simplificação do uso da área de botões, correcção do comportamento do Esc
- KProtocolInfo: enchimento da 'cache' para procurar os protocolos recém-instalados
- KIO::CopyJob: migração para o qCDebug (com a sua própria área, dado que este poderá ser bastante descritivo)
- KPropertiesDialog: adição de página de Códigos de Validação)
- Limpeza da localização do URL antes da inicialização do KUrlNavigator

### KItemModels

- KRearrangeColumnsProxyModel: correcção de validação no índice(0, 0) com um modelo vazio
- Correcção do KDescendantsProxyModel::setSourceModel(), que não limpava as 'caches' internas
- KRecursiveFilterProxyModel: correcção de invalidação do QSFPM, devido à filtragem do sinal 'rowsRemoved' (erro 349789)
- KExtraColumnsProxyModel: implementação do 'hasChildren()'

### KNotification

- Não mudar manualmente o item-pai do 'sublayout', o que silencia o aviso

### Plataforma de Pacotes

- Inferência do ParentApp a partir do 'plugin' PackageStructure
- Possibilidade de o kpackagetool5 gerar a informação do AppStream para os componentes do KPackage
- Possibilidade de carregamento do ficheiro 'metadata.json' do kpackagetool5

### Kross

- Remoção das dependências do KF5 não usadas

### KService

- applications.menu: remoção das referências às categorias não-usadas
- Actualizar sempre o processador do Trader a partir do código yacc/lex

### KTextEditor

- Não perguntar duas vezes para sobrepor um ficheiro nas janelas nativas
- adição da sintaxe do FASTQ

### KWayland

- [cliente] Uso de um QPointer para o 'enteredSurface' no Pointer
- Exposição do Geometry no PlasmaWindowModel
- Adição de um evento de geometria ao PlasmaWindow
- [src/server] Verificação se a superfície tem um recurso antes de enviar um evento de entrada do cursor
- Adição do suporte para o xdg-shell
- [servidor] Enviar uma limpeza adequada da selecção antes de o teclado ficar em primeiro plano
- [servidor] Tratar da situação sem XDG_RUNTIME_DIR de forma mais ordeira

### KWidgetsAddons

- [KCharSelect] Correcção do estoiro ao pesquisar sem um ficheiro de dados presente (erro 300521)
- [KCharSelect] Tratamento dos caracteres fora do BMP (erro 142625)
- [KCharSelect] Actualização do kcharselect-data para o Unicode 9.0.0 (erro 336360)
- KCollapsibleGroupBox: Paragem da animação se o destruidor estiver ainda em execução
- Actualização para a paleta do Brisa (sincronização do KColorScheme)

### KWindowSystem

- [xcb] Certificação de que o sinal 'compositingChanged' é emitido se o NETEventFilter for criado de novo (erro 362531)
- Adição de uma API de conveniência para consultar o sistema/plataforma de janelas usado pelo Qt

### KXMLGUI

- Correcção da sugestão de tamanho mínimo (corte de texto) (erro 312667)
- [KToggleToolBarAction] Respeitar a restrição 'action/options_show_toolbar'

### NetworkManagerQt

- Uso por omissão do WPA2-PSK e do WPA2-EAP ao obter o tipo de segurança a partir das definições da ligação

### Ícones do Oxygen

- adição do 'application-menu' ao Oxygen (erro 365629)

### Plataforma do Plasma

- Manter o 'slot' 'createApplet' compatível com as Plataformas 5.24
- Não remover duas vezes a textura de GL na miniatura (erro 365946)
- Adição do domínio da tradução no objecto QML do papel de parede
- Não apagar manualmente as 'applets'
- Adição de um 'kapptemplate' para um Papel de Parede do Plasma
- Modelos: registo dos modelos na categoria de topo própria "Plasma/"
- Modelos: Actualização das referências na Wiki da Base Técnica nos README's
- Definição de quais as estruturas de pacotes do Plasma estendem o 'plasmashell'
- suporte de um tamanho para a adição de 'applets'
- Definição do PackageStructure do Plasma como 'plugins' PackageStructure normais do KPackage
- Correcção: actualização do 'config.qml' do exemplo de papel de parede Outono para o QtQuick.Controls
- Uso do KPackage para instalar pacotes do Plasma
- Se for passado um QIcon como argumento para o IconItem::Source, este será usado
- Adição do suporte de camadas para o IconItem do Plasma
- Adição do Qt::Dialog às opções predefinidas para tornar o QXcbWindow::isTransient() funcional (erro 366278)
- [Tema Brisa do Plasma] Adição dos ícones 'network-flightmode-on/off'
- Emissão do 'contextualActionsAboutToShow' antes de mostrar o menu 'contextualActions' da 'applet' (erro 366294)
- [TextField] Associação ao tamanho do TextField em vez do texto
- [Estilos de Botões] Centrar na horizontal no modo apenas com ícones (erro 365947)
- [Contentor] Tratar o HiddenStatus como um estado baixo
- Adição do ícone da bandeja de sistema do KRuler por Yuri Fabirovsky
- Correcção do erro incómodo 'as janelas de diálogo aparecem no Gestor de Tarefas' mais uma vez
- correcção do ícone de disponibilidade de rede sem-fios com um emblema '?' (erro 355490)
- IconItem: Usar uma melhor abordagem para desactivar a animação quando passar de invisível para visível
- Mudança do tipo de janela do Tooltip na ToolTipDialog, através da API do KWindowSystem

### Solid

- Actualização sempre do processador do Predicate a partir do código do yacc/lex

### Sonnet

- hunspell: Limpeza do código de pesquisa por dicionários e pastas do XDG (erro 361409)
- Tentativa de uma melhor correcção do uso do filtro de línguas na detecção das mesmas

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
