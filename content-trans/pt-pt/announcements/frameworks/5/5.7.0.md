---
aliases:
- ../../kde-frameworks-5.7.0
date: '2015-02-14'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Geral

- Um conjunto de correcções para compilar com a próxima versão do Qt 5.5

### KActivities

- Correcção do início e paragem das actividades
- Correcção da antevisão da actividade a mostrar um papel de parede errado, em algumas ocasiões

### KArchive

- Criação de ficheiros temporários na pasta temporária, em vez da pasta actual

### KAuth

- Correcção da geração dos ficheiros serviços auxiliares de DBus do KAuth

### KCMUtils

- Correcção de validação quando os locais de DBus contêm um '.'

### KCodecs

- Adição do suporte para o CP949 no KCharsets

### KConfig

- O 'kconf_update' não processa mais os ficheiros *.upd do KDE SC 4. Adição de "Version=5" ao topo do ficheiro 'upd' para as actualizações que deveriam ser aplicado aos programas de Qt5/KF5
- Correcção do KCoreConfigSkeleton ao comutar um valor com gravações pelo meio

### KConfigWidgets

- KRecentFilesAction: correcção da ordem de itens do menu (para corresponder à ordem do 'kdelibs4')

### KCoreAddons

- KAboutData: Invocar o addHelpOption e o addVersionOption automaticamente, por conveniência e consistência
- KAboutData: Regresso do "Use por favor o http://bugs.kde.org para comunicar erros." quando não estiver definido outro e-mail/URL
- KAutoSaveFile: o allStaleFiles() agora funciona como esperado para os ficheiros locais; correcção também do staleFiles()
- O KRandomSequence agora usa inteiros internamente e expõe a API de inteiros para remover a ambiguidade dos 64-bits
- Definições de tipos MIME: os ficheiros *.qmltypes e *.qmlproject agora também têm o tipo MIME text/x-qml
- KShell: fazer o 'quoteArgs' codificar os URL's com o QChar::isSpace(), dado que os espaços fora do normal não eram devidamente tratados
- KSharedDataCache: correcção da criação da pasta que contém a 'cache' (erro de migração)

### KDBusAddons

- Adição do método auxiliar KDEDModule::moduleForMessage para criar mais serviços do tipo do 'kded', como o 'kiod'

### KDeclarative

- Adição de um componente de gráficos
- Adição do método de substituição do Formats::formatDuration, que recebe um número inteiro
- Novas propriedades 'paintedWidth' e 'paintedHeight' para o QPixmapItem e o QImageItem
- Correcção da pintura do QImageItem e do QPixmapItem

### Kded

- Adição do suporte para o carregamento de módulos do 'kded' com meta-dados em JSON

### KGlobalAccel

- Agora inclui o componente de execução, tornando-se assim uma plataforma de nível 3
- A infra-estrutura em Windows voltou a funcionar
- Reactivação da infra-estrutura para o Mac
- Correcção do estoiro durante a finalização da execução em X11 do KGlobalAccel

### KI18n

- Marcar os resultados como necessários no aviso, quando a API é mal usada
- Adição da opção de compilação BUILD_WITH_QTSCRIPT para permitir um conjunto de funcionalidades reduzido em sistemas embebidos

### KInit

- OSX: carregamento das bibliotecas dinâmicas correctas no arranque
- Correcções de compilação do Mingw

### KIO

- Correcção do estoiro nas tarefas ao compilar com o KIOWidgets, mas usando apenas uma QCoreApplication
- Correcção da edição dos atalhos da Web
- Adição da opção KIOCORE_ONLY, para compilar apenas o KIOCore e os seus programas auxiliares, mas não o KIOWidgets ou o KIOFileWidgets, reduzindo assim em grande medidas as dependências necessárias
- Adição da classe KFileCopyToMenu, que adiciona as opções "Copiar Para / Mover Para" nos menus de contexto
- Protocolos com SSL activo: adição do suporte para os protocolos TLSv1.1 e TLSv1.2, remoção do SSLv3
- Correcção do 'negotiatedSslVersion' e 'negotiatedSslVersionName' para devolver o protocolo negociado real
- Aplicação do URL introduzido à janela, quando se carrega no botão que muda o navegador de volta para o modo de navegação
- Correcção das duas janelas/barras de progresso que aparecem nas tarefas de cópia/movimento
- O KIO agora usa o seu próprio serviço 'kiod' para os serviços fora-do-processo que corriam antigamente no 'kded', para reduzir as dependências; de momento só substitui o 'kssld'
- Correcção do erro "Não é possível escrever em &lt;local&gt;" quando o 'kioexec' é activado
- Correcção dos avisos "QFileInfo::absolutePath: Construído com um nome de ficheiro vaio" ao usar o KFilePlacesModel

### KItemModels

- Correcção do KRecursiveFilterProxyModel para o Qt 5.5.0+, devido ao facto de o QSortFilterProxyModel agora usar o parâmetro 'roles' para o sinal 'dataChanged'

### KNewStuff

- Recarregar sempre os dados em XML dos URL's remotos

### KNotifications

- Documentação: menção dos requisitos do nome do ficheiro nos ficheiros .notifyrc
- Correcção de um ponteiro 'à solta' no KNotification
- Correcção de fuga no 'knotifyconfig'
- Instalação do cabeçalho do 'knotifyconfig' em falta

### KPackage

- Mudança de nome da página de 'man' do kpackagetool para kpackagetool5
- Correcção da instalação nos sistemas de ficheiros sem distinção de maiúsculas

### Kross

- Correcção do Kross::MetaFunction para que funcione com o sistema de meta-objectos do Qt5

### KService

- Inclusão das propriedades desconhecidas quando converter o KPluginInfo a partir do KService
- KPluginInfo: correcção das propriedades não copiadas a partir do KService::Ptr
- OS X: correcção de performance para o kbuildsycoca4 (ignorar os grupos de aplicações)

### KTextEditor

- Correcção do deslocamento de ratos por toque de alta precisão
- Não emitir o 'documentUrlChanged' durante o recarregamento
- Não quebrar a posição do cursor no recarregamento de documentos nas linhas com tabulações
- Não voltar a (des)dobrar a primeira linha, caso tenha sido (des)dobrada manualmente
- modo VI: histórico de comandos através das teclas de cursores
- Não tentar criar um código de validação quando se receber um sinal KDirWatch::deleted()
- Performance: remover as inicializações globais

### KUnitConversion

- Correcção da recursividade infinita no Unit::setUnitMultiplier

### KWallet

- Detecção e conversão automática das carteiras antigas de ECB para CBC
- Correcção do algoritmo de encriptação em CBC
- Garantia que a lista de carteiras é actualizada quando um ficheiro de carteira é removido do disco
- Remoção do &lt;/p&gt; excedente no texto visível pelo utilizador

### KWidgetsAddons

- Usar o 'kstyleextensions' para definir um elemento de controlo personalizado para desenhar a barra do 'kcapacity', quando suportado, o que permite que o item gráfico possa ser devidamente estilizado
- Oferta de um nome acessível para o KLed

### KWindowSystem

- Correcção do NETRootInfo::setShowingDesktop(bool) não funcionar no Openbox
- Adição do método de conveniência KWindowSystem::setShowingDesktop(bool)
- Correcções no tratamento do formato dos ícones
- Adição do método NETWinInfo::icccmIconPixmap, que oferece uma imagem de ícone da propriedade WM_HINTS
- Adição de substituto do KWindowSystem::icon que reduz as chamadas ao servidor de X
- Adição do suporte para o _NET_WM_OPAQUE_REGION

### NetworkmanagerQt

- Não imprimir uma mensagem sobre o não-tratamento do "AccessPoints"
- Adição do suporte para o NetworkManager 1.0.0 (não necessário)
- Correcção do tratamento de senhas do VpnSetting
- Adição da classe GenericSetting para as ligações não geridas pelo NetworkManager
- Adição da propriedade AutoconnectPriority às ConnectionSettings

#### Plataforma do Plasma

- Correcção da abertura inválida de um menu de contexto com problemas, ao abrir a janela do Plasma com o botão do meio
- Mudança do botão de activação com a roda do rato
- Nunca dimensionar uma janela maior que o ecrã
- Recuperar os painéis quando uma 'applet' é recuperada
- Correcção de combinações de teclas
- Reposição do suporte do hint-apply-color-scheme
- Recarregamento da configuração com as mudanças do 'plasmarc'
- ...

### Solid

- Adição do 'energyFull' e do 'energyFullDesign' à Battery

### Mudanças no sistema de compilação (extra-cmake-modules)

- Novo módulo ECMUninstallTarget para criar um alvo de desinstalação
- Fazer com que o KDECMakeSettings importe o ECMUninstallTarget por omissão
- KDEInstallDirs: avisar sobre a mistura de locais de instalação relativos e absolutos na linha de comandos
- Fazer com que o módulo ECMAddAppIcon adicione os ícones para os alvos executáveis no Windows e Mac OS X
- Correcção do aviso CMP0053 com o CMake 3.1
- Não limpar as variáveis de 'cache' no KDEInstallDirs

### Frameworkintegration

- Correcção da actualização da definição de 'click' simples durante a execução
- Diversas correcções na integração com a bandeja do sistema
- Apenas instalar o esquema de cores nos itens gráficos de topo (para corrigir o QQuickWidgets)
- Actualização da configuração do XCursor nas plataformas X11

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
