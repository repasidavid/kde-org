---
aliases:
- ../../kde-frameworks-5.13.0
date: 2015-08-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Novas plataformas

- KFileMetadata: biblioteca de extracção e meta-dados de ficheiros
- Baloo: plataforma de pesquisa e indexação de ficheiros

### Modificações que afectam todas as plataformas

- A versão mínima do Qt necessária passou da 5.2 para a 5.3
- O resultado de depuração foi migrado para uma migração por categorias, para gerar menos ruído por omissão
- A documentação em Docbook foi revista e actualizada

### Integração da Plataforma

- Correcção de estoiro na janela de ficheiros apenas com pastas
- Não confiar em options()-&gt;initialDirectory() para o Qt &lt; 5.4

### Ferramentas de Doxygen do KDE

- Adição de páginas de manual para os programas do 'kapidox' e actualização dos dados de manutenção no setup.py

### KBookmarks

- KBookmarkManager: usar o KDirWatch em vez do QFileSystemWatcher para detectar a criação do ficheiro 'user-places.xbel'.

### KCompletion

- Correcções de HiDPI para o KLineEdit/KComboBox
- KLineEdit: Não permitir ao utilizador apagar texto quando o campo de edição está apenas para leitura

### KConfig

- Não recomendar a utilização de API's obsoletas
- Não gerar código obsoleto

### KCoreAddons

- Adição do Kdelibs4Migration::kdeHome() para casos não cobertos pelos recursos
- Correcção de aviso no tr()
- Correcção da compilação do KCoreAddons no Clang+ARM

### KDBusAddons

- KDBusService: documentação de como elevar a janela activa no Activate()

### KDeclarative

- Correcção da chamada obsoleta KRun::run
- Mesmo comportamento do MouseArea para associar as coordenadas dos eventos-filhos filtrados
- Detectar a criação do ícone de face
- Não actualizar a janela inteira quando se pede a actualização do desenhador (erro 348385)
- adição da propriedade de contexto 'userPaths'
- Não bloquear com QIconItem's vazios

### Suporte para a KDELibs 4

- O 'kconfig_compiler_kf5' passou para a 'libexec' - use o 'kreadconfig5' em alternativa para o teste do 'findExe'
- Documentação dos substitutos (não-óptimos) do KApplication::disableSessionManagement

### KDocTools

- mudança da frase de relatórios de erros, confirmada por 'dfaure'
- adaptação do 'user.entities' alemão ao 'en/user.entities'
- Actualização do 'general.entities': mudança da formatação das plataformas + Plasma da aplicação para o nome do produto
- Actualização do en/user.entities
- Actualização dos modelos Docbook e do 'man'
- Uso do CMAKE_MODULE_PATH no cmake_install.cmake
- ERRO: 350799 (erro 350799)
- Actualização do general.entities
- Pesquisa dos módulos de Perl obrigatórios.
- 'Namespace', uma macro auxiliar no ficheiro de macros instalado.
- Adaptação das traduções de nomes de chaves para traduções-padrão oferecidas pelo Termcat

### KEmoticons

- Instalação do tema Brisa
- Kemoticons: uso dos ícones emotivos do Brisa em vez do Glass
- Pacote de ícones emotivos do Brisa criado por Uri Herrera

### KHTML

- Permite ao KHtml ser usado sem pesquisar as dependências privadas

### KIconThemes

- Remoção de alocações de texto temporárias.
- Remoção do item de depuração da árvore Theme

### KIdleTime

- Instalação dos ficheiros de inclusão privados dos 'plugins' da plataforma.

### KIO

- Eliminação de interfaces do QUrl desnecessárias

### KItemModels

- Novo 'proxy': KExtraColumnsProxyModel, que permite adicionar colunas a um modelo existente.

### KNotification

- Correcção da posição em Y inicial para as mensagens em queda
- Redução das dependências e passagem para o Nível 2
- captura de itens de notificação desconhecidos (eliminação de ponteiros nulos) (erro 348414)
- Remoção de uma mensagem de aviso inútil

### Plataforma de Pacotes

- colocar os sub-títulos como tal ;)
- kpackagetool: Correcção do envio de texto não-latino para o 'stdout'

### KPeople

- Adição do AllPhoneNumbersProperty
- O PersonsSortFilterProxyModel pode agora ser usado no QML

### Kross

- krosscore: Instalação do cabeçalho CamelCase "KrossConfig"
- Correcção dos testes de Python2 para correrem com o PyQt5

### KService

- Correcção do kbuildsycoca --global
- KToolInvocation::invokeMailer: correcção dos anexos quando são mais que um

### KTextEditor

- guarda do nível de depuração predefinido para o Qt &lt; 5.4.0, correcção dos nomes de categorias no registo
- adição do realce para o Xonotic (erro 342265)
- adição de realce para o Groovy (erro 329320)
- actualização do realce para J (erro 346386)
- Permite a compilação com o MSVC2015
- menor uso do carregador de ícones, correcção de mais ícones 'pixelizados'
- activar/desactivar o botão 'procurar tudo' com as mudanças de padrões
- Melhorias na barra de pesquisa &amp; substituição
- remoção de regra inútil no modo de energia
- barra de pesquisa mais fina
- vi: Correcção de mau processamento da opção 'markType01'
- Uso da qualificação correcta para invocar o método de base.
- Remoção de verificações, o QMetaObject::invokeMethod já se protege contra isso.
- correcção de problemas com o HiDPI nos selectores de cores
- Limpeza de código: o QMetaObject::invokeMethod está seguro contra ponteiros nulos.
- mais comentários
- mudança da forma como as interface se protegem contra ponteiros nulos
- só mostrar avisos ou criticidades maiores por omissão
- remover os 'por-fazer' do passado
- Uso do QVarLengthArray paa gravar a iteração temporária do QVector.
- Mover o truque para indentar as legendas dos grupos para a altura da construção.
- Correcção de diversos problemas com o KateCompletionModel no modo em árvore.
- Correcção do desenho problemático de modelos, que se baseava no comportamento do Qt 4.
- cumprimento das regras do 'umask' ao gravar um novo ficheiro (erro 343158)
- adição do realce para o Meson
- Dado que o Varnish 4.x introduz diversas mudanças na sintaxe face ao Varnish 3.x, foram criados ficheiros de realce de sintaxe separados para o Varnish 4 (varnish4.xml,  varnishtest4.xml).
- correcção de problemas com o HiDPI
- modo VI: não estoirar se o comando &lt;c-e&gt; for executado no fim de um documento. (erro 350299)
- Suporte para textos multi-linhas no QML.
- correcção da sintaxe do 'oors.xml'
- adição do realce do CartoCSS por Lukas Sommer (erro 340756)
- correcção do realce de vírgula flutuante, usando o Float incorporado como no C (erro 348843)
- as direcções divididas foram revertidas (erro 348845)
- Erro 348317 - [MODIFICAÇÃO] o realce de sintaxe do Katepart deverá reconhecer as sequências de escape do JavaScript (erro 348317)
- adição do *.cljs (erro 349844)
- Adição do ficheiro de realce do GLSL.
- correcção das cores predefinidas para serem mais distintas

### KTextWidgets

- Remoção do realce antigo

### Plataforma da KWallet

- Correcção da compilação em Windows
- Apresentação de um aviso com o código de erro, se a abertura da carteira pelo PAM for mal-sucedida
- Devolução do erro da infra-estrutura, em vez de -1, quando a abertura de uma carteira for mal-sucedida
- Mudança do "cifra desconhecida" da infra-estrutura para um código de erro negativo
- Vigilância do PAM_KWALLET5_LOGIN no KWallet5
- Correcção do estoiro quando a verificação MigrationAgent::isEmptyOldWallet() for mal-sucedida
- O KWallet pode agora ser desbloqueado pelo PAM, usando o módulo 'kwallet-pam'

### KWidgetsAddons

- Nova API que recebe os parâmetros QIcon para mudar os ícones na barra de páginas
- KCharSelect: correcção da categoria em Unicode e o uso do 'boundingRect' para o cálculo da largura
- KCharSelect: correcção da largura da célula para caber o conteúdo
- As margens do KMultiTabBar agora estão OK nos ecrãs HiDPI
- KRuler: o método não-implementado KRuler::setFrameStyle() é agora obsoleto, limpeza de comentários
- KEditListWidget: remoção da margem, para que se alinhe melhor com os outros elementos

### KWindowSystem

- Reforço da leitura de dados do NETWM (erro 350173)
- protecção contra versões do Qt mais antigas, como no 'kio-http'
- Os ficheiros de inclusão privados dos 'plugins' da plataforma são agora instalados.
- Partes do código específicas da plataforma passadas para 'plugins'.

### KXMLGUI

- Correcção do comportamento do método KShortcutsEditorPrivate::importConfiguration

### Plataforma do Plasma

- O uso do gesto de redução poderá agoraa mudar entre os diferentes níveis de ampliação do calendário
- comentário sobre a duplicação de código na janela de ícones
- A cor do relevo da barra deslizante era fixo; foi modificado para usar o esquema de cores
- Usar o QBENCHMARK em vez de um requisito fixo para a performance da máquina
- A navegação do calendário foi melhorada em grande medida, oferecendo um vista anual e por décadas
- O PlasmaCore.Dialog tem agora uma propriedade 'opacity'
- Reserva de algum espaço para os botões das opções exclusivas
- Não mostrar o fundo circular se existir um menu
- Adição da definição X-Plasma-NotificationAreaCategory
- Mudança das notificações e do OSD para mostrar em todos os ecrãs
- Apresentação de um aviso útil quando não é possível obter um KPluginInfo válido
- Correcção de uma potencial recorrência infinita no PlatformStatus::findLookAndFeelPackage()
- Mudança do software-updates.svgz para o software.svgz

### Sonnet

- Adição de código do CMake para activar a compilação do 'plugin' Voikko.
- Implementação da fábrica Sonnet::Client para os verificadores ortográficos Voikko.
- Implementação de um verificador ortográfico baseado no Voikko (Sonnet::SpellerPlugin)

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
