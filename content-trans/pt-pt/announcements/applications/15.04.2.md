---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: O KDE Lança as Aplicações do KDE 15.04.2
layout: application
title: O KDE Lança as Aplicações do KDE 15.04.2
version: 15.04.2
---
2 de Junho de 2015. Hoje o KDE lançou a segunda actualização de estabilidade para as <a href='../15.04.0'>Aplicações do KDE 15.04</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

As mais de 30 correcções de erros registadas incluem as melhorias no 'gwenview', 'kate', 'kdenlive', 'kdepim', 'konsole', 'marble', 'kgpg', 'kig', 'ktp-call-ui' e no 'umbrello'.

Esta versão também inclui as versões de Suporte de Longo Prazo da Área de Trabalho do Plasma 4.11.20, a Plataforma de Desenvolvimento do KDE 4.14.9 e o pacote Kontact 4.14.9.
