---
aliases:
- ../announce-applications-17.12.1
changelog: true
date: 2018-01-11
description: KDE에서 KDE 프로그램 17.12.1 출시
layout: application
title: KDE에서 KDE 프로그램 17.12.1 출시
version: 17.12.1
---
January 11, 2018. Today KDE released the first stability update for <a href='../17.12.0'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Kontact, Dolphin, Filelight, Gwenview, KGet, Okteta, Umbrello 등에 20개 이상의 버그 수정 및 기능 개선이 있었습니다.

개선 사항:

- 특정 SMTP 서버로 Kontact에서 이메일 전송 오류 수정
- Gwenview의 타임라인 및 태그 검색 개선
- Umbrello UML 다이어그램 도구의 자바 가져오기 수정
