---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: KDE publie la version 19.08.1 des applications.
layout: application
major_version: '19.08'
release: applications-19.08.1
title: KDE publie la version 19.08.1 des applications.
version: 19.08.1
---
{{% i18n_date %}}

Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../19.08.0'>applications de KDE %[2 ]s</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traductions, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues identifiés, portant sur des améliorations de Kontact, Dolphin, Kdenlive, Konsole, Step parmi bien d'autres.

Les améliorations incluses sont :

- Plusieurs régressions dans la gestion des onglets sous Konsole ont été corrigées.
- Dolphin démarre de nouveau correctement en mode d'affichage scindé.
- La suppression d'un corps mou dans le simulateur de physique Step ne provoque plus de plantage.
