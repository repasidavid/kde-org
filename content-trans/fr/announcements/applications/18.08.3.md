---
aliases:
- ../announce-applications-18.08.3
changelog: true
date: 2018-11-08
description: KDE publie les applications KDE 18.08.3
layout: application
title: KDE publie les applications KDE 18.08.3
version: 18.08.3
---
08 Novembre 2018. Aujourd'hui, KDE a publié la troisième mise à jour de stabilisation des <a href='../18.08.0'>applications 18.08 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction. Il s'agit d'une mise à jour sûre et bénéfique pour tout le monde.

Plus de 29 corrections de bogues apportent des améliorations à Kontact, Ark, Dolphin, jeux KDE, Kate, Okular, Umbrello et bien d'autres.

Les améliorations incluses sont :

- Le mode d'affichage « HTML » dans KMail est restauré et charge de nouveau, les images externes, si autorisé.
- Kate se souvient maintenant des métadonnées (Y compris le signets) entre deux sessions d'édition.
- Le défilement automatique dans l'interface utilisateur pour le texte de Telepathy a été corrigé avec les plus récentes versions de « QtWebengine ».
