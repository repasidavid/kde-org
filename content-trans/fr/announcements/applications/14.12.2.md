---
aliases:
- ../announce-applications-14.12.2
changelog: true
date: '2015-02-03'
description: KDE publie la version 14.12.2 des applications.
layout: application
title: KDE publie KDE Applications 14.12.2
version: 14.12.2
---
03 Septembre 2015. Aujourd'hui, KDE a publié la seconde mise à jour de stabilisation des <a href='../14.12.0'>applications 14.12 de KDE</a>. Cette mise à jour ne contient que des corrections de bogues et des mises à jour de traduction, elle sera sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues référencées contiennent des améliorations pour le jeu d'anagramme Kanagram, l'outil de modélisation UML Umbrello, le lecteur de documents Okular and le globe de bureau Marble.

Cette version inclut également les versions maintenues à long terme des espaces de travail de Plasma 4.11.16, de la plate-forme de développement 4.14.5 et de la suite Kontact4.14.5.
