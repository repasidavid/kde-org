---
aliases:
- ../announce-applications-15.12.1
changelog: true
date: 2016-01-12
description: KDE publie les applications de KDE 14.12.1
layout: application
title: KDE publie les applications de KDE 14.12.1
version: 15.12.1
---
12 Janvier 2016. Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../15.12.0'>applications 15.12 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 30 corrections de bogues apportent des améliorations à kdelibs, kdepim, kdenlive, marble, konsole, spectacle, akonadi, ark and umbrello et bien d'autres.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.16 de KDE.
