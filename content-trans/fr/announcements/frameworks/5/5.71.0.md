---
aliases:
- ../../kde-frameworks-5.71.0
date: 2020-06-06
layout: framework
libCount: 70
---
### Baloo

- Stockage unique des termes pour le nom de fichier

### Icônes « Breeze »

- Improve accuracy of battery percentage icons
- Ajout d'une icône pour le type « MIME » « KML » 
- Change mouse icon to have better dark theme contrast (bug 406453)
- Nécessite une compilation des sources (bogue 421637)
- Ajout d'icônes en configuration 48 pixels (bogue 421144)
- Ajoute un raccourci manquant vers des icônes LibreOffice

### Modules additionnels « CMake »

- [android] Use newer Qt version in example
- [android] Allow specifying APK install location
- ECMGenerateExportHeader: add generation of *_DEPRECATED_VERSION_BELATED()
- ECMGeneratePriFile: fix for ECM_MKSPECS_INSTALL_DIR being absolute
- ECMGeneratePriFile: make the pri files relocatable
- Suppress find_package_handle_standard_args package name mismatch warning

### Outils KDE avec « DOxygen »

- Use logo.png file as logo as default, if present
- Change public_example_dir to public_example_dirs, to enable multiple dirs
- Abandon de la prise en charge du code avec Python 2
- Adaptation des liens vers « repo » à « invent.kde.org »
- Correction du lien vers les mentions légales de « kde.org »
- Les pages d'historique de KDE 4 ont été fusionnées dans une page générale d'historique
- Rétablir la génération avec des diagrammes « dep » pour Python 3 (Rupture avec Python 2)
- Exportation de la méta-liste vers un fichier « json »

### KAuth

- Utiliser « ECMGenerateExportHeader » pour mieux gérer les API déconseillés

### KBookmarks

- Utiliser le contexte des marques d'interface utilisateur dans plus d'appels « tr()  »
- [KBookmarkMenu] assigne « m_actionCollection » plus tôt pour prévenir le plantage (bogue 420820)

### KCMUtils

- Ajouter « X-KDE-KCM-Args » comme propriété, propriété « En lecture » dans le chargeur de module
- Correction d'un plantage lors du chargement d'une application « KCM » comme « yast » (bogue 421566)
- KSettings::Dialog: avoid duplicate entries due cascading $XDG_DATA_DIRS

### KCompletion

- Encapsulation aussi l'API « dox » avec l'utilisation de « KCOMPLETION_ENABLE_DEPRECATED_SINCE » par méthode
- Utiliser le contexte des marques d'interface utilisateur dans plus d'appels « tr()  »

### KConfig

- Ne pas essayer d'initialiser la valeur d'énumération déconseillée « SaveOptions »
- Ajout de « KStandardShortcut::findByName(const QString&amp;) » et la fonction déconseillée « find(const char*) »
- Correction de « KStandardShortcut::find(const char*) »
- Ajustement du nom de la méthode d'exportation interne comme suggéré dans D29347
- KAuthorized : méthode d'exportation pour recharger les restrictions

### KConfigWidgets

- [KColorScheme] Suppression du code dupliqué
- Make Header colors fallback to Window colors first
- Introduit un ensemble de couleurs pour en-tête

### KCoreAddons

- Correction du bogue 422291 - Aperçu de l'URI « XMPP » dans KMail (bogue 422291)

### KCrash

- Ne pas invoquer la fonction de localisation « qstring » dans une section critique

### KDeclarative

- Création de fonctions « kcmshell.openSystemSettings() » et « kcmshell.openInfoCenter()  »
- Portage de « KKeySequenceItem » vers « QQC2 »
- Alignement de pixels des enfants de « GridViewInternal »

### KDED

- Correction des icônes floues dans le menu d'applications pour barre de titre par ajout de l'option « UseHighDpiPixmaps »
- Ajoute d'un fichier de service utilisateur « systemd » pour « kded »

### KFileMetaData

- [TaglibExtractor] Ajout de la prise en charge des livres audio « Audio amélioré » pour Audible
- Traiter l'option « extractMetaData »

### KGlobalAccel

- Correction du bogue pour les composants contenant des caractères spéciaux (bogue 407139)

### KHolidays

- Mettre à jour les jours fériés de Taiwan
- holidayregion.cpp - Fourniture des chaînes de traduction pour les régions d'Allemagne
- holidays_de-foo - Définit le champ de nom pour tous les fichiers de vacances en Allemagne
- holiday-de-&lt;foo&gt; - où « foo » est une région d'Allemagne
- Ajout d'un fichier de vacances pour l'Allemagne et la Belgique (Allemagne / Berlin)
- holidays/plan2/holiday_gb-sct_en-gb

### KImageFormats

- Ajout de vérification de bonne santé et de limites

### KIO

- Introduction de « KIO::OpenUrlJob », une ré-écriture et remplacement de KRun
- KRun : directive de ne plus utiliser tous les méthodes statiques « run* » avec les instructions complètes de portabilité
- KDesktopFileActions : directive de ne plus utiliser « run/runWithStartup » et d'utiliser à la place « OpenUrlJob »
- Recherche de « kded » comme dépendance pour l'exécution
- [kio_http] Analyser un emplacement « FullyEncoded QUrl » avec « TolerantMode » (bogue 386406)
- [DelegateAnimationHanlder] Remplacement de la fonction déconseillée « QLinkedList » par « QList »
- RenameDialog : émission d'une alarme quand les tailles de fichiers ne sont pas les mêmes (bogue 421557)
- [KSambaShare] Vérification que les deux modules « smbd » et « testparm » sont disponibles (bogue 341263)
- Emplacements : utilisation de « Solid::Device::DisplayName » pour « DisplayRole » (bogue 415281)
- KDirModel : correction de la régression « hasChildren() » pour les arborescences de fichiers affichés (bogue 419434)
- file_unix.cpp : quand « ::rename » est utilisé comme condition, comparer son retour à -1
- [KNewFileMenu] Autorisation de créer un dossier nommé « ~ » (bogue 377978)
- [HostInfo] Définition de « QHostInfo::HostNotFound » quand un hôte n'est pas trouvé dans le cache DNS (bogue 421878)
- [DeleteJob] Report final numbers after finishing (bug 421914)
- KFileItem: localize timeString (bug 405282)
- [StatJob] Make mostLocalUrl ignore remote (ftp, http...etc) URLs (bug 420985)
- [KUrlNavigatorPlacesSelector] only update once the menu on changes (bug 393977)
- [KProcessRunner] Use only executable name for scope
- [knewfilemenu] Show inline warning when creating items with leading or trailing spaces (bug 421075)
- [KNewFileMenu] Suppression du paramètre d'emplacement redondant
- ApplicationLauncherJob : affichage de la boîte de dialogue « Ouvrir avec » si aucun service n'est passé
- Vérification que le programme existe et qu'il est exécutable avant son utilisation avec « kioexec / kdesu / etc »
- Correction des paramètres « URL » passés comme argument lors du lancement d'un fichier « .desktop » (bogue 421364)
- [kio_file] Gestion du renommage de fichiers « A » vers « a » sur des systèmes de fichiers « FAT32 »
- [CopyJob] Vérification sir le dossier de destination est un lien symbolique (bogue 421213)
- Correction du fichier de services spécifiant « Exécuter dans un terminal » produisant un code d'erreur « 100 » (bogue 421374)
- kio_trash : ajout de la prise en charge de renommage de dossiers dans le cache
- Génération d'une alarme si le paramètre « info.keepPassword » n'est pas défini
- [CopyJob] Vérification de l'espace libre pour les «  » distants avant la copie et autres améliorations (bogue 418443)
- [kcm trash] Modification dans « kcm » du pourcentage de taille pour la corbeille avec deux décimales
- [CopyJob] Suppression d'une ancienne liste de choses à faire et utilisation de « QFile::rename() »
- [LauncherJobs] Diffusion d'une description

### Kirigami

- Add section headers in sidebar when all the actions are expandibles
- Correction : remplissage de l'en-tête des feuilles en superposition
- Use a better default color for Global Drawer when used as Sidebar
- Fix inaccurate comment about how GridUnit is determined
- More reliable parent tree climbing for PageRouter::pushFromObject
- PlaceholderMessage depends on Qt 5.13 even if CMakeLists.txt requires 5.12
- Introduction d'un groupe d'en-tête
- Don't play close animation on close() if sheet is already closed
- Ajout d'un composant « SwipeNavigator »
- Introduit un composant d'avatars
- Fix the shaders resource initialisation for static builds
- Page d'à-propos : ajout des infobulles.
- Ensure closestToWhite and closestToBlack
- AboutPage: Add buttons for email, webAddress
- Always use Window colorset for AbstractApplicationHeader (bug 421573)
- Introduit les couleurs d'images
- Recommend better width calculation for PlaceholderMessage
- Amélioration de l'« API » pour « PageRouter »
- Use small font for BasicListItem subtitle
- Ajoute la prise en charge des calques pour PagePoolAction
- Introduit le contrôle « RouterWindow »

### KItemViews

- Utiliser le contexte des marques d'interface utilisateur dans plus d'appels « tr()  »

### KNewStuff

- Don't duplicate error messages in a passive notification (bug 421425)
- Fix incorrect colours in the KNS Quick messagebox (bug 421270)
- Do not mark entry as uninstalled if uninstallation script failed (bug 420312)
- KNS: Deprecate isRemote method and handle parse error properly
- KNS: Do not mark entry as installed if install script failed
- Fix showing updates when the option is selected (bug 416762)
- Correction de la mise à jour de l'auto-sélection (bogue 419959)
- Add KPackage support to KNewStuffCore (bug 418466)

### KNotification

- Implement lock-screen visibility control on Android
- Implémente le regroupement de notifications pour Android
- Display rich text notification messages on Android
- Implémentation des liens « URL » utilisant des astuces
- Utiliser le contexte des marques d'interface utilisateur dans plus d'appels « tr()  »
- Implémente la prise en charge de la priorité des notifications pour Android
- Remove checks for notification service and fallback to KPassivePopup

### KQuickCharts

- Vérification plus simple pour l'entourage de l'arrière-plan
- PieChart: Render a torus segment as background when it isn't a full circle
- PieChart: Add a helper function that takes care of rounding torus segments
- PieChart: Expose fromAngle/toAngle to shader
- Always render the background of the pie chart
- Always set background color, even when toAngle != 360
- Allow start/end to be at least 2pi away from each other
- Remaniement de l'implémentation pour les graphiques en secteur
- Fixup and comment sdf_torus_segment sdf function
- Don't explicitly specify smoothing amount when rendering sdfs
- Workaround lack of array function parameters in GLES2 shaders
- Handle differences between core and legacy profiles in line/pie shaders
- Correction de quelques erreurs de validation
- Update the shader validation script with the new structure
- Ajout d'une en-tête séparée pour les ombrages « GLES3 »
- Remove duplicate shaders for core profile, instead use preprocessor
- Exportation à la fois du nom complet et abrégé
- support different long/short labels (bug 421578)
- Conserver le modèle derrière un « QPointer »
- Ajout de « MapProxySource »

### KRunner

- Do not persist runner whitelist to config
- KRunner fix prepare/teardown signals (bug 420311)
- Detect local files and folders starting with ~

### KService

- Ajout de « X-KDE-DBUS-Restricted-Interfaces » aux champs d'entrées du bureau pour les applications
- Add missing compiler deprecation tag for 5-args KServiceAction constructor

### KTextEditor

- Ajout de « .diff » à « file-changed-diff » pour autoriser la détection de type « MIME » sous Windows
- scrollbar minimap: performance: delay update for inactive documents
- Make text always align with font base line
- Revert "Store and fetch complete view config in and from session config"
- Fix modified line marker in kate minimap

### KWidgetsAddons

- Be noisy about deprecated KPageWidgetItem::setHeader(empty-non-null string)

### KXMLGUI

- [KMainWindow] Invoke QIcon::setFallbackThemeName (later) (bug 402172)

### KXmlRpcClient

- Marquage de « KXmlRpcClient » comme une aide au portage

### Environnement de développement de Plasma

- PC3 : ajout d'un séparateur dans la barre d'outils
- Prise en compte du groupe de couleurs des en-têtes
- Implement scroll and drag adjustment of values for SpinBox control
- Use font: instead of font.pointSize: where possible
- kirigamiplasmastyle: Add AbstractApplicationHeader implementation
- Avoid potential disconnect of all signals in IconItem (bug 421170)
- Use small font for ExpandableListItem subtitle
- Ajout de « smallFont » au style de Kirigami de Plasma
- [Plasmoid Heading] Draw the heading only when there is an SVG in the theme

### QQC2StyleBridge

- Ajout de l'élément de style « ToolSeparator »
- Remove "pressed" from CheckIndicator "on" state (bug 421695)
- Prise en charge du groupe d'en-tête
- Implémentation de « smallFont » dans le module externe de Kirigami

### Opaque

- Ajout de « QString Solid::Device::displayName », utilisé dans le fichier « Fstab » pour les montages réseau (bogue 415281)

### Sonnet

- Allow overriding to disable auto language detection (bug 394347)

### Coloration syntaxique

- Update Raku extensions in Markdown blocks
- Raku: fix fenced code blocks in Markdown
- Assign "Identifier" attribute to opening double quote instead of "Comment" (bug 421445)
- Bash: fix comments after escapes (bug 418876)
- LaTeX: fix folding in end{...} and in regions markers BEGIN-END (bug 419125)

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
