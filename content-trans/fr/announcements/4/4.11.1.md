---
aliases:
- ../announce-4.11.1
date: 2013-09-03
description: KDE publie la version 4.11.1 des environnements de développement, des
  applications et de la plate-forme de développement.
title: KDE publie les mises à jour, pour Septembre, des environnements de bureaux
  Plasma, des applications et de la plate-forme de développement.
---
03 Septembre 2013. Aujourd'hui, KDE a publié les mises à jour pour ses applications d'environnements de bureaux et sa plate-forme de développement. Ces mises à jour sont les premières d'une série mensuelle de mises à jour de consolidation pour les versions 4.11. Comme annoncé durant cette publication, les environnements de bureaux continueront à être mis à jour pour les deux prochaines années. Cette mise à jour ne contient que des corrections de bogues et des mises à jour de traduction, elle sera sûre et appréciable pour tout le monde.

Plus de 70 corrections de bogues référencées contiennent des améliorations pour le gestionnaire de fenêtres KWin, le gestionnaire de fichiers Dolphin et d'autres programmes. Les utilisateurs peuvent se préparer à un démarrage du bureau Plasma plus rapide, un défilement pour Dolphin plus fluide ainsi qu'un usage réduit de mémoire pour de nombreux outils et applications. Ces améliorations incluent le retour du glisser-déposer de la barre de tâches vers le gestionnaire de bureau, des corrections de mise en valeur et de coloration dans Kate et de très nombreux petits bogues éliminés du jeu KMahjongg. Il y a de nombreuses corrections pour assurer la stabilité et les ajouts habituels pour les traductions.

Une <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>liste</a> plus complète de modifications peut être consultée dans l'outil de suivi des points ouverts de KDE. Pour la liste détaillée des modifications apportées avec 4.11.1, vous pouvez aussi parcourir les journaux de « Git ».

Pour télécharger le code source ou les paquets à installer, veuillez consulter la <a href='/info/4/4.11.1'>Page d'informations de KDE 4.11.1</a>. Si vous voulez obtenir plus d'informations concernant les versions 4.10 des environnements de bureaux, des applications et de l'environnement de développement de KDE, veuillez consulter la page <a href='/announcements/4.11/'>Notes de publication de KDE 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Le nouveau processus de traitement de données pour l'envoi différé dans Kontact` width="600px">}}

Les logiciels de KDE, y compris toutes les bibliothèques et les applications, sont disponibles librement sous les licences « Open Source ». Ils peuvent être obtenus sous forme de code source mais aussi sous forme de nombreux formats binaires à partir de <a href='http://download.kde.org/stable/4.11.1/'>download.kde.org</a> ou à partir de tous <a href='/distributions'>les systèmes GNU / Linux et UNIX</a> distribués aujourd'hui.
