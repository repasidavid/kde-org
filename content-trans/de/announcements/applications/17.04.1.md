---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE veröffentlicht die KDE-Anwendungen 17.04.1
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 17.04.1
version: 17.04.1
---
11. Mai 2017. Heute veröffentlicht KDE die erste Aktualisierung der <a href='../17.04.0'>KDE-Anwendungen 17.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für kdepim, Dolphin, Gwenview, Kate und Kdenlive.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
