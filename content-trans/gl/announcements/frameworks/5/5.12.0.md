---
aliases:
- ../../kde-frameworks-5.12.0
date: 2015-07-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Módulos adicionais de CMake

- Mellorar a información de erros da macro query_qmake

### BluezQt

- Retirar todos os dispositivos do adaptador antes de retirar o adaptador (fallo 349363)
- Actualizar as ligazóns en README.md

### KActivities

- Engadir a opción para non facer seguimento do usuario en actividades concretar (similar ao «modo privado» dos navegadores web)

### KArchive

- Conservar os permisos de execución dos ficheiros ao usar copyTo()
- Clarificar ~KArchive retirando código que non se usa.

### KAuth

- Permitir usar kauth-policy-gen desde distintas fontes

### KBookmarks

- Non engadir un marcador se o URL e o texto están baleiros
- Codificar o URL de KBookmark para recuperar a compatibilidade con aplicacións de KDE4

### KCodecs

- Retirar o probador de x-euc-tw

### KConfig

- Instalar kconfig_compiler en libexec
- Nova opción TranslationDomain= de xeración de código para usar co TranslationSystem=kde; que se necesita normalmente nas bibliotecas.
- Permitir usar kconfig_compiler desde distintas fontes

### KCoreAddons

- KDirWatch: Só estabelecer unha conexión a FAM se se solicita
- Permitir filtrar complementos e aplicacións por forma de dispositivo
- Permitir usar desktoptojson desde distintas fontes

### KDBusAddons

- Clarificar o valor «exit» para instancias de «Unique»

### KDeclarative

- Engadir un clon QQC de KColorButton
- Asignar un QmlObject a cada instancia de kdeclarative cando sexa posíbel
- Facer que o código Qt.quit() funcione desde QML
- Fusionar a rama «mart/singleQmlEngineExperiment»
- Engadir sizeHint en base a implicitWidth/height
- Subclase de QmlObject cun motor estático

### Compatibilidade coa versión 4 de KDELibs

- Corrixir KMimeType::Ptr::isNull.
- Permitir de novo que KDateTime envíe datos a kDebug/qDebug, para máis SC
- Cargar o catálogo de traducións correcto para kdebugdialog
- Non deixar de documentar os métodos obsoletos, para que a xente poida ler os consellos de migración

### KDESU

- Corrixir CMakeLists.txt para que pase KDESU_USE_SUDO_DEFAULT á compilación para que suprocess.cpp o use

### KDocTools

- Actualiza os modelos de docbook de K5

### KGlobalAccel

- instálase unha API privada en tempo de execución para permitir a KWin fornecer un complemento para Wayland.
- Plan B para a resolución de nomes de componentFriendlyForAction

### KIconThemes

- Non intentar pintar a icona se o tamaño é incorrecto

### KItemModels

- Novo modelo de proxys: KRearrangeColumnsProxyModel. Permite cambiar a orde e ocultar columnas do modelo de orixe.

### KNotification

- Corrixir os tipos de mapas de píxeles en org.kde.StatusNotifierItem.xml
- [ksni] Engadir un método para obter accións por nome (fallo 349513)

### KPeople

- Engadir funcionalidade de filtro a PersonsModel

### KPlotting

- KPlotWidget: engadir setAutoDeletePlotObjects, corrixir unha fuga de memoria en replacePlotObject
- Corrixir as marcas que faltaban cando x0 &gt; 0.
- KPlotWidget: non é necesario usar setMinimumSize ou resize.

### KTextEditor

- debianchangelog.xml: engadir Debian/Stretch, Debian/Buster e Ubuntu-Wily
- Corrixir o comportamento da parella substituta de UTF-16 borrar/suprimir.
- Permitir a QScrollBar xestionar WheelEvents (fallo 340936)
- Aplicar o parche de devel de KWrite de actualizar o realce de sintaxe básico puro de top, «Alexander Clay», &lt;tuireann@EpicBasic.org&gt;

### KTextWidgets

- Corrixir a activación e desactivación do botón de Aceptar

### Infraestrutura de KWallet

- Importouse e mellorouse a ferramenta de liña de ordes kwallet-query.
- Posibilidade de sobrescribir entradas de mapas.

### KXMLGUI

- Non mostrar a «versión das infraestruturas de KDE» no diálogo «Sobre KDE»

### Infraestrutura de Plasma

- Facer o tema escuro completamente escuro, tamén o grupo complementario
- Gardar en caché naturalsize de maneira separada por scalefactor
- ContainmentView: Non quebrar cando os metadatos de coroa son incorrectos
- AppletQuickItem: Non acceder a KPluginInfo se non é válido
- Corrixir as páxinas de configuración de miniaplicativo que ás veces están baleiras (fallo 349250)
- Mellorar a compatibilidade con HiDPI no compoñente de grade de calendario
- Verificar que KService ten información de complemento válida antes de usalo
- [calendar] Asegurarse de que a grade se pinta de novo cando cambia o tema
- [calendar] Comezar a contar as semanas sempre a partir do luns (fallo 349044)
- [calendar] Pintar de novo a grade cando a opción de mostrar os números de semana cambia
- Agora úsase un tema opaco cando só está dispoñíbel o efecto borroso (fallo 348154)
- Poñer na lista branca os miniaplicativos e as versións para un motor aparte
- Introducir unha nova clase ContainmentView

### Sonnet

- Permitir usar a corrección ortográfica da selección en QPainTextEdit

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
