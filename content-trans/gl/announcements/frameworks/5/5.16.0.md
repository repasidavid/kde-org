---
aliases:
- ../../kde-frameworks-5.16.0
date: 2015-11-13
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Biblioteca de monitorización: Usar Kformat::spelloutDuration para localizar a cadea de tempo
- Usar KDE_INSTALL_DBUSINTERFACEDIR para instalar interfaces de D-Bus
- UnindexedFileIndexer: xestionar os ficheiros que se moveron mentres baloo_file non estaba en execución
- Retirar Transaction::renameFilePath e engadir DocumentOperation para el.
- Facer explícitos os construtores cun único parámetro
- UnindexedFileIndexer: só indexar as partes necesarias do ficheiro
- Transaction: engadir un método para devolver a estrutura de timeInfo
- Engadíronse tipos MIME de exclusión á configuración de balooctl
- Bases de datos: Usar QByteArray::fromRawData ao pasar datos a un códec
- Balooctl: mover a orde «status» a unha clase de seu
- Balooctl: Mostrar un menú de axuda se non se recoñece a orde
- Balooshow: Permitirnos buscar ficheiros polo seu nodo de índice ou identificador de dispositivo
- Monitor de Balooctl: deter se Baloo morre
- MonitorCommand: Usar tanto o sinal started como o finished
- Balooctl monitor: mover a unha clase de orde axeitada
- Engadir unha notificación de D-Bus para cando comezamos ou rematamos de indexar un ficheiro
- FileIndexScheduler: Matar de maneira forzada fíos ao saír
- Remisión de WriteTransaction: evitar obter positionList salvo que sexa necesario
- WriteTransaction: asercións adicionais en replaceDocument

### BluezQt

- Agora isBluetoothOperational tamén dependen dun rfkill sen bloquear
- Corrixir a determinación do estado global do interruptor de rfkill
- API de QML: marcar as propiedades sen sinal de notificar como constantes

### Módulos adicionais de CMake

- Avisar en vez de informar dun erro se ecm_install_icons non atopa iconas (fallo 354610).
- permitir construír a versión 5 das infraestruturas de KDE cun Qt 5.5.x sen alterar instalado desde o instalador normal de qt.io en mac os
- Non retirar a definición de variábeis da caché en KDEInstallDirs (fallo 342717).

### Integración de infraestruturas

- Definir o valor predeterminado de WheelScrollLines
- Corrixir a opción WheelScrollLines con Qt ≥ 5.5 (fallo 291144)
- Cambiar á fonte Noto para Plasma 5.5

### KActivities

- Corrixir a construción con Qt 5.3
- Moveuse a inclusión boost.optional ao lugar que a usa
- Substituír o uso de boost.optional nas continuacións por unha estrutura optional_view máis sinxela
- Engadiuse a posibilidade de usar unha orde personalizada para os resultados ligados
- Permitir a QML invocar o KCM de actividades
- Engadir a posibilidade de eliminar actividades desde o KCM de actividades
- Nova interface de usuario de configuración de actividades
- Nova interface de usuario de configuración que permite engadir descrición e fondo de escritorio
- Agora a interface de usuario de configuración está correctamente dividida en módulos

### KArchive

- Corrixir KArchive por un cambio de comportamento de Qt 5.6
- Corrixir fugas de memoria, reducir o uso de memoria

### KAuth

- Xestionar o paso de mensaxes de qInfo
- Agardar a que remate o asistente inicial da chamada asíncrona antes de comprobar a resposta (fallo 345234)
- Corrixir o nome da variábel, senón a inclusión non pode funcionar

### KConfig

- Corrixir o uso de ecm_create_qm_loader.
- Corrixir variábeis de inclusión
- Use KDE*INSTALL_FULL* variant, so there is no ambiguity
- Permitir que KConfig use recursos como ficheiros de configuración de reserva

### KConfigWidgets

- Facer KConfigWidgets independente, empaquetar o ficheiro global único nun recurso
- Facer doctools opcional

### KCoreAddons

- KAboutData: apidoc: «is is» → «is»; addCredit(): ocsUserName → ocsUsername
- KJob::kill(Quiet) tamén debería saír do bucle de eventos
- Engadir a posibilidade de que KAboutData lea o nome dos ficheiros de escritorio
- Usar o carácter de escape correcto
- Reducir algunhas asignacións
- Simplificar KAboutData::translators e setTranslators
- Corrixir o código de exemplo de setTranslator
- desktopparser: saltar a clave Encoding=
- desktopfileparser: ter en conta os comentarios das recensións
- Permitir definir tipos de servizo en kcoreaddons_desktop_to_json()
- desktopparser: corrixir a análise de valores dobres e booleanos
- Engadir KPluginMetaData::fromDesktopFile()
- desktopparser: Permitir analizar rutas relativas aos ficheiros de tipo de servizo
- desktopparser: Usar máis rexistros con categoría
- QCommandLineParser usa -v para --version así que usar unicamente --verbose
- Retirar moreas de código duplicado de desktop{tojson,fileparser}.cpp
- Analizar os ficheiros de ServiceType ao ler ficheiros .desktop
- Facer de SharedMimeInfo un requisito opcional
- Retirar unha chamada a QString::squeeze()
- desktopparser: evitar descodificacións innecesarias de UTF-8
- desktopparser: non engadir outra entrada se a entrada remata nun separador
- KPluginMetaData: avisar cando unha entrada de lista non é unha lista JSON
- Engadir mimeTypes() a KPluginMetaData

### KCrash

- Mellorar a busca de drkonqui e manter silencio de maneira predeterminada se non se atopa

### KDeclarative

- Agora poden consultarse a ConfigPropertyMap as opcións de configuración inmutábeis mediante o método isImmutable(key)
- Sacar QJSValue no mapa de propiedades de configuración
- EventGenerator: Engadir a posibilidade de enviar eventos de roda
- corrixir un initialSize de QuickViewSharedEngine perdido ao inicializar.
- corrixir unha regresión crítica de QuickViewSharedEngine da remisión 3792923639b1c480fd622f7d4d31f6f888c925b9
- facer que o tamaño da vista indicada previamente preceda o tamaño de obxecto inicial en QuickViewSharedEngine

### KDED

- Facer doctools opcional

### Compatibilidade coa versión 4 de KDELibs

- Non intentar almacenar un QDateTime en memoria asignada con mmap
- Sincronizar e adoptar uriencode.cmake de kdoctools.

### KDesignerPlugin

- Engadir KCollapsibleGroupBox

### KDocTools

- actualizar as entradas de pt_BR

### KGlobalAccel

- Non desprazas XOR para KP_Enter (fallo 128982)
- Capturar todas as teclas para un símbolo (fallo 351198)
- Non obter keysyms dúas veces por cada golpe de tecla

### KHTML

- Corrixir a impresión desde KHTMLPart definindo correctamente o pai de printSetting

### KIconThemes

- Agora kiconthemes permite temas incrustados en recursos de Qt dentro do prefixo :/icons como o propio Qt fai en QIcon::fromTheme
- Engadir dependencias necesarias que faltan

### KImageFormats

- Recoñecer image/vnd.adobe.photoshop en vez de image/x-psd
- Reverteuse parcialmente d7f457a para evitar unha quebra ao saír da aplicación

### KInit

- Facer doctools opcional

### KIO

- Gardar o URL de proxy co esquema correcto
- Distribuír os «novos modelos de ficheiro» coa biblioteca KIOFileWidgets usando un .qrc (fallo 353642)
- Xestionar os clics centrais de maneira axeitada en navigatormenu
- Permitir despregar kio_http_cache_cleaner nos instaladores e paquetes de aplicación
- KOpenWithDialog: Corrixir que se crease un ficheiro de escritorio con tipo MIME baleiro
- Ler a información do protocolo dos metadatos do complemento
- Permitir o despregado local de kioslave
- Engadir un «.protocol» ao JSON convertido
- Corrixir unha emisión dupla do resultado e un aviso que faltaba cando a lista chega a un cartafol inaccesíbel (fallo 333436)
- Preservar os destinos das ligazóns relativas ao copiar ligazóns simbólicas (fallo 352927)
- Usar iconas axeitadas para os cartafoles predeterminados no cartafol persoal do usuario (fallo 352498)
- Engadir unha interface que permite aos complementos mostrar iconas de capa personalizadas
- Facer opcional a dependencia de KNotifications en KIO (kpac)
- Facer opcional a combinación de doctools e wallet
- Evitar quebras de KIO se non hai un servidor de D-Bus en execución
- Engadir KUriFilterSearchProviderActions para mostrar unha lista de accións para buscar texto usando atallos web
- Mover as entradas do menú «Crear un novo» de kde-baseapps/lib/konq a kio (fallo 349654)
- Mover konqpopupmenuplugin.desktop de kde-baseapps a kio (fallo 350769)

### KJS

- Usar a variábel global «_timezone» para MSVC en vez de «timezone». Corrixe a construción con MSVC 2015.

### KNewStuff

- Corrixir o ficheiro de escritorio e o URL da páxina web do «Xestor de particións de KDE»

### KNotification

- Agora que kparts xa non necesita knotifications, só aquelas cousas que de verdade queren notificación requiren esa infraestrutura
- Engadir unha descrición, o motivo da fala e Phonon
- Facer opcional a dependencia de Phonon, un cambio puramente interno, como se fai coa fala.

### KParts

- Usar deleteLater en Part::slotWidgetDestroyed().
- Retirar a dependencia KNotifications de KParts
- Usar unha función para consultar onde está ui_standards.rc en vez de definilo manualmente, permite que funcionen os recursos de reserva

### KRunner

- RunnerManager: simplificar o código de carga de complementos

### KService

- KBuildSycoca: gardar sempre, incluso se non se detectou ningún cambio no ficheiro .desktop. (fallo 353203)
- Facer doctools opcional
- kbuildsycoca: analizar todos os ficheiros mimeapps.list que se mencionan na nova especificación.7
- Usar a maior data do subdirectorio como data do directorio do recurso.
- Manter os tipos MIME separados ao converter KPluginInfo en KPluginMetaData

### KTextEditor

- salientado: gnuplot: engadir a extensión .plt
- corrixir o consello de validación, grazas a «Thomas Jarosch» &lt;thomas.jarosch@intra2net.com&gt;, engadir ademais un consello sobre a validación en tempo de compilación
- Non quebrar cando a orde non está dispoñíbel.
- Corrixir o fallo #307107
- Variábeis de salientado de Haskell que comezan por _
- simplificar a inicialización de git2 dado que requirimos unha versión suficientemente recente (fallo 353947)
- empaquetar as configuracións predeterminadas no recurso
- realce de sintaxe (d-g): usar os estilos predeterminados en vez de cores definidas a man
- mellor busca de scripts, cousas locais de primeiro usuario, a continuación as cousas dos recursos, logo o resto de cousas, dese xeito o usuario pode sobrescribir os scripts distribuídos polos locais
- empaquetar todas as cousas de JavaScript tamén en recursos, só 3 ficheiros de configuración que faltan e ktexteditor poderían usarse como unha biblioteca sen ningún ficheiro empaquetado
- seguinte intento: poñer todos os ficheiros de sintaxe de XML empaquetados nun recurso
- engadir un atallo de cambio de modo de entrada (fallo 347769)
- empaquetar os ficheiros XML no recurso
- realce de sintaxe (a-c): migrar aos novos estilos predeterminados, retirar cores definidas a man
- realce de sintaxe: retirar cores definidas a man e usar os estilos predeterminados no seu lugar
- realce de sintaxe: usar os novos estilos predeterminados (retira as cores definidas manualmente)
- Mellor estilo predeterminado de «Import»
- Introducir «Gardar como con codificación» para gardar un ficheiro cunha codificación distinta, usando un bonito menú agrupado de codificacións que temos e substituíndo todos os diálogos de gardar coas correctas do sistema operativo sen perder esta importante funcionalidade.
- empaquetar o ficheiro de interface de usuario na biblioteca usando a miña extensión de XMLGUI
- A impresión volve a respectar a fonte e o esquema de cores seleccionados (fallo 344976)
- Usar cores de Breeze para as liñas gardadas e modificadas
- Melloráronse as cores predeterminadas do bordo das iconas do esquema «Normal»
- chaves automáticas: só inserir unha chave cando o seguinte carácter estea baleira ou non sexa unha letra ou número
- autobrace: ao retirar o paréntese inicial con Borrar, retirar tamén o final
- autobrace: estabelecer a conexión unha única vez
- Chaves automáticas: comer os parénteses de peche cando se dean algunhas condicións
- Corrixir que shortcutoverride non se pasase a mainwindow
- Fallo 342659 - A cor predeterminada de «realce de corchetes» é difícil de ver (corrixiuse o esquema normal) (fallo 342659)
- Engadir cores predeterminadas axeitadas para a cor de «Número de liña actual»
- parellas de parénteses e parénteses automáticos: compartir código
- parellas dos parénteses: gardar contra valores negativos de maxLines
- coincidencia de corchetes: que un novo intervalo coincida co anterior non significa que non sexa necesario actualizar
- Engadir a anchura da metade dun espazo para permitir debuxar o cursor na fin de liña
- corrixir algúns problemas de HiDPI no bordo das iconas
- corrixir o fallo #310712: retirar os espazos ao final tamén na liña co cursor (fallo 310712)
- só mostrar a mensaxe «mark set» cando o modo de entrada de vi está activo
- retirar &amp; do texto do botón (fallo 345937)
- corrixir a actualización da cor do número de liña actual (fallo 340363)
- facer que se insiran corchetes ao escribir un corchete sobre unha selección (fallo 350317)
- Parénteses automáticos (fallo 350317)
- corrixir alerta HL (fallo 344442)
- non hai desprazamento de columna co axuste dinámico de palabras
- lembrar se o usuario definiu o realce a través das sesións para non perdelo ao gardar tras restaurar (fallo 332605)
- corrixir o pregado de tex (fallo 328348)
- corrixiuse o fallo #327842: a fin dos comentarios de estilo C detéctase mal (fallo 327842)
- gardar e restaurar o axuste de palabras dinámico ao gardar ou restaurar sesións (fallo 284250)

### KTextWidgets

- Engadir un novo menú subordinado a KTextEdit para cambiar de idioma de corrección ortográfica
- Corrixir a carga da configuración predeterminada de Sonnet

### Infraestrutura de KWallet

- Usar KDE_INSTALL_DBUSINTERFACEDIR para instalar interfaces de D-Bus
- Corrixíronse os avisos do ficheiro de configuración de KWallet ao iniciar sesión (fallo 351805)
- Prefixar correctamente a saída de kwallet-pam

### KWidgetsAddons

- Engadir o trebello contedor pregábel KCollapsibleGroupBox
- KNewPasswordWidget: inicialización de cor que faltaba
- Introducir KNewPasswordWidget

### KXMLGUI

- kmainwindow: encher previamente a información do tradutor cando estea dispoñíbel (fallo 345320).
- Permitir asociar a clave do menú contextual (inferior dereita) aos atallos (fallo 165542)
- Engadir unha función para consultar a ruta do ficheiro XML de estándares
- Permitir usar a infraestrutura kxmlgui sen ficheiros instalados
- Engadir dependencias necesarias que faltan

### Infraestrutura de Plasma

- Corrixir que os elementos da barra de lapelas se amontoen xuntos durante a creación inicial, como se pode observar por exemplo en Kickoff tras iniciarse Plasma
- Corrixir que soltar ficheiros no escritorio ou no panel non ofreza unha selección de accións para escoller
- Ter QApplication::wheelScrollLines en conta desde ScrollView
- Usar BypassWindowManagerHint só na plataforma X11
- eliminar o fondo vello de panel
- animación de carga máis lexíbel en tamaños pequenos
- view-history colorado
- calendario: Permitir premer toda a zona da cabeceira
- calendario: non usar o número de día actual en goToMonth
- calendar: Corrixir a actualización do resumo da década
- Usar o tema para as iconas de Breeze cando se carguen mediante IconItem
- Corrixir a propiedade minimumWidth de Button (fallo 353584)
- Introducir o sinal appletCreated
- Icona de Breeze de Plasma: engadir elementos de identificador de SVG á área táctil
- Icona de Breeze de Plasma: cambiar Touchpad ao tamaño 22×22 px
- Icona de Breeze: engadir a icona de trebello ás notas
- Un scripts para substituír cores definidas a man con follas de estilos
- Aplicar SkipTaskbar en ExposeEvent
- Non definir SkipTaskbar en todos os eventos

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
