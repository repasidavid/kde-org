---
aliases:
- ../../kde-frameworks-5.31.0
date: 2017-02-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Xeral

Many modules now have python bindings.

### Attica

- Engadir compatibilidade con display_name (nome para mostrar) nas categorías

### Iconas de Breeze

- demasiados cambios de iconas para listalos aquí

### Módulos adicionais de CMake

- Activar -Wsuggest-override para g++ ≥ 5.0.0
- Pasar -fno-operator-names cando se poida
- ecm_add_app_icon: ignorar os ficheiros SVG silenciosamente cando non se permitan
- API para outras linguaxes: Moitas correccións e melloras

### Integración de infraestruturas

- Permitir algunhas das preguntas de KNSCore usando notificacións

### KArchive

- Corrixir (a busca de) KCompressionDevice para que funcione con Qt ≥ 5.7

### KAuth

- Actualizar a meirande parte dos exemplos, retirar os obsoletos

### KConfig

- Corrixir a ligazón en Windows: non ligar kentrymaptest a KConfigCore

### KConfigWidgets

- Non facer nada en ShowMenubarActionFilter::updateAction se non hai barras de menú

### KCoreAddons

- Corrixir o fallo 363427 - hai caracteres inseguros analizados incorrectamente como parte do URL (fallo 363427)
- kformat: Permitir traducir correctamente datas relativas (fallo 335106)
- KAboutData: Documentar que o enderezo de correo electrónico do fallo tamén pode ser un URL

### KDeclarative

- [IconDialog] Definir os grupos de iconas correctos
- [QuickViewSharedEngine] Usar setSize en vez de setWidth e setHeight

### Compatibilidade coa versión 4 de KDELibs

- Sincronizar co KDE4Defaults.cmake de kdelibs
- Corrixir a comprobación de CMake HAVE_TRUNC

### KEmoticons

- KEmoticons: usar D-Bus para notificar aos procesos en execución sobre cambios feitos no KCM
- KEmoticons: mellora significativa de rendemento

### KIconThemes

- KIconEngine: centrar a icona no rectángulo solicitado

### KIO

- Engadir KUrlRequester::setMimeTypeFilters
- Corrixir a análise de listados de directorios nun servidor FTP específico (fallo 375610)
- preservar o grupo e o dono ao copiar un ficheiro (fallo 103331)
- KRun: marcar runUrl() como obsoleto en favor de runUrl() con RunFlags
- kssl: Asegurarse de crear o directorio de certificados de usuario antes de usalo (fallo 342958)

### KItemViews

- Aplicar o filtro ao proxy de maneira inmediata

### KNewStuff

- Permitir adoptar recursos, principalmente para configuración do sistema
- Non fallar ao mover o directorio temporal ao instalar
- Marcar a clase de seguranza como obsoleta
- Non bloquear ao executar a orde de post-instalación (fallo 375287)
- [KNS] Ter en conta o tipo de distribución
- Non preguntar se o ficheiro se obtén de /tmp

### KNotification

- Engadir de novo notificacións de rexistro a ficheiros (fallo 363138)
- Marcar as notificacións non persistentes como transitorias
- Engadir compatibilidade coas «accións predeterminadas»

### Infraestrutura KPackage

- Non xerar AppData se se marca como NoDisplay
- Corrixir a lista cando a ruta solicitada é absoluta
- corrixir a xestión de arquivos cun cartafol neles (fallo 374782)

### KTextEditor

- corrixir a renderización do minimapa en ambientes con HiDPI

### KWidgetsAddons

- Engadir métodos para agochar a acción de mostrar o contrasinal
- KToolTipWidget: non facerse dono do trebello de contido
- KToolTipWidget: agochar inmediatamente se o contido se destrúe
- Corrixir a sobreposición de foco en KCollapsibleGroupBox
- Corrixir un aviso ao destruír un KPixmapSequenceWidget
- Instalar tamén cabeceiras adiantadas con MaiúsculasIniciais de clases de cabeceiras de varias clases
- KFontRequester: atopar a coincidencia máis parecida dunha fonte que falte (fallo 286260)

### KWindowSystem

- Permitir que Tab se modifique con Maiús (fallo 368581)

### KXMLGUI

- Informador de fallos: permitir un URL (non só un enderezo de correo electrónico) para informes personalizados
- Saltar atallos baleiros na comprobación de ambigüidade

### Infraestrutura de Plasma

- [Interface de contedor] values() non é necesario, contains() busca claves
- Diálogo: agochar cando o foco cambia a ConfigView con hideOnWindowDeactivate
- [PlasmaComponents Menu] Engadir a propiedade maximumWidth
- Falta dunha icona ao conectarse a OpenVPN mediante unha rede Bluetooth (fallo 366165)
- Asegurarse de mostrar un ListItem activado ao cubrilo
- facer todas as alturas da cabeceira do calendario iguais (fallo 375318)
- corrixir o estilo de cor na icona de Plasma de rede (fallo 373172)
- Definir o wrapMode como Text.WrapAnywhere (fallo 375141)
- actualizar a icona de KAlarm (fallo 362631)
- pasar correctamente o estado dos miniaplicativos ao contedor (fallo 372062)
- Usar KPlugin para cargar os complementos de Calendar
- usar a cor de salientar para o texto seleccionado (fallo 374140)
- [Elemento de icona] Arredondar o tamaño no que queremos cargar un mapa de píxeles
- a propiedade de retrato non é relevante cando non hai texto (fallo 374815)
- Corrixir as propiedades renderType de varios compoñentes

### Solid

- Corrixir un fallo de redondeo de obtención de propiedade de D-Bus (fallo 345871)
- Tratar a falta de frase de paso como o erro Solid::UserCanceled

### Sonnet

- Engadiuse o ficheiro de datos de trigrama para grego
- Corrixir o fallo de segmento na xeración de trigramas e expoñer a constante MAXGRAMS na cabeceira
- Buscar un libhunspell.so sen versión, debería ser máis estábel

### Realce da sintaxe

- Salientado de C++: actualizar a Qt 5.8

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
