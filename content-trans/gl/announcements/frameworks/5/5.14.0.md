---
aliases:
- ../../kde-frameworks-5.14.0
date: 2015-09-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### En moitas infraestruturas

- Cambiar de nome as clases privadas para evitar exportalas accidentalmente

### Baloo

- Engadir a interface org.kde.baloo ao obxecto raíz para manter compatibilidade cara atrás
- Instalar un org.kde.baloo.file.indexer.xml falso para corrixir a compilación de plasma-desktop 5.4
- Reorganizar as interfaces de D-Bus
- Usar metadatos en JSON no complemento de kded e corrixir o nome do complemento
- Crear unha instancia de Database por proceso (fallo 350247)
- Evitar que se mate a baloo_file_extractor mentres está remitindo
- Xerar un ficheiro XML de interface usando qt5_generate_dbus_interface
- Correccións do monitor de Baloo
- Mover a exportación do URL do ficheiro ao fío principal
- Asegurarse de que as configuracións en cascada se teñen en conta
- Non instalar namelink para a biblioteca privada
- Instalar traducións, detectado por Hrvoje Senjan.

### BluezQt

- Non retransmitir o sinal deviceChanged tras retirar o dispositivo (fallo 351051)
- Respectar -DBUILD_TESTING=OFF

### Módulos adicionais de CMake

- Engadir un macro para xerar declaracións de categoría de rexistro para Qt 5.
- ecm_generate_headers: Engadir a opción COMMON_HEADER e funcionalidade de varias cabeceiras
- Engadir -pedantic para código de KF5 (ao usar gcc ou clang)
- KDEFrameworkCompilerSettings: só activar os iteradores estritos en modo de depuración
- Definir tamén agochada como visibilidade predeterminada para código en C.

### Integración de infraestruturas

- Propagar tamén os títulos de xanela para os diálogos de ficheiro de só cartafoles.

### KActivities

- Só crear un cargador de acción (fío) cando as accións do FileItemLinkingPlugin non están inicializadas (fallo 351585)
- Corrixir os problemas de corrección cambiando de nome as clases privadas (11030ffc0)
- Engadir a inclusión de boost que falta para construír en OS X
- A configuración dos atallos moveuse á configuración da actividade
- Configurar o modo de actividade privada funciona
- Cambios internos na interface de usuario da configuración
- Os métodos de actividade básica funcionan
- Interface de usuario para a configuración da actividade e as xanelas emerxentes de eliminación
- Interface de usuario básica para a sección de creación, eliminación e configuración de actividades en KCM
- Aumentouse o tamaño dos anacos para cargar os resultados
- Engadiuse unha inclusión que faltaba para std::set

### Ferramentas de Doxygen de KDE

- Corrección para Windows: retirar ficheiros existentes antes de substituílos con os.rename.
- Usar rutas nativas ao chamar a Python para corrixir construcións para Windows

### KCompletion

- Corrixir un mal comportamento ou esgotamento de memoria en Windows (fallo 345860)

### KConfig

- Optimizar readEntryGui
- Evitar QString::fromLatin1() no código xerado
- Minimizar as chamadas a QStandardPaths::locateAll(), que require recursos significativos
- Rematar a migración a QCommandLineParser (agora ten addPositionalArgument)

### Compatibilidade coa versión 4 de KDELibs

- Migrar o complemento de kded solid-networkstatus a metadatos en JSON
- KPixmapCache: crear o directorio se non existe

### KDocTools

- Sincronizar as user.entities en catalán coa versión en inglés.
- Engadir entidades para sebas e plasma-pa

### KEmoticons

- Rendemento: gardar en caché unha instancia de KEmoticons, non de KEmoticonsTheme.

### KFileMetaData

- PlainTextExtractor: activar a rama O_NOATIME nas plataformas que usan libc de GNU
- PlainTextExtractor: facer que a rama de Linux funcione tamén sen O_NOATIME
- PlainTextExtractor: corrixir a comprobación de erro nos fallos de open(O_NOATIME)

### KGlobalAccel

- Só iniciar kglobalaccel5 se se necesita.

### KI18n

- Xestionar correctamente a falta de salto de liña ao final dun ficheiro pmap

### KIconThemes

- KIconLoader: corrixir que reconfigure() esquecese os temas herdados e os directorios de aplicacións
- Adherir mellor á especificación de carga de iconas

### KImageFormats

- eps: corrixir inclusións relacionadas co rexistro con categoría de Qt

### KIO

- Usar Q_OS_WIN en vez de Q_OS_WINDOWS
- Facer que KDE_FORK_SLAVES baixo Windows
- Desactivar a instalación do ficheiro de escritorio para o módulo de kded ProxyScout
- Fornecer unha orde determinista para KDirSortFilterProxyModelPrivate::compare
- Mostrar de novo as iconas de cartafol personalizadas (fallo 350612)
- Mover kpasswdserver de kded a kiod
- Corrixir fallos de migración en kpasswdserver
- Retirar código vello para comunicarse con versións extremadamente vellas de kpasswdserver.
- KDirListerTest: usar QTRY_COMPARE en ambas sentencias para corrixir unha condición de carreira exposta polo sistema de integración continua
- KFilePlacesModel: completar unha vella tarefa pendente sobre usar trashrc en vez de un KDirLister completo.

### KItemModels

- Novo proxymodel: KConcatenateRowsProxyModel
- KConcatenateRowsProxyModelPrivate: corrixir a xestión de layoutChanged.
- Máis comprobacións na selección tras ordenar.
- KExtraColumnsProxyModel: corrixir un fallo en sibling() que rompeu, por exemplo, as seleccións

### Infraestrutura de paquetes

- kpackagetool pode desinstalar un paquete desde un ficheiro de paquete
- kpackagetool é agora máis intelixente para atopar o servicetype axeitado

### KService

- KSycoca: comprobar as datas e executar kbuildsycoca se se necesita. Xa non require kded.
- Non pechar ksycoca xusto despois de abrilo.
- Agora KPluginInfo xestiona correctamente metadatos de FormFactor

### KTextEditor

- Fusionar asignación de TextLineData e o bloque de conta de referencias.
- Cambiar o atallo de teclado predeterminado para «ir á liña de edición anterior»
- Correccións de salientado de sintaxe de comentarios de Haskell
- Acelerar a aparición da xanela emerxente de completado de código
- minimap: Intentar mellorar a aparencia (fallo 309553)
- comentarios aniñados no salientado de sintaxe de Haskell
- Corrixir un problema de sangrado incorrecto en Python (fallo 351190)

### KWidgetsAddons

- KPasswordDialog: permitir ao usuario cambiar a visibilidade do contrasinal (fallo 224686)

### KXMLGUI

- Corrixir que KSwitchLanguageDialog non mostrase a maior parte dos idiomas

### KXmlRpcClient

- Evitar QLatin1String cando asigna memoria na rima

### ModemManagerQt

- Corrixir o conflito de metatipo co último cambio de nm-qt

### NetworkManagerQt

- Engadíronse novas propiedades das últimas capturas ou versións de NM

### Infraestrutura de Plasma

- cambiar o pai a sacudíbel se se pode
- corrixir a lista de paquetes
- plasma: Corrixir que as accións do miniaplicativo poidan ser punteiros nulos (fallo 351777)
- Agora o sinal onClicked de PlasmaComponents.ModelContextMenu funciona correctamente
- Agora o ModelContextMenu de PlasmaComponents pode crear seccións de Menu
- Migrar o complemento de kded platformstatus a metadatos en JSON…
- Xestionar metadatos incorrectos en PluginLoader
- Permitir que RowLayout determine o tamaño da etiqueta
- mostrar sempre o menú de editar cando o cursor estea visíbel
- Corrixir o ciclo en ButtonStyle
- Non cambiar a calidade de plano dun botón ao premelo
- en pantallas táctiles e móbiles as barras de desprazamento son transitorias
- axustar a velocidade de sacudida e deceleración a ppp
- delegado de cursor personalizado só en móbil
- cursor de texto axeitado para toque
- corrixir a política de pai e de emerxencia
- declarar __editMenu
- engadir que a falta de cursor xestione delegados
- reescribir EditMenu
- usar o menú para móbiles só de maneira condicional
- cambiar o pai do menú á raíz

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
