---
aliases:
- ../../kde-frameworks-5.43.0
date: 2018-02-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Novos módulos

KHolidays: Holiday calculation library

This library provides a C++ API that determines holiday and other special events for a geographical region.

Purpose: Offers available actions for a specific purpose

This framework offers the possibility to create integrate services and actions on any application without having to implement them specifically. Purpose will offer them mechanisms to list the different alternatives to execute given the requested action type and will facilitate components so that all the plugins can receive all the information they need.

### Baloo

- balooctl status: Producir saída que se poida analizar
- Corrixir as copias profundas de cartafoles etiquetados co escravo de KIO. Isto rompe a funcionalidade de listar cartafoles etiquetados na árbore de etiquetas, pero é mellor que ter copias rotas
- Non poñer na cola ficheiros que xa non se poden indexar e retiralos do índice inmediatamente
- Eliminar do índice ficheiros movidos que xa non poden indexarse

### Iconas de Breeze

- Engadir iconas de Krusader que faltaban para a sincronización de cartafoles (fallo 379638)
- Actualizar a icona list-remove con - en vez de a icona de cancelar (fallo 382650)
- engadir iconas para o plasmoide de PulsAudio (fallo 385294)
- usar a mesma opacidade, 0,5, en todas partes
- Nova icona para VirtualBox (fallo 7384357)
- facer weather-fog neutral de día e de noite (fallo 388865)
- instalar de verdade o contexto das novas animacións
- Os MIME de ficheiro de QML teñen agora a mesma aparencia en todos os tamaños (fallo 376757)
- Actualizar as iconas de animacións (fallo 368833)
- engadir unha icona colorada de emblem-shared
- Corrixir os ficheiros de index.theme rotos, faltaba «Context=Status» en status/64
- Retirar o permiso de execución dos ficheiros SVG
- A iconas de acción download lígase con edit-download (fallo 382935)
- Actualizar o tema de iconas da área de notificacións de Dropbox (fallo 383477)
- Falta emblem-default-symbolic (fallo 382234)
- Tipo no nome de ficheiro do tipo MIME (fallo 386144)
- Usar un logo de Octave máis específico (fallo 385048)
- engadir iconas de caixas fortes (fallo 386587)
- cambiar o tamaño en píxeles das iconas de estado (fallo 386895)

### Módulos adicionais de CMake

- FindQtWaylandScanner.cmake: Usar qmake-query para HINT
- Asegurarse de buscar qmlplugindump baseado en Qt 5
- ECMToolchainAndroidTest xa non existe (fallo 389519)
- Non definir LD_LIBRARY_PATH en prefix.sh
- Engadir FindSeccomp a find-modules
- Usar o nome de idioma como reserva para a busca de traducións se o nome da configuración rexional falla
- Android: Engadir máis inclusións

### KAuth

- Corrixir unha regresión de ligazón introducida en 5.42.

### KCMUtils

- Engade consellos aos dous botóns de cada entrada

### KCompletion

- Corrixir unha emisión incorrecta de textEdited() por parte de KLineEdit (fallo 373004)

### KConfig

- Usar Ctrl+Maiús+, como atallo estándar para «Configurar &lt;Programa&gt;»

### KCoreAddons

- Coincidir tamén as claves spdx LGPL-2.1 e >LGPL-2.1
- Usar o método urls() de QMimeData, moito máis rápido (fallo 342056)
- Optimizar a infraestrutura de KDirWatch de inotify: asociar o wd de inotify a Entry
- Optimizar: usar QMetaObject::invokeMethod con functor

### KDeclarative

- [ConfigModule] Usar de novo o contexto e o motor de QML se os hai (fallo 388766)
- [ConfigPropertyMap] Engadir unha inclusión que faltaba
- [ConfigPropertyMap] Non emitir valueChanged durante a creación inicial

### KDED

- Non exportar kded5 como destino de CMake

### Compatibilidade coa versión 4 de KDELibs

- Facer cambios internos en Solid::NetworkingPrivate para ter código tanto compartido como específico de plataforma
- Corrixir o erro de compilación de mingw «src/kdeui/kapplication_win.cpp:212:22: erro: «kill» non se declarou neste ámbito»
- Corrixir o nome de D-Bus de kded nas instrucións de solid-networking

### KDesignerPlugin

- Facer opcional a dependencia de kdoctools

### KDESU

- Facer que o modo KDESU_USE_SUDO_DEFAULT volva construírse
- Facer que kdesu funcione cando PWD está en /usr/bin

### KGlobalAccel

- Usar a función de CMake «kdbusaddons_generate_dbus_service_file» desde kdbusaddons para xerar un ficheiro de servizo de D-Bus (fallo 382460)

### Complementos de interface gráfica de usuario de KDE

- Corrixir a ligazón do ficheiro QCH creado coa documentación de QtGui

### KI18n

- Corrixir o descubrimento de libintl ao usar compilación cruzada de paquetes nativos de Yocto

### KInit

- Corrixir a compilación cruzada con MinGW (MXE)

### KIO

- Reparar a copia de ficheiros a VFAT sen avisos
- kio_file: saltar a xestión de erros para permisos iniciais durante a copia de ficheiros
- kio_ftp: non emitir un sinal de erro antes de intentar todas as ordes de lista (fallo 387634)
- Rendemento: usar o obxecto KFileItem de destino para determinar se permite escritura en vez de crear KFileItemListProperties
- Rendemento: Usar o construtor de copia de KFileItemListProperties en vez da conversión de KFileItemList a KFileItemListProperties. Isto evita avaliar de novo todos os elementos
- Mellorar a xestión de erros no escravo de entrada e saída de ficheiros
- Retirar a marca de traballo PrivilegeExecution
- KRun: permitir executar «engadir un cartafol de rede» sen un diálogo de confirmación
- Permitir filtrar lugares por nome de aplicación alternativo
- [Fornecedor de busca de filtro de URI] Evitar unha eliminación dupla (fallo 388983)
- Corrixir unha sobreposición do primeiro elemento en KFilePlacesView
- Desactivar temporalmente a compatibilidade con KAuth en KIO
- previewtest: permitir indicas os complementos activados
- [KFileItem] Usar «emblem-shared» para ficheiros compartidos
- [DropJob] Permitir arrastras e soltar nun cartafol de só lectura
- [FileUndoManager] Permitir desfacer cambios en cartafoles de só lectura
- Engadir a posibilidade dunha execución privilexiada de traballos de KIO (desactivado temporalmente nesta versión)
- Engadir a posibilidade de compartir descritores de ficheiro entre escravos de KIO de ficheiros e o seu asistente de KAuth
- Corrixir KFilePreviewGenerator::LayoutBlocker (fallo 352776)
- Agora KonqPopupMenu/Plugin pode usar a clave X-KDE-RequiredNumberOfUrls para requirir seleccionar un número concreto de ficheiros que se mostren
- [KPropertiesDialog] Permitir o axuste de palabras para a descrición da suma de comprobación
- Usar a función de CMake «kdbusaddons_generate_dbus_service_file» desde kdbusaddons para xerar un ficheiro de servizo de D-Bus (fallo 388063)

### Kirigami

- compatibilidade con ColorGroups
- retirar a resposta a clic se o elemento non quere eventos de rato
- apaño para aplicacións que usan elementos de lista incorrectamente
- espazo para a barra de desprazamento (fallo 389602)
- Fornecer un consello para a acción principal
- CMake: usar a variábel oficial de CMake para construír como un complemento estático
- Actualizar a designación dos niveis para humanos na documentación da API
- [ScrollView] Desprazar unha páxina con Maiús+roda
- [PageRow] Navegar entre niveis cos botóns de adiante e atrás do rato
- Asegurarse de que DesktopIcon debuxa as proporcións correctas (fallo 388737)

### KItemModels

- KRearrangeColumnsProxyModel: corrixir unha quebra cando non hai modelo de orixe
- KRearrangeColumnsProxyModel: cambiar o código de sibling() para que funcione como se espera

### KJobWidgets

- Desduplicación de código en byteSize(double size) (fallo 384561)

### KJS

- Facer opcional a dependencia de kdoctools

### KJSEmbed

- Deixar de exportar kjscmd
- Facer opcional a dependencia de kdoctools

### KNotification

- Corrixiuse a acción de notificación de «Executar unha orde» (fallo 389284)

### KTextEditor

- Corrección: a vista salta cando se activa o desprazamento alén da fin do documento (fallo 306745)
- Usar polo menos a anchura solicitada para a árbore de consellos de argumento
- ExpandingWidgetModel: atopar a columna da dereita de todo segundo o lugar

### KWidgetsAddons

- KDateComboBox: corrixir que dateChanged() non se emita tras escribir unha data (fallo 364200)
- KMultiTabBar: Corrixir unha regresión na conversión ao novo estilo de connect()

### Infraestrutura de Plasma

- Definir unha propiedade en Units.qml para os estilos de Plasma
- windowthumbnail: Corrixir o código de selección de GLXFBConfig
- [Consello predeterminado] Corrixir a xestión do tamaño (fallo 389371)
- [Diálogo de Plasma] Chamar aos efectos de xanela só se son visíbeis
- Corrixir unha fonte de mensaxes de rexistro excesivas á que se fai referencia no fallo 388389 (pasouse un nome de ficheiro baleiro á función)
- [Calendario] Axustar as áncoras da barra de ferramentas do calendario
- [ConfigModel] Definir o contexto de QML en ConfigModule (fallo 388766)
- [Tema de iconas] Tratar as fontes que comecen por unha barra inclinada como ficheiros locais
- corrixir a aparencia de RTL en ComboBox (fallo 387558)

### QQC2StyleBridge

- Engadir BusyIndicator á lista de controis con estilo
- retirar o escintileo ao cubrir a barra de desprazamento

### Solid

- [UDisks] Só ignorar o ficheiro de respaldo non de usuario se se coñece (fallo 389358)
- Storage devices mounted outside of /media, /run/media, and $HOME are now ignored, as well as Loop Devices whose (bug 319998)
- [Dispositivo de UDisks] Mostrar o dispositivo de bucle co seu nome de ficheiro de respaldo e icona

### Sonnet

- Atopar dicionarios de Aspell en Windows

### Realce da sintaxe

- Corrixir a expresión regular de variábel de C#
- Permitir barras baixas en literais numéricos (Python 3.6) (fallo 385422)
- Salientar ficheiros Khronos Collada e glTF
- Corrixir o realce de ini de valores que conteñen os caracteres ; e #
- AppArmor: novas palabras clave, melloras e correccións

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
