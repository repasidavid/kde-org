---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDE publica a versión 16.12.1 das aplicacións de KDE
layout: application
title: KDE publica a versión 16.12.1 das aplicacións de KDE
version: 16.12.1
---
January 12, 2017. Today KDE released the first stability update for <a href='../16.12.0'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Esta versión corrixe un fallo de perda de datos no recurso de iCal que non podía crear std.ics se non existía.

As máis de 40 correccións de erros inclúen melloras en, entre outros, KDE PIM, Ark, Gwenview, Kajongg, Okular, Kate, Kdenlive.

This release also includes Long Term Support version of KDE Development Platform 4.14.28.
