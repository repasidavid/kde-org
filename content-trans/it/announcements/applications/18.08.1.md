---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE rilascia KDE Applications 18.08.1
layout: application
title: KDE rilascia KDE Applications 18.08.1
version: 18.08.1
---
6 settembre 2018. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../18.08.0'>KDE Applications 18.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Più di una dozzina di errori corretti includono, tra gli altri, miglioramenti a Kontact, Cantor, Gwenview, Okular e Umbrello.

I miglioramenti includono:

- Il componente KIO-MTP non si blocca più quando al dispositivo è già collegata un'altra applicazione
- L'invio di posta in KMail ora usa la password, quando specificata tramite richiesta password
- Okular ora ricorda la modalità della barra laterale dopo aver salvato documenti PDF
