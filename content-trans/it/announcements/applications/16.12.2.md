---
aliases:
- ../announce-applications-16.12.2
changelog: true
date: 2017-02-09
description: KDE rilascia KDE Applications 16.12.2
layout: application
title: KDE rilascia KDE Applications 16.12.2
version: 16.12.2
---
9 febbraio 2017. Oggi KDE ha rilasciato il secondo aggiornamento di stabilizzazione per <a href='../16.12.0'>KDE Applications 16.12</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 20 errori corretti includono, tra gli altri, miglioramenti a kdepim, Dolphin, Kate, Kdenlive, Ktouch e Okular.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.29.
