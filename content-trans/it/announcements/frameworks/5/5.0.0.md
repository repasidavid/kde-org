---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: KDE rilascia la prima versione di Frameworks 5.
layout: framework
qtversion: 5.2
title: Primo rilascio di KDE Frameworks 5
---
7 luglio 2014. La comunità KDE è orgogliosa di annunciare KDE Frameworks 5.0. Frameworks 5 è la generazione futura delle librerie KDE, rese modulari e ottimizzate per una facile integrazione nelle applicazioni Qt. Frameworks offre un'ampia varietà di funzionalità comunemente richieste comprese in librerie mature, revisionate e ben testate e con termini di licenza facili da capire. di questo rilascio fanno parte oltre 50 frameworks differenti che forniscono soluzioni includenti integrazione hardware, supporto al formato dei file, widget aggiuntivi, funzioni di progettazione, controllo ortografico e altro ancora. Molti framework sono multipiattaforma e contengono pochissime o nessuna dipendenza aggiuntive, rendendo semplice la loro compilazione e aggiunta a qualsiasi applicazione Qt.

KDE Frameworks rappresenta un sforzo di riscrittura delle potenti librerie KDE Platform 4 all'interno di un set di moduli indipendenti e multipiattaforma che saranno prontamente disponibili a tutti gli sviluppatori Qt per semplificare, accelerare e ridurre il costo dello sviluppo Qt. I singoli framework sono multipiattaforma, ben documentati e testati, il loro utilizzo sarà familiare agli sviluppatori Qt, e seguiranno lo stile e gli standard decisi da Qt Project. Frameworks è sviluppato in base al collaudato modello di governance KDE, con un programma di rilascio prevedibile, un processo di contribuzione chiaro e neutrale dal punto di vista commerciale, governance aperta e licenza flessibile (LGPL).

Frameworks possiede una struttura di dipendenze chiara, divisa in «Categories» (categorie) e «Tiers» (livelli). Le categorie si riferiscono alle dipendenze a tempo d'esecuzione:

- <strong>Functional</strong> gli elementi non hanno dipendenze a tempo di esecuzione.
- <strong>Integration</strong> denota il codice che può richiedere dipendenze a tempo d'esecuzione per l'integrazione, a seconda di quello che il sistema operativo o la piattaforma offre.
- <strong>Solutions</strong> hanno dipendenze obbligatorie a tempo d'esecuzione.

<strong>Tiers</strong> si riferisce alle dipendenze al tempo di compilazione su altri Frameworks. Tier 1 Frameworks non ha dipendenze all'interno di Frameworks e richiede solo Qt e altre librerie pertinenti. I Frameworks di Tier 2 possono dipendere solo da elementi in Tier 1. I Frameworks di Tier 3 possono dipendere da altri Frameworks di Tier 3, così come di Tier 2 e Tier 1.

La transizione da Platform a Frameworks è in corso da più di tre anni, guidata dai collaboratori tecnici di KDE più importanti. Per saperne di più su Frameworks 5 <a href='http://dot.kde.org/2013/09/25/frameworks-5'>leggi questo articolo dell'anno scorso</a>.

## Novità

Ci sono oltre 50 Frameworks attualmente disponibili. Consulta l'elenco completo <a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>nella documentazione in linea delle API</a>. Di seguito una rapida carrellata di alcune funzionalità offerte da Frameworks per gli sviluppatori di applicazioni Qt.

<strong>KArchive</strong> offre supporto a molti e popolari codici di compressione in una libreria indipendente per l'estrazione e la compressione di file facile da usare e ricca di funzionalità. Basta fornirle dei file: non è necessario reinventare una funzione di compressione nella tua applicazione basata su Qt!

<strong>ThreadWeaver</strong> fornisce un'API ad alto livello per gestire i thread usando interfacce basate su job e code. Permette una pianificazione semplice dell'esecuzione dei thread specificando le dipendenze tra i thread e la loro esecuzione soddisfacendo queste dipendenze, e semplificando molto l'uso di thread multipli. Questi moduli sono disponibili per la produzione già ora.

<strong>KConfig</strong> è un framework per l'archiviazione e il recupero delle impostazioni di configurazione. Contiene un'API orientata ai gruppi. Funziona con i file INI e le directory a cascata conformi a XDG. Genera codice basato su file XML.

<strong>Solid</strong> offre rilevamento dell'hardware e può informare un'applicazione sui dispositivi di memorizzazione e i volumi, la CPU, lo stato della batteria, la gestione energetica, lo stato della rete e delle interfacce, il Bluetooth. Per le partizioni cifrate e i servizi di gestione energetica e di rete è richiesta l'esecuzione di demoni.

<strong>KI18n</strong> aggiunge il supporto per Gettext alle applicazioni, rendendo facile integrare il lavoro di traduzione delle applicazioni Qt nell'infrastruttura di traduzione generale di molti progetti.
