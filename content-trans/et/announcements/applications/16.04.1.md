---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE toob välja KDE rakendused 16.04.1
layout: application
title: KDE toob välja KDE rakendused 16.04.1
version: 16.04.1
---
May 10, 2016. Today KDE released the first stability update for <a href='../16.04.0'>KDE Applications 16.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 25 teadaoleva veaparanduse hulka kuuluvad Kdepimi, Arki, Kate, Dolphini, Kdenlive'i, Lokalize, Spectacle'i ja teiste rakenduste täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.20.
