---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE toob välja KDE rakendused 15.08.1
layout: application
title: KDE toob välja KDE rakendused 15.08.1
version: 15.08.1
---
September 15, 2015. Today KDE released the first stability update for <a href='../15.08.0'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 40 teadaoleva veaparanduse hulka kuuluvad Kdelibsi, Kdepimi, Kdenlive'i, Dolphin, Marble'i, Kompare, Konsooli, Arki ja Umbrello täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.12.
