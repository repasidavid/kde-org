---
aliases:
- ../../kde-frameworks-5.27.0
date: 2016-10-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
New mimetypes icons.

{{<figure src="/announcements/frameworks/5/5.27.0/kf-5.27-mimetypes-icons.png" >}}

### Baloo

- Korrektse seadistuskirje kasutamine automaatkäivituse tingimuses
- Sorditud lisamise (flat_map laadis lisamise) parandus (veateade 367991)
- Puuduva keskkonna sulgemise lisamine tänu Loïc Yhuel'ile (veateade 353783)
- Tehingut ei loodud =&gt; neid ei püüta hüljata
- puuduva m_env = nullptr omistamise parandus
- nt Baloo::Query lõime muutmine turvaliseks
- 64-bitistes süsteemides võimaldab baloo nüüd indeksi salvestit &gt; 5 GB (veateade 364475)
- ctime/mtime == 0 lubamine (veateade 355238)
- baloo_file'i indekseerimisandmebaasi riknemise käitlemine kas andmebaasi taasluues või loobudes, kui see nurjub

### BluezQt

- Krahhi vältimine, kui seadet püütakse lisada tundmatule adapterile (veateade 364416)

### Breeze'i ikoonid

- Uued MIME tüüpide ikoonid
- Mõne kstars'i ikooni uuendamine (veateade 364981)
- Vales stiilis toimingute/24/piirdevorminduse määramine (veateade 368980)
- waylandi rakenduseikooni lisamine
- xorg'i rakenduseikooni lisamine (veateade 368813)
- Jaoamise juhuslikustamise tagasivõtmine, kalendri näitamine ning muundamise paranduse taasrakendamine (veateade 367082)
- dokumentide kataloogi muutmine ühefaililisest mitmefaililiseks, sest selles kataloogis on faile rohkem kui üks (veateade 368224)

### CMake'i lisamoodulid

- Tagamine, et appstream'i testi ei lisata kaks korda

### KActivities

- Tegevuste sortimine puhvris tähestiku järgi nime alusel (veateade 362774)

### KDE Doxygeni tööriistad

- Palju muudatusi genereeritud API dokumentatsiooni üldises paigutuses
- Siltide asukoha korrigeerimine vastavalt sellele, kas teek kuulub gruppi või mitte
- Otsing: gruppi mittekuulutavate teekide href'i parandus

### KArchive

- Mälulekke parandus KTar'i KCompressionDevice'is
- KArchive: mälulekke parandus, kui sama nimega kirje on juba olemas
- Mälulekke parandus KZip'is tühjade kataloogide käitlemisel
- K7Zip: mälulekete parandus tõrgete korral
- ASAN-i leitud mälulekke parandus, kui open() seadmes nurjub
- ASAN-i avastatud halva KFilterDev'i määramise eemaldamine

### KCodecs

- Puuduvate eksportmakrode lisamine klassidele Decoder ja Encoder

### KConfig

- ASAN-i leitud mälulekke parandus SignalsTestNoSingletonDpointe'is

### KCoreAddons

- QPair&lt;QString,QString&gt; registreerimine metatüübina KJobTrackerInterface'is
- url-i ei teisendata url-iks, milles on topeltjutumärk
- Windowsis kompileerimise parandus
- Väga vana vea parandus, kui eemaldame url-is tühimärgi , näiteks "foo &lt;&lt;url&gt; &lt;url&gt;&gt;"

### KCrash

- CMake'i valik KCRASH_CORE_PATTERN_RAISE kernelile edastamiseks
- Vaikimisi logimistaseme muutmine Warning'i pealt Info peale

### KDELibs 4 toetus

- Puhastus. Ei paigaldata kaasatuid, mis osutavad olematutele kaasatutele, ja ka nende failide eemaldamine
- Korrektsema ja c++11 pakutava std::remove_pointer'i kasutamine

### KDocTools

- "checkXML5 kirjutab korras docbook'ide korral genereeritud html-i standardväljundisse" parandus (veateade 369415)
- Parandus, kui ei suudetud omatööriistu käivitada ristkompileeritud kdoctools'i abil
- Sihtmärkide paikapanek ristkompileerimiseks teiste pakettide kdoctools'i kasutamisel
- Ristkompileerimise toetuse lisamine docbookl10nhelper'ile
- Ristkompileerimise toetuse lisamine meinproc5'ile
- checkxml5 teisendamine qt täitmisfailiks platvormiülese toetuse nimel

### KFileMetaData

- epub'i ekstraktori täiustamine, vähem segmendivigu (veateade 361727)
- odf'i indekseerija muutmine tõrkekindlamaks, kontrollimine, kas failid on ikka olemas (ja kas need on üldse failid) (meta.xml + content.xml)

### KIO

- KIO-moodulite parandus ainult tls1.0 kasutades
- API katkisuse parandus kio's
- KFileItemActions: addPluginActionsTo(QMenu *) lisamine
- Kopeerimisnuppude näitamine alles pärast kontrollsumma arvutamist
- Puuduva tagasiside lisamine kontrollsumma arvutamisel (veateade 368520)
- KFileItem::overlays tühjade stringväärtuste tagastamise parandus
- Terminali .desktop-failide Konsoolist käivitamise parandus
- nfs4 ühendatud ressursside liigitamine probablySlow alla, nagu näiteks ka nfs/cifs/...
- KNewFileMenu: uue kataloogi toimingu kiirklahvi näitamine (veateade 366075)

### KItemViews

- Loendivaate režiimis kasutatakse vaikimisi moveCursor'i teostust

### KNewStuff

- KAuthorized'i kontrollide lisamine võimaldamaks ghns keelamist kdeglobals'is (veateade 368240)

### Paketiraamistik

- appstream'i faile ei genereerita komponentide puhul, mis ei asu rdn'is
- kpackage_install_package work töötab nüüd koos KDE_INSTALL_DIRS_NO_DEPRECATED'ga
- Kasutamata muutuja KPACKAGE_DATA_INSTALL_DIR eemaldamine

### KParts

- Parandus: lõpukaldkriipsudega URL-e peeti alati kataloogideks

### KPeople

- ASAN-i ehitamise parandus (duplicates.cpp kasutab KPeople::AbstractContact'i, mis asub KF5PeopleBackend'is)

### KPty

- ECM asukoha kasutamine utempter'i binaarfaili leidmiseks, mis on palju usaldusväärsem kui lihtsalt cmake'i prefiks
- utempter'i abilise täitmisfaili käsitsi väljakutsumine (veateade 364779)

### KTextEditor

- XML-failid: väärtuste koodi kirjutatud värvi eemaldamine
- XML: väärtuste koodi kirjutatud värvi eemaldamine
- XML-skeemi definitsioon: 'version' muutmine xs:integer'iks
- Definitsioonifailide esiletõstmine: versiooni ümardamine järgmise täisarvuni
- mitmemärgihõive toetamine ainult [xxx] sees tagasilanguste vältimiseks
- Support regular expressions replaces with captures &gt; 9, e.g. 111 (bug 365124)
- Järgmisele reale ulatuvate märkide renderdamise parandus, nt. ei lõigata allajoonimist enam mõne fondi/fondisuuruse korral maha (veateade 335079)
- Krahhi vältimine: tagamine, et ekraanikursor oleks pärast teksti voltimist korrektne (veateade 367466)
- KateNormalInputMode vajab SearchBar'i sisestamise meetodite taaskäivitamist
- püüti "parandada" allajoonimise ja muu sellise renderdamist (veateade 335079)
- Nupu "Näita erinevust" näitamine ainult siis, kui "diff" on paigaldatud
- Mittemodaalse teate vidina kasutamine väliselt muudetud faili märguanneteks (veateade 353712)
- tagasilanguse parandus: testNormal töötas ainult seepärast, et testi täideti ainult ühel korral
- treppimistesti tükeldamine eraldi käikudeks
- Toimingut "Laienda tipptaseme sõlmed" toetatakse taas (veateade 335590)
- Krahhi vältimine, kui üleval või all näidatakse teateid mitu korda
- Režiimiridade realõpu seadistuse parandus (veateade 365705)
- .nix-failide esiletõstmine samamoodi bash'iga, vaevalt see kahjuks tuleb (veateade 365006)

### KWalleti raamistik

- Kontroll, kas kwallet on Wallet::isOpen(name)-s lubatud (veateade 358260)
- Puuduva boost'i päise lisamine
- Topeltotsingu eemaldamine KF5DocTools'is

### KWayland

- [server] vajutamata klahvide ja topelt vajutatud klahvide korral ei saadeta klahvi vabastamise signaali (veateade 366625)
- [server] lõikepuhvri valiku asendamisel tuleb eelmine andmeallikas hüljata (veateade 368391)
- Surface'i sisenemis/lahkumissündmuste toetuse lisamine
- [klient] kõigi loodud väljundite jälitamine ja staatilise get-meetodi lisamine

### KXmlRpcClient

- Kategooriate teisendamine org.kde.pim.*-i

### NetworkManagerQt

- Initsialiseerimise käigus on tarvis määrata olek
- Kõigi initsialiseerimise blokkivate väljakutsete asendamine üheainsa blokkiva väljakutsega
- Standardse o.f.DBus.Properties'i liidese kasutamine signaali PropertiesChanged jaoks NM 1.4.0+ korral (veateade 367938)

### Oxygeni ikoonid

- Vigase kataloogi eemaldamine index.theme'ist
- Topelttesti ülevõtmine breeze-icons'ist
- Kõigi topeltikoonide teisendamine nimeviitadeks

### Plasma raamistik

- Ajaarvesti väljundi täiustus
- [ToolButtonStyle] Menüü noole parandus
- i18n: stringide käitlemine kdevtemplate'i failides
- i18n: stringide ülevaatamine kdevtemplate'i failides
- removeMenuItem'i lisamine PlasmaComponents.ContextMenu'sse
- ktorrent'i ikooni uuendamine (veateade 369302)
- [WindowThumbnail] pixmap'i hülgamine kaardisündmuste korral
- kdeglobals'it ei kaasata, kui tegu on puhvri seadistusega
- Plasma::knownLanguages'i parandus
- vaate suuruse muutmine kohe pärast konteineri seadistamist
- Välditakse KPluginInfo loomist KPluginMetaData isendi põhjal
- Ülesannete täitmisel tuleb ometi kasutada mõningaid näidikuid
- Tegumiriba read vastavalt RR 128802 marco andis sellele heakskiidu
- [AppletQuickItem] silmus katkestatakse, kui leitakse paigutus

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
