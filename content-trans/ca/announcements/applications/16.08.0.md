---
aliases:
- ../announce-applications-16.08.0
changelog: true
date: 2016-08-18
description: KDE distribueix les aplicacions 16.08.0 del KDE
layout: application
title: KDE distribueix les aplicacions 16.08.0 del KDE
version: 16.08.0
---
18 d'agost de 2016. Avui KDE presenta les Aplicacions 16.08 de KDE, amb una gamma impressionant d'actualitzacions que milloren la facilitat d'accés, la introducció de funcionalitats molt útils i l'eliminació de petits problemes, portant les Aplicacions KDE un pas endavant per oferir-vos la configuració perfecta per al vostre dispositiu.

El <a href='https://www.kde.org/applications/graphics/kolourpaint/'>Kolourpaint</a>, el <a href='https://www.kde.org/applications/development/cervisia/'>Cervisia</a> i el KDiskFree ara han estat adaptats als Frameworks 5 de KDE i estem a l'espera dels vostres comentaris i opinions sobre les funcionalitats noves introduïdes en aquest llançament.

En l'esforç continuat per dividir les biblioteques de la suite Kontact perquè siguin més fàcils d'usar per tercers, l'arxiu tar del kdepimlibs ha estat dividit en akonadi-contacts, akonadi-mime i akonadi-notes.

Hem deixat els següents paquets: kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu i mplayerthumbs. Això ens ajudarà a centrar-nos en la resta del codi.

### Mantenir el Kontact

<a href='https://userbase.kde.org/Kontact'>La suite Kontact</a> ha rebut en aquesta versió la rutina habitual de neteja del codi, correcció d'errors i optimitzacions. És notable l'ús del QtWebEngine en diversos components, el qual permet usar un motor de representació HTML més modern. També hem millorat la implementació per a VCard4, també s'han afegit connectors nous que poden advertir si es compleixen algunes condicions en enviar un correu electrònic, p. ex., verifica que voleu permetre l'enviament de missatges de correu electrònic amb una determinada identitat, o comprova si enviareu el correu electrònic com a text sense format, etc.

### Nova versió del Marble

El <a href='https://marble.kde.org/'>Marble</a> 2.0 forma part de les Aplicacions 16.08 del KDE i inclou més de 450 canvis en el codi que inclouen la navegació, representació i una representació vectorial experimental de les dades d'OpenStreetMap.

### Més arxivat

L'<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, ara pot extreure fitxers AppImage i .xar, també realitza proves sobre la integritat dels arxius ZIP, 7z i RAR. També pot afegir/editar els comentaris als arxius RAR

### Millores en el terminal

El <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a> ha rebut millores quant a les opcions de la presentació del tipus de lletra i la implementació de l'accessibilitat.

### I més!

El <a href='https://kate-editor.org'>Kate</a> té pestanyes mòbils. <a href='https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/'>Més informació...</a>

El <a href='https://www.kde.org/applications/education/kgeography/'>KGeography</a>, ha afegit províncies i regions als mapes de Burkina Faso.

### Control enèrgic de plagues

S'han esmenat més de 120 errors a les aplicacions, incloses la suite Kontact, Ark, Cantor, Dolphin, KCalc, Kdenlive i més!

### Registre complet de canvis
