---
aliases:
- ../announce-applications-15.12.2
changelog: true
date: 2016-02-16
description: KDE distribueix les aplicacions 15.12.2 del KDE
layout: application
title: KDE distribueix les aplicacions 15.12.2 del KDE
version: 15.12.2
---
16 de febrer de 2016. Avui KDE distribueix la segona actualització d'estabilització per a les <a href='../15.12.0'>aplicacions 15.12 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha més de 30 esmenes registrades d'errors que inclouen millores a les Kdelibs, Kdepim, Kdenlive, Marble, Konsole, Spectable, Akonadi, Ark i Umbrello, entre altres.

Aquest llançament també inclou les versions de suport a llarg termini de la plataforma de desenvolupament KDE 4.14.17.
