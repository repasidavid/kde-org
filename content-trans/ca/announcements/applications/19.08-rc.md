---
aliases:
- ../announce-applications-19.08-rc
date: 2019-08-02
description: Es distribueixen les aplicacions 19.08 versió candidata del KDE.
layout: application
release: applications-19.07.90
title: KDE distribueix la versió candidata 19.08 de les aplicacions del KDE
version_number: 19.07.90
version_text: 19.08 Release Candidate
---
2 d'agost de 2019. Avui KDE distribueix la versió candidata de les noves versions de les aplicacions del KDE. S'han congelat les dependències i les funcionalitats, i ara l'equip del KDE se centra a corregir els errors i acabar de polir-la.

Verifiqueu les <a href='https://community.kde.org/Applications/19.08_Release_Notes'>notes de llançament de la comunitat</a> per a informació quant als arxius tar i quant als problemes coneguts. Hi haurà un anunci més complet disponible per al llançament final.

La distribució 19.08 de les aplicacions del KDE necessita una prova exhaustiva per tal de mantenir i millorar la qualitat i l'experiència d'usuari. Els usuaris reals són imprescindibles per a mantenir una alta qualitat del KDE, perquè els desenvolupadors no poden provar totes les configuracions possibles. Comptem amb vós per ajudar a trobar errors amb anticipació, a fi que es puguin solucionar abans de la publicació final. Considereu unir-vos a l'equip instal·lant la versió candidata i <a href='https://bugs.kde.org/'>informant de qualsevol error</a>.
