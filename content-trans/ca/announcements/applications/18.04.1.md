---
aliases:
- ../announce-applications-18.04.1
changelog: true
date: 2018-05-10
description: KDE distribueix les aplicacions 18.04.1 del KDE
layout: application
title: KDE distribueix les aplicacions 18.04.1 del KDE
version: 18.04.1
---
10 de maig de 2018. Avui KDE distribueix la primera actualització d'estabilització per a les <a href='../18.04.0'>aplicacions 18.04 del KDE</a>. Aquesta publicació només conté esmenes d'errors i actualitzacions de traduccions, proporcionant una actualització segura i millor per a tothom.

Hi ha unes 20 esmenes registrades d'errors que inclouen millores al Kontact, Cantor, Dolphin, Gwenview, JuK, Okular i Umbrello, entre d'altres.

Les millores inclouen:

- Les entrades duplicades al plafó de llocs del Dolphin ja no provocaran fallades
- S'ha solucionat un error antic en recarregar fitxers SVG al Gwenview
- La importació C++ de l'Umbrello ara entén la paraula clau «explicit»
