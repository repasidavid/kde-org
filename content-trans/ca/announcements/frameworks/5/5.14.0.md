---
aliases:
- ../../kde-frameworks-5.14.0
date: 2015-09-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### En molts Frameworks

- Reanomenar les classes privades per evitar exportar-les accidentalment

### Baloo

- Afegir la interfície org.kde.baloo a l'objecte arrel per compatibilitat inversa
- Instal·lar un org.kde.baloo.file.indexer.xml fals per esmenar la compilació del plasma-desktop 5.4
- Reorganitzar les interfícies de D-Bus
- Usar les metadades del json en el connector kded i esmenar el nom del connector
- Crear una instància de «Database» per procés (error 350247)
- Evitar que es mati el baloo_file_extractor en fer el «commit»
- Generar el fitxer d'interfície XML usant «qt5_generate_dbus_interface»
- Esmenes del monitor del Baloo
- Moure l'exportació d'URL de fitxer al fil principal
- Assegura que es tenen en compte les configuracions en cascada
- No instal·lar «namelink» a les biblioteques privades
- Instal·lar traduccions, detectat per en Hrvoje Senjan.

### BluezQt

- No reenviar el senyal «deviceChanged» després d'eliminar el dispositiu (error 351051)
- Respectar -DBUILD_TESTING=OFF

### Mòduls extres del CMake

- Afegir una macro per a generar les declaracions de categoria de registre a les Qt5.
- ecm_generate_headers: Afegir l'opció COMMON_HEADER i la funcionalitat de capçaleres múltiples
- Afegir «-pedantic» per al codi dels KF5 (en usar gcc o clang)
- KDEFrameworkCompilerSettings: Només activar els iteradors estrictes en mode de depuració
- Definir també la visibilitat per defecte del codi C a oculta.

### Integració del marc de treball

- Propagar també els títols de les finestres per als diàlegs de fitxers de només carpetes.

### KActivities

- Només activar un carregador d'accions (fil) quan les accions del «FileItemLinkingPlugin» no estan inicialitzades (error 351585)
- Esmenar els problemes de construcció introduïts en reanomenar les classes Privades (11030ffc0)
- Afegir un camí d'inclusió de Boost que mancava per construir en l'OS X
- La configuració de les dreceres s'ha mogut a la configuració de les activitats
- Ara funciona l'opció de mode privat d'activitat
- Refactoritzar els paràmetres de la IU
- Els mètodes d'activitat bàsics són funcionals
- IU per a la configuració d'activitat i missatges emergents de supressió
- IU bàsica per a la secció de creació/supressió/configuració d'activitats en el KCM
- S'ha incrementat la mida dels blocs per carregar els resultats
- S'ha afegit la include per std::set que mancava

### Eines de Doxygen del KDE

- Esmena per al Windows: Eliminar els fitxers existents abans de reemplaçar-los amb «os.rename».
- Usar camins natius en cridar al Python per a esmenar les construccions al Windows

### KCompletion

- Esmenar un comportament incorrecte / exhaurir memòria al Windows (error 345860)

### KConfig

- Optimitzar «readEntryGui»
- Evitar QString::fromLatin1() en el codi generat
- Minimitzar les crides al costós «QStandardPaths::locateAll()»
- Finalitzar l'adaptació a QCommandLineParser (ara té «addPositionalArgument»)

### Compatibilitat amb les KDELibs 4

- Adaptar el connector del kded solid-networkstatus a metadades json
- KPixmapCache: Crear directori si no existeix

### KDocTools

- Sincronitzar user.entities en català amb la versió anglesa (en).
- Afegir entitats per «sebas» i «plasma-pa»

### KEmoticons

- Rendiment: Fer una còpia en memòria cau d'una instància del KEmoticons aquí, no una KEmoticonsTheme.

### KFileMetaData

- PlainTextExtractor: Activar la branca O_NOATIME en plataformes amb la libc de la GNU
- PlainTextExtractor: Fer que la branca Linux també funcioni sense O_NOATIME
- PlainTextExtractor: Esmenar l'error de verificació de fallada a «open(O_NOATIME)»

### KGlobalAccel

- Només iniciar «kglobalaccel5» si cal.

### KI18n

- Gestionar adequadament la manca de línia nova al final del fitxer «pmap»

### KIconThemes

- KIconLoader: Esmenar reconfigure() oblidant els temes heretats i els directoris d'aplicacions
- Millorar el compliment de l'especificació de la càrrega de les icones

### KImageFormats

- eps: Esmenar les includes relacionades amb el Registre Categoritzat de les Qt

### KIO

- Usar Q_OS_WIN en lloc de Q_OS_WINDOWS
- Fer que KDE_FORK_SLAVES funcioni amb el Windows
- Desactivar la instal·lació del fitxer «desktop» per al mòdul «ProxyScout» del kded
- Proporcionar un sentit d'ordenació deterministic per KDirSortFilterProxyModelPrivate::compare
- Tornar a mostrar les icones personalitzades de carpetes (error 350612)
- Moure el «kpasswdserver» des de kded a kiod
- Esmenar errors d'adaptació al «kpasswdserver»
- Eliminar codi antic per parlar de versions molt antigues del «kpasswdserver».
- KDirListerTest: Usar QTRY_COMPARE en les dues sentències, per esmenar un error de concurrència mostrat per CI
- KFilePlacesModel: Implementar tasques pendents antigues usant «trashrc» en lloc d'un KDirLister complet.

### KItemModels

- Nou model de servidor intermedi: KConcatenateRowsProxyModel
- KConcatenateRowsProxyModelPrivate: Esmenar la gestió de layoutChanged.
- Més comprovacions en la selecció després d'ordenar.
- KExtraColumnsProxyModel: Esmenar un error a «sibling()» que trencava, per exemple, les seleccions

### Paquets dels Frameworks

- El «kpackagetool» pot desinstal·lar un paquet des d'un fitxer de paquet
- El «kpackagetool» ara és més intel·ligent en trobar el tipus de servei correcte

### KService

- KSycoca: Verificar els segells de temps i executar «kbuildsycoca» si cal. Ja no hi ha més dependència del kded.
- No tancar «ksycoca» després de la seva obertura.
- El KPluginInfo ara gestiona correctament les metadades de FormFactor

### KTextEditor

- Fusionar l'assignació de TextLineData i el bloc comptador de referències.
- Canviar la drecera de teclat per defecte per «Ves a la línia d'edició anterior»
- Esmenar els comentaris en el ressaltat de sintaxi del Haskell
- Accelerar l'aparició del missatge emergent de compleció de codi
- minimap: Intent de millorar l'aspecte i comportament (error 309553)
- Comentaris imbricats en el ressaltat de sintaxi del Haskell
- Esmenar un problema amb treure sagnat incorrecte per a Python (error 351190)

### KWidgetsAddons

- KPasswordDialog: Permetre que l'usuari canviï la visibilitat de la contrasenya (error 224686)

### KXMLGUI

- Esmenar el KSwitchLanguageDialog que no mostra la majoria dels idiomes

### KXmlRpcClient

- Evitar QLatin1String onsevulla que assigni memòria en monticles

### ModemManagerQt

- Esmenar un conflicte de «metatype» amb el darrer canvi del nm-qt

### NetworkManagerQt

- S'han afegit propietats noves dels darrers punts de sincronisme/versions del NM

### Frameworks del Plasma

- Tornar a fixar el pare com a «flickable» si és possible
- Esmenar el llistat de paquets
- Plasma: Esmenar les accions de miniaplicacions que podrien ser «nullptr» (error 351777)
- El senyal «onClicked» del PlasmaComponents.ModelContextMenu ara funciona adequadament
- PlasmaComponents ModelContextMenu ara pot crear seccions Menu
- Adaptar el connector del kded «platformstatus» a metadades json...
- Gestionar una metadata no vàlida en el PluginLoader
- Permetre que el RowLayout esbrini la mida de l'etiqueta
- Mostrar sempre el menú d'edició quan el cursor és visible
- Esmenar un bucle en el ButtonStyle
- No canviar la qualitat de pla d'un botó en ser premut
- En pantalles tàctils i mòbils les barres de desplaçament són transitòries
- Ajustar la velocitat i desacceleració del lliscament a ppp
- Delegació de cursor personalitzat només si és mòbil
- Cursor de text tàctil d'ús fàcil
- Esmenar la política de fixar pare i d'aparició
- Declarar __editMenu
- Afegir delegats de gestió de cursor que manquen
- Tornar a escriure la implementació de l'EditMenu
- Usar el menú mòbil només condicionalment
- Tornar a fixar el menú a l'arrel

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
