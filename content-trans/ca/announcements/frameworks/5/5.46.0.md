---
aliases:
- ../../kde-frameworks-5.46.0
date: 2018-05-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Evita bucles infinits en recuperar l'URL del DocumentUrlDB (error 393181)
- Afegeix senyals de D-Bus del Baloo per a fitxers moguts o eliminats
- Instal·la un fitxer «pri» per permetre el «qmake» i es documenta al «metainfo.yaml»
- Baloodb: Afegeix l'ordre «clean»
- Balooshow: Només acoloreix quan està adjuntat a un terminal
- Elimina FSUtils::getDirectoryFileSystem
- Evita escriure al codi font els sistemes de fitxers que admeten CoW
- Permet desactivar el CoW a error quan el sistema de fitxers no l'admet
- Databasesanitizer: Usa els indicadors per al filtratge
- Esmena la fusió de termes a l'AdvancedQueryParser
- Usa QStorageInfo en lloc d'una implementació artesana
- Sanitizer: Millora el llistat dels dispositius
- Aplica immediatament «termInConstruction» quan el terme està complet
- Gestiona correctament els caràcters especials adjacents (error 392620)
- Afegeix un cas de prova per l'anàlisi del doble «((» d'obertura (error 392620)
- Usa «statbuf» coherentment

### Icones Brisa

- Afegeix la icona de la safata del sistema «plasma-browser-integration»
- Afegeix la icona Virt-manager gràcies al ndavis
- Afegeix «video-card-inactive»
- «overflow-menu» com a «view-more-symbolic», i horitzontal
- Usa la icona «two sliders» més adequada per «configure»

### Mòduls extres del CMake

- Inclou FeatureSummary abans de cridar «set_package_properties»
- No instal·la connectors a «lib» a l'Android
- Possibilita construir diverses «apk» fora d'un projecte
- Verifica si el paquet de l'aplicació «androiddeployqt» té un símbol «main()»

### KCompletion

- [KLineEdit] Usa la funcionalitat de botó neteja integrat de les Qt
- Esmena KCompletionBox en el Wayland

### KCoreAddons

- [KUser] Comprova si «.face.icon» es pot llegir realment abans de retornar-lo
- Fa públics els senyals del KJob, de manera que la sintaxi de connexió de les Qt5 pugui funcionar

### KDeclarative

- Carrega el reinici dels gràfics NV basant-se en la configuració
- [KUserProxy] Ajusta als comptes de servei (error 384107)
- Optimitzacions del Plasma Mobile
- Fa espai per al peu i la capçalera
- Política nova de redimensió (error 391910)
- Permet la visibilitat d'accions
- Admet les notificacions de reinici de «nvidia» al QtQuickViews

### KDED

- Afegeix la detecció i ajustament de la plataforma al «kded» (opció automàtica de $QT_QPA_PLATFORM)

### KFileMetaData

- Afegeix la descripció i finalitat de la dependència de Xattr
- Extractors: Oculta els avisos de les capçaleres del sistema
- Esmena la detecció de la «taglib» en compilar per l'Android
- Instal·la un fitxer «pri» per permetre el «qmake» i es documenta al «metainfo.yaml»
- Fa que les cadenes concatenades es puguin ajustar
- ffmpegextractor: Silencia els avisos d'obsolescència
- taglibextractor: Soluciona un error amb el gènere buit
- Gestiona més etiquetes al «taglibextractor»

### KHolidays

- holidays/plan2/holiday_sk_sk - Esmena del dia del professorat (error 393245)

### KI18n

- [API dox] Marcador nou de la IU @info:placeholder
- [API dox] Marcador nou de la IU @item:valuesuffix
- No es necessita executar ordres d'iteració prèvies una altra vegada (error 393141)

### KImageFormats

- [Carregador XCF/GIMP] Eleva la mida màxima permesa de les imatges a 32767x32767 a les plataformes de 64 bits (error 391970)

### KIO

- Escalat suau de les miniatures al selector de fitxers (error 345578)
- KFileWidget: Alinea perfectament el nom de fitxer del giny amb la vista de la icona
- KFileWidget: Desa l'amplada del plafó Llocs després d'ocultar el plafó
- KFileWidget: Evita que l'amplada del plafó Llocs creixi 1px iterativament
- KFileWidget: Desactiva els botons de zoom un cop s'arribi al mínim o al màxim
- KFileWidget: Estableix la mida mínima del control lliscant del zoom
- No selecciona l'extensió del fitxer
- concatPaths: processa correctament el «path1» buit
- Millora la disposició de la quadrícula d'icones al diàleg del selector de fitxers (error 334099)
- Oculta el KUrlNavigatorProtocolCombo si només hi ha un protocol admès
- Mostra només els esquemes admesos al KUrlNavigatorProtocolCombo
- El selector de fitxers llegeix la vista prèvia de miniatures des de la configuració del Dolphin (error 318493)
- Afegeix Escriptori i Baixades a la llista predeterminada de Llocs
- KRecentDocument ara emmagatzema QGuiApplication::desktopFileName en lloc d'«applicationName»
- [KUrlNavigatorButton] Tampoc fa «stat» MTP
- «getxattr» recull 6 paràmetres a macOS (error 393304)
- Afegeix un element de menú «Recarrega» al menú contextual del KDirOperator (error 199994)
- Desa l'arranjament de vista de diàleg inclús en cancel·lar (error 209559)
- [KFileWidget] Escriure al codi font el nom d'usuari d'exemple
- No mostra l'aplicació «Obre amb» de dalt per a les carpetes; només per als fitxers
- Detecta un paràmetre incorrecte a «findProtocol»
- Usa el text «Altres aplicacions...» al submenú «Obre amb»
- Codifica correctament els URL de les miniatures (error 393015)
- Ajusta les amplades de les columnes a la vista en arbre als diàlegs d'obrir/desar fitxers (error 96638)

### Kirigami

- No avisa en usar «Page {}» fora d'un «pageStack»
- Refà InlineMessages per esmenar diversos problemes
- Solucionat a les Qt 5.11
- Basa en unitats la mida del botó d'eina
- Color de la icona de tancar en passar per sobre
- Mostra un marge sota el peu de pàgina quan calgui
- Soluciona «isMobile»
- També s'esvaeix a les animacions d'obrir/tancar
- Inclou l'entorn de D-Bus només als Unix no Android, ni Apple
- Controla el «tabletMode» del KWin
- En mode escriptori mostra les accions en passar-hi per sobre (error 364383)
- Maneta a la barra d'eines superior
- Usa un botó gris de tancar
- Menys dependències d'«applicationWindow»
- Menys avisos sense «applicationWindow»
- Funciona correctament sense l'«applicationWindow»
- No té una mida no entera als separadors
- No mostra les accions si estan desactivades
- Es poden activar els elements FormLayout
- Usa icones diferents en el conjunt de colors d'exemple
- Inclou icones només a l'Android
- Fa que funcioni amb les Qt 5.7

### KNewStuff

- Esmena els marges dobles al voltant de DownloadDialog
- Soluciona els consells als fitxers IU quant a les subclasses de ginys personalitzats
- No ofereix el connector Qml com a destí d'enllaç

### Framework del KPackage

- Usa KDE_INSTALL_DATADIR en lloc de FULL_DATADIR
- Afegeix els URL de donació per provar dades

### KPeople

- Esmena el filtratge de PersonSortFilterProxyModel

### Kross

- També fa opcional la instal·lació de documentació traduïda

### KRunner

- L'executor de D-Bus admet comodins al nom del servei

### KTextEditor

- Optimització de KTextEditor::DocumentPrivate::views()
- [ktexteditor] «positionFromCursor» molt més ràpid
- Implementa el clic únic al número de línia per seleccionar la línia de text
- Esmena el marcador que manca bold/italic/... amb les versions modernes de les Qt (&gt;= 5.9)

### Frameworks del Plasma

- Esmena un marcador d'esdeveniment que no es mostra al calendari amb els temes Air i Oxygen
- Usa «Configura %1...» per al text de l'acció de configuració de la miniaplicació
- [Estils de botons] Omple l'alçada i l'alineació vertical (error 393388)
- Afegeix la icona «video-card-inactive» per a la safata del sistema
- Aspecte correcte per als botons plans
- [Containment Interface] No entra en mode d'edició quan és immutable
- Assegura que «largespacing» és un múltiple perfecte de «small»
- Crida «addContainment» amb els paràmetres adequats
- No mostra el fons si «Button.flat»
- Assegura que el contenidor creat té l'activitat per la qual s'ha demanat
- Afegeix una versió «containmentForScreen» amb activitat
- No altera la gestió de la memòria per ocultar un element (error 391642)

### Purpose

- Assegura que es dona espai vertical als connectors de configuració
- Adapta la configuració del connector del KDEConnect al QQC2
- Adapta AlternativesView al QQC2

### QQC2StyleBridge

- Exporta els espais de la disposició des del «qstyle», inici des del Control
- [quadre combinat] Esmena la gestió de la roda del ratolí
- Fa que l'àrea del ratolí no interfereixi amb els controls
- Esmena «acceptableInput»

### Solid

- Actualitza el punt de muntatge després de l'operació de muntatge (error 370975)
- Invalida la memòria cau de la propietat quan s'elimina una interfície
- Evita la creació d'entrades duplicades de propietats a la memòria cau
- [UDisks] Optimitza diverses comprovacions de propietats
- [UDisks] Gestió correcta dels sistemes de fitxers extraïbles (error 389479)

### Sonnet

- Esmena l'activació/desactivació del botó d'eliminació
- Esmena l'activació/desactivació del botó d'afegir
- Cerca diccionaris als subdirectoris

### Ressaltat de la sintaxi

- Actualitza l'URL del projecte
- «Headline» és un comentari, així que es basa en el «dsComment»
- Afegeix el ressaltat per als fitxers «listings» i «gdbinit» d'ordres de GDB
- Afegeix el ressaltat de sintaxi per als fitxers Logcat

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
