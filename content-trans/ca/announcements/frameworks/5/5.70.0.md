---
aliases:
- ../../kde-frameworks-5.70.0
date: 2020-05-02
layout: framework
libCount: 70
---
### Baloo

- [FileWatch] Elimina un sòcol «watchIndexedFolders()» redundant
- [ModifiedFileIndexer] Aclareix un comentari
- [FileWatch] Corregeix les actualitzacions de vigilància dels canvis de configuració
- [KInotify] Corregeix la concordança del camí en eliminar vigilants
- [Extractor] Usa un enregistrament categoritzat
- Usa KFileMetaData per treballar amb XAttr en lloc d'una reimplementació privada
- Reverteix «Afegeix senyals de D-Bus del Baloo per a fitxers moguts o eliminats»
- [QML Monitor] Mostra el temps romanent tan aviat com sigui possible
- [FileContentIndexer] Corregeix l'actualització de l'estat i l'ordre del senyal
- [Monitor] Corregeix l'estat del monitor i l'ordenació dels senyals
- [Extractor] Corregeix la informació de progrés
- [Coding] Evita el desacoblament recurrent i les comprovacions de mida
- [baloo_file] Elimina el KAboutData del «baloo_file»
- [Searchstore] Reserva espai per termes de consulta de frases
- [SearchStore] Permet consultar les coincidències exactes per a les no propietats
- [PhraseAndIterator] Elimina les matrius temporals en comprovar coincidències
- [Extractor] Balanç millor dels modes en espera i ocupat
- [Extractor] Corregeix el monitoratge en espera
- [Extractor] Elimina la classe contenidora IdleStateMonitor
- [OrpostingIterator] Permet ometre elements, implementa «skipTo»
- [PhraseAndIterator] Substitueix la implementació recursiva «next()»
- [AndPostingIterator] Substitueix la implementació recursiva «next()»
- [PostingIterator] Assegura que «skipTo» també funciona per al primer element
- Reanomena i exporta el senyal «newBatchTime» a «filecontentindexer»
- [SearchStore] Gestiona valors dobles a les consultes de propietats
- [AdvancedQueryParser] Mou la gestió semàntica de testimonis a SearchStore
- [Inotify] Elimina el no tant OptimizedByteArray
- [Inotify] Elimina codi mort/duplicat
- [QueryParser] Substitueix el «helper» d'un sol ús per «std::none_of»

### Icones Brisa

- Mou el plegat de la cantonada del document a la part superior dreta a dues icones
- Afegeix la icona «konversation» de 16px
- Nom correcte de la icona «vscode»
- Afegeix la icona del «vvave» de 16px
- Afegeix la icona de l'«alligator»
- Afegeix les icones «preferences-desktop-tablet» i «preferences-desktop-touchpad»
- Actualitza els enllaços del README.md
- Construcció: posa entre cometes el camí al directori del codi font
- Permet construir des d'una ubicació de només lectura del codi font
- Afegeix icones d'expandir/contraure per acompanyar les icones existents «expand-all/collapse-all»
- Afegeix «auth-sim-locked» i «auth-sim-missing»
- Afegeix les icones de dispositiu de targeta SIM
- Afegeix icones de gir
- Afegeix una icona de l'Arranjament del sistema de 16px
- Canvia el ButtonFocus a Highlight
- Millora l'aspecte de «kcachegrind»
- Elimina la vora de les icones «format-border-set-*»

### Mòduls extres del CMake

- Android: inclou l'arquitectura al nom de l'«apk»
- ECMAddQch: corregeix l'ús de les marques de citació per PREDEFINED a la configuració del «doxygen»
- Adapta el FindKF5 a algunes comprovacions més estrictes del «find_package_handle_standard_args» més nou
- ECMAddQch: ajuda a gestionar Q_DECLARE_FLAGS al «doxygen», de manera que aquests tipus aconsegueixin la documentació
- Corregeix els avisos de l'escàner al Wayland
- ECM: intenta corregir «KDEInstallDirsTest.relative_or_absolute» al Windows

### Eines de Doxygen del KDE

- Corregeix l'espai en blanc que manca després de «Platform(s):x a la pàgina inicial
- Corregeix l'ús de les marques de citació per PREDEFINED de les entrades a Doxygile.global
- Ensenya el «doxygen» quant a Q_DECL_EQ_DEFAULT i Q_DECL_EQ_DELETE
- Afegeix un calaix al mòbil i neteja el codi
- Ensenya el «doxygen» quan a Q_DECLARE_FLAGS, de manera que aquest tipus es puguin documentar
- Adapta a Aether Bootstrap 4
- Refà «api.kde.org» per semblar més com l'Aether

### KBookmarks

- Crea sempre «actioncollection»
- [KBookMarksMenu] Defineix «objectName» per a «newBookmarkFolderAction»

### KCMUtils

- KSettings::Dialog: afegeix el suport per als KPluginInfos sense un KService
- Optimització petita: crida «kcmServices()» només una vegada
- Rebaixa l'avís quant als KCM de l'estil antic a «qDebug», fins als KF6
- Usa «ecm_setup_qtplugin_macro_names»

### KConfig

- kconfig_compiler: genera la configuració del «kconfig» amb un subgrup
- Esmena diversos avisos del compilador
- Afegeix el comportament de desar per força a KEntryMap
- Afegeix la drecera estàndard per «Mostra/oculta fitxers ocults» (error 262551)

### KContacts

- Alinea la descripció al «metainfo.yaml» amb la del README.md

### KCoreAddons

- API dox: usa el «typedef» «ulong» amb Q_PROPERTY(percent) per evitar un error del «doxygen»
- API dox: documenta els indicadors basats en Q_DECLARE_FLAGS
- Marca l'antic «typedef» KLibFactory com a obsolet
- [KJobUiDelegate] Afegeix l'indicador AutoHandlingEnabled

### KCrash

- Elimina l'ús del «klauncher» al KCrash

### KDeclarative

- Anomena adequadament el contingut del projecte «kcmcontrols»
- Ajusta la documentació del «kcmcontrols»
- Afegeix el mètode «startCapture»
- [KeySequenceHelper] Treball amb el comportament modificador de la Meta
- Allibera també la finestra al destructor

### KDED

- Adapta KToolInvocation::kdeinitExecWait a QProcess
- Elimina la segona fase endarrerida

### KHolidays

- Festius de Nicaragua
- Festius taiwanesos
- Actualitza els festius romanesos

### KI18n

- Macro KI18N_WRAP_UI: estableix la propietat «SKIP_AUTOUIC» al fitxer UI i genera la capçalera

### KIconThemes

- Afegeix una nota quant a l'adaptació de «loadMimeTypeIcon»

### KImageFormats

- Afegeix la implementació per a les imatges del GIMP/fitxers XCF moderns

### KIO

- [RenameDialog] Afegeix una fletxa indicant la direcció des de l'origen al destí (error 268600)
- KIO_SILENT Ajusta la documentació de l'API per coincidir amb la realitat
- Mou la gestió dels programes no confiables a ApplicationLauncherJob
- Mou la comprovació de servei no vàlid des de KDesktopFileActions a ApplicationLauncherJob
- Detecta executables sense el permís +x al $PATH per a millorar el missatge d'error (error 415567)
- Fa més útil la plantilla del fitxer HTML (error 419935)
- Afegeix el constructor del JobUiDelegate amb l'indicador AutoErrorHandling i la finestra
- Corregeix el càlcul del directori de la memòria cau en afegir a la paperera
- File protocol: assegura que KIO::StatAcl funciona sense dependre implícitament de KIO::StatBasic
- Afegeix el valor detallat de KIO::StatRecursiveSize de manera que «kio_trash» només ho faci a demanda
- CopyJob: en fer «stat» del destí, usa StatBasic
- [KFileBookMarkHandler] Adapta al KBookmarkMenu-5.69 nou
- Marca KStatusBarOfflineIndicator com a obsolet
- Substitueix KLocalSocket per QLocalSocket
- Evita una fallada en el mode d'alliberament després d'avisar dels elements fills no esperats (error 390288)
- Docu: elimina la menció a un senyal no existent
- [renamedialog] Substitueix l'ús de KIconLoader per QIcon::fromTheme
- kio_trash: afegeix la mida, modificació, accés i data de creació a «trash:/» (error 413091)
- [KDirOperator] Usa la drecera estàndard nova «Mostra/Oculta els fitxers ocults» (error 262551)
- Mostra les vistes prèvies als sistemes de fitxers encriptats (error 411919)
- [KPropertiesDialog] Desactiva el canvi de les icones als directoris remots (errors 205954)
- [KPropertiesDialog] Corregeix l'avís de QLayout
- API dox: documenta més els valors predeterminats de la propietat de KUrlRequester
- Corregeix DirectorySizeJob de manera que no depengui de l'ordre de la llista
- KRun: corregeix l'asserció en fallar l'inici d'una aplicació

### Kirigami

- Presenta Theme::smallFont
- Fa més útil BasicListItem donant-li la propietat «subtitle»
- Fa que PageRouterAttached tingui menys «segfaults»
- PageRouter: cerca millor dels pares d'elements
- Elimina la QtConcurrent sense ús de «colorutils»
- PlaceholderMessage: elimina l'ús de les unitats del Plasma
- Permet que PlaceholderMessage sigui sense text
- Centra verticalment els fulls si no tenen barra de desplaçament (error 419804)
- Té en compte el marge superior i inferior per a l'alçada predeterminada de la targeta
- Correccions diverses a les targetes noves (error 420406)
- Icon: millora la renderització de les icones a les configuracions multipantalla multippp
- Corregeix un error a PlaceholderMessage: es desactiven les accions, no s'oculten
- Presenta el component PlaceholderMessage
- Revisió: corregeix un tecleig incorrecte a les funcions de matriu FormLayout
- Revisió de SwipeListItem: usa Array.prototype.*.call
- Revisió: usa Array.prototype.some.call a ContextDrawer
- Revisió de D28666: usa Array.prototype.*.call en lloc d'invocar funcions a objectes de «list»
- Afegeix la variable «m_sourceChanged» que manca
- Usa ShadowedRectangle per al fons de les targetes (error 415526)
- Actualitza la comprovació de visibilitat d'ActionToolbar comprovant l'amplada amb menys-«igual»
- Un parell de correccions «trivials» de codi trencat
- No tanca mai quan el clic és dins el contingut del full (error 419691)
- El full ha d'estar sota les altres finestres emergents (error 419930)
- Afegeix el component PageRouter
- Afegeix ColorUtils
- Permet definir radis separats a les cantonades de ShadowedRectangle
- Elimina l'opció STATIC_LIBRARY per corregir les construccions estàtiques

### KJobWidgets

- Afegeix el constructor KDialogJobUiDelegate(KJobUiDelegate::Flags)

### KJS

- Implementa l'operador = d'UString per fer feliç el «gcc»
- Silencia l'avís del compilador quant a la còpia de dades no trivials

### KNewStuff

- KNewStuff: corregeix el camí del fitxer i la crida del procés (error 420312)
- KNewStuff: adapta des de KRun::runApplication a KIO::ApplicationLauncherJob
- Substitueix Vokoscreen per VokoscreenNG (error 416460)
- Presenta un informe d'errors més visible per a l'usuari a les instal·lacions (error 418466)

### KNotification

- Implementa l'actualització de notificacions a l'Android
- Gestiona les notificacions multilínia i amb text enriquit a l'Android
- Afegeix el constructor KNotificationJobUiDelegate(KJobUiDelegate::Flags)
- [KNotificationJobUiDelegate] Afegeix «Failed» als missatges d'error

### KNotifyConfig

- Usa coherentment «knotify-config.h» per passar indicadors quant al Canberra/Phonon

### KParts

- Afegeix el constructor sobrecarregat StatusBarExtension(KParts::Part *)

### KPlotting

- Adapta un «foreach» (obsolet) a un «range for»

### KRunner

- Executor de D-Bus: afegeix la propietat «service» per a sol·licitar accions una vegada (error 420311)
- Imprimeix un avís si l'executor és incompatible amb el KRunner

### KService

- Fa obsolet KPluginInfo::service(), ja que un constructor amb un KService és obsolet

### KTextEditor

- Corregeix l'arrossegament i deixar anar a la vora esquerra del giny (error 420048)
- Emmagatzema i recupera la configuració de la vista completa a i des de la sessió de configuració
- Reverteix l'adaptació prematura a les Qt 5.15 no publicades i que mentrestant han canviat

### KTextWidgets

- [NestedListHelper] Corregeix el sagnat de la selecció, afegeix proves
- [NestedListHelper] Millora el codi del sagnat
- [KRichTextEdit] Assegura que les capçaleres no es barregen amb la pila de desfer
- [KRichTextEdit] Corregeix el salt del desplaçament quan s'afegeix la regla horitzontal (error 195828)
- [KRichTextWidget] Elimina una solució temporal antiga i corregeix una regressió (commit 1d1eb6f)
- [KRichTextWidget] Afegeix suport per a les capçaleres
- [KRichTextEdit] Sempre tracta una tecla premuda com una modificació única a la pila de desfer (error 256001)
- [findreplace] Gestiona la cerca de WholeWordsOnly al mode Regex

### KUnitConversion

- Afegeix el galó imperial i la pinta EUA (error 341072)
- Afegeix la Krona islandesa a les divises

### KWayland

- [Wayland] Afegeix l'ordre d'apilament de finestres al protocol del PlasmaWindowManagement
- [server] Afegeix diversos senyals de cicle de vida a la «sub-surface»

### KWidgetsAddons

- [KFontChooser] Elimina NoFixedCheckBox DisplayFlag, redundant
- [KFontChooser] Afegeix un DisplayFlag nou; modifica com s'utilitzen els indicadors
- [KFontChooser] Fa més precís «styleIdentifier()» afegint l'«styleName» del tipus de lletra (error 420287)
- [KFontRequester] Adapta des de QFontDialog a KFontChooserDialog
- [KMimeTypeChooser] Afegeix la possibilitat de filtrar la vista en arbre amb un QSFPM (error 245637)
- [KFontChooser] Fa que el codi sigui lleugerament més intel·ligible
- [KFontChooser] Afegeix una casella de selecció per commutar la visualització dels tipus de lletra monoespai
- Elimina una inclusió no necessària

### KWindowSystem

- Imprimeix un avís força informatiu quan no hi ha QGuiApplication

### KXMLGUI

- [KRichTextEditor] Afegeix suport per a capçaleres
- [KKeySequenceWidget] Treball amb el comportament modificador de la Meta

### NetworkManagerQt

- Substitueix un «foreach» per un «range-for»

### Frameworks del Plasma

- [PlasmaCore.IconItem] Regressió: corregeix una falla en canviar l'origen (error 420801)
- [PlasmaCore.IconItem] Refactoritza la gestió de l'origen per a tipus diferents
- Fa coherent l'espaiat del text dels consells d'eina de les miniaplicacions
- [ExpandableListItem] Facilita l'ús tàctil
- [ExpandableListItem] Usa icones per expandir i reduir semànticament més correctes
- Corregeix el bucle de vinculació BusyIndicator de PC3
- [ExpandableListItem] Afegeix l'opció nova «showDefaultActionButtonWhenBusy»
- Elimina les vores arrodonides al «plasmoidHeading»
- [ExpandableListItem] Afegeix el senyal «itemCollapsed» i no emet «itemExpanded» quan està reduït
- Afegeix «readme» per aclarir l'estat de les versions del component del Plasma
- [configview] Simplifica el codi / solució temporal per fallada amb les Qt5.15
- Crea ExpandableListItem
- Fa coherents les durades de les animacions amb els valors del Kirigami

### QQC2StyleBridge

- Detecta la versió del QQC2 en temps de construcció amb la detecció real
- [ComboBox] Usa un enfosquidor transparent

### Solid

- [Solid] Adapta un «foreach» (obsolet) a un «range/index for»
- [FakeCdrom] Afegeix un numerador nou UnknownMediumType a MediumType
- [FstabWatcher] Corregeix la pèrdua del vigilant del «fstab»
- [Fstab] No emet «deviceAdded» per duplicat en els canvis al «fstab/mtab»

### Ressaltat de la sintaxi

- debchangelog: afegeix Groovy Gorilla
- Actualitza la implementació del llenguatge Logtalk
- TypeScript: afegeix l'operador de tipus «awaited»

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
