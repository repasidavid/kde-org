---
aliases:
- ../../kde-frameworks-5.24.0
date: 2016-07-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Canvis generals

- La llista de les plataformes admeses per cada Framework ara és més explícita. S'ha afegit l'Android a la llista de les plataformes admeses a tots els Frameworks quan és el cas.

### Baloo

- DocumentUrlDB::del: Fer «assert» només quan existeixi realment el fill del directori
- Ignorar consultes mal formades que tenen un operador binari sense el primer argument

### Icones Brisa

- Moltes icones noves i millorades
- Solucionar l'error 364931 en què la icona de l'usuari en espera no era visible (error 364931)
- Afegir un programa per convertir fitxers enllaçats simbòlicament a àlies «qrc»

### Mòduls extres del CMake

- Integrar els camins relatius a biblioteques en l'APK
- Usa «${BIN_INSTALL_DIR}/data» per DATAROOTDIR al Windows

### KArchive

- Assegura que l'extracció d'un arxiu no instal·la fitxers fora de la carpeta d'extracció, per motius de seguretat. En canvi, extreure aquests fitxers a l'arrel de la carpeta d'extracció.

### KBookmarks

- Netejar el «KBookmarkManagerList» abans que la «qApp» surti, per a evitar interbloqueigs amb el fil de D-Bus

### KConfig

- Fer obsolet «authorizeKAction()» i substituir per «authorizeAction()»
- Solucionar la reproductibilitat de les compilacions assegurant la codificació UTF-8

### KConfigWidgets

- KStandardAction::showStatusbar: Retorna l'acció proposada

### KDeclarative

- Fer opcional l'«epoxy»

### KDED

- [OS X] Convertir el kded5 en un agent, i construir-lo com una aplicació normal

### Compatibilitat amb les KDELibs 4

- Eliminar la classe «KDETranslator», ja no hi ha cap «kdeqt.po»
- Documentar la substitució del «use12Clock()»

### KDesignerPlugin

- Afegir la implementació per al «KNewPasswordWidget»

### KDocTools

- Permetre que KDocTools sempre localitzi com a mínim la seva pròpia instal·lació
- Usar CMAKE_INSTALL_DATAROOTDIR per cercar a «docbook» en lloc de «share»
- Actualitzar el «docbook» de la pàgina «man» de «qt5options» a Qt 5.4
- Actualitzar el «docbook» de la pàgina «man» de «kf5options»

### KEmoticons

- Moure el tema «Glass» a «kde-look»

### KGlobalAccel

- Usar QGuiApplication en lloc de QApplication

### KHTML

- Solucionar l'aplicació del valor heretat per la propietat abreviada del contorn
- Gestionar el radi de la vora inicial i l'heretat
- Descartar la propietat si s'ha detectat una longitud|percentatge no vàlid com a mida del fons
- El «cssText» ha de donar com a sortida una llista de valors separats per comes per aquestes propietats
- Solucionar l'anàlisi de «background-clip» en una abreviatura
- Implementar «background-size» en una abreviatura
- Marcar les propietats com a definides en repetir patrons
- Solucionar l'herència de les propietats del fons
- Solucionar l'aplicació del valor inicial i l'heretat per la propietat «background-size»
- Desplegar el fitxer «kxmlgui» en «khtml» en un fitxer de recurs Qt

### KI18n

- Cercar també als catàlegs les variants «stripped» dels valors a la variable d'entorn LANGUAGE
- Esmenar les anàlisis del modificador dels valors de la variable d'entorn WRT i del conjunt de codis, efectuats en l'ordre incorrecte

### KIconThemes

- S'ha afegit la implementació per carregar i usar automàticament un tema d'icones en un fitxer RCC
- Documentar el desplegament dels temes d'icones al MacOS i Windows, vegeu https://api.kde.org/frameworks/kiconthemes/html/index.html

### KInit

- Permetre un temps d'espera en «reset_oom_protection» mentre espera una SIGUSR1

### KIO

- KIO: Afegir SlaveBase::openPasswordDialogV2 per a una millor verificació d'errors, si us plau, adapteu-hi els «kioslaves»
- Solucionar el diàleg d'obertura de fitxer KUrlRequester en el directori incorrecte (error 364719)
- Solucionar un «cast» insegur a KDirModelDirNode*
- Afegir l'opció KIO_FORK_SLAVES del CMake per definir el valor predeterminat
- Filtre ShortUri: Solucionar el filtratge de mailto:user@host
- Afegir OpenFileManagerWindowJob per ressaltar un fitxer en una carpeta
- KRun: Afegir el mètode «runApplication»
- Afegir el proveïdor de cerques SoundCloud
- Solucionar un problema d'alineació amb l'estil «macintosh» natiu de l'OS X

### KItemModels

- Afegir KExtraColumnsProxyModel::removeExtraColumn, serà necessari per StatisticsProxyModel

### KJS

- kjs/ConfigureChecks.cmake - Definir adequadament HAVE_SYS_PARAM_H

### KNewStuff

- Assegura que hi ha una mida a oferir (error 364896)
- Solucionar «El diàleg de baixada falla quan manquen totes les categories»

### KNotification

- Solucionar la notificació de la barra de tasques

### KNotifyConfig

- KNotifyConfigWidget: Afegir el mètode «disableAllSounds()» (error 157272)

### KParts

- Afegir un commutador per desactivar la gestió de les KParts dels títols de les finestres
- Afegir l'element del menú Donatius al menú d'ajuda de les aplicacions

### Kross

- Esmenar el nom del numerador «StandardButtons» del QDialogButtonBox
- Eliminar el primer intent de carregar la biblioteca perquè igualment s'intentarà a «libraryPaths»
- Solucionar una fallada quan un mètode exposat al Kross retorna QVariant amb dades no reubicables
- No usar els «casts» d'estil C a «void*» (error 325055)

### KRunner

- [QueryMatch] Afegir «iconName»

### KTextEditor

- Mostrar la vista prèvia de text a la barra de desplaçament després d'un retard de 250 ms
- Ocultar la vista prèvia i altres en veure el desplaçament del contingut
- Definir el pare + la vista d'eina, creiem que cal per evitar entrada del commutador de tasques en el Win10
- Eliminar «KDE-Standard» del quadre de codificació
- Vista prèvia del plegat activa de manera predeterminada
- Evitar el subratllat de guions per la vista prèvia i evitar l'enverinament de la memòria cau de la disposició de la línia
- Activar sempre l'opció «Mostra la vista prèvia del text plegat»
- TextPreview: Ajustar el «grooveRect-height» quan «scrollPastEnd» és actiu
- Vista prèvia a la barra de desplaçament: Usar «grooveRect» si la barra de desplaçament no usa l'alçada completa
- Afegir KTE::MovingRange::numberOfLines() com ho té KTE::Range
- Vista prèvia de plegat del codi: Definir l'alçada del missatge emergent de manera que s'ajusti a totes les línies ocultes
- Afegir una opció per desactivar la vista prèvia del text plegat
- Afegir el mode de línia «folding-preview» de tipus «bool»
- Vista de ConfigInterface: Implementar «folding-preview» de tipus «bool»
- Afegir «bool» KateViewConfig::foldingPreview() i «setFoldingPreview(bool)»
- Funcionalitat: Mostrar la vista prèvia en passar per sobre del bloc de codi plegat
- KateTextPreview: Afegir «setShowFoldedLines()» i «showFoldedLines()»
- Afegir els modes de línia «scrollbar-minimap» [bool], i «scrollbar-preview» [bool]
- Activar de manera predeterminada el minimapa a les barres de desplaçament
- Funcionalitat nova: Mostrar la vista prèvia en passar per sobre de la barra de desplaçament
- KateUndoGroup::editEnd(): passar KTE::Range per una referència «const»
- Esmenar la gestió de les dreceres del mode vim, després dels canvis de comportament en les Qt 5.5 (error 353332)
- Autobrace: No inserir el caràcter ' en el text
- ConfigInterface: Afegir la clau de configuració «scrollbar-minimap» per activar/desactivar el minimapa a la barra de desplaçament
- Solucionar KTE::View::cursorToCoordinate() quan el giny del missatge superior és visible
- Refactorització de la barra d'ordres emulada
- Solucionar els defectes de dibuixat en desplaçar-se quan les notificacions són visibles (error 363220)

### KWayland

- Afegir un esdeveniment «parent_window» a la interfície Plasma Window
- Gestionar adequadament la destrucció d'un recurs «Pointer/Keyboard/Touch»
- [servidor] Suprimir codi mort: KeyboardInterface::Private::sendKeymap
- [servidor] Afegir la implementació per establir manualment la selecció del porta-retalls DataDeviceInterface
- [servidor] Assegura que el Resource::Private::get retorna «nullptr» si se li passa un «nullptr»
- [servidor] Afegir una comprovació de recurs a QtExtendedSurfaceInterface::close
- [servidor] Netejar l'apuntador SurfaceInterface en els objectes referenciats quan es destrueix
- [servidor] Esmenar el missatge d'error a la interfície QtSurfaceExtension
- [servidor] Presentar un senyal Resource::unbound emès des d'un gestor desvinculat
- [servidor] No fer «assert» en destruir una BufferInterface encara referenciada
- Afegir una sol·licitud del destructor per «org_kde_kwin_shadow» i «org_kde_kwin_shadow_manager»

### KWidgetsAddons

- Solucionar la lectura de dades Unihan
- Esmenar la mida mínima de KNewPasswordDialog (error 342523)
- Solucionar un constructor ambigu a MSVC 2015
- Esmenar un problema d'alineació amb l'estil «macintosh» natiu de l'OS X (error 296810)

### KXMLGUI

- KXMLGui: Solucionar els índexs de fusió en eliminar els clients «xmlgui» amb accions en grups (error 64754)
- No avisar quant «fitxer trobat en una ubicació compatible» si no s'ha trobat res
- Afegir l'element del menú Donatius al menú d'ajuda de les aplicacions

### NetworkManagerQt

- No definir l'etiqueta «peap» basada en la versió del «peap»
- Fer que la versió del Network Manager es verifiqui en temps d'execució (per evitar fer-ho en la compilació) (error 362736)

### Frameworks del Plasma

- [Calendari] Girar els botons de fletxa en els idiomes d'esquerra a dreta
- Plasma::Service::operationDescription() cal que retorni un QVariantMap
- No incloure contenidors incrustats a «containmentAt(pos)» (error 361777)
- Solucionar el tema del color de la icona de reinici del sistema (pantalla d'entrada) (error 364454)
- Desactivar les miniatures de la barra de tasques amb «llvmpipe» (error 363371)
- Protegir-se contra miniaplicacions no vàlides (error 364281)
- PluginLoader::loadApplet: Restaurar la compatibilitat de les miniaplicacions y desinstal·lades
- Carpeta correcta per PLASMA_PLASMOIDS_PLUGINDIR
- PluginLoader: Millorar el missatge d'error quant a la compatibilitat de la versió dels connectors
- Esmenar les comprovacions per mantenir el QMenu en pantalla per a les disposicions multipantalla
- Tipus de contenidor nou per la safata del sistema

### Solid

- Esmenar la comprovació de validesa de la CPU
- Gestionar la lectura de /proc/cpuinfo per als processadors ARM
- Cercar les CPU per subsistema abans que per controlador

### Sonnet

- Marcar l'executable «helper» com a aplicació no IGU
- Permetre que el «nsspellcheck» es compili de manera predeterminada al Mac

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
