---
aliases:
- ../../kde-frameworks-5.30.0
date: 2017-01-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Icones Brisa

- Afegeix les icones Haguichi del Stephen Brandt (gràcies)
- Implementació final per les icones del Calligra
- Afegeix la icona del CUPS gràcies al Colin (error 373126)
- Actualitza la icona del KAlarm (error 362631)
- Afegeix la icona d'aplicació del KRfb (error 373362)
- Afegir la implementació del tipus MIME de l'R (error 371811)
- i més

### Mòduls extres del CMake

- Appstreamtest: gestiona programes no instal·lats
- Activa els avisos acolorits a la sortida del Ninja
- Esmena els :: que manquen a la documentació de l'API per activar l'estil del codi
- Ignora els fitxers libs/includes/cmakeconfig del servidor a la cadena d'eines d'Android
- Documenta l'ús de «gnustl_shared» amb la cadena d'eines d'Android
- No usa mai «-Wl,--no-undefined» al Mac (Apple)

### Integració del marc de treball

- Millora al KNSHandler del KPackage
- Cerca més precisa de la versió requerida de l'AppstreamQt
- Afegeix la implementació del KNewStuff al KPackage

### KActivitiesStats

- Fer que compili amb «-fno-operator-names»
- No recupera més recursos enllaçats quan un d'ells desapareix
- Afegeix el rol de model per recuperar la llista d'activitats enllaçades per a un a recurs

### Eines de Doxygen del KDE

- Afegeix l'Android a la llista de plataformes disponibles
- Inclou fitxers font ObjC++

### KConfig

- Genera una instància amb KSharedConfig::Ptr per a «singleton» i «arg»
- kconfig_compiler: Usa «nullptr» al codi generat

### KConfigWidgets

- Oculta l'acció «Mostra la barra de menús» si totes les barres de menús són natives
- KConfigDialogManager: retira les classes de les Kdelibs3

### KCoreAddons

- Retorna els tipus «stringlist»/«boolean» a KPluginMetaData::value
- DesktopFileParser: Respecta el camp ServiceTypes

### KDBusAddons

- Afegeix les vinculacions de Python per al «KDBusAddons»

### KDeclarative

- Presenta la importació QML per org.kde.kconfig amb KAuthorized

### KDocTools

- kdoctools_install: quadra amb el camí complet del programa (error 374435)

### KGlobalAccel

- [runtime] Presenta una variable d'entorn KGLOBALACCEL_TEST_MODE

### Complements de la IGU del KDE

- Afegeix les vinculacions de Python per al «KGuiAddons»

### KHTML

- Defineix les dades del connector, per tal que funcioni el Visualitzador Incrustat d'Imatges

### KIconThemes

- Informa també el QIconLoader quan canviï el tema d'icones de l'escriptori (error 365363)

### KInit

- Tenir en compte la propietat X-KDE-RunOnDiscreteGpu en iniciar l'aplicació usant el «klauncher»

### KIO

- El KIO::iconNameForUrl ara retornarà icones especials per a les ubicacions dels «xdg» com la carpeta Imatges de l'usuari
- ForwardingSlaveBase: Esmena per passar l'indicador Overwrite al «kio_desktop» (error 360487)
- Millora i exporta la classe «KPasswdServerClient», l'API del client per al kpasswdserver
- [KPropertiesDialog] Mata l'opció «Situa a la safata del sistema»
- [KPropertiesDialog] No canvia el «nom» de l'«Enllaç» dels fitxers «.desktop» si el fitxer només és de lectura
- [KFileWidget] Usa «urlFromString» en lloc de «QUrl(QString)» a «setSelection» (error 369216)
- Afegeix una opció per executar una aplicació en una targeta gràfica dedicada al KPropertiesDialog
- Finalitza adequadament DropJob abans de descartar un executable
- Esmena l'ús de la categoria registre al Windows
- DropJob: emet una còpia iniciada del treball després de la creació
- Afegeix la implementació per invocar «suspend()» en un CopyJob abans que comenci
- Afegeix la implementació per suspendre treballs immediatament, com a mínim per als SimpleJob i els FileCopyJob

### KItemModels

- Actualitza els intermediaris per a les classes d'error recentment detectades («layoutChanged handling»)
- Fa possible que el KConcatenateRowsProxyModel funcioni amb el QML
- KExtraColumnsProxyModel: El model persistent indexa després d'emetre «layoutChange», no abans
- Soluciona una asserció (a «beginRemoveRows») en desseleccionar un fill buit d'un fill seleccionat al KOrganizer

### KJobWidgets

- No dona focus a la finestra de progrés (error 333934)

### KNewStuff

- [Botó GHNS] Ocult quan s'apliquin les restriccions del KIOSK
- Soluciona la configuració del ::Engine quan es crea amb un camí absolut al fitxer de configuració

### KNotification

- Afegeix una prova manual per als llançadors de l'Unity
- [KNotificationRestrictions] Permet que l'usuari pugui especificar un text amb el motiu de la restricció

### Framework del KPackage

- [PackageLoader] No accedeix a un KPluginMetadata no vàlid (error 374541)
- Esmena la descripció de l'opció -t a la pàgina del «man»
- Millora el missatge d'error
- Esmena el missatge d'ajuda per a «--type»
- Millora el procés d'instal·lació dels paquets del KPackage
- Instal·la un fitxer «kpackage-generic.desktop»
- Exclou els fitxers «qmlc» de la instal·lació
- No llista de manera separada els plasmoides per «metadata.desktop» o «.json»
- Solució addicional per als paquets de tipus diferents però amb els mateixos ID
- Soluciona una fallada del CMake quan dos paquets de tipus diferents tenen el mateix ID

### KParts

- Crida el «checkAmbiguousShortcuts()» nou des de MainWindow::createShellGUI

### KService

- KSycoca: no segueix els enllaços simbòlics a directoris, crea un risc de recursivitat

### KTextEditor

- Esmena: El reenviament del text arrossegat provoca una selecció errònia (error 374163)

### Framework del KWallet

- Especifica el requisit mínim del GpgME++ versió 1.7.0
- Reverteix «Si no es troba el Gpgmepp, intenta usar el KF5Gpgmepp»

### KWidgetsAddons

- Afegeix vinculacions del Python
- Afegeix KToolTipWidget, un consell d'eina que conté un altre giny
- Esmena les validacions del KDateComboBox per a dates introduïdes vàlides
- KMessageWidget: usa un color vermell més fosc quan el tipus és Error (error 357210)

### KXMLGUI

- Avisa en iniciar quan hi ha dreceres ambigües (amb una excepció per a Maj+Supr)
- Els MSWin i Mac tenen un comportament similar per a desar automàticament la mida de la finestra
- Mostra la versió de l'aplicació a la capçalera del diàleg Quant (error 372367)

### Icones de l'Oxygen

- Sincronitza amb les icones del Brisa

### Frameworks del Plasma

- Soluciona les propietats del «renderType» per a diversos components
- [ToolTipDialog] Usa el KWindowSystem::isPlatformX11() que està a la memòria cau
- [Icon Item] Soluciona l'actualització de la mida implícita quan canvien les mides de les icones
- [Dialog] Usa «setPosition» / «setSize» en lloc de definir-ho tot individualment
- [Units] Fer constant la propietat «iconSizes»
- Ara hi ha una instància global «Units» per reduir el consum de memòria i temps de creació dels elements SVG
- [Icon Item] Admet icones no quadrades (error 355592)
- Cerca/substitució dels tipus antics en el codi font del plasmapkg2 (error 374463)
- Soluciona el tipus X-Plasma-Drop* (error 374418)
- [Plasma ScrollViewStyle] Mostra el fons de la barra de desplaçament només en passar-hi per sobre
- Solució del núm. 374127: situació incorrecta de les finestres emergents de les finestres acoblades
- Fa obsolet l'API del Plasma::Package en el PluginLoader
- Torna a verificar la representació que cal usar a «setPreferredRepresentation»
- [declarativeimports] Usa QtRendering en els dispositius de telèfon
- Considera un plafó buit sempre com «miniaplicacions carregades» (error 373836)
- Emet «toolTipMainTextChanged» si canvia en resposta a un canvi de títol
- [TextField] Permet desactivar el botó de veure la contrasenya a través d'una restricció del KIOSK
- [AppletQuickItem] Implementa un missatge d'error de llançament
- Soluciona la lògica de la gestió de les fletxes a les configuracions regionals RTL (error 373749)
- TextFieldStyle: Soluciona el valor «implicitHeight» de manera que el cursor del text estigui centrat

### Sonnet

- CMake: també cerca l'Hunspell-1.6

### Ressaltat de la sintaxi

- Soluciona el ressaltat del Makefile.inc afegint una prioritat al «makefile.xml»
- Ressaltat dels fitxers SCXML com els XML
- makefile.xml: moltes millores, massa llarg per llistar aquí
- Sintaxi del Python: s'han afegit literals «f» i s'ha millorat la gestió de les cadenes

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
