---
aliases:
- ../../kde-frameworks-5.38.0
date: 2017-09-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Mancava a l'anunci del darrer mes: els KF5 inclouen un marc de treball nou, el Kirigami, un conjunt de connectors del QtQuick per a construir les interfícies d'usuari basades en les directrius UX del KDE.

### Baloo

- Soluciona la cerca basada en directoris

### Mòduls extres del CMake

- Estableix CMAKE_*_OUTPUT_5.38 per executar les proves sense instal·lar
- Inclou un mòdul per cercar importacions de QML com a dependències en temps d'execució

### Integració del marc de treball

- Retorna la icona d'alta resolució de neteja de la línia d'edició
- Esmena l'acceptació de diàlegs amb Ctrl+Retorn quan es reanomenen els botons

### KActivitiesStats

- Refactoritza la consulta que combina recursos enllaçats i usats
- Recarrega el model quan es desenllaça el recurs
- Esmena la consulta que fusiona recursos enllaçats i usats

### KConfig

- Esmena les etiquetes de les accions DeleteFile/RenameFile (error 382450)
- kconfigini: Elimina els espais en blanc inicials en llegir les entrades de valors (error 310674)

### KConfigWidgets

- Fa obsoletes KStandardAction::Help i KStandardAction::SaveOptions
- Esmena les etiquetes de les accions DeleteFile/RenameFile (error 382450)
- Usa «document-close» com a icona per a KStandardAction::close

### KCoreAddons

- DesktopFileParser: Afegeix una cerca de reserva a «:/kservicetypes5/*»
- Afegeix la implementació per a connectors no instal·lats a «kcoreaddons_add_plugin»
- desktopfileparser: Esmena l'anàlisi clau/valor que no compleixi (error 310674)

### KDED

- Implementació de X-KDE-OnlyShowOnQtPlatforms

### KDocTools

- CMake: Esmena l'escurçament de nom d'objectiu en construir directoris que tenen caràcters especials (error 377573)
- Afegeix CC BY-SA 4.0 International i l'estableix com a predeterminada

### KGlobalAccel

- KGlobalAccel: Adapta «symXModXToKeyQt» al nou mètode de KKeyServer, per solucionar les tecles del teclat numèric (error 183458)

### KInit

- klauncher: Esmena la coincidència de l'«appId» per a les aplicacions Flatpak

### KIO

- Adapta el KCM de les dreceres web des del «KServiceTypeTrader» al «KPluginLoader::findPlugins»
- [KFilePropsPlugin] Format de la configuració regional de «totalSize» durant el càlcul
- KIO: Soluciona una fuita de memòria de fa molt temps en sortir
- Afegeix capacitats de filtratge per tipus MIME al KUrlCompletion
- KIO: Adapta els connectors de filtres d'URI des del KServiceTypeTrader al Json+KPluginMetaData
- [KUrlNavigator] Emet «tabRequested» en clicar amb el mig el lloc en el menú (error 304589)
- [KUrlNavigator] Emet «tabRequested» quan el selector de llocs es clica amb el mig (error 304589)
- [KACLEditWidget] Permet un clic doble per editar entrades
- [kiocore] Esmena un error lògic en el «commit» previ
- [kiocore] Comprova si el «klauncher» està executant-se o no
- Limita realment la ràtio de missatges INF_PROCESSED_SIZE (error 383843)
- No neteja l'emmagatzematge del certificat de la CA de l'SSL de les Qt
- [KDesktopPropsPlugin] Crea el directori de destinació si no existeix
- [File KIO slave] Esmena l'aplicació dels atributs especials de fitxers (error 365795)
- Elimina una verificació en bucle ocupada a TransferJobPrivate::slotDataReqFromDevice
- Fa que kiod5 sigui un «agent» al Mac
- Soluciona el KCM de Servidor intermediari que no carrega correctament els servidors intermediaris manuals

### Kirigami

- Oculta les barres de desplaçament quan no siguin útils
- Afegeix un exemple bàsic per ajustar l'amplada de la columna amb una maneta que es pot arrossegar
- Oculta les capes en posicionar les manetes
- Soluciona el posicionament de la maneta quan se superposa la darrera pàgina
- No mostra la maneta fictícia a la darrera columna
- No emmagatzema res en els delegats (error 383741)
- Com que ja s'ha definit «keyNavigationEnabled», també es defineixen els embolcalls
- Millora l'alineació a l'esquerra per al botó enrere (error 383751)
- No té en compte la capçalera 2 vegades en el desplaçament (error 383725)
- No embolcalla les etiquetes de capçalera
- Corregeix un FIXME: Elimina «resetTimer» (error 383772)
- No desplaça «applicationheader» en no mòbil
- Afegeix una propietat per ocultar el separador PageRow que coincideixi amb AbstractListItem
- Esmena el desplaçament amb el flux «originY» i «bottomtotop»
- Elimina avisos referits a la mida tant dels píxels com dels punts
- No activa el mode accessible a les vistes inverses
- Té en compte el peu de pàgina
- Afegeix un exemple lleugerament més complex d'una aplicació de xat
- Més proves de fallades per cercar el peu correcte
- Verifica la validesa de l'element abans d'usar-lo
- Respecta la posició de la capa per «isCurrentPage»
- Usa una animació en lloc d'un animador (error 383761)
- Deixa l'espai necessari per al peu de pàgina, si és possible
- Un enfosquidor millor per als calaixos de l'«applicationitem»
- Enfosquiment del fons per «applicationitem»
- Esmena adequadament els marges del botó enrere
- Marges adequats per al botó enrere
- Menys avisos a ApplicationHeader
- No usa l'escalat del Plasma per a les mides de les icones
- Aspecte nou per les manetes

### KItemViews

### KJobWidgets

- Inicialitza l'estat del botó «Pausa» al seguidor del giny

### KNotification

- No bloqueja l'inici del servei de notificació (error 382444)

### Framework del KPackage

- Refactoritza el «kpackagetool» a part de les opcions «stringy»

### KRunner

- Neteja les accions prèvies en actualitzar
- Afegeix executors remots sobre D-Bus

### KTextEditor

- Adapta l'API de creació de scripts de Document/View a una solució basada en QJSValue
- Mostra les icones en el menú contextual de vores d'icones
- Substitueix KStandardAction::PasteText per KStandardAction::Paste
- Permet l'escalat fraccionari en generar la vista prèvia de la barra lateral
- Canvia des del QtScript al QtQml

### KWayland

- Tracta les memòries intermèdies RGB d'entrada com a «premultiplied»
- Actualitza les sortides de SurfaceInterface quan es destrueix una sortida global
- KWayland::Client::Surface segueix la destrucció de la sortida
- Evita els oferiments d'enviament de dades des d'un origen no vàlid (error 383054)

### KWidgetsAddons

- Simplifica «setContents» permetent que les Qt facin la majoria de la feina
- KSqueezedTextLabel: Afegeix «isSqueezed()» per oportunitat
- KSqueezedTextLabel: Millores petites a la documentació de l'API
- [KPasswordLineEdit] Estableix l'intermediari del focus a l'edició de la línia (error 383653)
- [KPasswordDialog] Reinicia la propietat geometria

### KWindowSystem

- KKeyServer: Esmena la gestió del KeypadModifier (error 183458)

### KXMLGUI

- Estalvia una pila de crides «stat()» en iniciar l'aplicació
- Esmena la posició del KHelpMenu en el Wayland (error 384193)
- Elimina la gestió errònia del clic amb el botó del mig (error 383162)
- KUndoActions: Usa «actionCollection» per establir la drecera

### Frameworks del Plasma

- [ConfigModel] Protegeix davant l'addició d'una ConfigCategory nul·la
- [ConfigModel] Permet afegir i eliminar per programa ConfigCategory (error 372090)
- [EventPluginsManager] Exposa «pluginPath» en el model
- [Icon Item] No dessassignar «imagePath» sense necessitat
- [FrameSvg] Usa QPixmap::mask() en lloc de l'obsoleta manera de convolució via «alphaChannel()»
- [FrameSvgItem] Crea l'objecte «margins/fixedMargins» sota demanda
- Esmena l'estat de comprovació per als elements de menú
- Força l'estil Plasma per al QQC2 a les miniaplicacions
- Instal·la la carpeta PlasmaComponents.3/private
- Elimina les restes dels temes «locolor»
- [Theme] Usa KConfig SimpleConfig
- Evita diverses cerques de contingut de tema innecessàries
- Ignora els esdeveniments de les redimensions errònies a mides buides (error 382340)

### Ressaltat de la sintaxi

- Afegeix la definició de sintaxi per a les llistes de filtre d'Adblock Plus
- Torna a escriure la definició de la sintaxi del Sieve
- Afegeix el ressaltat per als fitxers de configuració del QDoc
- Afegeix la definició de ressaltat per a Tiger
- Escapa els guions a les expressions regulars del rest.xml (error 383632)
- Soluciona: El text net es ressalta com a «powershell»
- Afegeix ressaltat de sintaxi per a Metamath
- El ressaltat de la sintaxi del SCSS es traspassa a Less (error 369277)
- Afegeix el ressaltat del Pony
- Torna a escriure la definició de la sintaxi del correu electrònic

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
