---
aliases:
- ../../kde-frameworks-5.17.0
date: 2015-12-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Rätta datumfilter använt av timeline://
- BalooCtl: Returnera efter kommandon
- Städa och förstärk Baloo::Database::open(), hantera fler kraschtillstånd
- Lägg till kontroll i Database::open(OpenDatabase) så att det misslyckas om databasen inte finns

### Breeze-ikoner

- Många ikoner tillagda eller förbättrade
- Använd stilmallar i Breeze-ikoner (fel 126166)
- Fel 355902: Rätta och ändra system-lock-screen (fel 355902 rätta och ändra system-lock-screen)
- Lägg till 24 bildpunkters dialoginformation för GTK-program (fel 355204)

### Extra CMake-moduler

- Varna inte när SVG(Z)-ikoner tillhandahålls med flera storlekar eller detaljnivåer
- Säkerställ att översättningar läses in i huvudtråden (fel 346188).
- Se över ECM-byggsystemet.
- Gör det möjligt att aktivera Clazy för alla KDE-projekt
- Hitta inte XCB:s XINPUT-bibliotek som förval.
- Rensa exportkatalog innan en APK genereras igen
- Använd quickgit för Git-arkivets webbadress.

### Integrering med ramverk

- Lägg till att installation av Plasmoid misslyckades i plasma_workspace.notifyrc

### KActivities

- Rätta ett lås vid första start av demonen
- Flytta att skapa QAction till huvudtråden (fel 351485).
- Ibland tar clang-format ett dåligt beslut (fel 355495)
- Ta död på potentiella synkroniseringsproblem
- Använd org.qtproject istället för com.trolltech
- Ta bort användning av libkactivities från insticksprogrammen
- Inställning av KAStats borttagen från programmeringsgränssnittet
- Tillägg av länkning och borttagning av länk till ResultModel

### KDE Doxygen-verktyg

- Gör kgenframeworksapidox robustare.

### KArchive

- Rätta KCompressionDevice::seek(), som anropas när KTar skapas över KCompressionDevice.

### KCoreAddons

- KAboutData: Tillåt https:// och andra webbadresser på hemsidan (fel 355508).
- Reparera egenskapen MimeType när kcoreaddons_desktop_to_json() används

### KDeclarative

- Konvertera KDeclarative att använda KI18n direkt
- DragArea delegateImage kan nu vara en sträng, som kan användas för att automatiskt skapa en ikon
- Lägg till nytt bibliotek CalendarEvents

### KDED

- Ta bort definitionen av miljövariabeln SESSION_MANAGER istället för att tilldela den ett tomt värde

### Stöd för KDELibs 4

- Rätta några anrop till i18n.

### KFileMetaData

- Markera m4a som läsbar av taglib

### KIO

- Kakdialogruta: Få den att fungera som avsett
- Rätta att filnamnsförslag ändras till något slumpmässigt när Mime-typ för spara som ändras.
- Registrera DBus-namn för kioexec (fel 353037)
- Uppdatera KProtocolManager efter inställningsändring.

### KItemModels

- Rätta användning av KSelectionProxyModel i QTableView (fel 352369)
- Rätta återställning eller ändring av källmodellen för en KRecursiveFilterProxyModel.

### KNewStuff

- registerServicesByGroupingNames kan definiera flera förvalda objekt
- Gör KMoreToolsMenuFactory::createMenuFromGroupingNames lat

### KTextEditor

- Lägg till syntaxfärgläggning för TaskJuggler och PL/I
- Gör det möjligt att inaktivera komplettering av nyckelord via inställningsgränssnittet.
- Ändra storlek av trädet när kompletteringsmodellen återställs.

### Ramverket KWallet

- Hantera fallet när användaren inaktiverar oss korrekt

### KWidgetsAddons

- Rätta en mindre förvrängning i KRatingWidget vid hög upplösning.
- Omstrukturera och rätta funktionen introducerad i fel 171343 (fel 171343)

### KXMLGUI

- Anropa inte QCoreApplication::setQuitLockEnabled(true) vid initiering.

### Plasma ramverk

- Lägg till enkel Plasmoid som exempel i utvecklingsguiden
- Lägg till några mallar för Plasmoider i kapptemplate/kdevelop
- [calendar] Fördröj återställning av modell tills vyn är redo (fel 355943)
- Ändra inte position vid döljning (fel 354352).
- [IconItem] Krascha inte när KIconLoader tema är null (fel 355577)
- Att släppa bildfiler på en panel ger inte längre ett erbjudande om att använda dem som skrivbordsunderlägg i panelen
- Att släppa en .plasmoid-fil på en panel eller skrivbordet installerar och lägger till den
- Tog bort KDED-modulen platformstatus som nu är oanvänd (fel 348840)
- Tillåt att klistra in i lösenordsfält
- Rätta placering av redigeringsmeny, lägg till en knapp för att välja
- [calendar] Använd användargränssnittsspråk för att hämta månadsnamnet (fel 353715)
- [calendar] Sortera också händelserna enligt typ
- [calendar] Flytta insticksbiblioteket till KDeclarative
- [calendar] qmlRegisterUncreatableType behöver några fler argument
- Tillåt att inställningskategorier läggs till dynamiskt
- [calendar] Flytta hantering av insticksprogram till en separat klass
- Tillåt att insticksprogram tillhandahåller händelseinformation till kalenderns miniprogram (fel 349676)
- Kontrollera om en slot finns innan anslutning eller borttagning av anslutning (fel 354751)
- [plasmaquick] Länka inte explicit till OpenGL
- [plasmaquick] Ta bort beroende av XCB::COMPOSITE och DAMAGE

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
