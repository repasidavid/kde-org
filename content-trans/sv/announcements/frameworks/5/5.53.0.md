---
aliases:
- ../../kde-frameworks-5.53.0
date: 2018-12-09
layout: framework
libCount: 70
---
### Baloo

- Rätta sökningar efter betyg 10 (5 stjärnor) (fel 357960)
- Undvik att skriva oförändrad data till termdatabaser
- Lägg inte till Type::Document/Presentation/Spreadsheet två gånger för MS Office-dokument
- Initiera faktiskt kcrash på ett riktigt sätt
- Säkerställ att det bara finns en MTime per dokument i MTimeDB
- [Extractor] Använd QDataStream serialisering istället för behandlad
- [Extractor] Ersätt hemmagjord I/O-hanterare med QDataStream, fånga HUP

### Breeze-ikoner

- Lägg till ikoner för application-vnd.appimage/x-iso9660-appimage
- Lägg till 22 bildpunkters dialog-warning ikon (fel 397983)
- Rätta vinkel och marginal för 32 bildpunkters dialog-ok-apply (fel 393608)
- Ändra primära svartvita ikonfärger så att de motsvarar nya HIG-färger
- Ändra archive-* åtgärdsikoner så att de representerar arkiv (fel 399253)
- Lägg till symbolisk länk help-browser till 16 och 22 bildpunkters kataloger (fel 400852)
- Lägg till nya generella sorteringsikoner, byt namn på befintliga sorteringsikoner
- Lägg till rotversion av drive-harddisk (fel 399307)

### Extra CMake-moduler

- Ny modul: FindLibExiv2.cmake

### Integrering med ramverk

- Lägg till alternativ BUILD_KPACKAGE_INSTALL_HANDLERS för att hoppa över att bygga installationshanterare

### KDE Doxygen-verktyg

- Lägg till upptagetindikering under efterforskning och gör efterforskningen asynkron (fel 379281)
- Normalisera alla inmatade sökvägar med funktionen os.path.normpath (fel 392428)

### KCMUtils

- Perfekt justering mellan rubriker för QML och QWidget inställningsmoduler
- Lägg till sammanhang i anslutning av kcmodule till lambda-funktioner (fel 397894)

### KCoreAddons

- Gör det möjligt att använda KAboutData/License/Person från QML
- Rätta krasch om katalogen XDG_CACHE_HOME är för liten eller har slut på utrymme (fel 339829)

### KDE DNS-SD

- Installerar nu kdnssd_version.h för att kontrollera bibliotekets version
- Läck inte upplösare i fjärrtjänst
- Förhindra avahi signalkapplöpning
- Rättning för MacOS

### KFileMetaData

- Återuppliva egenskapen 'Description' för DublinCore-metadata
- Lägg till en beskrivningsegenskap i KFileMetaData
- [KFileMetaData] Lägg till extrahering av DSC-överensstämmande (Encapsulated) Postscript
- [ExtractorCollection] Undvik beroende av kcoreaddons för CI-tester
- Lägg till stöd för speex-filer i taglibextractor
- Lägg till två ytterligare Internet-källor rörande tagginformation
- Förenkla hantering av id3-taggar
- [XmlExtractor] Använd QXmlStreamReader för bättre prestanda

### KIO

- Rätta assert när symboliska länkar städas i PreviewJob
- Lägg till möjlighet att ha en snabbtangent för att skapa en fil
- [KUrlNavigator] Återaktiivera med musens mittenknapp (fel 386453)
- Ta bort oanvända sökleverantörer
- Konvertera fler sökleverantörer till HTTPS
- Exportera KFilePlaceEditDialog igen (fel 376619)
- Återställ stöd för sendfile
- [ioslaves/trash] Hantera felaktiga symboliska länkar i borttagna underkataloger (fel 400990)
- [RenameDialog] Rätta layout när flaggan NoRename används
- Lägg till saknad @since för KFilePlacesModel::TagsType
- [KDirOperator] Använd den nya ikonen <code>view-sort</code> för val av sorteringsordning
- Använd HTTPS för alla sökleverantörer som stöder det
- Inaktivera avmonteringsalternativ för / eller /home (fel 399659)
- [KFilePlaceEditDialog] Rätta inkluderingsskydd
- [Platspanelen] Använd ny ikon <code>folder-root</code> för rotobjekt
- [KSambaShare] Gör tolken "net usershare info" testbar
- Ge fildialogrutor en menyknapp "Sortera enligt" i verktygsraden

### Kirigami

- DelegateRecycler: Skapa inte en ny propertiesTracker för varje delegat
- Flytta sidan Om från Discover till Kirigami
- Dölj sammanhangslåda när det finns en global verktygsrad
- Säkerställ att alla objekt placeras ut (fel 400671)
- Ändra index vid nedtryckning, inte vid klick (fel 400518)
- Nya textstorlekar för rubriker
- Sidoraders lådor flyttar inte globalt sidhuvud och sidfot

### KNewStuff

- Lägg till användbar felsignalering programmatiskt

### KNotification

- Byt namn på NotifyByFlatpak till NotifyByPortal
- Notification portal: Stöd bildpunktsavbildningar i underrättelser

### Ramverket KPackage

- Skapa inte appstream-data för filer som saknar en beskrivning (fel 400431)
- Hämta paketmetadata innan installationen startar

### Kör program

- När körprogram återanvänds vid återinläsning, läs in deras inställning igen (fel 399621)

### KTextEditor

- Tillåt negativa prioriteter för syntaxdefinitioner
- Exponera funktionen "Växla kommentar" via verktygsmeny och standardgenväg (fel 387654)
- Rätta dolda språk i lägesmenyn
- SpellCheckBar: Använd DictionaryComboBox istället för en vanlig QComboBox
- KTextEditor::ViewPrivate: Undvik varning "Text requested for invalid range"
- Android: Inget behov av att definiera log2 längre
- Koppla bort sammanhangsberoende meny från alla aboutToXXContextMenu mottagare (fel 401069)
- Introducera AbstractAnnotationItemDelegate för större kontroll av konsumenten

### KUnitConversion

- Uppdatera vid enheter från petroleumindustri (fel 388074)

### Kwayland

- Generera loggfil automatiskt och rätta kategorifil
- Lägg till VirtualDesktops i PlasmaWindowModel
- Uppdatera test av PlasmaWindowModel för att motsvara ändringar av VirtualDesktop
- Städa upp windowInterface i tester innan windowManagement förstörs
- Ta bort rätt objekt i removeDesktop
- Städa bort Virtual Desktop Manager listpost i PlasmaVirtualDesktop destruktor
- Rätta version av nytillagt PlasmaVirtualDesktop gränssnitt
- [server] Innehållstips och syfte för textinmatning per protokollversion
- [server] Lägg till text-inmatning (in-)aktivera, in-/aktivera återanrop i underliggande klasser
- [server] Lägg till inställning av omgivande text-återanrop med uint i v0-klass
- [server] Lägg till viss text-inmatning för v0-exklusiva återanrop i v0-klass

### KWidgetsAddons

- Lägg till programmeringsgränssnitt för nivå från Kirigami.Heading

### KXMLGUI

- Uppdatera texten "Om KDE"

### NetworkManagerQt

- Rättade ett problem (fel?) i inställning av ipv4 &amp; ipv6
- Lägg till inställningar för ovs-bridge och ovs-interface
- Uppdatera inställning av IP-tunnel
- Lägg till proxy- och användarinställning
- Lägg till inställning av IP-tunnel
- Vi kan nu bygga tun-inställningstest hela tiden
- Lägg till saknade alternativ för ipv6
- Lyssna efter tillagda D-Bus-gränssnitt istället för registrerade tjänster (fel 400359)

### Plasma ramverk

- Funktionslikhet för meny med skrivbordsstilen
- Qt 5.9 är nu minimal version som krävs
- Lägg till (av misstag) borttagen rad i CMakeLists.txt
- 100% c likhet med Kirigami rubrikstorlekar
- Mer homogent utseende jämfört med Kirigami rubriker
- Installera behandlad version av privata importer
- Textmarkeringskontroll för mobilenheter
- Uppdatera färgscheman för breeze-light och breeze-dark
- Rätta ett antal minnesläckor (tack till ASAN)

### Syfte

- Phabricator insticksprogram: Använd Arcanists diff.rev. ordning (fel 401565)
- Tillhandahåll en rubrik för JobDialog
- Tillåt JobDialog att få en snygg initial storlek (fel 400873)
- Gör det möjligt för menydemonstrationen att dela olika webbadresser
- Använd QQC2 för JobDialog (fel 400997)

### QQC2StyleBridge

- Konsekvent storleksändring av objekt jämfört med QWidgets
- Rätta menystorleksändring
- Säkerställ att bläddringsbart objekt är bildpunktsjusterat
- Stöd för program baserade på QGuiApplication (fel 396287)
- Textkontroller för pekskärm
- Storleksändra enligt angiven ikonbredd och -höjd
- Ta hänsyn till egenskapen flat för knappar
- Rätta problem när det bara finns ett element i menyn (fel 400517)

### Solid

- Rätta ändring av diskikon så att den inte felaktigt ändrar andra ikoner

### Sonnet

- DictionaryComboBoxTest: Lägg till stretch för att undvika att dumpknappen expanderas

### Syntaxfärgläggning

- BrightScript: Tillåt sub att vara namnlös
- Lägg till färgläggningsfil för Wayland felsökningsspårningar
- Lägg till syntaxfärgläggning för  TypeScript och TypeScript React
- Rust och Yacc/Bison: Förbättra kommentarer
- Prolog och Lua: Rätta !#
- Rätta inläsning av språk efter att nyckelord från språket inkluderats i en annan fil
- Lägg till syntax för BrightScript
- debchangelog: Lägg till Disco Dingo

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
