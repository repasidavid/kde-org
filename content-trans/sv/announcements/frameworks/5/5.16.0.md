---
aliases:
- ../../kde-frameworks-5.16.0
date: 2015-11-13
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Monitor-bibliotek: Använd Kformat::spelloutDuration för att översätta tidssträng
- Använd KDE_INSTALL_DBUSINTERFACEDIR för att installera D-Bus-gränssnitt
- UnindexedFileIndexer: Hantera filer som har flyttats när baloo_file inte körde
- Ta bort Transaction::renameFilePath och lägg till DocumentOperation för den.
- Gör konstruktorer med en enda parameter explicita
- UnindexedFileIndexer: Indexera bara nödvändiga delar av filer
- Transaction: Lägg till metod för att returnera timeInfo struct
- Tillägg av undantag för Mime-typer i balooctls inställningar
- Databaser: Använd QByteArray::fromRawData när data skickas till en kodare
- Balooctl: Flytta kommandot 'status' till sin egen klass
- Balooctl: Visa hjälpmeny om kommandot inte känns igen
- Balooshow: Tillåt att filer slås upp enligt inode + devId
- Balooctl-monitor: Stop om baloo dödas
- MonitorCommand: Använd både signalerna started och finished
- Balooctl-monitor: Flytta till en riktig kommandoklass
- Lägg till underrättelse via D-Bus när indexering av en fil påbörjas/avslutas
- FileIndexScheduler: Tvinga att trådar dödas vid avslutning
- Incheckning av WriteTransaction: Undvik att hämta positionList om det inte behövs
- WriteTransaction: Extra assert i replaceDocument

### BluezQt

- isBluetoothOperational beror nu också på att rfkill inte är blockerad
- Rätta hur globalt tillstånd hos rfkill bestäms
- QML API: Markera egenskaper utan underrättelsesignal som konstanter

### Extra CMake-moduler

- Varning istället för fel om ecm_install_icons inte hittar några ikoner (fel 354610).
- Gör det möjligt att bygga KDE Ramverk 5 med en vanlig Qt 5.5.x installerad med det normala installationsprogrammet qt.io på Mac OS
- Ta inte bort tilldelning av cache-variabler i KDEInstallDirs (fel 342717).

### Integrering med ramverk

- Ställ in standardvärde för WheelScrollLines
- Rätta inställningen av WheelScrollLines med Qt ≥ 5.5 (fel 291144)
- Byt till teckensnittet Noto för Plasma 5.5

### KActivities

- Rätta byggning med Qt 5.3
- Flytta inkludering av boost.optional till platsen som använder det
- Ersätt användning av boost.optional i fortsättningar med den smalare strukturen optional_view
- Tillägg av stöd för en egen sortering av länkade resultat
- Tillåt att QML startar KDE:s IM Aktiviteter
- Lägg till stöd för borttagning av aktiviteter i KDE:s IM Aktiviteter
- Nytt användargränssnitt för inställning av aktiviteter
- Nytt användargränssnitt för inställning som stöder tillägg av beskrivning och skrivbordsunderlägg
- Användargränssnittet för inställning är nu modulariserat på ett riktigt sätt

### KArchive

- Rätta KArchive på grund av beteendeförändring i Qt 5.6
- Rätta minnesläckor, lägre minnesanvändning

### KAuth

- Hantera proxying qInfo-meddelanden
- Vänta på att asynkrona anrop som startar hjälprutiner är klara innan svaret kontrolleras (fel 345234)
- Rätta variabelnamn, annars finns det inget sätt som inkluderingen kan fungera

### KConfig

- Rätta användning av ecm_create_qm_loader.
- Rätta inkluderingsvariabel
- Använd KDE*INSTALL_FULL* variant, så att det inte finns någon tvetydighet
- Tillåt att KConfig använder resurser som reserv för inställningsfiler

### KConfigWidgets

- Gör KConfigWidgets fristående, lägg in den enda globala filen i en resurs
- Gör doctools valfri

### KCoreAddons

- KAboutData: Dokumentation av programmeringsgränssnittet, "is is" -&gt; "is", addCredit(): ocsUserName -&gt; ocsUsername
- KJob::kill(Quiet) ska också avsluta händelsesnurran
- Lägg till stöd för skrivbordsfilnamn i KAboutData
- Använd riktigt undantagstecken
- Reducera vissa minnestilldelningar
- Gör KAboutData::translators/setTranslators enkel
- Rätta exempelkod för setTranslator
- desktopparser: Hoppa över nyckeln Encoding=
- desktopfileparser: Ta hand om granskningskommentarer
- Tillåt att tjänsttyper ställs in i kcoreaddons_desktop_to_json()
- desktopparser: Rätta tolkning av double- och bool-värden
- Lägg till KPluginMetaData::fromDesktopFile()
- desktopparser: Tillåt att relativa sökvägar skickas till tjänsttypfiler
- desktopparser: Använd mer kategoriserad loggning
- QCommandLineParser använder -v för --version så använd bara --verbose
- Ta bort mängder av duplicerad kod för desktop{tojson,fileparser}.cpp
- Tolka ServiceType-filer när.desktop-filer läses
- Gör SharedMimeInfo ett valfritt krav
- Ta bort anrop till QString::squeeze()
- desktopparser: Undvik onödig UTF8-avkodning
- desktopparser: Lägg inte till en ny post om posten avslutas med ett skiljetecken
- KPluginMetaData: Varna när en listpost inte är en JSON-lista
- Lägg till mimeTypes() i KPluginMetaData

### KCrash

- Förbättra sökning för drkonqui och låt den normalt vara tyst om inget hittas

### KDeclarative

- ConfigPropertyMap kan nu efterfrågas för oföränderliga inställningsalternativ genom att använda metoden isImmutable(nyckel)
- Packa upp QJSValue i egenskapsavbildning för inställning
- EventGenerator: Lägg till stöd för att skicka hjulhändelser
- Rätta förlorad QuickViewSharedEngine initialSize vid initiering.
- Rätta kritisk regression för QuickViewSharedEngine av incheckning 3792923639b1c480fd622f7d4d31f6f888c925b9
- Låt fördefinierad vystorlek få företräde framför ursprunglig objektstorlek i QuickViewSharedEngine

### KDED

- Gör doctools valfri

### Stöd för KDELibs 4

- Försök inte lagra en QDateTime i mmap:at minne
- Synkronisera och inför uriencode.cmake från kdoctools.

### KDesignerPlugin

- Lägg till KCollapsibleGroupBox

### KDocTools

- Uppdatera pt_BR entiteter

### KGlobalAccel

- Gör inte XOR-skift för KP_Enter (fel 128982)
- Hämta alla nycklar för en symbol (fel 351198)
- Hämta inte tangentsymboler två gånger för varje tangentnedtryckning

### KHTML

- Rätta utskrift från KHTMLPart genom att ställa in överliggande printSetting

### KIconThemes

- kiconthemes stöder nu teman inbäddade i Qt-resurser inne i prefixet :/icons liksom Qt gör själv för QIcon::fromTheme
- Lägg till saknade nödvändiga beroenden

### KImageFormats

- Känn igen image/vnd.adobe.photoshop istället för image/x-psd
- Återställ d7f457a delvis för att förhindra krasch när program avslutas

### KInit

- Gör doctools valfri

### KIO

- Spara proxywebbadress med korrekt metod
- Leverera "nya filmallar" i biblioteket kiofilewidgets genom att använda en .qrc (fel 353642)
- Hantera mittenklick riktigt i navigeringsmeny
- Gör det möjligt att distribuera kio_http_cache_cleaner i programinstallation eller packen
- KOpenWithDialog: Rätta att skapa skrivbordsfil med tom Mime-typ
- Läs protokollinformation från insticksprogrammets metadata
- Tillåt lokal distribution av kioslave
- Lägg till en .protocol i JSON converted
- Rätta att resultat skickas två gånger och saknad varning när listningen träffar på en katalog som inte går att komma åt (fel 333436)
- Bevara relativa länkmål när symboliska länkar kopieras (fel 352927).
- Använd lämpliga ikoner för standardkataloger i användarens hemkatalog (fel 352498)
- Lägg till ett gränssnitt som låter insticksprogram visa egna överlagrade ikoner
- Gör beroende av KNotifications i KIO (kpac) valfritt
- Gör doctools + wallet valfria
- Undvik krascher i kio om ingen D-Bus-server kör
- Lägg till KUriFilterSearchProviderActions, för att visa en lista över åtgärder för att söka efter någon text med användning av webbgenvägar
- Flytta alternativen för menyn "Skapa ny" från kde-baseapps/lib/konq till kio (fel 349654)
- Flytta konqpopupmenuplugin.desktop från kde-baseapps till kio (fel 350769)

### KJS

- Använd global variabel "_timezone" för MSVC istället för "timezone". Rättar byggning med MSVC 2015.

### KNewStuff

- Rätta skrivbordsfil för 'KDE:s partitionshanterare' och hemsidans webbadress

### KNotification

- Nu när kparts inte längre behöver knotification, beror bara saker som verkligen behöver underrättelser på det här ramverket
- Lägg till beskrivning och syfte för speech och phonon
- Gör beroende av phonon valfritt, en rent intern ändring, som det är gjort för speech.

### KParts

- Använd deleteLater i Part::slotWidgetDestroyed().
- Ta bort KNotifications beroende från KParts
- Använd funktion för att hitta plats för ui_standards.rc istället för att hårdkoda den, tillåter att resurs som reserv fungerar

### Kör program

- RunnerManager: Förenkla kod för laddning av insticksprogram

### KService

- KBuildSycoca: Spara alltid, även om ingen ändring av .desktop-filen upptäcktes (fel 353203).
- Gör doctools valfri
- kbuildsycoca: Tolka alla mimeapps.list filer som nämns i den nya specifikationen.
- Använd senaste tidsstämpeln i underkatalog som tidsstämpel för resurskatalog.
- Behåll Mime-typer separata vid konvertering av KPluginInfo till KPluginMetaData

### KTextEditor

- färgläggning: gnuplot: lägg till filändelse .plt
- Rätta valideringstips, tack till "Thomas Jarosch" &lt;thomas.jarosch@intra2net.com&gt;, lägg också till tips om validering vid kompileringstillfälle
- Krascha inte när kommandot inte är tillgängligt.
- Rätta fel nr. 307107
- Haskell färgläggningsvariabler börjar med _
- Förenkla git2 initiering, givet att vi kräver en tillräckligt ny version (fel 353947)
- Lägg in standardinställningar i resurs
- Syntaxfärgläggning (d-g): Använd standardstilar istället för hårdkodade färger
- Bättre skriptsökning, först användarens lokala grejer, sedan grejerna i våra resurser, sedan alla andra grejer. På så sätt kan användaren skriva över våra levererade skript med lokala.
- Paketera också alla JS-grejer i resurser, bara tre inställningsfiler saknas för att ktexteditor bara ska kunna användas som ett bibliotek utan några tillagda filer
- Nästa försök: Lägg in alla hopslagna XML-syntaxfiler i en resurs
- Lägg till genväg för byte av inmatningsläge (fel 347769)
- Lägg in XML-filer i resurs
- Syntaxfärgläggning (a-c): Flytta till de nya standardstilarna, ta bort hårdkodade färger
- Syntaxfärgläggning: Ta bort hårdkodade färger och använda förvalda stilar istället
- Syntaxfärgläggning: Använda nya förvalda stilar (tar bort hårdkodade färger)
- Bättre förvald stil för "import"
- Introducera "Spara som med kodning" för att spara en fil med en annan kodning, med användning av den snyggt grupperade kodningsmenyn vi har, och ersätt alla dialogrutor för att spara med operativsystemets rätta utan att den här viktiga funktionen går förlorad.
- Lägg in ui-fil i bibliotek, genom att använda min utökning av xmlgui
- Utskrift tar åter hänsyn till valt teckensnitts- och färgschema (fel 344976)
- Använd Breeze-färger för sparade och ändrade rader
- Förbättrade standardfärger för ikonkanter i schemat "Normal"
- autobrace: Infoga bara parentes när nästa bokstav är tom eller inte alfanumerisk
- autobrace: Om inledande parentes tas bor med backsteg, ta också bort avslutande
- autobrace: Upprätta bara anslutning en gång
- autobrace: Sluka avslutande parentes under vissa villkor
- Rätta att shortcutoverride inte skickas vidare till huvudfönstret
- Fel 342659: Förvald färg för "parentesfärgläggning" är svår att se (normalschema rättat) (fel 342659)
- Lägg till riktiga standardfärger för färgen på "aktuellt radnummer"
- Parentesmatchning och automatiska parenteser: Dela kod
- Parentesmatchning: Skydd för negativ maxLines
- Parentesmatchning: Bara för att det nya intervallet motsvarar det gamla betyder det inte att ingen uppdatering behövs
- Lägg till bredden ett halvt mellanslag för att tillåta att markören ritas vid radslut
- Rätta några problem med hög upplösning för ikonkanten
- Rätta fel nr. 310712: Ta också bort efterföljande mellanslag på raden med markören (fel 310712)
- Visa bara meddelandet "markering gjord" när vi-inmatningsläge är aktivt
- Ta bort &amp; från knapptext (fel 345937)
- Rätta uppdatering av färg på aktuellt radnummer (fel 340363)
- Implementera att parenteser infogas när en parentes skrivs in för en markering (fel 350317)
- Automatiska parenteser (fel 350317)
- Rätta alert HL (fel 344442)
- Ingen rullning av kolumner när dynamisk radbrytning är på
- Kom ihåg om färgläggning ställts in av användaren mellan sessioner, för att inte förlora det när man sparar efter en återställning (fel 332605)
- Rätta textvikning för Tex (fel 328248)
- Rättade fel nr. 327842: Slutet av C-typ av kommentar detekteras felaktigt (fel 327842)
- Spara och återställ dynamisk radbrytning när session sparas eller återställs (fel 284250)

### KTextWidgets

- Lägg till en ny undermeny i KTextEdit för att byta mellan språk för stavningskontroll
- Rättade inläsning av standardinställningar för Sonnet

### Ramverket KWallet

- Använd KDE_INSTALL_DBUSINTERFACEDIR för att installera D-Bus-gränssnitt
- Rättade varningar för plånbokens inställningsfil vid inloggning (fel 351805)
- Lägg till riktiga prefix för utmatning från kwallet-pam

### KWidgetsAddons

- Lägg till hopfällbar grafisk omgivningskomponent, KCollapsibleGroupBox
- KNewPasswordWidget: Saknar initiering av färger
- Introducera KNewPasswordWidget

### KXMLGUI

- kmainwindow: Fyll i översättarinformation i förväg om tillgänglig (fel 345320).
- Tillåt att tangenten för sammanhangsberoende meny (nere till höger) tilldelas till genvägar (fel 165542)
- Lägg till funktion för att fråga efter standardplats för XML-fil
- Tillåt att ramverket kxmlgui används utan några installerade filer
- Lägg till saknade nödvändiga beroenden

### Plasma ramverk

- Rätta att objekt i TabBar är hoptryckta från början, vilket exempelvis kunde ses i Kickoff efter att Plasma startats
- Rätta att inget val av åtgärder erbjuds när filer släpps på skrivbordet eller panelen
- Ta hänsyn till QApplication::wheelScrollLines från ScrollView
- Använd bara BypassWindowManagerHint på X11-plattform
- Ta bort gammal panelbakgrund
- Mer läsbar spinnare vid små storlekar
- Färglagd vyhistorik
- kalender: Gör hela rubrikområdet klickbart
- kalender: Använd inte aktuellt dagnummer i goToMonth
- kalender: Rätta uppdatering av tioårsöversikt
- Ge Breeze-ikoner tema när de läses in via IconItem
- Rätta knappegenskapen minimumWidth (fel 353584)
- Introducera signalen appletCreated
- Plasma Breeze-ikon: Lägg till SVG id-element för styrplatta
- Plasma Breeze-ikon: Ändra styrplatta till 22x22 bildpunkters storlek
- Breeze-ikon: Lägg till grafisk ikonkomponent till anteckningar
- Ett skript för att ersätta hårdkodade färger med stilmallar
- Verkställ SkipTaskbar vid ExposeEvent
- Ställ inte in SkipTaskbar för varje händelse

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
