---
aliases:
- ../announce-applications-16.08.3
changelog: true
date: 2016-11-10
description: KDE levererar KDE-program 16.08.3
layout: application
title: KDE levererar KDE-program 16.08.3
version: 16.08.3
---
10:e november, 2016. Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../16.08.0'>KDE-program 16.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än 20 registrerade felrättningar omfattar förbättringar av bland annat kdepim, ark, umbrello och Minröjaren.

Utgåvan inkluderar också versioner för långtidsunderhåll av KDE:s utvecklingsplattform 4.14.26.
