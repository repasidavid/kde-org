---
aliases:
- ../announce-applications-17.08.0
changelog: true
date: 2017-08-17
description: KDE levererar KDE-program 17.08.0
layout: application
title: KDE levererar KDE-program 17.08.0
version: 17.08.0
---
17:e augusti, 2017. KDE Program 17.08 är här. På det hela taget, har vi arbetat för att göra både programmen och de underliggande biblioteken stabilare och enklare att använda. Genom att undanröja rynkor och lyssna på återkoppling, har vi gjort KDE:s programsvit mindre benägen att råka ut för tekniska fel och mycket vänligare. Njut av de nya programmen!

### Mer konvertering till KDE Ramverk 5

Vi är nöjda med att följande program som var baserade på kdelibs4 nu är baserade på KDE Ramverk 5: kmag, kmousetool, kgoldrunner, kigo, konquest, kreversi, ksnakeduel, kspaceduel, ksudoku, kubrick, lskat och umbrello. Tack till de hårt arbetande utvecklarna som bidrog med sin tid och sitt arbete för att få det att hända.

### Vad är nytt i KDE-program 17.08

#### Dolphin

Utvecklarna av Dolphin rapporterar att Dolphin nu visar 'Borttagningstid' i papperskorgen, och visar 'Skapad tid' om operativsystemet stöder det, såsom för BSD.

#### KIO-Extras

Kio-Extras tillhandahåller nu bättre stöd för delade Samba-resurser.

#### Kalgebra

Utvecklarna av Kalgebra har arbetat med att förbättra sitt Kirigami-gränssnitt på skrivbordet, och implementerat kodkomplettering.

#### Kontact

- Utvecklare har åter aktiverat stöd för Akonadi-överföring i KMailtransport, skapat stöd för insticksprogram, och återskapat stöd för sendmail brevöverföring.
- I SieveEditor har många fel för automatiskt skapade skript rättats och stängts. Tillsammans med allmän felrättning har en radeditor med stöd för reguljära uttryck lagts till.
- I KMail har möjligheten att använda en extern editor återskapats som ett insticksprogram.
- Akonadi importguiden har nu 'konvertera allt konvertering' som ett insticksprogram, så att utvecklare enkelt kan skapa nya konverterare.
- Program beror nu på Qt 5.7. Utvecklare har rättat ett stort antal kompileringsfel på Windows. Hela kdepim går inte att kompilera på Windows än, men utvecklarna har gjort stora framsteg. Till att börja med skapade utvecklarna ett craft-recipe för det. Många felrättningar har gjorts för att modernisera koden (C++ 11). Wayland-stöd på Qt 5.9. Kdepim-runtime har lagt till en resurs för Facebook.

#### Kdenlive

I Kdenlive har gruppen rättat den felaktiga "Fryseffekten". I senare versioner var det omöjligt att ändra den frysta bildrutan i fryseffekten. Nu tillåts en snabbtangent för att extrahera bildruta. Användaren kan nu spara skärmbilder från tidslinjen med en snabbtangent, och ett namn baserat på bildnummer föreslås: <a href='https://bugs.kde.org/show_bug.cgi?id=381325'>https://bugs.kde.org/show_bug.cgi?id=381325</a>. Rättning så att nerladdade övergångsluminanser nu inte visas i gränssnittet: <a href='https://bugs.kde.org/show_bug.cgi?id=382451'>https://bugs.kde.org/show_bug.cgi?id=382451</a>. Rättning av problem med ljudklick (kräver för närvarande att beroende MLT byggs från git till en ny MLT utgåva görs): <a href='https://bugs.kde.org/show_bug.cgi?id=371849'>https://bugs.kde.org/show_bug.cgi?id=371849</a>.

#### Krfb

Utvecklarna har slutfört konvertering av X11-insticksprogrammet till Qt5, och krfb fungerar återigen med användning av ett X11-gränssnitt som är mycket snabbare än Qt-insticksprogrammet. Det finns en ny inställningssida som låter användaren ändra föredraget insticksprogram för rambuffert.

#### Konsole

Konsole tillåter nu obegränsad historik att utökas förbi 2 GiB-gränsen (32 bitar). Nu låter Konsole användare mata in vilken plats som helst för att lagra historikfiler. Dessutom har en regression rättats, så att Konsole återigen tillåter att Konsole-delprogrammet anropar dialogrutan Hantera profil.

#### KAppTemplate

Det finns nu ett nytt alternativ i Kapptemplate för att installera nya mallar från filsystemet. Fler mallar har tagits bort från Kapptemplate och istället integrerats i motsvarande produkter: insticksprogrammallen ktexteditor och mallen kapartsapp (nu konverterad till Qt5/KF5) ingår i KDE Ramverken KTextEditor och KParts sedan 5.37.0. Ändringarna bör förenkla att skapa mallar i KDE Program.

### Felutplåning

Mer än 80 fel har rättats i program, inklusive Kontact-sviten, Ark, Dolphin, K3b, Kdenlive, KGpg, Konsole med flera.

### Fullständig ändringslogg
