---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: KDE stelt KDE Applicaties 15.04.2 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 15.04.2 beschikbaar
version: 15.04.2
---
2 juni 2015. Vandaag heeft KDE de tweede update voor stabiliteit vrijgegeven voor <a href='../15.04.0'>KDE Applicaties 15.04</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 30 aangegeven bugreparaties inclusief verbeteringen aan gwenview, kate, kdenlive, kdepim, konsole, marble, kgpg, kig, ktp-call-ui en umbrello.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van Plasma Workspaces 4.11.20, KDE Development Platform 4.14.9 en de Kontact Suite 4.14.9.
