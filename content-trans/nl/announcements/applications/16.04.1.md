---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE stelt KDE Applicaties 16.04.1 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 16.04.1 beschikbaar
version: 16.04.1
---
10 mei 2016. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../16.04.0'>KDE Applicaties 16.04</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 25 aangegeven bugreparaties inclusief verbeteringen aan kdepim, ark, kate, dolphin, kdenlive, lokalize, spectacle, naast anderen.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.20.
