---
aliases:
- ../announce-applications-15.12-rc
date: 2015-12-03
description: KDE stelt Applications 15.12 Release Candidate beschikbaar.
layout: application
release: applications-15.11.90
title: KDE stelt de Release Candidate van KDE Applicaties 15.12 beschikbaar
---
3 december 2014. Vandaag heeft KDE de "release candidate" van de nieuwe versies van KDE Applications vrijgegeven. Met het bevriezen van afhankelijkheden en functies, is het team van KDE nu gefocust op repareren van bugs en verder oppoetsen.

Met de verschillende toepassingen gebaseerd op KDE Frameworks 5, weet de KDE Applications 15.12 uitgave de kwaliteit en gebruikservaring te handhaven en te verbeteren. Echte gebruikers zijn kritisch in het proces om de hoge kwaliteit van KDE te handhaven, omdat ontwikkelaars eenvoudig niet elke mogelijke configuratie kunnen testen. We rekenen op u om in een vroeg stadium bugs te vinden zodat ze gekraakt kunnen worden voor de uiteindelijke vrijgave. Ga na of u met het team mee kunt doen door de uitgavekandidaat te installeren <a href='https://bugs.kde.org/'>en elke bug te rapporteren</a>.
