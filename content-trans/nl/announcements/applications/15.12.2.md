---
aliases:
- ../announce-applications-15.12.2
changelog: true
date: 2016-02-16
description: KDE stelt KDE Applicaties 15.12.2 beschikbaar
layout: application
title: KDE stelt KDE Applicaties 15.12.2 beschikbaar
version: 15.12.2
---
16 februari 2016. Vandaag heeft KDE de tweede stabiele update vrijgegeven voor <a href='../15.12.0'>KDE Applicaties 15.12</a> Deze uitgave bevat alleen bugreparaties en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 30 aangegeven bugreparaties inclusief verbeteringen aan kdelibs, kdepim, kdenlive, marble, konsole, spectacle, akonadi, ark en umbrello, naast anderen.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van KDE Development Platform 4.14.17.
