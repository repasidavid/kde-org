---
aliases:
- ../announce-applications-17.12-rc
custom_spread_install: true
date: 2017-12-01
description: KDE stelt Applications 17.12 Release Candidate beschikbaar.
layout: application
release: applications-17.11.90
title: KDE stelt de Release Candidate van KDE Applicaties 17.12 beschikbaar
---
1 december 2017. Vandaag heeft KDE de "release candidate" van de nieuwe versies van KDE Applications vrijgegeven. Met het bevriezen van afhankelijkheden en functies, is het team van KDE nu gefocust op repareren van bugs en verder oppoetsen.

Kijk in de <a href='https://community.kde.org/Applications/17.12_Release_Notes'>uitgavenotities van de gemeenschap</a> voor informatie over nieuwe tarballs, tarballs die nu op KF5 zijn gebaseerd en bekende problemen. Een meer complete aankondiging zal beschikbaar zijn voor de uiteindelijke uitgave

De uitgave KDE Applications 17.12 heeft grondig testen nodig om de kwaliteit en gebruikservaring te handhaven en te verbeteren. Echte gebruikers zijn kritiek in het proces om de hoge kwaliteit van KDE te handhaven, omdat ontwikkelaars eenvoudig niet elke mogelijke configuratie kunnen testen. We rekenen op u om in een vroeg stadium bugs te vinden zodat ze gekraakt kunnen worden voor de uiteindelijke vrijgave. Ga na of u met het team mee kunt doen door de uitgavekandidaat te installeren <a href='https://bugs.kde.org/'>en elke bug te rapporteren</a>.

#### Installeren van KDE Applications 17.12 Release Candidate binaire pakketten

<em>Pakketten</em>. Sommige Linux/UNIX OS leveranciers zijn zo vriendelijk binaire pakketten van KDE Applications 17.12 Release Candidate (intern 17.11.90) voor sommige versies van hun distributie en in andere gevallen hebben vrijwilligers uit de gemeenschap dat gedaan. Extra binaire pakketten, evenals updates voor de pakketten zijn nu beschikbaar of zullen beschikbaar komen in de komende weken.

<em>Pakketlocaties</em>. Voor een huidige lijst met beschikbare binaire pakketten waarover het KDE Project is geïnformeerd, bezoekt u de <a href='http://community.kde.org/Binary_Packages'>Wiki van de gemeenschap</a>.

#### De KDE Applications 17.12 Release Candidate compileren

De complete broncode voor KDE Applications 17.12 Release Candidate kan <a href='http://download.kde.org/unstable/applications/17.11.90/src/'>vrij gedownload</a> worden. Instructies over compileren en installeren zijn beschikbaar vanaf de <a href='/info/applications/applications-17.11.90'>Informatiepagina van KDE Applications 17.12 Release Candidate</a>.
