---
aliases:
- ../../kde-frameworks-5.76.0
date: 2020-11-07
layout: framework
libCount: 70
qtversion: 5.12
---
### Baloo

* CJK termen splitsen bij tekens voor punctuatie, code optimaliseren
* Herzie de broncode in voorbereiding voor integratie in ICU

### Breeze pictogrammen

* 48px pictogram voor dialoogwaarschuwing toevoegen
* Stijl van media-repeat-single wijzigen naar nummer 1 gebruiken
* Meer door git genegeerde bestanden toevoegen
* bestaan van bestand controleren alvorens te verwijderen
* Bestemmingsbestand altijd eerst verwijderen bij genereren van symbolische koppelingen
* Enige gekleurde pictogrammen voor Okular toevoegen
* Pictogrammen voor taak-voltooid toevoegen (bug 397996)
* Netwerk-beperking-pictogram toevoegen
* Kopieer 32px kup symlink naar apps/48 om falen van schaalbare test te repareren
* Pictogram voor bijeenkomst-organisator toevoegen (bug 397996)
* Vingerafdrukpictogram toevoegen
* Pictogrammen voor terugkerende taken en terugkerende afspraken toevoegen (bug 392533)
* Schakel de generatie-pictogrammen tijdelijk uit op Windows
* Symbolische koppeling kup.svg naar preferences-system-backup.svg maken

### Extra CMake-modules

* Laat androiddeployqt bibliotheken en QML plug-ins zonder installatie vinden
* find-modules/FindReuseTool.cmake - zoeken naar hulpmiddel reuse repareren
* Standaard formatteeropties verbeteren
* Optie invoegen om LLVM voor gebruikers met Qt < 5.14 te gebruiken
* ontbrekende minimum versie voor parameter RENAME toevoegen
* Wanneer FindGradle is toegevoegd documenteren
* FindGradle uit KNotification toevoegen

### KAuth

* De backend-naam eerder naar hoofdletters converteren
* Helper toevoegen om de uid van de aanroeper te verkrijgen

### KCalendarCore

* Dubbelzinnigheid in ICalFormat::toString() in testen laten zien
* Eigenschapserialisatie van COLOR uit RFC7986 toevoegen
* MemoryCalendar::rawEvents(QDate, QDate) laten werken voor open bindingen

### KCMUtils

* Overzetting van QStandardPaths::DataLocation naar QStandardPaths::AppDataLocation
* Ondersteuning voor naamruimte toevoegen aan KCModuleData CMake macro
* KSettings::PluginPage afkeuren
* referentie naar ongedefinieerde header verwijderen
* De KCMUtilsGenerateModuleData verplaatsen naar juiste plaats
* Alle vooraf aangemaakte subpagina's van een KCM "push"en
* Functie CMake toevoegen om basis modulegegevens te genereren
* Leesbaarheid van inline QML in kcmoduleqml.cpp verbeteren
* Juiste kophoogte met een plaatshouder
* [kcmoduleqml] topmarge voor QML KCM's repareren

### KConfig

* Ontbrekende Qt5DBus afhankelijkheid zoeken
* kconf_update: herhaalde testen in --testmode toestaan door kconf_updaterc te negeren

### KConfigWidgets

* http: naar https: wijzigen

### KContacts

* Bug 428276 repareren - KContacts kan niet gebruikt worden in qmake project (bug 428276)

### KCoreAddons

* KJob: setProgressUnit() toevoegen, om kiezen hoe percentage wordt berekend
* Potentieel geheugenlek in KAboutData::registerPluginData repareren
* suggestName() opsplitsen; de opsplitsmethode controleert niet of bestand bestaat
* KAboutData: pluginData() & registerPluginData() afkeuren
* De event-loop in KJobTest::slotResult() niet afbreken
* Ffunctor gebaseerde singleShot() overload in TestJob::start() gebruiken

### KDeclarative

* [abstractkcm] expliciete opvulling instellen
* [simplekcm] aangepaste opvulbehandeling verwijderen
* [kcmcontrols] gedupliceerde code verwijderen
* Bron aan KDeclarativeMouseEvent toevoegen
* overlaysheets naar de root van ouder voorzien
* GridViewKCM en ScrollVieKCM laten erven van AbstractKCM
* Getter-methode voor subPages toevoegen

### KDocTools

* xml-indeling in contributor.entities repareren
* Korean update: HTML bestanden van GPL, FDL opnieuw formattering en LGPL toevoegen

### KFileMetaData

* [ExtractionResult] binaire compatibiliteit herstellen
* [TaglibWriter|Extractor] raw speex mime-type verwijderen
* [TaglibWriter] de lezen-schrijven ook op Windows openen
* [Extractor|WriterCollection] niet-bibliotheekbestanden uitfilteren
* [EmbeddedImageData] probeer om MSVC domheid heen te werken
* [ExtractionResult] ExtractEverything afkeuren, reparatie sinds
* [EmbeddedImageData] testgrondwaarheidsimage slecht één keer lezen
* [EmbeddedImageData] private implementatie van hoesschrijven verwijderen
* [EmbeddedImageData] schrijfimplementatie naar taglib writerplug-in verplaatsen
* [EmbeddedImageData] private implementatie van hoesextractie verwijderen
* [EmbeddedImageData] implementatie naar taglib extractorplug-in verplaatsen

### KGlobalAccel

* systemd dbus activatie

### KIconThemes

* Aspectverhouding behouden bij omhoog schalen

### KIdleTime

* Enkel-arg signaal KIdleTime::timeoutReached(int identifier) afkeuren

### KImageFormats

* Ondersteuning voor RLE gecomprimeerde, 16 bits per kanaal PSD bestanden toevoegen
* Niet-ondersteund teruggeven bij lezen van 16 bit RLE gecomprimeerde PSD bestanden
* feat: ondersteuning van psd kleurdiepte == 16 formaat toevoegen

### KIO

* Onder voorwaarde vergelijken met blanke QUrl in plaats van / op Windows met mkpathjob
* KDirModel: twee reparaties voor QAbstractItemModelTester
* CopyJob: overgeslagen bestanden meenemen in voortgangsberekeningen bij hernoemen
* CopyJob: overgeslagen bestanden in de melding niet tellen (bug 417034)
* In bestandsdialogen, selecteer een bestaande map bij poging het aan te maken
* CopyJob: totaal aantal bestanden/mapppen in voortgangsdialoog (bij verplaatsen)
* FileJob::write() zich consistent laten gedragen
* Ondersteuning voor xattrs in kio kopiëren/verplaatsen
* CopyJob: geen mapgroottes meetellen in de totale grootte
* KNewFileMenu: crash repareren door m_text te gebruiken in plaats van m_lineEdit->text()
* FileWidget: geselecteerde bestandsvoorbeeld tonen bij verlaten van de muis (bug 418655)
* helpveld van gebruikerscontext meegeven in kpasswdserver
* KNewFileMenu: NameFinderJob gebruiken om een "Nieuwe map" naam te krijgen
* NameFinderJob die nieuwe "Nieuwe map" namen suggereert introduceren
* Niet expliciet exec-regels definiëren voor KCM's (bug 398803)
* KNewFileMenu: de code voor aanmaken van de dialoog splitsen naar een gescheiden methode
* KNewFileMenu: controleren of bestand niet al bestaat met vertraging voor verbeterde bruikbaarheid
* [PreviewJob] voldoende geheugen toewijzen voor SHM segment (bug 427865)
* Mechanisme voor versies gebruiken om de nieuwe plaatsen voor bestaande gebruikers toe te voegen
* Bladwijzers voor plaatjes, muziek en video's toevoegen (bug 427876)
* kfilewidget: de tekst in het naamvak behouden bij navigeren (bug 418711)
* KCM's in OpenUrlJob behandelen met KService API
* Bestandspad canoniek maken bij ophalen en aanmaken van miniaturen
* KFilePlacesItem: kdeconnect sshfs aankoppelingen verbergen
* OpenFileManagerWindowJob: venster uit hoofdjob juist oppakken
* Onnodig aftasten naar niet-bestaande miniatuurafbeeldingen vermijden
* [BUG] regressie bij selecteren van bestanden die `#` bevatten repareren
* KFileWidget: pictogramzoomknoppen naar de dichtstbijzijnde standaard grootte laten springen
* minimumkeepsize actueel in de netpref KCM stoppen (bug 419987)
* KDirOperator: de logica van zoomschuifregelaar van pictogrammen vereenvoudigen 
* UDSEntry: het verwachte tijdformaat voor tijdsleutels documenteren
* kurlnavigatortest: het bureaublad verwijderen:, heeft desktop.protocol nodig om te werken
* KFilePlacesViewTest: geen venster tonen, niet nodig
* OpenFileManagerWindowJob: crash repareren bij terugvallen op KRun-strategie (bug 426282)
* Internetsleutelwoorden: crash en mislukte tests repareren als scheidingsteken spatie is
* Voorkeur geven aan DuckDuckGo bangs boven andere scheidingstekens
* KFilePlacesModel: verborgen plaatsen negeren bij uitrekenen van closestItem (bug 426690)
* SlaveBase: gedrag van ERR_FILE_ALREADY_EXIST met copy() documenteren
* kio_trash: de logica repareren wanneer geen groottelimiet is ingesteld (bug 426704)
* In bestandsdialogen een map maken die al bestaat zou deze moeten selecteren
* KFileItemActions: eigenschap voor min/max aantal Url's toevoegen

### Kirigami

* [avatar]: getallen ongeldige namen maken
* [avatar]: cache-eigenschap van afbeelding laten zien
* Ook maximumWidth voor pictogrammen in globale la instellen (bug 428658)
* Afsluitsneltoets een actie maken en het blootstellen als een alleen-lezen eigenschap
* Handcursors gebruiken op ListItemDragHandle (bug 421544)
* [controls/avatar]: CJK namen voor initialen ondersteunen
* Uiterlijk van de formulierindeling op mobiel verbeteren
* Menu's in contextualActions repareren
* Item in code aangeroepen uit destructor van Item niet veranderen (bug 428481)
* de andere indeling-reversetwins niet wijzigen
* Focus naar overlay-vel zetten/weghalen bij openen/sluiten
* Alleen venster verslepen met de globale werkbalk bij indrukken & slepen
* OverlaySheet sluiten wanneer Esc-toets wordt ingedrukt
* Pagina: opvullen van eigenschappen in horizontalPadding en verticalPadding werkend maken
* ApplicationItem: achtergrondeigenschap gebruiken
* AbstractApplicationItem: ontbrekende eigenschappen & gedrag uit QQC2 ApplicationWindow toevoegen
* itembreedte beperken tot indelingsbreedte
* Terugknop die niet verschijnt op gelaagde paginakoppen op mobiel repareren
* Waarschuwing over "te activeren" bindinglus in ActionToolBar laten verstommen
* de volgorde van kolommen op rtl indelingen omwisselen
* omleiding om zeker te zijn dat ungrabmouse elke keer wordt aangeroepen
* controleren op bestaan van startSystemMove
* scheidingsteken op gespiegelde indelingen repareren
* venster verslepen door op lege gebieden te klikken
* niet schuiven bij slepen met muis
* Gevallen repareren wanneer het antwoord nul is
* Fout herontwerp van Forward/BackButton.qml repareren
* Nagaan leeg pictogram is gereed end wordt niet getekend als vorige pictogram
* Hoogte van knop terug/voorwaarts in PageRowGlobalToolBarUI beperken
* Console-spam uit ContextDrawer laten verstommen
* Console-spam uit ApplicationHeader laten verstommen
* Console-spam uit knoppen terug/voorwaarts laten verstommen
* Slepen met de muis voorkomen bij verslepen van een OverlaySheet
* Voorvoegsel lib laten vallen bij bouwen voor Windows
* beheer van uitlijning van twinformlayouts repareren
* Leesbaarheid van ingebedde QML in C++ code verbeteren

### KItemModels

* KRearrangeColumnsProxyModel: crash repareren wanneer er geen bronmodel is
* KRearrangeColumnsProxyModel: alleen kolom 0 heeft afgesplitsten

### KNewStuff

* Foute logica geïntroduceerd in e1917b6a repareren
* Crash door dubbel verwijderen in kpackagejob (bug 427910)
* Button::setButtonText() afkeuren en API-documenten repareren, er is niets voorgevoegd
* Alle schrijven naar op-schijf-cache uitstellen totdat we een rustige seconde hebben gehad
* Crash wanneer lijst van geïnstalleerde bestanden leeg is repareren

### KNotification

* KNotification::activated() markeren als afgekeurd
* Enige controles op juistheid toepassen op actietoetsen (bug 427717)
* FindGradle uit ECM gebruiken
* Conditie om dbus te gebruiken repareren
* Reparatie: verouderd vak op platforms zonder dbus inschakelen
* notifybysnore herschrijven om meer betrouwbare ondersteuning voor Windows te leveren
* Commentaar toevoegen aan DesktopEntry-veld in bestand notifyrc

### KPackage-framework

* Waarschuwing "geen metagegevens" een ding bij alleen-bij-debuggen maken

### KPty

* Ondersteuning van AIX, Tru64, Solaris, Irix uitrukken

### KRunner

* Verouderde RunnerSyntax methoden afkeuren
* ignoreTypes en RunnerContext::Type afkeuren
* Het type niet naar bestand/map zetten als het niet bestaat (bug 342876)
* Onderhouder bijwerken zoals genoemd in de e-maillijst
* Ongebruikte constructor voor RunnerManager afkeuren
* Categorieën mogelijkheid afkeuren
* Onnodige controle of starten is onderbroken verwijderen
* Methoden defaultSyntax en setDefaultSyntax afkeuren
* Niet-functioneel gebruik van RunnerSyntax opschonen

### KService

* NotShowIn=KDE toepassingen, in de lijst mimeapps.list, toestaan te worden gebruikt (bug 427469)
* Terugvalwaarde voor KCM Exec regels schrijven met het juiste programma (bug 398803)

### KTextEditor

* [EmulatedCommandBar::switchToMode] niets doen wanneer de oude en nieuwe modi are hetzelfde zijn (bug 368130 als volgt:)
* KateModeMenuList: speciale marges voor Windows verwijderen
* Geheugenlek in KateMessageLayout repareren
* verwijderen van aangepaste stijlen voor accentueringen waar we niets aan veranderden proberen te vermijden (bug 427654)

### KWayland

* Gemaksmethodes bieden rond wl_data_offet_accept()
* Enums markeren in een Q_OBJECT, Q_ENUM

### KWidgetsAddons

* nieuw setUsernameContextHelp op KPasswordDialog
* KFontRequester: de nu overbodige, nearestExistingFont helper, verwijderen

### KWindowSystem

* xcb: detectie van schermgroottes voor High-DPI repareren

### NetworkManagerQt

* Enum en declaraties toevoegen om doorgeven van mogelijkheden in het registratieproces aan NetworkManager toe te staan

### Plasma Framework

* BasicPlasmoidHeading component
* Altijd knoppen ExpandableListitem tonen, niet alleen bij er boven zweven (bug 428624)
* [PlasmoidHeading]: impliciete afmetingen juist instellen
* De kleuren van de kop van Breeze Dark en Breeze Light vergrendelen (bug 427864)
* Beeldverhouding van 32px en 22px pictogrammen van batterij unificeren
* Tips over marges aan toolbar.svg toevoegen en PC3 werkbalk opnieuw ontwerpen
* AbstractButton en Paneel aan PC3 toevoegen
* exclusieve actiegroepen in de contextuele acties ondersteunen
* Draaiende BusyIndicator zelfs indien zichtbaar, opnieuw
* Kleuren die niet worden toegepast aan pictogram van mobiele takenschakelaarpictogram
* Takenschakelaar van plasma mobile toevoegen en toepassingpictogrammen sluiten (voor takenpaneel)
* Beter menu in PlasmaComponents3
* Onnodige ankers in het ComboBox.contentItem verwijderen
* Positie van hendel van schuifregelaar afronden
* [ExpandableListItem] uitgevouwen weergave op afroep laden
* Ontbrekende `PlasmaCore.ColorScope.inherit: false` toevoegen
* PlasmoidHeading colorGroup in root-element zetten
* [ExpandableListItem] gekleurde tekst 100% dekking geven (bug 427171)
* BusyIndicator: niet draaien indien onzichtbaar (bug 426746)
* ComboBox3.contentItem moet een QQuickTextInput zijn om automatisch aanvullen te repareren (bug 424076)
* FrameSvg: de cache niet resetten bij wijziging van grootte
* Plasmoids omschakelen wanneer sneltoets is geactiveerd (bug 400278)
* TextField 3: ontbrekende import toevoegen
* ID's in plasmavault_error pictogram repareren
* PC3: kleur van TabButton-label repareren
* Een tip gebruiken in plaats van een bool
* Plasmoids toestaan de marges te negeren

### Omschrijving

* Beschrijving toevoegen aan kaccounts-youtube-leverancier

### QQC2StyleBridge

* Bindinglus van ToolBar contentWidth repareren
* Label van sneltoets direct met id refereren in plaats van impliciet
* ComboBox.contentItem moet een QQuickTextInput zijn om automatisch aanvullen te repareren (bug 425865)
* Conditionele clausules in verbindingen vereenvoudigen
* Waarschuwingen over verbindingen repareren op ComboBox
* Ondersteuning toevoegen voor qrc-pictogrammen aan StyleItem (bug 427449)
* Status van focus juist aangeven van ToolButton
* TextFieldContextMenu toevoegen voor rechts klik contextmenu's op TextField en TextArea
* Een achtergrondkleur instellen in ScrollView van ComboBox

### Solid

* Ondersteuning voor sshfs aan de fstab-backend toevoegen
* CMake: pkg_search_module gebruiken bij zoeken naar plist
* Backend imobiledevice repareren: API versie voor DEVICE_PAIRED controleren
* Bouwen van imobiledevice-backend repareren
* Backend Solid toevoegen bij gebruik van libimobiledevice voor zoeken naar iOS apparaten
* QHash gebruiken voor overeen laten komen waar volgorde niet nodig is

### Sonnet

* Moderne syntaxis van signaal-slot-verbinding gebruiken

### Accentuering van syntaxis

* De kernfunctie "compact" ontbreekt
* maak de controle commentaar, voeg commentaar toe waarom dit niet langer hier werkt
* De waarde position:sticky ontbreekt
* php/* generatie voor nieuw Comments hl repareren
* Functie: Alerts zonder Special-Comments syntaxis vervangen en Modelines verwijderen
* Functie: de `comments.xml` toevoegen als een paraplu-syntaxis voor verschillende soorten commentaar
* Reparatie: CMake syntax markeert nu `1` en `0` als speciale booleaanse waarden
* Verbetering: modelines-regels invoegen in bestanden waar Alerts zijn toegevoegd
* Verbetering: wat meer booleaanse waarden toevoegen aan `cmake.xml`
* Overbelichte thema's: scheidingsteken verbeteren
* Verbeteringen: bijwerkelementen voor CMake 3.19
* Ondersteuning voor eenheidsbestanden van systemd toevoegen
* debchangelog: Hirsute Hippo toevoegen
* Functie: meerdere `-s` opties bieden voor hulpmiddel `kateschema2theme`
* Verbetering: verschillende testen toevoegen aan de converter
* meer hulpbronscripts naar een betere locatie verplaatsen
* move update-kate-editor-org.pl script naar een betere plaats verplaatsen
* kateschema2theme: een Python hulpmiddel toevoegen aan converteren van oude schemabestanden
* Dekkind verminderen in scheidingsteken van Breeze & Dracula thema's
* README met "Kleurthemabestanden" sectie bijwerken
* Reparatie: `KDE_INSTALL_DATADIR` gebruiken bij installeren van syntaxisbestanden
* rendering van --syntax-trace=region met meerdere grafieken op dezelfde offset
* enige problemen voor fish-shell repareren
* StringDetect door DetectChar / Detect2Chars vervangen
* enige RegExpr vervangen door StringDetect
* RegExpr="." + lookAhead vervangen door fallthroughContext
* \s* vervangen door DetectSpaces

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
