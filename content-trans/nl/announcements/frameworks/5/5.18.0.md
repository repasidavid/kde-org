---
aliases:
- ../../kde-frameworks-5.18.0
date: 2016-01-09
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Verschillende problemen met aan mtime gerelateerd zoeken repareren
- PostingDB Iter: Niets toekennen aan MDB_NOTFOUND
- Balooctl status: Tonen van 'Indexering van inhoud' over mappen vermijden
- StatusCommand: de juiste status voor mappen tonen
- SearchStore: behandel lege waarden van termen netjes (bug 356176)

### Breeze pictogrammen

- updates van pictogrammen en toevoegingen
- ook 22px grootte voor statuspictogrammen voor 32px omdat u deze nodig hebt in het systeemvak
- Vaste naar schaalbare waarde naar 32ps mappen in Breeze Dark gewijzigd

### Extra CMake-modules

- De KAppTemplate CMake module globaal maken
- CMP0063-waarschuwingen met KDECompilerSettings stil maken
- ECMQtDeclareLoggingCategory: &lt;QDebug&gt; met het gegenereerde bestand invoegen
- CMP0054-waarschuwingen repareren

### KActivities

- Het laden van QML voor KCM stroomlijnen (bug 356832)
- Work-around voor de Qt SQL bug die verbindingen niet juist schoont (bug 348194)
- Een plug-in samengevoegd die toepassingen uitvoert bij statuswijziging van activiteit
- Overbrengen van KService naar KPluginLoader
- Verander plug-ins om kcoreaddons_desktop_to_json() te gebruiken

### KBookmarks

- Initialiseer DynMenuInfo volledig in teruggegeven waarde

### KCMUtils

- KPluginSelector::addPlugins: toekenning als 'config' parameter de standaard is repareren (bug 352471)

### KCodecs

- Bewust een volle buffer overladen vermijden

### KConfig

- Ga na dat group op de juiste manier de escape verwerkt in kconf_update

### KCoreAddons

- KAboutData::fromPluginMetaData(const KPluginMetaData &amp;plugin) toevoegen
- KPluginMetaData::copyrightText(), extraInformation() en otherContributors() toevoegen
- KPluginMetaData::translators() en KAboutPerson::fromJson() toevoegen
- use-after-free in desktop bestandenparser repareren
- Zorg dat KPluginMetaData is te construeren uit een json pad
- desktoptojson: een ontbrekend servicetypebestand een fout veroorzaken voor de binary
- aanroepen van kcoreaddons_add_plugin zonder SOURCES een fout laten zijn

### KDBusAddons

- Aanpassen aan dbus-in-secondary-thread van Qt 5.6

### KDeclarative

- [DragArea] eigenschap dragActive toevoegen
- [KQuickControlsAddons MimeDatabase] QMimeType commentaar laten zien

### KDED

- kded: aanpassen aan threaded dbus van Qt 5.6: messageFilter moet laden van module starten in de hoofdthread

### Ondersteuning van KDELibs 4

- kdelibs4support vereist kded (voor kdedmodule.desktop)
- CMP0064 waarschuwing repareren door policy CMP0054 op NEW in te stellen
- Geen symbolen exporteren die ook bestaan in KWidgetsAddons

### KDESU

- Geen fd lekken bij aanmaken van socket

### KHTML

- Windows: kdewin afhankelijkheid verwijderen

### KI18n

- De regel voor het eerste argument documenteren voor meervouden in QML
- Ongewenste wijzigingen in type verminderen
- Het mogelijk maken om dubbels te gebruiken voor i18np*() aanroepen in QML

### KIO

- kiod voor threaded dbus van Qt 5.6 repareren: messageFilter moet wachten totdat de module is geladen alvorens terug te keren
- De foutcode wijzigen bij doorgeven/verplaatsen in een submap
- Probleem met geblokkeerde emptyTrash repareren
- Verkeerde knop in KUrlNavigator voor remote URL's repareren
- KUrlComboBox: teruggeven van een absoluut pad uit urls() repareren
- kiod: sessiebeheer uitschakelen
- Automatisch aanvullen voor invoer van '.' die alle verborgen bestanden/mappen* aanbiedt (bug 354981)
- ktelnetservice: afwijking met één in controle argc repareren, patch van Steven Bromley

### KNotification

- [Notify By Popup] Stuur gebeurtenis-ID mee
- Stel standaard niet-leeg reden voor uitstellen van schermbeveiliging; (bug 334525)
- Een hint toevoegen om groepering van meldingen over te slaan (bug 356653)

### KNotifyConfig

- [KNotifyConfigWidget] Selectie van een specifieke gebeurtenis toestaan

### Pakket Framework

- Mogelijk maken de metagegevens in json aan te leveren

### KPeople

- Mogelijke dubbel verwijdering in DeclarativePersonData repareren

### KTextEditor

- Syntax h/l voor pli: ingebouwde functies toegevoegd, uitbreiding regio's toegevoegd

### KWallet Framework

- kwalletd: FILE* lek repareren

### KWindowSystem

- xcb variant voor statische KStartupInfo::sendFoo methoden toevoegen

### NetworkManagerQt

- het werkend maken met oudere NM-versies

### Plasma Framework

- [ToolButtonStyle] Altijd activeFocus aangeven
- De SkipGrouping-vlag gebruiken voor de melding "widget verwijderd" (bug 356653)
- Op de juiste manier omgaan met symlinks in pad naar pakketten
- HiddenStatus toevoegen voor zelf-verbergen van plasmoid
- Redirecting vensters stoppen wanneer item is uitgeschakeld of verborgen. (bug 356938)
- Geen statusChanged uitgeven als het niet is gewijzigd
- Element-id's voor oost-oriëntatie repareren
- Containment: geen appletCreated uitgeven met null applet (bug 356428)
- [Containment Interface] foutieve hoge precisie scrollen repareren
- Eigenschap van KPluginMetada X-Plasma-ComponentTypes lezen als een lijst met tekenreeksen
- [Window Thumbnails] niet crashen als Composite is uitgeschakeld
- Laat containments boven CompactApplet.qml gaan

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
