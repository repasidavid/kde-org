---
aliases:
- ../../kde-frameworks-5.39.0
date: 2017-10-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Alleen echte MIME-typen, bijv. niet "ruwe CD-image" (bug 364884)
- Remove pf.path() uit container alvorens de referentie erdoor verstoort wordt.remove()
- Tags van KIO-slave protocolbeschrijving gerepareerd
- Markdown-bestanden als Documenten beschouwen

### Breeze pictogrammen

- overflow-menu-pictogram toevoegen (bug 385171)

### Extra CMake-modules

- Python bindings compilatie na 7af93dd23873d0b9cdbac192949e7e5114940aa6 repareren

### Frameworkintegratie

- KStandardGuiItem::discard overeen laten komen met QDialogButtonBox::Discard

### KActivitiesStats

- De standaard querylimiet naar nul wijzigen
- Optie om modeltester in te schakelen toegevoegd

### KCMUtils

- KCMultiDialog schuifmogelijkheid maken (bug 354227)

### KConfig

- KStandardShortcut::SaveOptions verouderen

### KConfigWidgets

- KStandardAction::PasteText en KPasteTextAction verouderen

### KCoreAddons

- desktoptojson: heuristische typedetectie van verouderde service verbeteren (bug 384037)

### KDeclarative

- Licentie wijzigen naar LGPL2.1+
- openService() methode naar KRunProxy toegevoegd

### KFileMetaData

- crash repareren wanneer meer dan één exemplaar van ExtractorCollection vernietigd zijn

### KGlobalAccel

- "KGlobalAccel: overzetten naar nieuwe methode symXModXToKeyQt van KKeyServer, om toetsen op numerieke pad te repareren" terugdraaien (bug 384597)

### KIconThemes

- een methode om aangepast palet te resetten toevoegen
- qApp-&gt;palette() gebruiken wanneer geen aangepaste is gezet
- de juiste buffergrootte toekennen
- een eigen palet instellen toestaan in plaatst van colorSets
- de kleurenset voor het stijlblad laten zien

### KInit

- Windows: klauncher die absolute installeerpad gebruikt tijdens compileren voor het vinden van kioslave.exe repareren

### KIO

- kioexec: bewaak het bestand wanneer het kopiëren heeft beëindigd (bug 384500)
- KFileItemDelegate: reserveer altijd ruimte voor pictogrammen (bug 372207)

### Kirigami

- zet themabestand in niet in BasicTheme
- een nieuwe Verder-knop toevoegen
- minder contrast naar de achtergrond van de schuifbalk van het vel
- meer betrouwbaar invoegen en verwijderen uit overflow-menu
- betere rendering van contextpictogram
- voorzichtiger de actieknop centreren
- pictogramgroottes gebruiken voor actieknoppen
- pictogramgrootte past perfect met pixels op bureaublad
- geselecteerd effect om hendelpictogram te faken
- kleur van handvatten repareren
- betere kleur voor de hoofdactieknop
- contextmenu voor bureaubladstijl repareren
- beter "meer" menu voor de werkbalk
- een juist menu voor het contextmenu voot intermediaire pagina's
- een tekstveld toevoegen die een toetsenbord laat verschijnen
- niet crashen wanneer gestart met niet bestaande stijlen
- concept van kleurenset in thema
- beheer van wheel vereenvoudigen(bug 384704)
- nieuwe voorbeeldapp met bureaublad/mobiel hoofd-qml-bestanden
- ga na dat currentIndex geldig is
- Genereer de appstreammetagegevens van de gallerij-app
- Zoek naar QtGraphicalEffects, zodat pakketmakers het niet vergeten
- De besturing over de onderste decoratie niet invoegen (bug 384913)
- lichterr kleuren wanneer lijstweergave geen activeFocus heeft
- enige ondersteuning voor indelingen RnL
- Sneltoetsen uitschakelen wanneer een actie is uitgeschakeld
- de gehele plug-instructuue in de bouwmap aanmaken
- toegankelijkheid voor de hoofdpagina van de galerij repareren
- Als plasma niet beschikbaar is, is KF5Plasma dat ook niet. Zou de CI-fout moeten repareren

### KNewStuff

- Kirigami 2.1 vereisen in plaats van 1.0 voor KNewStuffQuick
- KPixmapSequence op de juiste manier aanmaken
- Niet klagen over het knsregistry-bestand niet aanwezig voordat het nuttig is

### KPackage-framework

- kpackage: bundel een kopie van servicetypes/kpackage-generic.desktop
- kpackagetool: bundel een kopie van servicetypes/kpackage-generic.desktop

### KParts

- KPartsApp sjabloon: installatielocatie van kpart-bureaubladbestand

### KTextEditor

- Standaard markering negeren in pictogramrand voor enkelvoudig selecteerbare markering
- QActionGroup gebruiken voor selectie van invoermodus
- ontbrekende balk voor spellingcontrole repareren (bug 359682)
- De terugval "zwartheid" waarde voor unicode &gt; 255 tekens repareren (bug 385336)
- Spaties achteraan voor visualisatie van RnL regels repareren

### KWayland

- OutputConfig sendApplied / sendFailed alleen naar de juiste hulpbron zenden
- Geen crash als een client (legaal) verwijderde globale contrastbeheerder gebruikt
- XDG v6 ondersteunen

### KWidgetsAddons

- KAcceleratorManager: pictogramtekst zetten bij acties om CJK-markeringen te verwijderen (bug 377859)
- KSqueezedTextLabel: tekst samenpakken bij wijziging van inspringen of marge
- Bewerk-verwijder-pictogram voor destructieve weggooiactie gebruiken (bug 385158)
- Bug 306944 repareren - het muiswiel gebruiken om de datums te verhogen of verlagen (bug 306944)
- KMessageBox: vraagtekenpictogram gebruiken voor vraagdialogen
- KSqueezedTextLabel: indentering, marges en framebreedte respecteren

### KXMLGUI

- Loop  van KToolBar opnieuw tekenen repareren (bug 377859)

### Plasma Framework

- org.kde.plasma.calendar met Qt 5.10 repareren
- [FrameSvgItem] onderliggende nodes juist itereren
- [Containment Interface] containmentacties niet toevoegen aan appletacties op bureaublad
- Nieuwe component voor grijs gemaakte labels in Item Delegates toevoegen
- FrameSVGItem met de softwarerenderer repareren
- IconItem niet animeren in softwaremodus
- [FrameSvg] nieuwe-stijl verbinding gebruiken
- Mogelijkheid om een aangekoppelde kleurreeks in te stellen voor niet erven
- Een extra visuele indicator voor Checkbox/Radio toetsenbordfocus toevoegen
- geen nulpixmap maken
- Item aan rootObject() doorgeven omdat het nu een singleton is (bug 384776)
- Tabbladnamen niet tweemaal in de lijst zetten
- geen actieve focus op tabblad accepteren
- revisie 1 voor QQuickItem registreren
- [Plasma Components 3] RnL in sommige widgets repareren
- Ongeldige id in weergave item repareren
- pictogram voor melding over e-mail bijwerken voor beter contrast (bug 365297)

### qqc2-desktop-stijl

Nieuwe module: QtQuickControls 2 stijl die QWidget's QStyle gebruikt voor tekenen. Dit maakt het mogelijk een hogere graad van consistentie te bereiken tussen op QWidget gebaseerde en QML gebaseerde toepassingen.

### Solid

- [solid/fstab] ondersteuning voor x-gvfs stijl opties in fstab
- [solid/fstab] leverancier- en producteigenschappen omwisselen, i18n van beschrijving toestaan

### Accentuering van syntaxis

- Ongeldige referenties naar itemData van 57 accentueringsbestanden repareren
- Ondersteuning voor aangepaste zoekpaden voor toepassingspecifieke syntaxis en themadefinities
- AppArmor: DBus-regels
- Indexeringsaccentueerder: factor out controles voor kleinere while-loop
- ContextChecker: '!' contextomschakeling en fallthroughContext ondersteunen
- Indexeringsaccentueerder: bestaan van gerefereerde contextnamen controleren
- Wijzig licentie van accentuering in qmake naar MIT licentie
- Accentuering in qmake laten winnen boven Prolog voor .pro bestanden (bug 383349)
- "@" macro van clojure met brackets ondersteunen
- Syntaxisaccentuering voor AppArmor profielen toevoegen
- Accentueringsindexeerder: ongeldige a-Z/A-z reeksen in reguliere expressies vangen
- Onjuiste reeksen met hoofdletters in reguliere expressies repareren
- ontbrekende referentiebestanden voor tests toevoegen, ziet er goed uit, denk ik
- Ondersteuning voor Intel HEX bestand voor de Syntaxisaccentueringsdatabase toegevoegd
- Spellingcontrole voor tekenreeksen in Sieve-scripts uitschakelen

### ThreadWeaver

- Geheugenlek repareren

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
