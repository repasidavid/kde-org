---
aliases:
- ../../kde-frameworks-5.26.0
date: 2016-09-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Qt5Network als een publieke afhankelijkheid toevoegen

### BluezQt

- Include dir in pri-bestand repareren

### Breeze pictogrammen

- Ontbrekende definities van naamruimteprefix toevoegen
- SVG pictogrammen op goede vorm controleren
- Alle pictogrammen edit-clear-location-ltr repareren (bug 366519)
- ondersteuning van kwin-effect-pictogram toegevoegd
- hernoem caps-on naar input-caps-on
- Hoofdletter-pictogrammen toevoegen voor invoer van tekst
- enige gnome specifieke pictogrammen uit Sadi58 toevoegen
- App-pictogrammen toevoegen uit gnastyle
- Pictogrammen voor Dolphin, Konsole en Umbrello zijn geoptimaliseerd voor 16px, 22px, 32px
- VLC pictogram voor 22px, 32px en 48px bijgewerkt
- App-pictogram toegevoegd voor maker van ondertitels
- Nieuw pictogram van Kleopatra repareren
- App-pictogrammen toegevoegd voor Kleopatra
- Pictogrammen voor Wine en Wine-qt toegevoegd
- presentatie woord bug repareren dankzij Sadi58 (bug 358495)
- systeem-afmeld-pictogram in 32px toevoegen
- 32px systeem-pictogrammen toegevoegd, gekleurde systeem-pictogrammen verwijderd
- ondersteuning voor pidgin-, banshee-pictogrammen toevoegen
- vlc-app-pictogram verwijderd vanwege probleem met licentie, nieuw VLC-pictogram toegevoegd (bug 366490)
- ondersteuning voor gthumb-pictogram toevoegen
- HighlightedText gebruiken voor mappictogrammen
- plaatsen van mappictogrammen gebruiken nu stijlsheet (accentueringskleur)

### Extra CMake-modules

- ecm_process_po_files_as_qm: vertalingen die niet gereed zijn overslaan
- Het standaard niveau voor loggen van categorieën zou Info moeten zijn in plaats van Warning
- ARGS variabele in de create-apk-* targets documenteren
- Een test maken die de appstream-informatie van projecten valideert

### KDE Doxygen hulpmiddelen

- Voorwaarde toevoegen als platforms van groepen niet zijn gedefinieerd
- Sjabloon: platforms alfabetisch sorteren

### KCodecs

- Breng uit kdelibs het bestand gebruikt om kentities.c te genereren

### KConfig

- Doneer-item toevoegen in KStandardShortcut

### KConfigWidgets

- Standaard actie voor doneren toevoegen

### KDeclarative

- [kpackagelauncherqml] desktop-bestandssnaam is hetzelfde als pluginId aannemen
- QtQuick renderinginstellingen uit een configuratiebestand laden en standaard instellen
- icondialog.cpp - reparatie op de juiste maniet compileren die m_dialog niet afschermt
- Crash repareren wanneer geen QApplication beschikbaar is
- Vertalingendomein zichtbaar maken

### Ondersteuning van KDELibs 4

- Windows compilatiefout in kstyle.h repareren

### KDocTools

- paden voor config, cache + data aan general.entities toevoegen
- Up-to-date gemaakt met de Engelse versie
- Entities voor Space- en Meta-toets toevoegen aan src/customization/en/user.entities

### KFileMetaData

- Alleen Xattr vereisen als het besturingssysteem Linux is
- Bouwen van Windows herstellen

### KIdleTime

- [xsync] XFlush in simulateUserActivity

### KIO

- KPropertiesDialog: waarschuwing uit document verwijderen, de bug is weg
- [test program] relatieve paden oplossen met QUrl::fromUserInput
- KUrlRequester: foutmelding repareren bij selectie van een bestand en de bestandsdialoog opnieuw openen
- Bied een fallback als slaves het . item niet laten zien (bug 366795)
- Aanmaken symbolische koppeling over "desktop" protocol repareren
- KNewFileMenu: bij aanmaken van symbolische koppelingen gebruik KIO::linkAs in plaatst van KIO::link
- KFileWidget: dubbele '/' in pad repareren
- KUrlRequester: statische connect() syntaxis gebruiken, was inconsistent
- KUrlRequester: window() doorgeven als ouder voor de QFileDialog
- aanroepen van connect(null, .....) vermijden vanuit KUrlComboRequester

### KNewStuff

- archieven uitpakken in submappen
- Installeren in generieke gegevensmap niet langer toestaan vanwege een potentieel gat in de beveiliging

### KNotification

- Van StatusNotifierWatcher eigenschap ProtocolVersion ophalen op een asynchrone manier

### Pakket Framework

- contentHash waarschuwingen over veroudering stil maken

### Kross

- "Ongebruikte KF5 afhankelijkheden verwijderen" terugdraaien

### KTextEditor

- accel clash verwijderen (bug 363738)
- accentueren van e-mailadres in doxygen repareren (bug 363186)
- iets meer json bestanden detecteren, zoals uw eigen projecten ;)
- MIME-type herkenning verbeteren (bug 357902)
- Bug 363280 - accentuering: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif (bug 363280)
- Bug 363280 - accenturing: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif
- Bug 351496 - invouwen bij Python werkt niet gedurende initieel typen (bug 351496)
- Bug 365171 - Python syntaxis accenturing: werkt niet juist voor escape sequences (bug 365171)
- Bug 344276 - php nowdoc niet juist ingevouwen (bug 344276)
- Bug 359613 - Enige CSS3 eigenschappen worden niet ondersteund in syntaxis accenturen (bug 359613)
- Bug 367821 - wineHQ syntaxis: de sectie in een reg-bestand wordt niet juist geaccentueerd (bug 367821)
- Swap bestandsbehandeling verbeteren indien swap-map gespecificeerd
- Crash repareren bij herladen van documenten met automatisch afgebroken regel vanwege regellengtelimiet (bug 366493)
- Constante crashes gerelateerd aan de vi commandobalk repareren (bug 367786)
- Reparatie: regelnummers in afgedrukte documenten beginnen nu bij 1 (bug 366579)
- Backup van bestanden op afstand: behandel aangekoppelde bestanden ook als bestanden op afstand
- logica voor creatie van zoekbalk opschonen
- accentuering voor Magma toevoegen
- Slechts één niveau van recursie toestaan
- Gebroken swap-bestand op windows repareren
- Patch: bitbake ondersteuning voor syntaxis accentueringsengine toevoegen
- autobrace: kijk naar attribuut van spellingcontrole waar het teken is ingevoerd (bug 367539)
- QMAKE_CFLAGS accentueren
- Niet uit de hoofdcontext springen
- Enkele namen van veel gebruikte programma's toevoegen

### KUnitConversion

- Britse eenheid van massa "stone" toevoegen

### KWallet Framework

- Docbook van kwallet-query naar de juiste submap verplaatsen
- Naamgeving van -&gt; repareren

### KWayland

- Maak linux/input.h optioneel op moment van compileren

### KWidgetsAddons

- Achtergrond van niet-BMP tekens repareren
- C octal escaped UTF-8 zoeken toevoegen
- Maak dat de standaard KMessageBoxDontAskAgainMemoryStorage opslaat naar QSettings

### KXMLGUI

- Standaard actie voor doneren overzetten
- Ga weg van verouderde authorizeKAction

### Plasma Framework

- apparaatpictogram 22px pictogram werkte niet in het oude bestand
- WindowThumbnail: Aanroepen van GL's in de juiste thread uitvoeren (bug 368066)
- Zorg dat plasma_install_package werkt met KDE_INSTALL_DIRS_NO_DEPRECATED
- marge en padding toevoegen aan het pictogram start.svgz
- stijlsheet zaken in computer-pictogram repareren
- computer- en laptop-pictogram voor kicker toevoegen (bug 367816)
- Waarschuwing kan geen undefined toekennen aan double in DayDelegate repareren
- stijlsheed svgz-bestanden houden niet van mij repareren
- de 22px pictogrammen hernoemen naar 22-22-x en de 32px pictogrammen naar x voor kicker
- [PlasmaComponents TextField] maak je geen zorgen over laden van pictogrammen voor ongebruikte knoppen
- Extra bewaking in Containment::corona in het geval van het speciale systeemvak
- Bij markering van een container als verwijderd, markeer ook alle sub-applets als verwijderd - repareert systeemvakcontainerconfiguraties die niet werden verwijderd
- Apparaatmelderpictogram repareren
- systeem-zoeken aan systeem toevoegen in 32 en 22px grootte
- monochrome pictogrammen voor kicker toevoegen
- Kleurschema op pictogram voor systeem-zoeken instellen
- Systeem-zoeken in system.svgz plaatsen
- Foute of ontbrekende "X-KDE-ParentApp" in desktop-bestandsdefinities repareren
- API dox van Plasma::PluginLoader repareren: verwarring met applets/dataengine/services/..
- pictogram van systeem-zoeken voor sddm-thema toevoegen
- nepomuk 32px pictogram toevoegen
- touchpad-pictogram voor het systeemvak bijwerken
- Code die nooit uitgevoerd kan worden verwijderen
- [ContainmentView] panelen tonen wanneer UI gereed is
- Eigenschap implicitHeight niet opnieuw declareren
- QQuickViewSharedEngine::setTranslationDomain gebruiken (bug 361513)
- ondersteuning 22px en 32px plasma breeze pictogrammen toevoegen
- gekleurde systeem-pictogrammen verwijderen en 32px monochrome toevoegen
- Een optionele knop voor wachtwoord laten zien in tekstveld toevoegen
- De standaard tekstballonnen worden nu gespiegeld in een rechts-naar-links taal
- Prestaties bij wijziging van maanden in de agenda is aanzienlijk verbeterd

### Sonnet

- Namen van talen niet naar kleine letters omzetten bij trigram-parsing
- Onmiddellijke crash bij opstarten vanwege plug-in-pointer naar nul
- Woordenboeken zonder juiste namen behandelen
- Handmatig opgeschoonde lijst met mappings van scripttalen vervangen, juiste namen voor talen gebruiken
- Hulpmiddel om trigrams te genereren toevoegen
- Maak taaldetectie een beetje minder gebroken
- Gebruik geselecteerde taal als suggestie voor detectie
- Spelling in cache gebruiken in taaldetectie, verbetert de prestatie enigszins
- Taaldetectie verbeteren
- Lijst met suggesties tegen beschikbare woordenboeken filteren, dubbelen verwijderen
- Herinneren on de laatste trigram overeenkomst toe te voegen
- Controleer of elk van de trigrams ook echt overeenkomt
- Behandel meerdere talen met dezelfde score in trigram-matcher
- Niet twee keer controleren op minimale grootte
- Trim de lijst met talen tegen de beschikbare talen
- Dezelfde minimale lengte overal in langdet gebruiken
- Controle op juistheid dat het geladen model de juiste hoeveelheid trigrams voor elke taal heeft

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
