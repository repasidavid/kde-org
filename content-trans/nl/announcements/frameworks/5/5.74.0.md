---
aliases:
- ../../kde-frameworks-5.74.0
date: 2020-09-06
layout: framework
libCount: 70
---
### Attica

+ Q_DECLARE_OPERATORS_FOR_FLAGS in dezelfde naamruimte gebruiken als vlaggendefinitie

### Baloo

+ Licentie opnieuw uitdelen naar LGPL-2.0-of-later
+ Gemeenschappelijke UDS creatiecode ook gebruiken voor tags (bug 419429)
+ Gemeenschappelijke UDS creatiecode verwijderen uit KIO-workers
+ [balooctl] toegestane formaten in helptekst
+ [balooctl] huidige bestand in status uitvoer in indexeringsstatus tonen
+ [balooctl] Modus QDBusServiceWatcher instellen uit constructor
+ [balooctl] opschoning van formattering
+ Q_DECLARE_OPERATORS_FOR_FLAGS in dezelfde naamruimte gebruiken als vlaggendefinitie
+ [OrPostingIterator] niet verder gaan wanneer gevraagde id lager is dan de huidige
+ [Extractor] QWidgets afhankelijkheid van extractorhelper verwijderen
+ [Extractor] KAboutData uit uitvoerbaar programma extractor helper verwijderen
+ Verschillende referenties in README bijwerken
+ [FileContentIndexer] ongebruikte configuratieconstructorargument en lid verwijderen
+ [balooctl] waarschuwing over veroudering van QProcess::start repareren, lever lege argumentenlijst
+ [Engine] transactiefouten propageren (bug 425017)
+ [Engine] ongebruikte methode hasChanges uit {Write}Transaction verwijderen
+ Geen .ytdl bestanden indexeren (bug 424925)

### Breeze pictogrammen

+ Het pictogram van keepassxc meer laten gelijken op de officiële
+ Meer symbolische koppelingen voor nieuw keepassxc systeemvakpictogramnamen (bug 425928)
+ Een andere alias voor keepass-pictogram toevoegen (bug 425928)
+ Pictogram voor Godot-project MIME-type toevoegen
+ Pictogram voor Anaconda installatieprogramma toevoegen
+ 96px plaatsenpictogrammen toevoegen
+ Ongebruikte plaatsen om te communiceren verwijderen
+ application-x-bzip-compressed-tar uitvoeren via <code>scour</code> (bug 425089)
+ Symlink maken van application-gzip naar application-x-gzip (bug 425059)
+ Aliassen voor MP3 pictogrammen toevoegen (bug 425059)

### Extra CMake-modules

+ Voorafgaande nullen van numerieke versienummers in C++ code strippen
+ Tijdslimiet toevoegen voor aanroepen van qmlplugindump
+ Zoekmodule van WaylandProtocols toevoegen
+ update-mime-database met -n aanroepen

### Frameworkintegratie

+ Licentie-statement verhelderen volgens de KDElibs geschiedenis

### KActivitiesStats

+ Boost::boost gebruiken voor oudere CMake versies

### KActivities

+ Lege X-KDE-PluginInfo-Depends laten vallen

### KDE Doxygen hulpmiddelen

+ Afhankelijkheden documenteren in requirements.txt en ze installeren in setup.py
+ Opt-In Display van Library licentie

### KAuth

+ Q_DECLARE_OPERATORS_FOR_FLAGS in dezelfde naamruimte gebruiken als vlaggendefinitie

### KBookmarks

+ KBookmarkManager: boomstructuur van geheugen wissen wanneer het bestand is verwijderd

### KCalendarCore

+ Eigenschappen X-KDE-VOLATILE-XXX altijd als tijdelijk opslaan
+ De verwachte TZ transities in Praag documenteren

### KCMUtils

+ KCModuleData: headers repareren, api doc en method van hernoemen verbeteren 
+ KCModuleData uitbreiden met revertToDefaults en matchQuery functionaliteit
+ Horizontale schuifbalk proberen te vermijden in KCMultiDialog
+ KCModuleData toevoegen als basis klasse voor plug-in
+ Extra knop toestaan om toe te voegen aan KPluginSelector (bug 315829)

### KConfig

+ KWindowConfig::allConnectedScreens() statisch en intern maken (bug 425953)
+ Standaard sneltoets toevoegen voor "Map aanmaken"
+ Methode introduceren om standaard waarde van KConfigSkeletonItem op te vragen
+ Venstergroottes onthouden op basis van een per-scherm-arrangement
+ Code extraheren om een lijst van verbonden schermen te krijgen in een opnieuw te gebruiken functie
+ Functies toevoegen om vensterposities op te slaan en te herstellen op niet-Wayland platforms (bug 415150)

### KConfigWidgets

+ Omwisselen van standaarden vermijden om KConfigSkeletonItem standaard waarde te lezen
+ KLanguageName::nameForCodeInLocale repareren voor codes die QLocale niet kent
+ KLanguageName::allLanguageCodes: rekening houden met dat er meer dan één lokale map kan zijn
+ KConfigDialog: horizontale schuifbalken proberen te vermijden
+ Functie die de lijst met taalcodes teruggeeft

### KContacts

+ Addressee::parseEmailAddress(): lengte van volledige naam controleren vóór trimmen

### KCoreAddons

+ *.kcrash glob patroon toevoegen aan KCrash Report MIME-type
+ Q_DECLARE_OPERATORS_FOR_FLAGS in dezelfde naamruimte gebruiken als vlaggendefinitie
+ Niet oneindig wachten op fam-events (bug 423818)
+ [KFormat] formatteren van waarden naar willekeurige binaire eenheden toestaan
+ Het mogelijk maken om KPluginMetadata uit QML te halen
+ [KFormat] binair voorbeeld repareren

### KDAV

+ metainfo.yaml: tier-sleutel toevoegen en beschrijving aanpassen

### KDeclarative

+ [KKeySequenceItem] Meta+Shift+num sneltoetsen werkend maken
+ Eigenschap checkForConflictsAgainst exposeren
+ Een nieuwe AbstractKCM klasse toevoegen
+ KRunProxy herschrijven weg van KRun

### KDED

+ org.kde.kded5.desktop: ontbrekende vereiste sleutel "Name" toevoegen (bug 408802)

### KDocTools

+ contributor.entities bijwerken
+ Markering van plaatshouders gebruiken

### KEmoticons

+ EmojiOne licentie-informatie herstellen

### KFileMetaData

+ Oude MAC-regeleindes in liedteksttags converteren (bug 425563)
+ Nieuwe module FindTaglib overzetten naar ECM

### KGlobalAccel

+ Servicebestanden voor sneltoetsen voor map van toepassingengegevens als terugval laden (bug 421329)

### KI18n

+ Mogelijke preprocessorraceconditie repareren met i18n definities

### KIO

+ StatJob: mostLocalUrl laten werken met alleen protoclClass == :local
+ KPropertiesDialog: ook plug-ins laden met JSON metagegevens
+ "[KUrlCompletion] geen / achtervoegen aan voltooide mappen" terugdraaien (bug 425387)
+ Constructor KProcessRunner vereenvoudigen
+ CCBUG en FEATURE sleutelwoorden toestaan voor bugs.kde.org (bug )
+ Helptekst bijwerken voor bewerken van het toepassingcommando in een .desktop-item om comply te voldoen aan de huidige specificatie (bug 425145)
+ KFileFilterCombo: de optie allTypes niet toevoegen als we slechts 1 item hebben
+ Q_DECLARE_OPERATORS_FOR_FLAGS in dezelfde naamruimte gebruiken als vlaggendefinitie
+ Potentiële crash repareren bij verwijderen van een menu in gebeurtenisbehandelaar (bug 402793)
+ [filewidgets] aanvullen van KUrlNavigatorButton op broodkruimels repareren (bug 425570)
+ ApplicationLauncherJob: documenten aanpassen
+ Niet vertaalde items repareren in kfileplacesmodel
+ ApplicationLauncherJob: crash repareren als er geen openen-met behandelaar is ingesteld
+ "Websnelkoppelingen" van KCM hernoemen naar "Webzoektrefwoorden"
+ KFileWidget: configuratie opnieuw ontleden om mappen, toegevoegd door andere exemplaren van de toepassing, te pakken (bug 403524)
+ Websnelkoppelingen van kcm verplaatsen naar zoekcategorie
+ Witruimte achteraan automatisch verwijderen in plaats van een waarschuwing tonen
+ Door systemd gestarte toepassing behoeden voor sluiten van afsplitsingen (bug 425201)
+ smb: controle op beschikbaarheid van share-naam repareren
+ smb: stderr van netcommando's op toevoegen/verwijderen behouden (bug 334618)
+ smb: rejigger guest toegestane controle en als areGuestsAllowed publiceren
+ KFileWidget: URL's wissen voordat de lijst opnieuw wordt gebouwd
+ KFileWidget: toppadcombinatie van standaard URL's verwijderen
+ KFilePlacesModel: standaard plaatsen toevoegen bij opwaardering van oudere versie
+ 2-jaren regressie in KUrlComboBox repareren, setUrl() voegde niet langer achter

### Kirigami

+ kirigami.pri: managedtexturenode.cpp toevoegen
+ buddyfor custom positionering opnieuw laten werken
+ Rekening houden met meer grrootte van knoppen als we al weten dat het zichtbaar zal zijn
+ Een eigenschap toevoegen aan ToolBarLayout om te besturen hoe om te gaan met hoogte van item (bug 425675)
+ labels van keuzevakje opnieuw leesbaar maken
+ displayComponent ondersteunen voor acties
+ submenu's zichtbaar maken
+ Voorwaarts-declaratie QSGMaterialType repareren
+ Laat OverlaySheet kop- en voetregel toepasselijke achtergrondkleuren gebruiken
+ Lage power omgevingsvariabele op waarde controleren, niet alleen indien het is ingesteld
+ Functie setShader toevoegen aan ShadowedRectangleShader om instellen van schaduwmakers te vereenvoudigen
+ Een lage powerversie van het sdf-functiesbestand toevoegen
+ Laat overloads van sdf_render het volledig argument-sdf_render-functie gebruiken
+ Noodzaak voor kernprofiel definities in schaduwrechthoek hoofdschaduwmaker verwijderen
+ Binnenste rechthoek op buitenste rechthoek voor randschaduwrechthoek
+ De juiste shaders voor ShadowedTexture gebruiken bij gebruik van het kernprofiel
+ "lowpower" versies toevoegen van de shadowedrectangle shaders
+ m_component-lid in PageRoute initialiseren
+ Material SwipeListItem repareren
+ nieuwe logica om versnellingsmarkeringen te verwijderen (bug 420409)
+ Voor vu forceSoftwarerendering verwijderen
+ alleen het bronitem tonen bij rendering met software
+ een software terugval voor de beschaduwde textuur
+ De nieuwe eigenschap showMenuArrow op de achtergrond gebruiken voor de menupijl
+ De kop niet verbergen bij er overheen schieten
+ ManagedTextureNode verplaatsen in eigen bestand
+ ToolBarLayout: spatiëring toevoegen aan visibleWidth als we de meer-knop tonen
+ Koppixelgrootte in BreadCrumbControl niet overschrijven (bug 404396)
+ App-sjablonen bijwerken
+ [passivenotification] expliciete opvulling instellen (bug 419391)
+ Afsnijden inschakelen in de GlobalDrawer StackView
+ Dekking uit uitgeschakeld PrivateActionToolButton verwijderen
+ ActionToolBar een besturing maken
+ swipenavigator: besturing toevoegen over welke pagina's getoond worden
+ Het onderliggende type van de DisplayHint enum declareren als uint
+ ToolBarLayout/ToolBarLayoutDelegate aan pri-bestand toevoegen
+ kirigami.pro: bronbestandlijst uit kirigami.pri gebruiken
+ Geen incubators verwijderen in aanvullen met terugroepen
+ Verzeker dat menuActions een array blijven in plaats van een lijsteigenschap
+ Type van teruggeven toevoegen aan DisplayHint singleton lambda
+ Rekening houden met hoogte van item bij verticaal centreren van gedelegeerden
+ Altijd een nieuwe indeling in de wachtrij zetten, zelfs als we nu een indeling maken
+ InlineMessages-indeling herbewerken met gebruik van ankers
+ Volledige breedte gebruiken om te controleren of alle acties passen
+ Commentaar toevoegen over de extra spatiëring voor centreren
+ Controle op tekstballontekst in PrivateActionToolButton repareren
+ Omweg maken om qqc2-desktop-style ToolButton die de niet-platte IconOnly niet respecteert
+ De voorkeur geven aan invouwen van KeepVisible acties boven later verbergen van KeepVisible acties
+ Alle volgende acties verbergen wanneer de eerste actie verborgen raakt
+ Een meldingsignaal toevoegen voor ToolBarLayout::actions en stuur het uit op de juiste tijd
+ Actie blootstellen aan scheidingsteken van ActionsMenu
+ Eigenschap ActionsMenu loaderDelegate kirigamiAction hernoemen tot actie
+ Ontbrekende loaderDelegate toevoegen die zichtbaarheid controleren
+ Actiegedelegeerden verbergen totdat ze gepositioneerd zijn
+ ToolBarLayout: aangepaste lazy-loading van gedelegeerden door QQmlIncubator vervangen
+ displayHintSet verplaatsen naar C++ zodat het van daar aangeroepen kan worden
+ Modus rechts-naar-links in ToolBarLayout ondersteunen
+ Eigenschap minimumWidth toevoegen aan ToolBarLayout
+ PrivateActionToolButton opnieuw bewerken voor verbeterde prestaties
+ ActionToolBar::hiddenActions afkeuren
+ Tips voor indeling voor ActionToolBar instellen
+ Niet-afgekeurde DisplayHint in ToolBarPageHeader gebruiken
+ Componentfouten tonen als gedelegeerde items maken mislukt
+ Gedelegeerden van acties die zijn verwijderd verbergen
+ Volledig/pictogram gedelegeerde items bij verwijderen van gedelegeerden opschonen
+ Zichtbaarheid van volledig/pictogram gedelegeerd item afdwingen
+ ToolBarLayout voor indeling van ActionToolBar gebruiken
+ Enig commentaar toevoegen aan ToolBarLayout::maybeHideDelegate
+ Eigenschap visibleWidth toevoegen aan ToolBarLayout
+ ToolBarLayout inheems object introduceren
+ DisplayHint verplaatsen van Action naar C++ enums bestand
+ Pictogrammen gebruikt in de pagina Info over toevoegen naar cmake pictogram packaging macro

### KNewStuff

+ Problemen met synchronisatie van cache met QtQuick dialoog repareren (bug 417985)
+ Lege X-KDE-PluginInfo-Depends laten vallen
+ Items verwijderen als ze niet langer overeenkomen met het filter (bug 425135)
+ Randgeval repareren waar KNS vast loopt (bug 423055)
+ De interne kpackage jobtaak minder fragiel maken (bug 425811)
+ Gedownload bestand verwijderen bij gebruik van kpackage installatie
+ Een andere stijl van kpackage knsrc voor terugval ondersteunen
+ Notatie /* behandelen voor RemoveDeadEntries (bug 425704)
+ Het gemakkelijker maken om de cache voor een al geïnitialiseerde engine op te halen
+ Een omgekeerd opzoeken voor items gebaseerd op hun geïnstalleerde bestanden toevoegen
+ Notatie /* gebruiken voor decompressie van submap en decompressie van submap toestaan als bestand archief is
+ Een vloed aan waarschuwingen "Pixmap is een null-pixmap" repareren
+ Slashes verwijderen uit itemnaam bij het als pad interpreteren (bug 417216)
+ Een soms-crash met de KPackageJob taak repareren (bug 425245)
+ Zelfde spinner voor laden en initialiseren gebruiken (bug 418031)
+ Detailsknop verwijderen (bug 424895)
+ Script voor installatie ongedaan maken asynchroon uitvoeren (bug 418042)
+ Zoeken uitschakelen indien niet beschikbaar
+ Laden van meer spinner verbergen tijdens bijwerken/installeren (bug 422047)
+ Geen doelmap van download toevoegen aan itembestanden
+ Teken * verwijderen bij doorgeven map naar script
+ Geen focus op eerste element in modus pictogramweergave (bug 424894)
+ Onnodig starten van van job voor installatie ongedaan maken vermijden
+ Een optie RemoveDeadEntries toevoegen voor knsrc-bestanden (bug 417985)
+ [QtQuick dialoog] laatste exemplaar van onjuist bijwerkpictogram repareren
+ [QtQuick dialog] meer toepasselijke pictogrammen voor installatie ongedaan maken

### KPackage-framework

+ Pakketbasis niet verwijderen als pakket is verwijderd (bug 410682)

### KQuickCharts

+ Een eigenschap fillColorSource toevoegen aan lijngrafieken
+ Gladheid berekenen gebaseerd op grootte van item en pixelverhouding van apparaat
+ Gemiddelde van grootte gebruiken in plaats van maximum om gladheid van lijn te bepalen
+ Hoeveelheid gladstrijken in lijngrafieken baseren op grafiekgrootte

### KRunner

+ Sjabloon toevoegen voor python-starter
+ Sjabloon bijwerken voor compatibiliteit van KDE Store en README verbeteren
+ Ondersteuning toevoegen voor syntaxis van starters naar starters van dbus
+ RunnerContext na elke overeenkomstsessie opslaan (bug 424505)

### KService

+ invokeTerminal op Windows met werkmap, commando en omgevingen implementeren
+ Ordening van voorkeur voor toepassingen voor mimetypes met meerdere overerving repareren (bug 425154)
+ Het servicetype van toepassing bundelen in een qrc-bestand
+ Teken tilde expanderen bij lezen van werkmap (bug 424974)

### KTextEditor

+ Vimode: het kleine register voor verwijderen (-) direct toegankelijk maken
+ Vimode: het gedrag van genummerde registers van vim kopiëren
+ Vimode: De implementatie van achtervoegen-kopiëren vereenvoudigen
+ "Zoeken naar selectie" zoeken maken als er geen selectie is
+ Modus multi-regel heeft alleen zin voor multi-regel reguliere expressie
+ Commentaar toevoegen over controle van pattern.isEmpty()
+ Het zoekinterface overzetten van QRegExp naar QRegularExpression
+ Vimode: achtervoegen-kopiëren implementeren
+ *Heel wat* versnellen bij laden van grote bestanden
+ Zoomniveau alleen tonen wanneer het niet 100% is
+ Een zoom-indicator toevoegen aan de statusbalk
+ Een aparte configuratieoptie voor voorbeeld van haakjesovereenkomst toegevoegd
+ Een voorbeeld van overeenkomend openhaakjesregel tonen
+ Meer controle over aanroepen van aanvulmodellen toestaan wanneer automatisch aanroepen niet wordt gebruikt

### KWallet Framework

+ Botsing met een macro in ctype.h uit OpenBSD vermijden

### KWidgetsAddons

+ KRecentFilesMenu toevoegen om KRecentFileAction te vervangen

### KWindowSystem

+ Platformplug-ins installeren in een map zonder punten in de bestandsnaam (bug 425652)
+ [xcb] geschaalde pictogramgeometrie correct overal

### KXMLGUI

+ Terugtrekken uit herinneren van vensterposities in X11 toestaan (bug 415150)
+ Positie van hoofdvenster opslaan en herstellen (bug 415150)

### Plasma Framework

+ [PC3/BusyIndicator] onzichtbare animatie uitvoeren vermijden
+ Geen highlightedTextColor gebruiken voor TabButtons
+ Layout.minimumWidth uit knop en hulpmiddelknop verwijderen
+ De eigenschap spatiëring voor de spatiëring tussen knop/hulpmiddelknoppictogrammen en labels gebruiken
+ private/ButtonContent.qml voor PC3 knoppen en hulpmiddelknoppen toevoegen
+ PC3 knop en hulpmiddelknop implicitWidth en implicitHeight wijzigen naar account voor invulwaarden
+ implicitWidth en implicitHeight aan ButtonBackground toevoegen
+ Onjuiste standaard voor PlasmaExtras.ListItem repareren (bug 425769)
+ De achtergrond niet kleiner laten worden dan de svg (bug 424448)
+ Q_DECLARE_OPERATORS_FOR_FLAGS in dezelfde naamruimte gebruiken als vlaggendefinitie
+ PC3 BusyIndicator zichtbaren een 1:1 beeldverhouding laten behouden (bug 425504)
+ ButtonFocus en ButtonHover in PC3 ComboBox gebruiken
+ ButtonFocus en ButtonHover in PC3 RoundButton gebruiken
+ ButtonFocus en ButtonHover in PC3 CheckIndicator gebruiken
+ Het vlakke/normale gedrag van PC3 knoppen/hulpmiddelknoppen unificeren (bug 425174)
+ Kop PC3-label laten gebruiken
+ [PlasmaComponents3] laat tekst van keuzevakje zijn indeling opvullen
+ PC2 schuifregelaar implicitWidth en implicitHeight geven
+ Bestanden kopiëren in plaats van gebroken symbolische koppelingen
+ Marges van toolbutton-hover in button.svg repareren (bug #425255)
+ [PlasmaComponents3] erg kleiner herziening van ToolButton schaduwcode
+ [PlasmaComponents3] omgekeerde conditie repareren voor vlakke ToolButton schaduw
+ [PlasmaComponents3] mnemonic ampersands weghalen uit tekstballontekst
+ De focusindicator alleen tonen wanneer we focus krijgen via het toetsenbord (bug 424446)
+ Impliciete minimale afmeting uit PC2 en PC3 knoppen laten vallen
+ PC3 equivalent toevoegen aan PC2 ListItem
+ Werkbalk-svg repareren
+ [pc3] ToolBar meer laten lijken op qqc2-desktop-style one
+ De applet metagegevens in AppletInterface blootstellen
+ DPR niet afkorting naar een geheel getal in cache-ID
+ Timeout-eigenschap toevoegen aan ToolTipArea
+ Type naar Dialog in vlaggen instellen als type Dialog::Normal is

### Omschrijving

+ Initiële configuratiegegevens toepassen bij laden van configuratie UI
+ Gedrag van AlternativesView herstellen
+ [jobcontroller] separaat proces uitschakelen
+ Behandeling van jobweergave opnieuw bewerken (bug 419170)

### QQC2StyleBridge

+ StandardKey sneltoetsseries repareren in MenuItem getoond als nummers
+ Geen ouderhoogte/breedte voor impliciete ToolSeparator afmeting gebruiken (bug 425949)
+ "focus"-stijl alleen voor niet-platte hulpmiddelknoppen bij indrukken gebruiken
+ Opvulling boven en onder toevoegen aan ToolSeparator
+ ToolSeparator topPadding en bottomPadding waarden laten respecteren
+ MenuSeparator berekende hoogte van achtergrond laten gebruiken niet implicitheight
+ Hulpmiddelknoppen met menu's repareren met gebruik van nieuwer Breeze
+ De gehele CheckBox-control tekenen via de QStyle

### Solid

+ Statische storageAccessFromPath methode wordt toegevoegd, nodig door https://phabricator.kde.org/D28745

### Syndication

+ Uitzondering op licentie corrigeren

### Accentuering van syntaxis

+ alle thema's naar nieuwe sleutels voor bewerkerkleuren
+ thema json-formaat wijzigen, meta-object enum namen voor kleuren van bewerker gebruiken
+ kateversion &gt;= 5.62 controleren voor fallthroughContext zonder fallthrough="true" en attrToBool voor boolean attribuut gebruiken
+ Syntaxisdefinitie toevoegen voor todo.txt
+ matchEscapedChar() repareren: het laatste teken van een regel wordt genegeerd
+ isDigit(), isOctalChar() en isHexChar() repareren: moet alleen ascii tekens laten overeenkomen
+ overzicht van thema's genereren
+ metagegevens van thema toevoegen aan kop van HTML pagina's die we genereren
+ met themaverzamelinspagina beginnen
+ 'Standaard' hernoemen naar 'Breeze Light'
+ Varnish, Vala &amp; TADS3: de standaard kleurstijl gebruiken
+ Vim Dark kleurenthema verbeteren
+ Vim donker kleurthema toevoegen
+ syntaxisaccentueringsthema toevoegen aan accentuering van json
+ Ruby/Rails/RHTML: spellChecking in itemDatas toevoegen
+ Ruby/Rails/RHTML: de standaard kleurstijl gebruiken en andere verbeteringen
+ LDIF, VHDL, D, Clojure &amp; ANS-Forth94: de standaard kleurstijl gebruiken
+ ASP: de standaard kleurstijl gebruiken en andere verbeteringen
+ objective-c laten winnen voor .m bestanden
+ .mm is waarschijnlijker Objective-C++ dan meta math
+ notAsciiDelimiters alleen gebruiken met een niet-ascii teken
+ isWordDelimiter(c) met ascii teken optimaliseren
+ Context::load optimaliseren
+ std::make_shared gebruiken die de allocatie op het besturingsblok verwijderd
+ juiste licentie om scripty bij te werken toevoegen
+ script voor bijwerken voor kate-editor.org/syntax naar syntax-opslagruimte
+ SELinux CIL &amp; Schema: haakjeskleuren voor donkere thema's bijwerken
+ POV-Ray: standaard kleurstijl gebruiken
+ krita installatieprogramma NSIS-script als voorbeeldinvoer gebruiken, genomen uit krita.git
+ tip om script voor website bij te werken toevoegen
+ Minimaal Django sjabloonvoorbeeld toevoegen
+ refs na laatste hl wijzigingen bijwerken
+ CMake: onleesbare kleuren in donkere thema's repareren en andere verbeteringen
+ pipe en ggplot2 voorbeeld toevoegen
+ naamgevingfout voor varnish hl repareren
+ juiste accentuering voor 68k ASM: Motorola 68k (VASM/Devpac) gebruiken
+ XML om geldig te zijn met betrekking tot XSD repareren
+ BrightScript: uitzondering op syntaxis toevoegen
+ Commentaar in enige syntaxisdefinities verbeteren (deel 3)
+ R Script: standaard kleurstijl en andere verbeteringen gebruiken
+ PicAsm: onbekende accentuering van preprocessor sleutelwoorden repareren
+ Commentaar in enige syntaxisdefinities verbeteren (deel 2)
+ Modelines: LineContinue regels verwijderen
+ quickfix van gebroken hl bestand
+ Optimalisatie: controleer of we op een commentaar zijn alvorens ##Doxygen te gebruiken die verschillende RegExpr bevat
+ Assembly talen: verschillende reparaties en meer accentuering van context
+ ColdFusion: de standaard kleurstijl gebruiken en enige RegExpr regels vervangen
+ Commentaar in enige syntaxisdefinities verbeteren, deel 1
+ Angle brackets gebruiken voor contextinformatie
+ Verwijdering van byte-order-mark terugdraaien
+ xslt: kleur van XSLT tags wijzigen
+ Ruby, Perl, QML, VRML &amp; xslt: de standaard kleurstijl gebruiken en commentaar verbeteren
+ txt2tags: verbeteringen en reparaties, gebruik de standaard kleurstijl
+ Fouten repareren en fallthroughContext=AttrNormal toevoegen voor elke context van diff.xml omdat alle regels column=0 bevatten
+ problemen gevonden door statische controle repareren
+ Pure accentuering importeren uit https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/pure.xml
+ kate-versieattribuut repareren
+ volgorde van opzoeken van accentuering onafhankelijk maken van vertalingen
+ versieformaat + spatiëringszaken repareren
+ Modula-3 accentuering importeren uit https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/modula-3.xml
+ versieformaat repareren
+ spatiëringszaken gevonden door statische controle repareren
+ LLVM accentuering importeren uit https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/llvm.xml
+ spatiëringszaken gevonden door statische controle repareren
+ Idris importeren uit https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/idris.xml
+ fouten gevonden door statische controle repareren
+ ATS importeren uit https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/ats.xml
+ losmaken alleen voor niet vers aangemaakte gegevens
+ StateData op aanvraag aanmaken

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
