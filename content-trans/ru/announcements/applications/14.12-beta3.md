---
aliases:
- ../announce-applications-14.12-beta3
custom_spread_install: true
date: '2014-11-20'
description: KDE Ships Applications 14.12 Beta 2.
layout: application
title: KDE выпускает третью бета-версию KDE Applications 14.12
---
20 ноября 2014 года. Сегодня KDE выпустило бета-версию KDE Applications. Теперь, когда программные зависимости и функциональность «заморожены», команда KDE сконцентрируется на исправлении ошибок и наведении красоты.

With various applications being based on KDE Frameworks 5, the KDE Applications 14.12 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the beta <a href='https://bugs.kde.org/'>and reporting any bugs</a>.

#### Установка двоичных пакетов KDE Applications 14.12 Beta 3

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 14.12 Beta 3 (internally 14.11.95) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### Компиляция KDE Applications 14.12 Beta 3

The complete source code for KDE Applications 14.12 Beta 3 may be <a href='http://download.kde.org/unstable/applications/14.11.95/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-14.11.95'>KDE Applications Beta 3 Info Page</a>.
