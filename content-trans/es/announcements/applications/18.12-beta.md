---
aliases:
- ../announce-applications-18.12-beta
date: 2018-11-16
description: KDE lanza la Beta de las Aplicaciones 18.12.
layout: application
release: applications-18.11.80
title: KDE lanza la beta de las Aplicaciones de KDE 18.12
---
16 de noviembre de 2018. KDE ha lanzado hoy la versión beta de las nuevas versiones de las Aplicaciones de KDE. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Consulte las <a href='https://community.kde.org/Applications/18.12_Release_Notes'>notas de lanzamiento de la comunidad</a> para obtener información sobre los paquetes y sobre los problemas conocidos. Se realizará un anuncio más completo para la versión final.

Los lanzamientos de las Aplicaciones de KDE 18.12 necesitan una prueba exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son críticos para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con usted para ayudarnos a encontrar errores de manera temprana para poder corregirlos antes de la versión final. Considere la idea de unirse al equipo instalando la beta <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.
