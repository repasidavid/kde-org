---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE lanza las Aplicaciones de KDE 18.04.0
layout: application
title: KDE lanza las Aplicaciones de KDE 18.04.0
version: 18.04.0
---
19 de abril de 2018. Publicadas las Aplicaciones de KDE 18.04.0.

Trabajamos continuamente en la mejora del software incluido en nuestras series de Aplicaciones de KDE y esperamos que encuentre útiles todas las nuevas mejoras y correcciones de errores.

## Novedades de las Aplicaciones de KDE 18.04

### Sistema

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

El mayor lanzamiento en 2018 de <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, el potente gestor de archivos de KDE, contiene numerosas mejoras en sus paneles:

- Las secciones de los «Lugares» del panel se pueden ocultar ahora libremente si prefiere no mostrarlas. Ahora existe una nueva sección «Red» que contiene las entradas de ubicaciones remotas.
- El panel «Terminal» se puede anclar a cualquier lado de la ventana, y si intenta abrirlo sin tener instalado Konsole, Dolphin le mostrará una advertencia y le ayudará a instalarlo.
- Se ha mejorado el uso de HiDPI en el panel «Información».

La vista de carpetas y los menús también se han actualizado:

- La carpeta «Papelera» muestra ahora un botón para «Vaciar la papelera».
- Se ha añadido una opción de menú «Mostrar destino» para ayudar a encontrar los destinos de los enlaces simbólicos.
- Se ha mejorado la integración con Git: el menú de contexto para las carpetas de git muestra ahora dos nuevas acciones para el «registro de git» y la «fusión de git».

Otras mejoras incluyen:

- Se ha introducido un nuevo acceso rápido de teclado para proporcionarle la opción de abrir la barra de filtrado pulsando la tecla de barra inversa («/»).
- Ahora puede ordenar y organizar fotos por la fecha en la que se hicieron.
- Arrastrar y soltar muchos archivos pequeños en Dolphin se ha vuelto ahora más rápido. Ahora es posible deshacer trabajos de cambio de nombre por lotes.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

Para que trabajar en la línea de órdenes sea todavía más grato, <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, la aplicación de emulación de terminal de KDE, se muestra ahora más vistosa:

- Puede descargar esquemas de colores usando KNewStuff.
- La barra de desplazamiento se mezcla mejor con el esquema de color activo.
- Por omisión, la barra de pestañas se muestra solo cuando es necesario.

Los colaboradores de Konsole no se han detenido ahí, ya que han introducido numerosas nuevas funcionalidades:

- Se ha añadido un nuevo modo de solo lectura y una propiedad de perfiles para conmutar la copia de texto como HTML.
- Si usa Wayland, Konsole muestra ahora un menú para las operaciones de arrastrar y soltar.
- Se han realizado numerosas mejoras que tienen que ver con el protocolo ZMODEM: Konsole puede manejar ahora el indicador de envío B01 de zmodem; se muestra la progresión mientras se transfieren datos; el botón del diálogo «Cancelar» funciona ahora como era de esperar; la trasferencia de archivos grandes funciona mejor al leerlos en memoria en bloques de 1 MB.

Otras mejoras incluyen:

- Se ha corregido el desplazamiento de la rueda del ratón con «libinput», y ahora se impide recorrer el historial de la shell cuando de desplaza con la rueda del ratón.
- Las búsquedas se actualizan tras cambiar la opción de la expresión regular de búsqueda, y cuando se pulsa 'Ctrl' + 'Retroceso', Konsole se comportará del mismo modo que «xterm».
- Se ha corregido el acceso rápido «--background-mode».

### Multimedia

<a href='https://juk.kde.org/'>JuK</a>, el reproductor de música de KDE, ya se puede usar en Wayland. Las nuevas funcionalidades de la interfaz de usuario incluyen la posibilidad de ocultar la barra de menú y mostrar una indicación visual de la pista que se está reproduciendo. Al haber desactivado el anclaje de la bandeja del sistema, JuK ya no falla cuando se intenta salir usando el icono «cerrar» de la ventana y la interfaz de usuario permanecerá visible. También se han corregido los fallos de la lista de reproducción al volver del estado de reposo en Plasma 5 y del manejo de la columna de la lista de reproducción.

Para el lanzamiento de 18.04, los colaboradores de <a href='https://kdenlive.org/'>Kdenlive</a>, el editor de vídeo no lineal de KDE, se han centrado en el mantenimiento:

- El cambio de tamaño del clip ya no daña los fundidos y los fotogramas clave.
- Los usuarios que usan escalado de pantalla en monitores HiDPI pueden disfrutar de iconos más nítidos.
- Se ha corregido un posible bloqueo en el inicio con algunas configuraciones.
- Ahora se requiere que MLT tenga la versión 6.6.0 o posterior, y se ha mejorado la compatibilidad.

#### Gráficos

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

A lo largo de los últimos meses, los colaboradores de <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, el visor y organizador de imágenes de KDE, han trabajado en una plétora de mejoras. Entre ellas destacamos:

- Se ha añadido la compatibilidad con controladores MPRIS, por lo que ahora es posible controlar las presentaciones de diapositivas a pantalla completa mediante KDE Connect, las teclas multimedia del teclado y el plasmoide del reproductor multimedia.
- Los botones que se muestran en las miniaturas al situar el cursor del ratón sobre ellas se pueden desactivar ahora.
- La herramienta de recorte ha recibido varias mejoras, ya que su configuración se recuerda ahora al cambiar a otra imagen, la forma del cuadro de selección se puede bloquear ahora manteniendo pulsadas las teclas 'Mayúsculas' o 'Ctrl', y también se puede bloquear a las proporciones de la imagen que se muestra.
- Ahora se puede salir del modo de pantalla completa usando la tecla 'Escape' y la paleta de colores reflejará su tema de color activo. Si sale de Gwenview en este modo, dicha preferencia se recordará y la nueva sesión también se iniciará a pantalla completa.

El cuidado de los detalles es importante, por lo que Gwenview se ha pulido en las siguientes áreas:

- Gwenview mostrará más rutas de archivo legibles por humanos en la lista de 'Carpetas recientes', mostrará el menú de contexto adecuado para los elementos de la lista de 'Archivos recientes' y olvidará correctamente todo cuando use la función 'Desactivar historial'.
- Al pulsar sobre una carpeta de la barra lateral se puede alternar entre los modos «Examinar» y «Ver» y se recuerda el último modo usado al cambiar entre carpetas, lo que permite una navegación más rápida en colecciones enormes de imágenes.
- La navegación con el teclado se ha simplificado aún más al indicar correctamente el foco del teclado en el modo de navegación.
- La función «Ajustar al ancho» se ha sustituido por una función «Rellenar» más general.

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Incluso las mejoras más pequeñas pueden hacer que el modo de trabajar del usuario sea más agradable:

- Para mejorar la coherencia, las imágenes SVG se amplían ahora como el resto de imágenes cuando se activa «Vista de imágenes → Ampliar las imágenes más pequeñas».
- Tras editar una imagen o deshacer los cambios, la vista de la imagen y la miniatura ya no estarán sin sincronizar.
- Al cambiar el nombre de las imágenes, la extensión del nombre de archivo no se seleccionará de forma predeterminada, y el diálogo «Mover/Copiar/Enlazar» muestra ahora de forma predeterminada la carpeta actual.
- Se han corregido muchos defectos visuales: en la barra de URL, en la barra de herramientas de pantalla completa y en las animaciones de ayuda emergente de las miniaturas. También se han añadido los iconos que faltaban.
- Por último, aunque no menos importante, el botón de desplazamiento de pantalla completa verá directamente la imagen en lugar de mostrar el contenido de la carpeta, y la configuración avanzada ahora permite un mayor control sobre el propósito de reproducción de color ICC.

#### Oficina

En <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, el visor universal de documentos de KDE, la renderización de PDF y la extracción de texto se pueden cancelar ahora si tiene la versión 0.63 o superior de «poppler», lo que significa que si tiene un archivo PDF complejo y cambia la ampliación mientras se renderiza, se cancelará inmediatamente en lugar de esperar a que finalice la renderización.

Encontrará una mejor compatibilidad con JavaScript en PDF para «AFSimple_Calculate», y si tiene la versión 0.64 de «poppler» u otra superior, Okular admitirá cambios de JavaScript en PDF en el estado de solo lectura de los formularios.

La gestión de mensajes de confirmación de reservas en <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, el potente cliente de correo electrónico de KDE, se ha mejorado bastante al admitir reservas de trenes y el uso de la base de datos de aeropuertos de Wikidata para mostrar vuelos con información correcta de zonas horarias. Para facilitarle el trabajo aún más, se ha implementado un nuevo extractor para los mensajes que no contienen datos de reserva estructurados.

Otras mejoras incluyen:

- La estructura del mensaje se puede volver a mostrar con el nuevo complemento «Experto».
- Se ha añadido un complemento al editor Sieve para seleccionar mensajes de correo electrónico de la base de datos de Akonadi.
- La búsqueda de texto del editor se ha mejorado con el uso de expresiones regulares.

#### Utilidades

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

La mejora de la interfaz de usuario de <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, la versátil herramienta de captura de pantalla de KDE, se ha centrado en:

- La fila inferior de botones ha recibido una revisión y ahora muestra un botón para abrir la ventana de Preferencias y un nuevo botón «Herramientas» que revela métodos para abrir la última carpeta de captura de pantalla usada y lanzar un programa de grabación de pantalla.
- Ahora se recuerda de forma predeterminada el último modo usado para guardar.
- El tamaño de la ventana se adapta ahora a las proporciones de la captura de pantalla, lo que tiene como resultado una miniatura de la captura más agradable y más eficiente con el espacio que ocupa.
- La ventana de preferencias se ha simplificado sustancialmente.

Además, el usuario puede simplificar su forma de trabajar con estas nuevas funcionalidades:

- Tras capturar una ventana determinada, se puede añadir su título de forma automática al nombre del archivo de la captura de pantalla.
- El usuario puede escoger ahora si se debe salir automáticamente de Spectacle tras una operación de copiar o de guardar.

Las correcciones de errores más importantes incluyen:

- Arrastrar y soltar a las ventanas de Chromium funciona ahora como era de esperar.
- El uso repetido de los accesos rápidos de teclado para guardar una captura de pantalla ya no genera un diálogo de advertencia de acceso rápido ambiguo.
- Para capturas de regiones rectangulares, el borde inferior de la selección se puede ajustar con mayor precisión.
- Se ha mejorado la fiabilidad de la captura de ventanas que tocan los bordes de la pantalla cuando la composición está desactivada.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

En <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, el gestor de certificados e interfaz grafica criptográfica universal de KDE, se pueden generar claves EdDSA Curve25519 si se usa con una versión reciente de GnuPG. Se ha añadido una vista de «Bloc de notas» para acciones criptográficas basadas en texto y ahora se puede firmar/cifrar y descifrar/verificar directamente en la aplicación. En la vista «Detalles del certificado» podrá encontrar ahora una acción para exportar como texto para copiar y pegar. Aún más, se puede importar el resultado usando la nueva vista de «Bloc de notas».

En <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, la herramienta gráfica de KDE para comprimir y descomprimir archivos de diversos formatos, ahora se puede detener la compresión o la extracción cuando se usa el motor «libzip» con archivos ZIP.

### Aplicaciones que se han unido al calendario de lanzamientos de las Aplicaciones de KDE

La grabadora de la webcam de KDE <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> y el programa de copias de respaldo <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> siguen ahora el ciclo de lanzamientos de las aplicaciones de KDE. El programa de mensajería instantánea <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a> se ha vuelto a introducir tras su adaptación a KDE Frameworks 5.

### Aplicaciones que se han movido a su propio calendario de lanzamientos

El editor hexadecimal <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> tendrá su propio calendario de lanzamientos a petición de su encargado.

### A la caza de errores

Se han resuelto más de 170 errores en aplicaciones, que incluyen la suite Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular y Umbrello, entre otras.

### Registro de cambios completo
