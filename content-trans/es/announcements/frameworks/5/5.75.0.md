---
aliases:
- ../../kde-frameworks-5.75.0
date: 2020-10-10
layout: framework
libCount: 70
qtversion: 5.12
---
### Baloo

+ [AdvancedQueryParser] Relajar el análisis de cadenas que terminan entre paréntesis.
+ [AdvancedQueryParser] Relajar el análisis de cadenas que terminan con comparador.
+ [AdvancedQueryParser] Corregir el acceso fuera de límites si el último carácter es un comparador.
+ [Term] Sustituir el constructor «Term::Private» con valores por omisión.
+ [BasicIndexingJob] Acceso rápido de obtención de archivos sin atributos en XAttr.
+ [extractor] Corregir el tipo de documento en el resultado de la extracción.
+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.

### BluezQt

+ Se ha añadido la propiedad «rfkill» a «manager».
+ Se ha añadido la propiedad «status» a «rfkill».
+ Registrar «Rfkill» para QML.
+ Exportar Rfkill.
+ Permitir que se proporcionen valores de datos de servicio para anuncios de LE.

### Iconos Brisa

+ Se ha añadido un nuevo icono «behavior» genérico.
+ Hacer que la validación de iconos dependa de la generación de iconos solo si está activado.
+ Sustituir el script bash de iconos de 24px con un script en python.
+ Usar iconografía al estilo de banderas para «view-calendar-holiday».
+ Se ha añadido el logo de Plasma Nano.
+ Se ha añadido «application-x-kmymoney».
+ Se ha añadido el icono de KMyMoney.

### Módulos CMake adicionales

+ Se ha corregido la traducción de la extracción de URL de Invent.
+ Incluir «FeatureSummary» y la búsqueda de módulos.
+ Introducir la verificación de plausibilidad para la licencia saliente.
+ Se ha añadido «CheckAtomic.cmake».
+ Se ha corregido la configuración con «pthread» en Android de 32 bits.
+ Se ha añadido el parámetro «RENAME» a «ecm_generate_dbus_service_file».
+ Se ha corregido «find_library» en Android con NDK &lt; 22.
+ Ordenar explícitamente las listas de versiones de Android.
+ Guardar «{min,target,compile}Sdk» de Android en variables.

### Herramientas KDE Doxygen

+ Mejoras en las licencias.
+ Corregir «api.kde.org» en la plataforma móvil.
+ Hacer de «api.kde.org» un PWA.

### KArchive

+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.

### KAuth

+ Usar nueva variable de instalación (error 415938).
+ Marcar a David Edmundson como encargado de KAuth.

### KCalendarCore

+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.

### KCMUtils

+ Eliminar el manejo de eventos internos del código de las pestañas (fallo 423080).

### KCompletion

+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.

### KConfig

+ CMake: definir también «SKIP_AUTOUIC» en los archivos generados.
+ Usar orden inverso en «KDesktopFile::locateLocal» para iterar sobre las rutas de configuración genéricas.

### KConfigWidgets

+ Corregir el «isDefault» que hacía que KCModule no actualizase correctamente su estado predeterminado.
+ [kcolorscheme]: Añadir «isColorSetSupported» para comprobar si un esquema de color tiene un determinado conjunto de colores.
+ [kcolorscheme] Leer correctamente los colores personalizados para el estado «inactivo» de los fondos.

### KContacts

+ Se ha eliminado el archivo de licencia obsoleto de «LGPL-2.0-only».
+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.

### KCoreAddons

+ KJob: Emitir «result()» y «finished()» al menos una vez.
+ Añadir el «getter» protegido «KJob::isFinished()».
+ Marcar como obsoleto «KRandomSequence» en favor de «QRandomGenerator».
+ Inicializar variable en la clase de la cabecera + hacer constante la variable/puntero.
+ Endurecer las pruebas basadas en mensajes contra el entorno (fallo 387006).
+ Simplificar la prueba de vigilancia de qrc (error 387006).
+ Instancias de «refcount» y borrar «KDirWatchPrivate» (error 423928).
+ Marcar como obsoleto «KBackup::backupFile()» debido a su pérdida de funcionalidad.
+ Marcar como obsoleto «KBackup::rcsBackupFile(...)» porque no tiene ningún usuario conocido.

### KDBusAddons

+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.

### KDeclarative

+ Se ha añadido QML para internacionalización en KF 5.17.
+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.
+ Bloquear los accesos rápidos cuando se graban secuencias de teclas (error 425979).
+ Añadir «SettingHighlighter» como una versión manual de resaltado realizado por «SettingStateBinding».

### Soporte de KDELibs 4

+ KStandardDirs: Resolver siempre los enlaces simbólicos de los archivos de configuración.

### KHolidays

+ Campos de descripción no documentados para los archivos de festividades «mt_*».
+ Añadir las festividades nacionales de Malta en inglés (en-gb) y en maltés (mt).

### KI18n

+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.

### KIO

+ KUrlNavigator: Usar siempre «desktop:/» en lugar de «desktop:».
+ Implementar el uso de la sintaxis del «bang» de DuckDuckGo en los accesos rápidos de la web (error 374637).
+ KNewFileMenu: «KIO::mostLocalUrl» solo es útil con los protocolos «:local».
+ Marcar como obsoleto «KIO::pixmapForUrl».
+ kio_trash: Eliminar la comprobación de permisos estricta que no era necesaria (error 76380).
+ OpenUrlJob: Manejar con consistencia todos los scripts de texto (error 425177).
+ KProcessRunner: Más metadatos de «systemd».
+ KDirOperator: No llamar a «setCurrentItem» con URL vacías (error 425163).
+ KNewFileMenu: se ha corregido la creación de nuevos directorios con nombre que empiece por «:» (error 425396).
+ StatJob: Hacer más patente que «mostLocalUrl» solo funciona con protocolos «:local».
+ Documentar cómo se añaden nuevos roles «aleatorios» en «kfileplacesmodel.h».
+ Eliminar el antiguo código de «kio_fonts» en «KCoreDirLister», ya que «hostname» se eliminaba de forma incorrecta.
+ KUrlCompletion: Acomodar los protocolos «:local» que usan «hostname» en su URL.
+ Dividir el código del método «addServiceActionsTo» en métodos de menor tamaño.
+ [kio] Error: Permitir comillas dobles en el diálogo para abrir/guardar (error 185433).
+ StatJob: Cancelar el trabajo si la URL no es válida (error 426367).
+ Conectar «slots» explícitamente en lugar de usar conexiones automáticas.
+ Hacer que la API de «filesharingpage» funcione realmente.

### Kirigami

+ AbstractApplicationHeader: anchors.fill instead of position-dependent anchoring
+ Se ha mejorado el aspecto y la consistencia de «GlobalDrawerActionItem».
+ Eliminar el sangrado del formulario para disposiciones estrechas.
+ Revertir «Permitir la personalización de los colores de la cabecera».
+ Permitir la personalización de los colores de la cabecera.
+ Se ha añadido el «@since» que faltaba para la propiedades del área de dibujo.
+ Introducir las propiedades «paintedWidth/paintedHeight» del estilo de imagen de QtQuick.
+ Añadir una propiedad de imagen predeterminada al icono (en el estilo al que recurrir).
+ Eliminar el comportamiento personalizado «implicitWidth/implicitHeight» del icono.
+ Proteger de un potencial puntero nulo (parece que es sorprendentemente común).
+ «setStatus» es protegido.
+ Se ha introducido la propiedad «status».
+ Permitir los proveedores de imágenes de tipo «ImageResponse» y «Texture» en «Kirigami::Icon».
+ Advertir que no se use «ScrollView» en «ScrollablePage».
+ Revertir «Mostrar siempre el separador».
+ Hacer que «mobilemode» use delegados personalizados para el título.
+ Ocultar la línea de separación de las «migas de pan» si el diseño de los botones es visible pero de ancho 0 (fallo 426738).
+ [icono] Considerar el icono no válido cuando la fuente es una URL vacía.
+ Cambiar «Units.fontMetrics» para que use «FontMetrics» realmente.
+ Añadir la propiedad «Kirigami.FormData.labelAlignment».
+ Mostrar siempre el separador.
+ Usar colores de cabeceras para el estilo de escritorio «AbstractApplicationHeader».
+ Usar el contexto del componente cuando se crean delegados para «ToolBarLayout».
+ Interrumpir y borrar incubadoras cuando se borra «ToolBarLayoutDelegate».
+ Eliminar acciones y delegados de «ToolBarLayout» cuando se destruyen (fallo 425670).
+ Sustituir el uso de la conversión de puntero de estilo C en «sizegroup».
+ Las constantes binarias son una extensión de C++14.
+ sizegroup: Corregir las advertencias no manejadas en el enumerador.
+ sizegroup: Corregir las conexiones con 3 argumentos.
+ sizegroup: Añadir «CONSTANT» a las señales.
+ Corregir algunos casos de uso de bucles de intervalos en contenedores Qt que no son constantes.
+ Limitar la altura del botón en la barra de herramientas global.
+ Mostrar un separador entre las «migas de pan» y los iconos de la izquierda.
+ Usar «KDE_INSTALL_TARGETS_DEFAULT_ARGS».
+ Calcular el tamaño de «ApplicationHeaders» usando «SizeGroup».
+ Se ha introducido «SizeGroup».
+ Corrección: Hacer que el indicador de actualización aparezca sobre las cabeceras de las listas.
+ Colocar hojas de superposición sobre los cajones.

### KItemModels

+ Ignorar «sourceDataChanged» en índices no válidos.
+ Permitir que existan nodos «plegables» en «KDescendantProxyModel».

### KNewStuff

+ Realizar un seguimiento manual del ciclo de vida de los componentes internos de nuestro lanzador de «kpackage».
+ Actualizar las versiones para las actualizaciones «falsas» de kpackage (fallo 427201).
+ No usar el parámetro predeterminado cuando no se necesita.
+ Se ha corregido un fallo de la aplicación al instalar «kpackages» (error 426732).
+ Detectar cuándo cambia la caché y reaccionar en consecuencia.
+ Corregir la actualización de la entrada si el número de versión está vacío (fallo 417510).
+ Convertir puntero en constante e inicializar variable en la cabecera.
+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.
+ Aceptar la sugerencia de toma de control del mantenimiento.

### KNotification

+ Bajar la versión del complemento de Android hasta que esté disponible un nuevo Gradle.
+ Usar las versiones de SDK de Android de ECM.
+ Marcar como obsoleto el constructor de «KNotification» que usa «widget» como parámetro.

### Framework KPackage

+ Corregir las notificaciones de DBus cuando se instala/actualiza.
+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.

### KParts

+ Instalar los archivos de definición de tipo de servicio «krop» y «krwp» por coincidencia de nombres de archivo.

### KQuickCharts

+ Evite el bucle de enlace dentro de «Legend».
+ Eliminar la comprobación de GLES3 en «SDFShader» (fallo 426458).

### KRunner

+ Añadir la propiedad «matchRegex» para evitar la creación innecesaria de hilos.
+ Permitir la definición de acciones en «QueryMatch».
+ Permitir especificar acciones individuales para las coincidencias de lanzadores de D-Bus.
+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.
+ Añadir propiedad de recuento mínimo de letras.
+ Considerar la variable de entorno «XDG_DATA_HOME» para los directorios de instalación de plantillas.
+ Mejorar los mensajes de error de los lanzadores de D-Bus.
+ Empezar la emisión de advertencias de adaptación de metadatos en tiempo de ejecución .

### KService

+ Traer de vuelta «disableAutoRebuild» desde el borde (fallo 423931).

### KTextEditor

+ [kateprinter] Adaptar el uso de métodos obsoletos de «QPrinter».
+ No crear un búfer temporal para detectar el tipo MIME de los archivos locales guardados.
+ Evitar que se cuelgue debido a que el diccionario y los trigramas se cargan al escribir por primera vez.
+ [Vimode] Mostrar siempre los búferes a-z en minúsculas.
+ [KateFadeEffect] Emitir «hideAnimationFinished()» cuando se interrumpe un fundido a negro mediante una aparición gradual.
+ Asegurar el borde perfecto de píxeles incluso para visualización con escalado.
+ Asegurar que se pinta el separador del borde sobre el resto de cosas, como las marcas de resaltado del plegado.
+ Mover el separador de entre los bordes del icono y los números de la línea hasta entre la barra y el texto.
+ [Vimode] Corregir el comportamiento de los registros numerados.
+ [Vimode] Situar el texto borrado en el registro correcto.
+ Restaurar el comportamiento de encontrar la selección cuando no hay disponible ninguna selección.
+ Ya no hay más archivos «LGPL-2.1-only» ni «LGPL-3.0-only».
+ Se ha cambiado la licencia de archivos a LGPL 2.0 o posterior.
+ Usar el método de asignación no necesario para algunos colores del tema.
+ 5.75 será una vez incompatible; usar como predeterminado «Selección automática de tema de color» para los temas.
+ Alterar esquema =&gt; tema en el código para evitar confusión.
+ Acortar la cabecera de licencia propuesta al estado actual.
+ Asegurar que siempre acabamos con un tema válido.
+ Se ha mejorado el diálogo «Copiar...».
+ Corregir más nombres nuevo =&gt; copia.
+ Añadir algún «KMessageWidget» que sugiere temas de solo lectura para copiarlos; cambiar «new» =&gt; «copy».
+ Desactivar la edición de temas de solo lectura.
+ El ahorro de resaltar estilos específicos funciona, solo se guardan las diferencias.
+ Simplificar la creación de atributos, los colores transparentes se manejan ahora correctamente en «Format».
+ Tratar de limitar la exportación a los atributos de los cambios; esto funciona a medias, aunque todavía se exportan los nombres incorrectos de las definiciones incluidas.
+ Empezar a calcular los valores predeterminados «reales» basados en el tema actual y formatos sin sobrecarga de estilo para el resaltado.
+ Se ha corregido la acción de reinicio.
+ Almacenamiento de sobrecargas específicas de sintaxis; por ahora solo se almacena todo lo que se carga en la vista de árbol.
+ Empezar a trabajar en el resaltado de sintaxis de sobrecargas específicas; por ahora, mostrar solo los estilos que realmente tiene un resaltado.
+ Permitir que se puedan guardar los cambios del estilo predeterminado.
+ Permitir que se puedan guardar los cambios de color.
+ Implementar la exportación de temas: copia de archivos sencilla.
+ No resaltar los «import/export» específicos; no tiene sentido con el nuevo formato de «.theme».
+ Implementar la importación de archivos «.theme».
+ Usar tema para el trabajo de «new» y «delete»; «new» copiará el tema actual como punto de inicio.
+ Usar colores del tema en todas partes.
+ Deshacerse de más código de esquemas antiguo en favor de «KSyntaxHighlighting::Theme».
+ Empezar a usar los colores como se definen en el tema, sin una lógica propia para ello.
+ Inicializar «m_pasteSelection» e incrementar la versión de archivo UI.
+ Se ha añadido un acceso rápido para pegar la selección del ratón.
+ Evitar «setTheme»; podemos pasar nuestro tema a las funciones auxiliares.
+ Corregir la ayuda emergente; esto solo reiniciará al valor predeterminado del tema.
+ Exportar la configuración de los estilos predeterminados a tema JSON.
+ Comenzar a trabajar en la exportación JSON del tema, que se activa al usar la extensión «.theme» en el diálogo de exportación.
+ Cambiar el nombre de «Usar tema de color de KDE» a «Usar colores predeterminados», que es el efecto real.
+ No proporcionar el tema «KDE» vacío de forma predeterminada.
+ Permitir el uso de selección automática del tema correcto para el tema de color Qt/KDE actual.
+ Convertir nombres de temas antiguos a los nuevos; usar el nuevo archivo de configuración; transferir datos una vez.
+ Mover la configuración de la fuente al aspecto; cambiar nombre de esquema =&gt; tema de color.
+ Eliminar el nombre de tema predeterminado programado y usar los accesores de KSyntaxHighlighting.
+ Cargar los colores a los que recurrir desde el tema.
+ No empaquetar colores integrados.
+ Usar la función correcta para buscar el tema.
+ Los colores del editor que se usan son ahora del tema.
+ Usar el enumerador «KSyntaxHighlighting::Theme::EditorColorRole».
+ Contemplar la transición «Normal =&gt; Predeterminado».
+ Primer paso: Cargar la lista de temas de «KSyntaxHighlighting», ahora que ya la tenemos como esquemas conocidos en «KTextEditor».

### KUnitConversion

+ Usar letras mayúsculas para «Eficiencia de combustible».

### KWayland

+ No poner en la caché el iterador «QList::end()» si se usa «erase()».

### KWidgetsAddons

+ kviewstateserializer.cpp: Salvaguardias contra fallos en «restoreScrollBarState()».

### KWindowSystem

+ Cambiar la licencia de los archivos para que sea compatible con LGPL-2.1.

### KXMLGUI

+ [kmainwindow] No borrar entradas de un «kconfiggroup» no válido (fallo 427236).
+ No solapar ventanas principales cuando se abren instancias adicionales (error 426725).
+ [kmainwindow] No crear ventanas nativas para las ventanas que no son de nivel superior (error 424024).
+ KAboutApplicationDialog: Evitar la pestaña «Bibliotecas» vacía si se ha definido «HideKdeVersion».
+ Mostrar el código del idioma además del nombre del idioma (traducido) en el diálogo para cambiar el idioma de la aplicación.
+ Marcar como obsoleto «KShortcutsEditor::undoChanges()» en favor del nuevo «undo()».
+ Gestionar el doble cierre en la ventana principal (error 416728).

### Framework de Plasma

+ Corregir que «plasmoidheading.svgz» se instale en un lugar incorrecto (error 426537).
+ Proporcionar un valor de «lastModified» en «ThemeTest».
+ Detectar si estamos buscando un elemento vacío y salir antes.
+ Hacer que las ayudas emergentes de «PlasmaComponents3» usen el estilo típico de las ayudas emergentes (error 424506).
+ Usar colores en la cabecera de «PlasmoidHeading».
+ Cambiar la animación del movimiento de resaltado de «TabBar» de PC2 facilitando el tipo a «OutCubic».
+ Permitir el uso del conjunto de colores de «Tooltip».
+ Corregir que los iconos de «Button/ToolButton» de PC3 no tengan siempre el color correcto definido (error 426556).
+ Asegurar que «FrameSvg» usa la marca de tiempo de «lastModified» cuando se usa la caché (error 426674).
+ Asegurar que siempre tenemos una marca de tiempo «lastModified» válida cuando se llama a «setImagePath».
+ Marcar como obsoleta la marca de tiempo «0» de «lastModified» en «Theme::findInCache» (error 426674).
+ Adaptar la importación de QQC2 al nuevo esquema de versiones.
+ [windowthumbnail] Verificar que existe el «GLContext» relevante, no cualquier otro.
+ Se ha añadido la importación de «PlasmaCore» que faltaba a «ButtonRow.qml».
+ Se han corregido varios errores más de referencias en «PlasmaExtras.ListItem».
+ Se ha corregido un error de «implicitBackgroundWidth» en «PlasmaExtras.ListItem».
+ Llamar al modo de edición «Modo de edición».
+ Se ha corregido un «TypeError» en «QueryDialog.qml».
+ Corregir «ReferenceError» para «PlasmaCore» en «Button.qml».

### QQC2StyleBridge

+ Usar también el color de resaltado cuando se resalta «checkDelegate» (error 427022).
+ Respetar el clic en la barra de desplazamiento para saltar a la posición indicada (error 412685).
+ No heredar colores en el estilo de la barra de herramientas del escritorio de forma predeterminada.
+ Se ha cambiado la licencia del archivo a LGPL 2.0 o posterior.
+ Usar colores en la cabecera solo en las barras de herramientas de cabecera.
+ Mover la declaración de asignación de colores a un lugar en el que se pueda sobrescribir.
+ Usar colores en la cabecera de la barra de herramientas al estilo del escritorio.
+ Se ha añadido la propiedad «isItem» que faltaba y que es necesaria para los árboles.

### Sonnet

+ Desactualizar la salida de trigramas.

### Resaltado de sintaxis

+ AppArmor: Se ha corregido la expresión regular para detectar rutas.
+ AppArmor: se ha actualizado el resaltado de sintaxis para AppArmor 3.0.
+ Caché de colores para conversiones de RGB a «ansi256colors» (acelera la carga de Markdown).
+ SELinux: Usar palabras clave «include».
+ SubRip Subtitles y Logcat: Mejoras menores.
+ Generador para «doxygenlua.xml».
+ Corregir las fórmulas LaTeX de «doxygen» (error 426466).
+ Usar «Q_ASSERT» como en el framework restante + corregir «asserts».
+ Se ha cambiado el nombre de «--format-trace» a «--syntax-trace».
+ Aplicar un estilo a las regiones.
+ Seguir el rastro de contextos y de regiones.
+ Usar el color de fondo del editor de forma predeterminada.
+ Resaltado de sintaxis ANSI.
+ Añadir el copyright y actualizar el color del separador en el tema «Radical».
+ Actualizar el color del separador en los temas solarizados.
+ Mejorar el color del separador y el borde del icono en los temas Ayu, Nord y Vim oscuro.
+ Hacer que el color del separador sea menos prominente.
+ Importar el esquema de Kate para el conversor de temas, por Juraj Oravec.
+ Sección sobre licencia más prominente, enlace a nuestra copia de «MIT.txt».
+ Primera plantilla para el generador «base16», https://github.com/chriskempson/base16
+ Añadir información correcta de licencia a todos los temas.
+ Se ha añadido el tema de color «Radical».
+ Se ha añadido el tema de color «Nord».
+ Mejorar el escaparate de temas para mostrar más estilos.
+ Añadir los temas claro y oscuro de «gruvbox», con licencia MIT.
+ Añadir el tema de color «ayu» (con variantes clara, oscura y espejismo).
+ Añadir alternativa POSIX para asignación sencilla de variables.
+ Herramientas para generar un gráfico a partir de un archivo de sintaxis.
+ Corregir conversión automática del color sin asignar de «QRgb == 0» a «negro» en lugar de a «transparente».
+ Añadir registro de cambios de Debian y controlar los archivos de ejemplo.
+ Se ha añadido el tema de color «Drácula».

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
