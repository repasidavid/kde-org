---
aliases:
- ../../kde-frameworks-5.73.0
date: 2020-08-01
layout: framework
libCount: 70
---
### Iconos Brisa

+ Se han centrado los iconos «help-about» y «help-whatsthis» de 16 píxeles.
+ Se ha añadido el icono «kirigami-gallery».
+ Se ha añadido el icono para Kontrast.
+ Se ha añadido el icono para el tipo MIME «applications/pkcs12».
+ Hacer que los iconos del volumen del sonido de Brisa oscuro coincidan con los de Brisa.
+ Hacer que el estilo del micrófono sea más consistente con otros iconos de sonido y que tenga los píxeles alineados.
+ Usar un 35% de opacidad para las ondas desvanecidas en los iconos de volumen de sonido y hacer que las ondas silenciadas se desvanezcan.
+ Hacer que los iconos «audio-off» y «audio-volume-muted» sean los mismos.
+ Se ha añadido el icono «snap-angle».
+ Se ha corregido «application/x-ms-shortcut» enlazándolo a un icono de 16 píxeles.
+ Se han corregido inconsistencias en los iconos de tipos MIME entre las versiones claras y oscuras.
+ Se han corregido las diferencias invisibles de claro/oscuro en los iconos «vscode» y «wayland».
+ Se ha añadido el icono «document-replace» (por ejemplo, para la acción «sobrescribir»).
+ Se ha añadido una plantilla para crear guiones en Python para editar archivos SVG.
+ Se ha añadido el icono de estado SMART (error 423997).
+ Se han añadido los iconos «task-recurring» y «appointment-recurring» (error 397996).

### Módulos CMake adicionales

+ Se ha añadido «ecm_generate_dbus_service_file».
+ Se ha añadido «ecm_install_configured_file».
+ Exportar «Wayland_DATADIR».

### KActivitiesStats

+ Ignorar «BoostConfig.cmake» si está presente (error 424799).

### KCMUtils

+ Permitir el indicador de resaltado para el módulo basado en QWidget y QtQuick.
+ Se ha añadido un método para borrar el selector de complementos (error 382136).

### KConfig

+ Actualizar «sGlobalFileName» cuando «TestMode» de «QStandardPaths» se conmuta.
+ API dox: Declarar explícitamente la codificación esperada para la clave y los nombres de grupo de KConfig.

### KConfigWidgets

+ KCModule: Indicar cuándo se ha modificado el valor predeterminado de alguna preferencia.
+ Cuando se reinicie a los valores predeterminados del sistema, no usar la paleta estándar del estilo.

### KCoreAddons

+ Se ha introducido «KRandom::shuffle(container)».

### KDeclarative

+ SettingStateBinding: indicar si está activado un resalte no predeterminado.
+ Asegurarse de que «KF5CoreAddons» está instalado antes de usar «KF5Declarative».
+ Añadir «KF5::CoreAddons» a la interfaz pública para «KF5::QuickAddons».
+ Introducir los elementos «SettingState*» para la facilitar la escritura de KCM.
+ Permitir notificaciones de configuración en «configpropertymap».

### Complementos KDE GUI

+ Mover «KCursorSaver» de «libkdepim», mejorado.

### KImageFormats

+ Adaptar la licencia a LGPL 2.0 o posterior.

### KIO

+ KFilterCombo: «application/octet-stream» también es «hasAllFilesFilter».
+ Introducir «OpenOrExecuteFileInterface» para manejar la apertura de ejecutables.
+ RenameDialog: Mostrar si los archivos son idénticos (error 412662).
+ [Diálogo de cambio de nombre] Se ha portado el botón «Sobrescribir» a «KStandardGuiItem::Overwrite» (error 424414).
+ Mostrar hasta tres acciones de archivos en línea, no solo una (error 424281).
+ KFileFilterCombo: Mostrar las extensiones si existe un comentario MIME repetido.
+ [KUrlCompletion] No añadir «/» a las carpetas completadas.
+ [Propiedades] Añadir el algoritmo SHA512 al widget de sumas criptográficas (error 423739).
+ [WebDav] Corregir las copias que incluyen sobrescrituras para el esclavo «webdav» (error 422238).

### Kirigami

+ Permitir la visibilidad de acciones en «GlobalDrawer».
+ Se ha introducido el componente «FlexColumn».
+ Optimizaciones para la capa móvil.
+ Una capa también debe cubrir la barra de pestañas.
+ PageRouter: Usar realmente páginas precargadas cuando se hace un «push».
+ Se han mejorado los documentos (Abstract)ApplicationItem.
+ Se ha corregido una violación de acceso al destruir «PageRouter».
+ Hacer que «ScrollablePage» se pueda desplazar con el teclado.
+ Se ha mejorado la accesibilidad de los campos de entrada de Kirigami.
+ Se ha corregido la compilación estática.
+ PageRouter: Se han añadido varias API de conveniencia para tareas distintas a las manuales.
+ Se han introducido las API de ciclo de vida de «PageRouter».
+ Forzar un orden Z más bajo para el último recurso de software de «ShadowedRectangle».

### KItemModels

+ KSelectionProxyModel: permitir el uso del modelo con el nuevo estilo de «connect».
+ KRearrangeColumnsProxyModel: se ha corregido «hasChildren()» cuando todavía no se han configurado columnas.

### KNewStuff

+ [Diálogo QtQuick] Usar el icono de actualización que existe (error 424892).
+ [Diálogo QtQuick de GHNS] Se han mejorado las etiquetas de las listas desplegables.
+ No mostrar el sector «downloaditem» para un único elemento descargable.
+ Reajustar el diseño un poco para que tenga un aspecto más equilibrado.
+ Se ha corregido el diseño de la tarjeta de estado «EntryDetails».
+ Enlazar solo con «Core», no con el complemento en quick (porque no funciona).
+ Se ha mejorado la pantalla de bienvenida (y no se usan diálogos QML).
+ Usar el diálogo cuando se pasa un archivo «knsrc», y no en caso contrario.
+ Se ha eliminado el botón de la ventana principal.
+ Se ha añadido un diálogo (para mostrar el diálogo directamente cuando se pasa un archivo).
+ Se han añadido los nuevos bits a la aplicación principal.
+ Se ha añadido un modelo sencillo (al menos por ahora) para mostrar archivos «knsrc».
+ Se ha movido el archivo QML principal a un «qrc».
+ Se ha convertido la herramienta de prueba del diálogo de KNewStuff en una herramienta adecuada.
+ Permitir el borrado de entradas actualizables (error 260836).
+ No ceder el foco automáticamente al primer elemento (error 417843).
+ Se ha corregido la visualización de los botones «Detalles» y «Desinstalar» en el modo de vista en mosaico (error 418034).
+ Se ha corregido el movimiento de los botones cuando se inserta un texto a buscar (error 406993).
+ Se ha corregido un parámetro que faltaba para la cadena de texto traducida.
+ Se ha corregido la lista desplegable de acciones de instalación en el diálogo (error 369561).
+ Se han añadido ayudas emergentes para distintos modos de visualización en el diálogo QML.
+ Definir la entrada como «no instalada» si falla la instalación (error 422864).

### KQuickCharts

+ Tener en cuenta también las propiedades del modelo cuando se usa «ModelHistorySource».

### KRunner

+ Implementar el supervisor de KConfig para los KCM de complementos y lanzadores activados (error 421426).
+ No eliminar el método virtual de la compilación (error 423003).
+ Desaconsejar el uso de «AbstractRunner::dataEngine(...)».
+ Se han corregido lanzadores desactivados y la configuración del lanzador del plasmoide.
+ Demorar la emisión de advertencias de adaptación de metadatos hasta KF 5.75.

### KService

+ Se ha añadido una sobrecarga para llamar a la terminal con variables de entorno (error 409107).

### KTextEditor

+ Se han añadido iconos a todos los botones del mensaje de archivo modificado (error 423061).
+ Usar URL canónicas de docs.kde.org.

### Framework KWallet

+ Usar un nombre mejor y seguir las directrices del HIG.
+ Marcar la API como obsoleta también en la descripción de la interfaz D-Bus.
+ Añadir una copia de «org.kde.KWallet.xml» sin la API obsoleta.

### KWayland

+ Gestión de ventanas de Plasma: se ha adaptado a los cambios del protocolo.
+ PlasmaWindowManagement: Adoptar los cambios del protocolo.

### KWidgetsAddons

+ KMultiTabBar: Dibujar los iconos de las pestañas como activos al pasar el ratón sobre ellos.
+ Se ha corregido «KMultiTabBar» para que dibuje el icono desplazado cuando está pulsado o marcado según el diseño del estilo.
+ Usar un nuevo icono «overwrite» para las acciones «sobrescribir» de la interfaz de usuario (error 406563).

### KXMLGUI

+ Hacer pública «KXmlGuiVersionHandler::findVersionNumber» en «KXMLGUIClient».

### NetworkManagerQt

+ Se ha eliminado el registro de datos de inicio de sesión.

### Framework de Plasma

+ Definir la propiedad de «cpp» en la instancia de unidades.
+ Se han añadido algunas importaciones de «PlasmaCore» que faltaban.
+ Se ha eliminado el uso de propiedades de contexto para el tema y las unidades.
+ PC3: se ha mejorado el aspecto del menú.
+ Se han vuelto a ajustar los iconos de sonido para que coincidan con los iconos de Brisa.
+ Hacer que «showTooltip()» se pueda llamar desde QML.
+ [PlasmaComponents3] Respetar la propiedad «icon.[nombre|origen]».
+ Filtrar por factores de forma, si está definido.
+ Se ha actualizado el estilo del icono «mute» para que coincida con el de los iconos de Brisa.
+ Eliminar el registro de metatipos dañados para los tipos que carecen de «*» en el nombre.
+ Usar el 35% de opacidad para los elementos atenuados en los iconos de redes.
+ No mostrar diálogos de Plasma en los selectores de tareas (error 419239).
+ Se ha corregido la URL de errores de Qt para el código de visualización de tipos de letra.
+ No usar el cursor de la mano porque no es consistente.
+ [ExpandableListItem] Usar tamaños estándares para los botones.
+ [PlasmaComponents3] Mostrar el efecto de foco de «ToolButton» y omitir el sombreado cuando es plano.
+ Unificar la altura de los botones PC3, los campos de texto y las listas desplegables.
+ [PlasmaComponents3] Se han añadido funcionalidades que faltaban a TextField.
+ [ExpandableListItem] Se ha corregido un feo error.
+ Se ha reescrito «button.svg» para que sea más fácil de entender.
+ Copiar las transmisiones de «DataEngine» antes de interactuar (error 423081).
+ Hacer más visible la potencia de la señal en los iconos de red (error 423843).

### QQC2StyleBridge

+ Usar el estilo «elevado» para botones de herramienta que no son planos.
+ Reservar espacio para el icono de «toolbutton» solo si se puede tener realmente el icono.
+ Permitir que se muestre una flecha de menú en los botones de herramienta.
+ Se ha corregido de verdad la altura del separador del menú en pantallas DPI.
+ Se ha actualizado «Mainpage.dox».
+ Definir correctamente la altura de «MenuSeparator» (error 423653).

### Solid

+ Borrar «m_deviceCache» antes de volver a realizar introspecciones (error 416495).

### Resaltado de sintaxis

+ Convertir «DetectChar» al «Detect2Chars» adecuado.
+ Mathematica: Algunas mejoras.
+ Doxygen: Se han corregido algunos errores; DoxygenLua: empezar solo con «---» o con «--!».
+ AutoHotkey: Se ha reescrito por completo.
+ Nim: Se han corregido los comentarios.
+ Se ha añadido resaltado de sintaxis para Nim.
+ Scheme: Se ha corregido un identificador.
+ Scheme: se han añadido comentarios de datos, comentarios anidados y otras mejoras.
+ Se han sustituido algunas «RegExp» por «AnyChar», «DetectChar», «Detect2Chars» o «StringDetect».
+ language.xsd: Se ha eliminado «HlCFloat» y se ha introducido el tipo «char».
+ KConfig: se ha corregido «$(...)» y los operadores +.
+ ISO C++: Se ha corregido el rendimiento al resaltar números.
+ Lua: Atributo con «Lua54» y otras mejoras.
+ Se ha sustituido «RegExpr=[.]{1,1}» por «DetectChar».
+ Se ha añadido el resaltado de PureScript basado en las reglas de Haskell. Proporciona una buena aproximación y un punto de partida para PureScript.
+ README.md: Usar URL de «docs.kde.org» de forma canónica.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
